/****************************************************************
 *  Copyright (C) 2020 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atol.gen.plugin.constraints.common

import fr.eseo.atlc.constraints.CompositeExp
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.ConstraintsFactory
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.Expression
import fr.eseo.atlc.constraints.ExpressionGroup
import fr.eseo.atlc.constraints.IntExp
import fr.eseo.atlc.constraints.VariableExp
import fr.eseo.atlc.constraints.VariableRelationExp
import java.util.Collection
import java.util.HashMap
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver

import static extension fr.eseo.aof.exploration.OCLByEquivalence.*

class ConstraintsHelpers {
	extension Constraints cstrExt
	extension Collection<? extends Boxable> boxables
	val AOF = AOFFactory.INSTANCE
	// TODO: is it safe to have multiple instances of this extension ?

	new(Collection<? extends Boxable> boxables, Constraints cstrExt) {
		this.boxables = boxables
		this.cstrExt = cstrExt
	}

	def flattenConstraints(IBox<Expression> constraints) {

		//sc collects expected objects from the new entry point
		//cg collects expected objects from the old entry point
		//cg.concat(sc) means we gather everything

		val sc = constraints.select(Constraint).collect[andConstraintToExpressionGroup]
		val cg = constraints.select(ExpressionGroup).concat(sc).closure[
			 it._expressions.select(ExpressionGroup)
		].expressions.select(Constraint)
		cg
//		sc.concat(
//			cg.select(Constraint).collect[it as Constraint]
//		)
	}

//@begin simplifyConstraint

	//TODO: add cache
	val emptyCstr = AOF.<Constraint>createOrderedSet
	def IBox<Constraint> simplifyConstraint(Constraint c) {
		if (c.operatorName.isArrayPredicate) { 
			val expressions = c._arguments.collect[simplifyExp].flatten
			//TODO : 
			// alDiff({a,b,c}) -> allDiff(a, b, c)
			// binPAcking({a,b,c}, {x,y,z}, ..) -> binPacking({a,b,c}, {x,y,z}, ...)
			// elementOf(a, {c1,c2,c3}) -> elementOf(a, {c1,c2,c3})
				val res = ConstraintsFactory.eINSTANCE.createConstraint => [
					operatorName = c.operatorName
					strength = c.strength
					_arguments.bind(expressions.asOrderedSet)
//					arguments.addAll(expressions.get(0)) //TODO: this is not active, should it be done in a active way ? (do not forget to create a new Constraint when recreating)
				]
				AOF.createOne(res)
			}
		else if (c.arguments.size == 0) {
			throw new RuntimeException('''No arguments''')
		}
		else if (c.arguments.size == 1) {
			val expressions = c.arguments.map[simplifyExp]
			expressions.get(0).collect[a |
				val res = ConstraintsFactory.eINSTANCE.createConstraint => [
					operatorName = c.operatorName
					strength = c.strength //TODO : strength is not copied in a active way
					arguments.add(a)
				]
				res
			]
		}
		else if (c.arguments.size == 2) {
			val expressions = c.arguments.map[simplifyExp]
			val a1 = expressions.get(0)
			val a2 = expressions.get(1)
			if (c.operatorName == "includes") {
				// includes(collection, var): ensure that var takes its value from the collection
				// thus collection should not be simplified
				a2.collect[r |
					val res = ConstraintsFactory.eINSTANCE.createConstraint => [
						operatorName = c.operatorName
						strength = c.strength
						arguments.add(r)
						arguments.addAll(a1) // TODO -- it is not active !
					]
					a1.addObserver(new DefaultObserver<Expression> {
						override added(int index, Expression element) {
							res.arguments.add(element)
						}

						override moved(int newIndex, int oldIndex, Expression element) {
							// nothing to do
						}

						override removed(int index, Expression element) {
							res.arguments.remove(element)
						}

						override replaced(int index, Expression newElement, Expression oldElement) {
							removed(index, oldElement)
							added(index, newElement)
						}
					})
					res
				]
			}
			else if (c.operatorName.isScalar) {
				a1.zipWith(a2, [l, r |
					val res = ConstraintsFactory.eINSTANCE.createConstraint => [
						operatorName = c.operatorName
						strength = c.strength
						arguments.addAll(l, r)
					]
					res
				])
			}
			else {
				a1.collectMutable[l |
					if (l === null) {
						emptyCstr
					}
					else {
						val res = a2.collect[r |
							val res = ConstraintsFactory.eINSTANCE.createConstraint => [
								operatorName = c.operatorName
								strength = c.strength
								arguments.addAll(l, r)
							]
							res
						]
						res
					}
				]
			}
		}
		else {
			throw new UnsupportedOperationException('''Cannot expand constraints with more than 2 arguments «c»''')
		}
	}
//@end simplifyConstraint

//@begin simplify
	def dispatch IBox<Expression> simplifyExp(Expression it) {
		throw new UnsupportedOperationException('''Cannot simplify «prettyPrint»''')
	}

	def dispatch IBox<Expression> simplifyExp(VariableExp it) {
		if (isVector) {
			simplifyVariableVector
		}
		else {
			simplifyVariable
		}
	}

	//TODO: add cache //TODO figure out if this todo is done
	val simplifyVariableCache = new HashMap<VariableExp, IBox<Expression>>
	def IBox<Expression> simplifyVariable(VariableExp it) {
		if (simplifyVariableCache.containsKey(it)) {
			return simplifyVariableCache.get(it)
		}
		//TODO : make incremental ?
		if (isConstant) {
			val IBox<Expression> ret = source.toReadOnlyBox(propertyName).collect[
				v |
               val r = ConstraintsFactory.eINSTANCE.createDoubleExp => [
                       value = v?.doubleValue
               ]
              r
			]
			simplifyVariableCache.put(it, ret)
			return ret
		}
		else {
			val v = ConstraintsFactory.eINSTANCE.createVariableExp
			v.source = source
			v.propertyName = propertyName
			v.isConstant = isIsConstant
			val IBox<Expression> ret = AOF.createOne(v)
			simplifyVariableCache.put(it, ret)
			return ret
		}
	}

	//TODO: add cache
	def dispatch IBox<Expression> simplifyExp(VariableRelationExp it) {
		if (!isVector) {
			val v = ConstraintsFactory.eINSTANCE.createVariableRelationExp
			v.source = source
			v.propertyName = propertyName
			v.relationName = relationName
			v.isVector = false
			v.isConstant = false
			AOF.createOne(v)	
		}
		else {
			val v = it
			val variables = v.source as IBox<Object>
			variables.select[
				it !== null
			].collect[s |
				val e = ConstraintsFactory.eINSTANCE.createVariableRelationExp => [
					source = s
					relationName = v.relationName
					propertyName = v.propertyName
					isVector = false
					isConstant = false
				]
				e
			]
		}
	}

	def dispatch IBox<Expression> simplifyExp(DoubleExp it) {
		val d = ConstraintsFactory.eINSTANCE.createDoubleExp
		d.value = value
		AOF.createOne(d)
	}

	def dispatch IBox<Expression> simplifyExp(IntExp it) {
		val d = ConstraintsFactory.eINSTANCE.createIntExp
		d.value = value
		AOF.createOne(d)
	}

	//TODO: add cache
	val emptyCst = AOF.<Expression>createOrderedSet
	def IBox<Expression> simplifyVariableVector(VariableExp v) {
		//TODO generate DoubleExp if variable is constant
		val variables = v.source as IBox<Object>
		variables.collectMutable[s |
			if (s === null) {
				emptyCst
			}
			else if (v.isConstant) {
				s.toReadOnlyBox(v.propertyName).collect[
					var ^val = 0
					if (it !== null) {
						^val = it.intValue
					}
					val res = ConstraintsFactory.eINSTANCE.createDoubleExp
					res.value = ^val
					res as Expression
				]
			}
			else {
				val e = ConstraintsFactory.eINSTANCE.createVariableExp => [
					source = s
					propertyName = v.propertyName
					isVector = false
					isConstant = false
				]
				AOF.<Expression>createOne(e)
			}
		]
	}
	
	def dispatch IBox<Expression> simplifyExp(CompositeExp it) {
		switch arguments.size {
			case 1: simplifyUnary
			case 2: simplifyBinary
			default:
				throw new UnsupportedOperationException('''Cannot compile expression with arity > 2 («prettyPrint»)''')
		}
	}
	
	def dispatch IBox<Expression> simplifyExp(Constraint it) {
		simplifyConstraint as IBox<?> as IBox<Expression>
	}

	//TODO: add cache
	public static var RIGHT_LEFT_DEP = false
	val emptyExp = AOF.<Expression>createOrderedSet
	def IBox<Expression> simplifyBinary(CompositeExp e) {
		val left = e.arguments.get(0).simplifyExp
		val right = e.arguments.get(1).simplifyExp
		if (e.operatorName.isScalar) {
			// FIXME: this works for Curriculum transfo but won't in the general case
			// right now I don't have any better idea ...
			//TODO: do dynamic analysis, if there is an intersection between properties used in operands then we have a left->right dependency
			left.zipWith(right, RIGHT_LEFT_DEP, [l, r |
				val ret = ConstraintsFactory.eINSTANCE.createCompositeExp => [
					arguments.addAll(l, r)
					operatorName = e.operatorName
				]
				return ret
			], [])
		}
		else {
			left.collectMutable[l |
				if (l === null) {
					emptyExp
				}
				else {
					right.collect[r |
						val b = ConstraintsFactory.eINSTANCE.createCompositeExp => [
							arguments.addAll(l, r)
							operatorName = e.operatorName
						]
						return b as Expression
					]
				}
			]
		}
	}

	//TODO: add cache
	def IBox<Expression> simplifyUnary(CompositeExp e) { //TODO: fix me
		val arg = e.arguments.get(0).simplifyExp
		if (e.operatorName.isArrayPredicate) {
			val res = AOF.<Expression>createOption()
			if(!arg.empty.get(0)) {
				res.set(ConstraintsFactory.eINSTANCE.createCompositeExp => [
					operatorName = e.operatorName
					arguments.addAll(arg)
				])
			}

			arg.addObserver(new DefaultObserver<Expression>() {
					override added(int index, Expression element) {
						recreate
					}

					override moved(int newIndex, int oldIndex, Expression element) {
						recreate
					}

					override removed(int index, Expression element) {
						recreate
					}

					override replaced(int index, Expression newElement, Expression oldElement) {
						recreate
					}

					def recreate() {
						if(arg.empty.get(0)){
							res.clear
						} else {
							res.set(ConstraintsFactory.eINSTANCE.createCompositeExp => [
								operatorName = e.operatorName
								arguments.addAll(arg)
							])
						}
					}
				})
			return res
		}
		else {
			arg.collect[a |
				val r = ConstraintsFactory.eINSTANCE.createCompositeExp => [
					operatorName = e.operatorName
					arguments.add(a)
				]
				r
			]
		}
	}
//@end simplify

	def toReadOnlyBox(Object o, String propertyName) {
		for (b : boxables) {
			if (
				b.hasProperty(o, propertyName)
			) {
				return b.toReadOnlyBox(o, propertyName)
			}
		}
			
		throw new NoSuchMethodException('''Cannot convert property «propertyName» of «o.class.simpleName» to constant''')
	}

	def isScalar(String op) { //TODO: +,-,*,/ and =,>,<,>=,>= have opposite meaning with scalar/cartesian
		op.startsWith(".")
	}

	def isArrayPredicate(String pred) {
		switch pred {
			case 'sum',
			case 'product',
			case 'elementOf',
			case 'allDifferent',
			case 'allDifferentExcept0': true

			default: false
		}
	}

	// TODO: not incremental if lhs or rhs change
	private def ExpressionGroup andConstraintToExpressionGroup(Constraint it) {
		val eg = ConstraintsFactory.eINSTANCE.createExpressionGroup
		if (operatorName == "and") {
			val left = it.arguments.get(0) as Constraint
			val right = it.arguments.get(1) as Constraint
			eg.expressions.add(left.andConstraintToExpressionGroup)
			eg.expressions.add(right.andConstraintToExpressionGroup)
		}
		else {
			eg.expressions.add(it)
		}
		return eg
	}
}
