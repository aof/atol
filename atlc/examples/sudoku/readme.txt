welcome to atol/atlc/examples/sudoku/

In this commit we have the boiler plate of displaying the sudoku model in xcore, a few slight changes to the sudoku.atl, a new css file.

model is in fr.eseo.atlc.example.sudoku/model/sudoku.xcore

in fr.eseo.atlc.example.sudoku.transfo/
	in src/fr/eseo/atlc/example/sudoku/transfo
		style.css
		sudoku.atl
		Sudoku.xtend <- uses the the model to make a java class
		Sudoku2JFX.xtend
		RunSudoku.xtend

	Xtend allow for us to have
	in xtend-gen/fr/eseo/atlc/example/sudoku/transfo
		Sudoku.java <- implementation of the metamodel
		Sudoku2JFX.java
		RunSudoku.java