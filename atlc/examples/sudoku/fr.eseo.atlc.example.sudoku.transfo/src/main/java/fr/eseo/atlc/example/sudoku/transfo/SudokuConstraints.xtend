/****************************************************************
 *  Copyright (C) 2021 Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Théo Le Calvar
 *	  - Matthew Coyle
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/
package fr.eseo.atlc.example.sudoku.transfo

import fr.eseo.aof.extensions.AOFExtensions
import fr.eseo.atol.gen.ATOLGen
import fr.eseo.atol.gen.ATOLGen.Metamodel
import fr.eseo.atol.gen.plugin.constraints.common.Constraints
import java.util.HashMap
import java.util.Map

@ATOLGen(transformation="src/main/resources/SudokuConstraints.atl", metamodels=#[
	@Metamodel(name="Sudoku", impl=Sudoku),
	@Metamodel(name="Constraints", impl=Constraints)
], extensions = #[Constraints])
class SudokuConstraints implements AOFExtensions {
	public val trace = new HashMap<Object, Map<String, Object>>
}

/****************************************************************
This class is coded by the @ATOLGen "precompilation"
Using the transformation information in sudokuConstraints.atl

It handles the transformation towards a model of an (in)complete sudoku with no mistakes
 ****************************************************************/