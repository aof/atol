/****************************************************************
 *  Copyright (C) 2021 Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Théo Le Calvar
 *	  - Matthew Coyle
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atlc.example.sudoku.transfo

import fr.eseo.atlc.constraints.Expression
import fr.eseo.atlc.example.sudoku.BoxGroup
import fr.eseo.atlc.example.sudoku.Cell
import fr.eseo.atlc.example.sudoku.Puzzle
import fr.eseo.atlc.example.sudoku.SudokuFactory
import fr.eseo.atlc.example.sudoku.SudokuPackage
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Cassowary
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Choco
import java.util.Collections
import java.util.HashMap
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.input.KeyEvent
import javafx.scene.layout.Pane
import javafx.scene.shape.Rectangle
import javafx.stage.Stage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.papyrus.aof.core.IBox

import static extension fr.eseo.atol.javafx.JFXUtils.*

class RunSudoku extends Application{
	val Puzzle puzzle //<- the grid model
	val IBox<Group> groups //<- IBOX incr operation set of GROUP JFX
	val Constraints2Cassowary c2cas
	val Constraints2Choco c2cho_check
	val Constraints2Choco c2cho_solve
	val Resource resource
	extension Sudoku sudokumm
	
	val target2Source = new HashMap<Node, EObject>

	def static void main(String[] args) {
		launch(args)
	}

	new () {
		val transfoView = new Sudoku2JFX //instantiation of the transformation to View model
		val transfoChecking = new SudokuConstraints //instantiation of the transformation to checking model
		val transfoSolving = new SudokuSolving //instantiation of the transformation to solving model
		extension val JFX = transfoView.FOOMM //get extension to add _nodes, _children, _constraints and _movable extension methods
		sudokumm = transfoView.SudokuMM //get extension to add _nodes, _arcs
		
		transfoView.Cell.registerCustomTrace = [s, t |
			target2Source.put(t.t, s.s)
			target2Source.put(t.rect, s.s)
			target2Source.put(t.text, s.s)
		]
		
		// Init meta model and EMF stuff
		val rs = new ResourceSetImpl

		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"xmi",
			new XMIResourceFactoryImpl
		)
		rs.packageRegistry.put(
			SudokuPackage.eNS_URI,
			SudokuPackage.eINSTANCE
		)

		// create a resource & load model from file
//		resource =rs.getResource(URI.createFileURI("data.xmi"), true)
//		puzzle = resource.contents.get(0) as Puzzle
		//create data.xmi if not present
		resource = rs.createResource(URI.createFileURI("data.xmi"))
		puzzle = SudokuFactory.eINSTANCE.createPuzzle()
		
		// First we're creating the groups which house our cells
		val vG = (0..8).map[
			val g = SudokuFactory.eINSTANCE.createRowGroup
			g.puzzle = puzzle
			g
		].toList
		val hG = (0..8).map[
			val g = SudokuFactory.eINSTANCE.createColGroup
			g.puzzle = puzzle
			g
		].toList
		val bG = (0..8).map[ i |
			val g = SudokuFactory.eINSTANCE.createBoxGroup
			g.puzzle = puzzle
			g.groupX = i % 3
			g.groupY = (i / 3) % 3
			g
		].toList()
		// now we create the cells by populating the boxgroups
		bG.forEach[ g |
			(0..2).forEach[ x |
				(0..2).forEach[ y |
					val c = SudokuFactory.eINSTANCE.createCell
					c.cellX = x
					c.cellY = y
					c.puzzle = puzzle
					//and we link these cells to their vertical and horizontal groups
					vG.get(x + 3 * g.groupX).cells.add(c)
					hG.get(y + 3 * g.groupY).cells.add(c)

					c.group = g
					c.lgroups.add(vG.get(x + 3 * g.groupX))
					c.lgroups.add(hG.get(y + 3 * g.groupY))
					g.cells.add(c)
				]
			]
		]

		resource.contents.add(puzzle)
		resource.save(Collections.EMPTY_MAP)

		transfoChecking.refine(resource)
		transfoSolving.refine(resource)

		val cellsResult = this.puzzle._cells.collect[cell | transfoView.Cell(cell)]
		val cellsGroups = cellsResult.collect[t]
		val cellsCstrs = cellsResult.collect[cstr as Expression]

		val groupResult = this.puzzle._cellGroups.select(BoxGroup).collect[group | transfoView.BoxGroup(group)]
		val groupsGroups = groupResult.collect[t]
		val groupsCstrs = groupResult.collect[cstr as Expression]

		groups = cellsGroups.concat(groupsGroups)
		val cstr = cellsCstrs.concat(groupsCstrs)

		// feed generated constraints to the constraint solver
		c2cas = new Constraints2Cassowary(JFX, sudokumm) //to deal with view positioning
		c2cas.apply(cstr)
		c2cas.solve
		
		
		val chocoConstraints = transfoChecking.allContents(resource, null).collect[
			transfoChecking.trace.get(it)
		].select[it !== null]
		.collect[
			get('cstr') as Expression
		].inspect('cstr ')

		c2cho_check = new Constraints2Choco(JFX,sudokumm)
		c2cho_check.defaultLowerBound = 0
		c2cho_check.defaultUpperBound = 9
		c2cho_check.apply(chocoConstraints)

		val solvingConstraints = transfoSolving.allContents(resource, null).collect[
			transfoSolving.trace.get(it)
		].select[it !== null]
		.collect[
			get('cstr') as Expression
		].inspect('cstr ')

		c2cho_solve = new Constraints2Choco(JFX,sudokumm)
		c2cho_solve.defaultLowerBound = 1
		c2cho_solve.defaultUpperBound = 9
		c2cho_solve.apply(solvingConstraints)

		
		// apply listeners to the movable elements
//		this.nodes.children.selectMutable[_movable].collect[
//			onPress
//			onDrag
//			''
//		]
		cellsResult.collect[rect].collect[onPress;null];
	}

	def saveModel() {
        resource.save(Collections.EMPTY_MAP)
    }

	override start(Stage stage) throws Exception {
		// Setup GUI stuff
		val Pane root = new Pane
		val scene = new Scene(root, 300, 300);
		stage.setScene(scene);
		stage.setTitle("Sudoku -- click a cell, set a number (0==reset), s or Enter to solve");
		stage.show();

		scene.stylesheets.add("/style.css")

		//Quit app on escape keypress
		scene.addEventHandler(KeyEvent.KEY_PRESSED, [KeyEvent t |
			switch t.code {
				case ESCAPE: {
					if(latestClickedNode !== null){
						val targetCell = target2Source.get(latestClickedNode) as Cell
						targetCell.isSelected = false
					}
					saveModel
					stage.close
				}
				case SPACE: {
					c2cas.debug
					c2cho_check.debug
					c2cho_solve.debug
				}
				case #{'s'}.contains(t.text),
				case ENTER: {
					println("Solving")
					c2cho_solve.solve
					println("Solved")
				}
				
				case UP,
				case DOWN,
				case LEFT,
				case RIGHT: {
					if(latestClickedNode === null) return
				}

				case #{'1', '2', '3', '4', '5', '6', '7', '8', '9'}.contains(t.text),
				case DIGIT1,
				case DIGIT2,
				case DIGIT3,
				case DIGIT4,
				case DIGIT5,
				case DIGIT6,
				case DIGIT7,
				case DIGIT8,
				case DIGIT9 : {
					if(latestClickedNode === null) return
					val newval = Integer.parseInt(t.text)
					val targetCell = target2Source.get(latestClickedNode) as Cell
					// /!\ this is not the normal way to do it /!\
					targetCell.value = newval
					//`Normal way is to suggest a new value
//					c2cho.suggest(targetCell._value, newval)
					if (!c2cho_check.solve) {
						println("Invalid Sudoku")
						// todo: write code to find invalid cells
						val box = targetCell.group
						val lines = targetCell.lgroups
						targetCell.hasConflict = false				
						box.cells.forEach[Cell c | 
							if (c.value == targetCell.value){
								targetCell.hasConflict = true								
								c.hasConflict = true
							} 
							else
								c.hasConflict = false
						]
						lines.forEach[l|
							l.cells.forEach[c|
								if (c.value == targetCell.value){
								targetCell.hasConflict = true								
								c.hasConflict = true
							} 
							else
								c.hasConflict = false
							]
						]
						
					} else {
						val box = targetCell.group
						val lines = targetCell.lgroups
						targetCell.hasConflict = false				
						box.cells.forEach[Cell c | 				
								c.hasConflict = false
						]
						lines.forEach[l|
							l.cells.forEach[c|
								c.hasConflict = false
							]
						]
					}
				}

				case #{'0'}.contains(t.text) :{
					if(latestClickedNode === null) return
					val targetCell = target2Source.get(latestClickedNode) as Cell
//					println("Invalid Sudoku")
					// todo: write code to find invalid cells
					val box = targetCell.group
					targetCell.hasConflict = false			
					box.cells.forEach[Cell c | if (c.value == targetCell.value) c.hasConflict = false ]
					targetCell.lgroups.forEach[l|l.cells.forEach[Cell c | if (c.value == targetCell.value) c.hasConflict = false ]]
					targetCell.value = 0
				}

				default: {}
			}
		]);
		
		root.children.toBox.bind(this.groups as IBox<?> as IBox<Node>)
	}
	
	var Node latestClickedNode

	def onPress(Node it) {
		onMousePressed = [e |
			val t = e.target
			switch t {
				Rectangle: {
//					println(t.styleClass)
					if(latestClickedNode!==null){
						val oldCell = target2Source.get(latestClickedNode) as Cell
						oldCell.isSelected = false
						oldCell.group.isSelected = false
						oldCell.lgroups.forEach[l| l.isSelected = false]
					}
					latestClickedNode = t
					val targetCell = target2Source.get(latestClickedNode) as Cell
					targetCell.isSelected = true
					targetCell.group.isSelected = true
					targetCell.lgroups.forEach[l| l.isSelected = true]
//					println(t.styleClass)
					e.consume
				}
			}
		]
	}
}