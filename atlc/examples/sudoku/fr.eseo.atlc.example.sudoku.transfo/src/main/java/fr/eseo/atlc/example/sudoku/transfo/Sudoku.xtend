/****************************************************************
 *  Copyright (C) 2021 Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Théo Le Calvar
 *	  - Matthew Coyle
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/
package fr.eseo.atlc.example.sudoku.transfo

import fr.eseo.aof.xtend.utils.AOFAccessors
import fr.eseo.atlc.example.sudoku.SudokuPackage
import org.eclipse.papyrus.aof.core.IBox

@AOFAccessors(SudokuPackage)
class Sudoku {
/****************************************************************
This class is coded by the @AOFAccessors "precompilation"
Using the model information in sudoku.xcore (via SudokuPackage)

Like a Java Bean, most methods are simple getsets, 
but the AOF Bean has particular versions

below is an additional behaviour for displaying the values in the cells
 ****************************************************************/ 

	def toStringValue(IBox<Integer> it) {
		collect([if (it === null || it == 0) {
			''
		} else {
			toString
		}])[
			if (it === null || empty) {
				0
			}
			else {
				Integer.parseInt(it)
			}
		]
	}
}