/****************************************************************
 *  Copyright (C) 2021 Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Théo Le Calvar
 *	  - Matthew Coyle
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/


package fr.eseo.atlc.example.sudoku.transfo

import fr.eseo.atol.gen.ATOLGen
import fr.eseo.atol.gen.ATOLGen.Metamodel
import fr.eseo.atol.gen.plugin.constraints.common.Constraints
import fr.eseo.atol.javafx.JFX

@ATOLGen(transformation="src/main/resources/sudoku.atl", metamodels=#[
	@Metamodel(name="Sudoku", impl=Sudoku),
	@Metamodel(name="FOO", impl=JFX), //impl JFX s'appel FOO dans la transformation (map mmodels transfor <-> real mmodel)
	@Metamodel(name="Constraints", impl=Constraints)
], extensions = #[Constraints])
class Sudoku2JFX {
}

/****************************************************************
This class is coded by the @ATOLGen "precompilation"
Using the transformation information in sudoku.atl

It handles the transformation towards the viewable sudoku
it's contraints are relevant to where cells are positioned
 ****************************************************************/ 