var svgRoot = document.getElementsByTagName('svg')[0];

var clickedElement;
var mousePosition = {x: 0, y: 0};

window.addEventListener('mousemove', ev => {
    mousePosition.x = ev.pageX;
    mousePosition.y = ev.pageY;
});

window.addEventListener('click', ev => {
    let nodeName = ev.target.localName;
    if (nodeName == "rect") {
    //console.log(ev.target.id);
        let nodeId = ev.target.id.split('.')
        nodeId.pop();
        clickedElement=nodeId.join('.');
    }
});

window.addEventListener("keypress", ev => {
    switch (ev.key) {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
       		placeNum(ev.key);
       		break;
       	case '0':
       	case 'd':
       	case 'r':
       	case 's':
       	case 'e':
       		placeNum(' ');
       		break;
    }
}, false);


function placeNum(num){
  //console.log(clickedElement);
	let use = document.getElementById(clickedElement);
	//console.log("placing "+num+" on "+use.id);
	for(const p of use.children) {
		//console.log(p);
        if(p.attributes.name.value === "value") {
            p.attributes.value.value = num;
        }
    }
    tcsvg.addedUse(use);
    sudokucheck();
}

function sudokucheck(){
  var c,c1,c2,cv,cv1,cv2;

  for(var i=1;i<=9;i++){
    for(var j=1;j<=9;j++){
      c = document.getElementById("c"+i+j);
      for(const p of c.children) {
        if(p.attributes.name.value === "value") {

            cv = p.attributes.value.value;      
        }
      }
      if (cv==" ") continue;

      var conflict = false;
      for(var k=1;k<=9;k++){
        c1 = document.getElementById("c"+k+j);
        c2 = document.getElementById("c"+i+k);
        for(const p of c1.children) {
          if(p.attributes.name.value === "value") {
              c1v = p.attributes.value.value;      
          }
        }
        for(const p of c2.children) {
          if(p.attributes.name.value === "value") {
              c2v = p.attributes.value.value;      
          }
        }
        if((c1v==cv && c1!=c) || (c2v==cv && c2!=c)) {
          conflict = true;
        }


      }
      let li = Math.floor((i-1)/3);
      let lj = Math.floor((j-1)/3);
      for(var ii=li*3+1;ii<=li*3+3;ii++){
        for(var jj=lj*3+1;jj<=lj*3+3;jj++){
          c1 = document.getElementById("c"+ii+jj);
          for(const p of c1.children) {
            if(p.attributes.name.value === "value") {
                c1v = p.attributes.value.value;      
            }
          }
          if((c1v==cv && c1!=c)) {
            conflict = true;
          }
        }
      }



      if(conflict){
      	console.log("conflict on c"+i+j);
        document.getElementById("c"+i+j+".text").setAttribute('class', "conflict");
        for(const p of c.children) {
          if(p.attributes.name.value === "hasConflict") {

              p.attributes.value.value = 1;
          }
        } 
      } else {
        document.getElementById("c"+i+j+".text").setAttribute('class', "");
        for(const p of c.children) {
          if(p.attributes.name.value === "hasConflict") {

              p.attributes.value.value = 0;      
          }
        }
      }
      tcsvg.addedUse(c)
    }
  }
}

function createCell(x,y,v) {
	let cellName='c'+x+y;

	let use = document.createSVGElement('use');
	use.setAttribute('id',cellName);
	use.setAttribute('href',"#cell");
	use.appendChild(createParam('x',x));
	use.appendChild(createParam('y',y));
	use.appendChild(createParam('value',v));
  use.appendChild(createParam('hasConflict',0));

	cells.appendChild(use);
	tcsvg.addedUse(use);
}

function createParam(name, value){
	let param = document.createSVGElement('param');
	param.setAttribute('name',name);
	param.setAttribute('value',value);

	return param;
}

function createGrid(size) {
  qq = new qqwing();
  qq.generatePuzzleSymmetry(qqwing.Symmetry.NONE);
  qq.setPrintStyle(qqwing.PrintStyle.ONE_LINE);
  sudoku = qq.getPuzzleString();

  for(i=1;i<=size;i++){
		for (var j = 1; j <=size; j++) {
			createCell(i,j,(sudoku[0]==".")?" ":sudoku[0]);
			sudoku = sudoku.substring(1);
		}
	}
}

document.createSVGElement = function(name) {
	return document.createElementNS("http://www.w3.org/2000/svg",name);
}

var cells = document.createSVGElement('g');
cells.setAttribute('id','cells');
svgRoot.appendChild(cells);

createGrid(9);