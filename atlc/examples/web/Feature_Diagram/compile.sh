#!/bin/bash

ATLC2TCSVG_PATH=/tmp/ATLC2TCSVG.jar

HOST=eseo-tech.github.io
BASE=ATL-Tools-Library/atlc/
PROTOCOL=https

if [ -e .env ] ; then
	source .env
fi

if [[ ! -e ${ATLC2TCSVG_PATH} ]];
then
    echo "set \$ATLC2TCSVG_PATH env var with correct path"
    exit 0
fi

java -jar "${ATLC2TCSVG_PATH}"  \
    -i ./FeatureDiagram2TCSVG.atl \
    -o ./FeatureDiagram2TCSVG.svg \
    -E style.css \
    -v \
    --embedSVG ./FeatureDiagramSample.svguses \
    -M ./FeatureDiagram.ecore   \
    -H $HOST  \
    -p "$PROTOCOL" \
    -B "$BASE" \
    # --html
