#!/bin/bash

ATLC2TCSVG_PATH=/tmp/ATLC2TCSVG.jar

HOST=eseo-tech.github.io
BASE=ecmfa2021-atlc-web/
PROTOCOL=https

if [ -e .env ] ; then
	source .env
fi

if [[ ! -e ${ATLC2TCSVG_PATH} ]];
then
    echo "set \$ATLC2TCSVG_PATH env var with correct path"
    exit 0
fi

processInternal() {
	local suffix=$1
	shift
	java -jar "${ATLC2TCSVG_PATH}"  \
	    -i ./MusicalNotation2TCSVG.atl \
	    -o ./MusicalNotation2TCSVG"$suffix".svg \
	    -v \
	    -g \
	    -n \
	    --embedSVG ./MusicalNotation.svgdefs \
	    $@ \
	    -M Music=./MusicalNotation.ecore   \
	    -E MusicalNotation.css \
	    -H $HOST  \
	    -p "$PROTOCOL" \
	    -B "$BASE"
}

process() {
	processInternal "-$1" --embedSVG ./"$1".svguses
}

#process Test
#process MajorGeneral

processInternal _Templates

