package fr.eseo.atlc2tcsvg

import com.google.common.collect.ImmutableMap
import fr.eseo.jatl.atltypeinference.ATLTypeInference
import java.util.ArrayList
import java.util.HashSet
import java.util.concurrent.Callable
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EReference
import org.eclipse.m2m.atl.common.ATL.CalledRule
import org.eclipse.m2m.atl.common.ATL.Helper
import org.eclipse.m2m.atl.common.ATL.LazyMatchedRule
import org.eclipse.m2m.atl.common.ATL.Library
import org.eclipse.m2m.atl.common.ATL.MatchedRule
import org.eclipse.m2m.atl.common.ATL.ModuleElement
import org.eclipse.m2m.atl.common.ATL.OutPatternElement
import org.eclipse.m2m.atl.common.ATL.Rule
import org.eclipse.m2m.atl.common.ATL.Unit
import org.eclipse.m2m.atl.common.OCL.Attribute
import org.eclipse.m2m.atl.common.OCL.BooleanExp
import org.eclipse.m2m.atl.common.OCL.IfExp
import org.eclipse.m2m.atl.common.OCL.IntegerExp
import org.eclipse.m2m.atl.common.OCL.IterateExp
import org.eclipse.m2m.atl.common.OCL.IteratorExp
import org.eclipse.m2m.atl.common.OCL.LetExp
import org.eclipse.m2m.atl.common.OCL.NavigationOrAttributeCallExp
import org.eclipse.m2m.atl.common.OCL.NumericExp
import org.eclipse.m2m.atl.common.OCL.OclExpression
import org.eclipse.m2m.atl.common.OCL.OclFeature
import org.eclipse.m2m.atl.common.OCL.OclModelElement
import org.eclipse.m2m.atl.common.OCL.OclType
import org.eclipse.m2m.atl.common.OCL.Operation
import org.eclipse.m2m.atl.common.OCL.OperationCallExp
import org.eclipse.m2m.atl.common.OCL.OperatorCallExp
import org.eclipse.m2m.atl.common.OCL.OrderedSetExp
import org.eclipse.m2m.atl.common.OCL.PrimitiveExp
import org.eclipse.m2m.atl.common.OCL.RealExp
import org.eclipse.m2m.atl.common.OCL.SequenceExp
import org.eclipse.m2m.atl.common.OCL.SetExp
import org.eclipse.m2m.atl.common.OCL.StringExp
import org.eclipse.m2m.atl.common.OCL.TupleExp
import org.eclipse.m2m.atl.common.OCL.VariableExp
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

import static extension fr.eseo.atlc2tcsvg.CompilationHelpers.*
import static extension fr.eseo.atlc2tcsvg.XMLUtils.*
import org.eclipse.m2m.atl.common.OCL.BagExp
import org.eclipse.m2m.atl.common.OCL.MapExp

@FinalFieldsConstructor
class ATLCCompilationUnit implements Callable<String> {
	val ModuleElement element

	extension val ATLC2TCSVGCompiler compiler
	extension val ATLTypeInference atlType

	val suggestedVariables = new ArrayList<String>

	val processedOutPatterns = new HashSet<OutPatternElement>
	val movableElements = new ArrayList<String>
	val attributesInitializers = new ArrayList<String>


	static val SVGMMtoSVGTag = #{
		'Group' -> 'g',
		'Circle' -> 'circle',
		'Rectangle' -> 'rect',
		'Text' -> 'text',
		'Line' -> 'line',
		'ClipPath' -> 'clipPath',
		'Polygon' -> 'polygon',
		'Marker' -> 'marker',
		'Pattern' -> 'pattern'
	}

	override String call() {
		switch element {
			Helper: element.compileHelpers
			Rule: element.compileRule
			default: throw new UnsupportedOperationException('''unexpected element type «element.class.simpleName» at «element.debugLocation»''')
		}
	}

	private def String compileHelpers(Helper it) {
		'''
			// «location»
			function «definition.feature.JSFunctionName»(«compileHelperParameters») {
				«IF hasConstraintsTag»
					return new tcsvg.Constraints().and(«definition.feature.compileATLHelper(true)»);
				«ELSE»
					return «definition.feature.compileATLHelper(false)»;
				«ENDIF»
			}
		'''
	}

	private def String compileHelperParameters(Helper it) {
		val feature = definition.feature
		switch (feature) {
			Attribute: {
				if (definition.context_ !== null) {
					'self'
				}
				else {
					''
				}
			}
			Operation: {
				if (definition.context_ !== null) {
					val params = new ArrayList<String>(#['self'])
					params.addAll(feature.parameters.map[varName])
					params.join(', ')
				}
				else {
					feature.parameters.map[it.varName].join(', ')
				}
			}
			default: throw new UnsupportedOperationException('''unexpected class "«feature.class.simpleName»" at «location»''')
		}
	}
	
	private dispatch def compileATLHelper(OclFeature it, boolean isConstraint) {
		throw new UnsupportedOperationException('''Unexpected class "«class.simpleName»" at «debugLocation»''')
	}

	private dispatch def compileATLHelper(Attribute it, boolean isConstraint) {
		'''
			«initExpression.compileExpression(isConstraint)»
		'''
	}

	private dispatch def compileATLHelper(Operation it, boolean isConstraint) {
		'''
			«body.compileExpression(isConstraint)»
		'''
	}

	private dispatch def String compileExpression(OclExpression it, boolean isConstraint) {
		throw new UnsupportedOperationException('''Unexpected class "«class.simpleName»" at «debugLocation»''')
	}

	private dispatch def String compileExpression(IfExp it, boolean isConstraint) {
		'''
		((«condition.compileExpression(false)») ? (
			«thenExpression.compileExpression(isConstraint)»
		) : (
			«elseExpression.compileExpression(isConstraint)»
		))'''
	}

	private dispatch def String compileExpression(LetExp it, boolean isConstraint) {
		'''
		((«variable.varName») => (
			«in_.compileExpression(isConstraint)»
		))(«variable.initExpression.compileExpression(isConstraint)»)'''
	}

	private dispatch def String compileExpression(IterateExp it, boolean isConstraint) {
		'''
			(OCLLibrary.iterate(«source.compileExpression(isConstraint)», «it.result.initExpression.compileExpression(isConstraint)»,
				(«it.result.varName», «iterators.get(0).varName») => «body.compileExpression(isConstraint)»
			))
		'''
	}

	private dispatch def String compileExpression(IteratorExp it, boolean isConstraint) {
		switch name {
			case 'forAll': '''OCLLibrary.iterate(«source.compileExpression(isConstraint)», «IF isConstraint»new tcsvg.Constraints()«ELSE»true«ENDIF», (cur, acc) => cur«IF isConstraint».and(acc)«ELSE» && acc«ENDIF»)'''
			case 'zipWith': {
				if (!(source instanceof TupleExp)) {
					throw new UnsupportedOperationException('''Expected Tuple as source of zipWith operation at «debugLocation»''')
				}
				val src = source as TupleExp
				if (src.tuplePart.size != 2) {
					throw new UnsupportedOperationException('''Expected exactly two member in source Tuple of zipWith operation at «debugLocation» got «src.tuplePart.size»''')
				}
				val leftVal = src.tuplePart.get(0).initExpression.compileExpression(isConstraint)
				val rightVal = src.tuplePart.get(1).initExpression.compileExpression(isConstraint)
				'''OCLLibrary.zipWith(«leftVal», «rightVal» ,(«FOR i : iterators SEPARATOR ', '»«i.varName»«ENDFOR») => («body.compileExpression(isConstraint)»))'''
			}
			default: '''OCLLibrary.«name»(«source.compileExpression(isConstraint)», («iterators.get(0).varName») => «body.compileExpression(isConstraint)»)'''
		}
	}

	private dispatch def String compileExpression(OperatorCallExp it, boolean isConstraint) {
		if (isConstraint) return compileConstraintExpression

		switch (operationName) {

			case '-': {
				if (arguments.empty) {
					'''(«operationName» «source.compileExpression(false)»)'''
				}
				else {
					'''(«source.compileExpression(false)») «operationName» («arguments.get(0).compileExpression(false)»)'''
				}
			}

			case 'not':	'''(! («source.compileExpression(false)»))'''
			case 'and': '''(«source.compileExpression(false)») && «arguments.get(0).compileExpression(false)»'''
			case 'or': '''(«source.compileExpression(false)») || «arguments.get(0).compileExpression(false)»'''
			case 'xor': '''((«source.compileExpression(false)») ? (!(«arguments.get(0).compileExpression(false)»)) : («arguments.get(0).compileExpression(false)»))'''
			case 'implies': '''((«source.compileExpression(false)») ? («arguments.get(0).compileExpression(false)») : true)'''

			case 'mod': '''(«source.compileExpression(false)») % («arguments.get(0).compileExpression(false)»)'''
			case '+',
			case '*',
			case '/',
			case '<',
			case '>',
			case '<=',
			case '>=': {
				'''(«source.compileExpression(false)») «operationName» («arguments.get(0).compileExpression(false)»)'''
			}
			case '=': '''(«source.compileExpression(false)») == («arguments.get(0).compileExpression(false)»)'''
			case '<>': '''(«source.compileExpression(false)») != («arguments.get(0).compileExpression(false)»)'''
			default:
				'''(«source.compileExpression(false)»).«operationName»(«FOR a : arguments SEPARATOR ', '»«a.compileExpression(false)»«ENDFOR»)'''
		}
	}

	private dispatch def String compileExpression(OperationCallExp it, boolean isConstraint) {
		val operation = it.operation

		switch operation {
			LazyMatchedRule: '''«arguments.get(0).compileExpression(isConstraint)»''' // ignore rule call and pass the source
			Operation: {
				val unit = operation.unit
				switch unit {
					case mainModule: {
						var prefix = ""
						var suffix = ""
						if (isConstraint && !operation.hasConstraintsTag) {
							prefix = "(new tcsvg.Expression("
							suffix = "))"
						}
						if (source.isThisModule) {
							'''«prefix»«operation.JSFunctionName»(«FOR a: arguments SEPARATOR ', '»«a.compileExpression(isConstraint)»«ENDFOR»)«suffix»'''
						}
						else {
							'''«prefix»«operation.JSFunctionName»(«source.compileExpression(isConstraint)»«FOR a: arguments BEFORE ', ' SEPARATOR ', '»«a.compileExpression(isConstraint)»«ENDFOR»)«suffix»'''
						}
					}
					Library case unit.name == "OCLLibrary":
						'''OCLLibrary.«operationName»(«source.compileExpression(false)»«FOR a : arguments BEFORE ', ' SEPARATOR ', '»«a.compileExpression(false)»«ENDFOR»)'''
					Library case unit.name == "ATOLLibrary":
						switch operationName {
							case 'toConstant': '''«source.compileExpression(true)»''' //TODO should compile to actual constant
							default: throw new UnsupportedOperationException('''cannot compile operation «operationName» at «debugLocation»''')
						}
					default: throw new UnsupportedOperationException('''cannot compile operation «operationName» at «debugLocation»''')
				}
			}
			EOperation:
				if (isConstraint && operation.name == 'stay') {
					'''«source.compileExpression(true)».st(«compileStrengthStay»)'''
				}
				else {
					'''«source.compileExpression(isConstraint)».«operation.name»(«FOR a : arguments SEPARATOR ', '»«a.compileExpression(isConstraint)»«ENDFOR»)'''
				}
			default:
				switch operationName {
					case 'stay': '''«source.compileExpression(true)».st(«compileStrengthStay»)'''
					default: throw new UnsupportedOperationException('''cannot compile operation «operationName» at «debugLocation»''')
				}
		}
	}

	private dispatch def String compileExpression(NavigationOrAttributeCallExp it, boolean isConstraint) {
		if (isConstraint) return compileConstraintExpression

		if (source instanceof VariableExp && (source as VariableExp).referredVariable.varName == 'thisModule') {
			val src = property
			if (src instanceof Attribute) {
				if (src.hasConstraintsTag) {
					throw new UnsupportedOperationException('''cannot call constraint helper outside of constraint at «debugLocation»''')
				}
				else {
					return '''«src.JSFunctionName»()'''
				}
			}
			else {
				throw new UnsupportedOperationException('''Cannot find global attribute helper with name «name» at «debugLocation»''')
			}
		}

		val src = property
		switch src {
			EAttribute: {
					if (src.isSourceElement && src.EType instanceof EDataType) {
							switch src.EType.name {
								case 'EDouble',
								case 'EInt':
									return '''(«source.compileExpression(true)».«name»*1)'''
							}
					}
					return '''«source.compileExpression(true)».«name»'''
				}

			Attribute: src.compileAttributeCall(source, isConstraint)
			ImmutableMap<String, Object> case src.containsKey('attributeHelper'):
				(src.get('attributeHelper') as Attribute).compileAttributeCall(source, isConstraint)


			ImmutableMap<String, Object>,
			EReference: '''«source.compileExpression(false)».«name»'''

			default: throw new UnsupportedOperationException('''cannot infer source type of «name» attribute call at «debugLocation»''')
		}
	}

	private dispatch def String compileExpression(IntegerExp it, boolean isConstraint) {
		if (isConstraint) {
			'''(new tcsvg.Expression(«integerSymbol»))'''
		}
		else {
			'''«it.integerSymbol»'''
		}
	}

	private dispatch def String compileExpression(RealExp it, boolean isConstraint) {
		if (isConstraint) {
			'''(new tcsvg.Expression(«realSymbol»))'''
		}
		else {
			'''«realSymbol»'''
		}
	}

	private dispatch def String compileExpression(BooleanExp it, boolean isConstraint) {
		if (isConstraint) {
			'''(new tcsvg.Constraints())'''
		}
		else {
			'''«booleanSymbol»'''
		}
	}

	private dispatch def String compileExpression(StringExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''unexpected string in constraints at «debugLocation»''')
		}
		else {
			'''"«stringSymbol»"'''
		}
	}

	private dispatch def String compileExpression(VariableExp it, boolean isConstraint) {
		if (isConstraint) {
			var refVar = referredVariable
			if (refVar.varName == 'self') 	return '''«referredVariable.varName»'''
			if (refVar.eContainer === null)	return '''this.«referredVariable.varName»'''
			if (refVar instanceof OutPatternElement)	return '''this.«referredVariable.varName»'''
			return '''«referredVariable.varName»'''
		}
		else {
			'''«referredVariable.varName»'''
		}
	}

	private dispatch def String compileExpression(SequenceExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''Sequence are not supported in constraints at «debugLocation»''')
		}
		else {
			'''[«FOR e : elements SEPARATOR ', '»«e.compileExpression(false)»«ENDFOR»]'''
		}
	}

	private dispatch def String compileExpression(BagExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''Sequence are not supported in constraints at «debugLocation»''')
		}
		else {
			'''[«FOR e : elements SEPARATOR ', '»«e.compileExpression(false)»«ENDFOR»]'''
		}
	}

	private dispatch def String compileExpression(SetExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''Sequence are not supported in constraints at «debugLocation»''')
		}
		else {
			'''(new Set([«FOR e : elements SEPARATOR ', '»«e.compileExpression(false)»«ENDFOR»]))'''
		}
	}

	private dispatch def String compileExpression(OrderedSetExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''Sequence are not supported in constraints at «debugLocation»''')
		}
		else {
			'''(new Set([«FOR e : elements SEPARATOR ', '»«e.compileExpression(false)»«ENDFOR»]))'''
		}
	}

	private dispatch def String compileExpression(MapExp it, boolean isConstraint) {
		if (isConstraint) {
			throw new UnsupportedOperationException('''Sequence are not supported in constraints at «debugLocation»''')
		}
		else {
			'''(new Map([«FOR e : elements SEPARATOR ', '»[«e.key.compileExpression(false)», «e.value.compileExpression(false)»]«ENDFOR»]))'''
		}
	}
 
	private dispatch def String compileConstraintExpression(OclExpression it) {
		throw new UnsupportedOperationException('''Unexpected class "«class.simpleName»"''')
	}

	private dispatch def String compileConstraintExpression(OperatorCallExp it) {
		val operators = #{
			'+' -> 'plus',
			'-' -> 'minus',
			'/' -> 'div',
			'*' -> 'times',
			'=' -> 'eq',
			'>=' -> 'ge',
			'<=' -> 'le',
			'and' -> 'and'
		}
		switch (operationName) {
			case '-': {
				if (arguments.empty) {
					'''«source.compileExpression(true)».neg'''
				}
				else {
					'''
						«source.compileExpression(true)»
					.minus(
						«arguments.get(0).compileExpression(true)»
					)'''
				}
			}

			case 'implies': '''
			(«source.compileExpression(false)» ?
				«arguments.get(0).compileExpression(true)»
			: new tcsvg.Constraints())''' //TODO this is not suitable for incremental computation

			case 'and',
			case '+',
			case '*',
			case '/':'''
				«source.compileExpression(true)»
			.«operators.get(operationName)»(
				«arguments.get(0).compileExpression(true)»
			)'''

			case '=',
			case '<=',
			case '>=': {
				val strength = extractStrenth
			'''
				«source.compileExpression(true)»
			.«operators.get(operationName)»(
				«arguments.get(0).compileExpression(true)»
				«IF parameters.debug», "«debugLocation»"«ENDIF»
			)«IF !(strength.empty)».setPriority(c.Strength.«strength»)«ENDIF»'''
			}

			default:
				throw new UnsupportedOperationException('''Operator "«operationName»" not supported in constraints at «debugLocation»''')
		}
	}

	private def String compileAttributeCall(Attribute it, OclExpression source, boolean isConstraint) {
		if (isConstraint && !hasConstraintsTag) {
			'''(new tcsvg.Expression(«JSFunctionName»(«source.compileExpression(isConstraint)»)))'''
		}
		else {
			 '''«JSFunctionName»(«source.compileExpression(isConstraint)»)'''
		}
	}

	private dispatch def String compileConstraintExpression(NavigationOrAttributeCallExp it) {
		if (source instanceof VariableExp && (source as VariableExp).referredVariable.varName == 'thisModule') {
			val src = property
			if (src instanceof Attribute) {
				if (src.hasConstraintsTag) {
					return '''«src.JSFunctionName»()'''
				}
				else {
					return '''(new tcsvg.Expression(«src.JSFunctionName»()))'''
				}
			}
			else {
				throw new UnsupportedOperationException('''Cannot find global attribute helper with name «name» at «debugLocation»''')
			}
		}
		else {
			val src = property
			switch src {
				EAttribute: {
					if (src.isSourceElement && src.EType instanceof EDataType) {
							switch src.EType.name {
								case 'EDouble',
								case 'EInt':
									return '''(new tcsvg.Expression(«source.compileExpression(true)».«name»*1))'''
							}
					}
					return '''«source.compileExpression(true)».«name»'''
				}

				ImmutableMap<String, Object> case src.containsKey("attributeHelper"):{
					val attr = src.get("attributeHelper") as Attribute
					attr.compileAttributeCall(source, true)
				}

				Attribute: {
					src.compileAttributeCall(source, true)
				}

				ImmutableMap<String, Object>,
				EReference: '''«source.compileExpression(true)».«name»'''

				default: throw new UnsupportedOperationException('''cannot find attribute «name» at «debugLocation»''')
			}
		}
	}

	private def compileStrengthStay(OperationCallExp it) {
		if (arguments.size == 0 || arguments.size > 2) {
			throw new UnsupportedOperationException('''cannot compile stay at «debugLocation», wrong number of parameter, expected 1 or 2 got «arguments.size»''')
		}
		val strength = arguments.get(0)
		if (strength instanceof StringExp) {
			if (arguments.size == 1) {
				'''c.Strength.«strength.stringSymbol.toLowerCase»'''
			}
			else {
				val weight = arguments.get(1)
				if (! (weight instanceof NumericExp)) {
					throw new UnsupportedOperationException('''Cannot compile stay at «debugLocation», second argument must be a number, got «weight.class.simpleName»''')
				}
				'''c.Strength.«strength.stringSymbol.toLowerCase», «weight.compileExpression(false)»'''
			}
		}
		else {
			throw new UnsupportedOperationException('''cannot compile stay at «debugLocation», expected String as first argument, got «strength.class.simpleName»''')
		}
	}

	private def String extractStrenth(OclExpression it) {
		val pattern = "(--\\s*)(weak|medium|strong|required)((\\s+)(\\d+(\\.\\d+)?))?.*"
		val foundStrength = commentsAfter.map[toLowerCase].findFirst[matches(pattern)]
		if (foundStrength !== null) {
			val ret = foundStrength.replaceAll(pattern, "$2, $5")
			if (ret.endsWith(', ')) {
				'''«ret» undefined'''
			}
			else {
				ret
			}
		}
		else {
			''
		}
	}

	private dispatch def isThisModule(OclExpression it) {
		false
	}

	private dispatch def isThisModule(VariableExp it) {
		referredVariable.varName == 'thisModule'
	}

	private def Unit getUnit(EObject it) {
		if (it instanceof Unit) {
			it
		}
		else if (eContainer !== null) {
			eContainer.unit
		}
		else {
			null
		}
	}

	private dispatch def compileRule(CalledRule it) {
		throw new UnsupportedOperationException('''Cannot compile called rule "«name»" at «debugLocation»''')
	}

	private dispatch def compileRule(LazyMatchedRule it) {
		if (isAbstract) {
			return ''''''
		}

		if (inPattern.elements.size > 1) {
			// TODO : how can when handle rules with multiple sources, only the first one is 'this' ?
			throw new UnsupportedOperationException('''Cannot compile rules with more than one source element, «name» at «debugLocation»''')
		}

		val constraints = outPattern.elements.filter[(type as OclModelElement).model.name == "Constraints"]
		val shapes = outPattern.elements.filter[(type as OclModelElement).model.name == "SVG"]

		if (shapes.filter[(type as OclModelElement).name == 'Group'].size > 1) {
			throw new UnsupportedOperationException('''excepted at most one SVG!Group per rule at «debugLocation», got «shapes.filter[(type as OclModelElement).name == 'Group'].size»''')
		}

		val group = shapes.findFirst[(type as OclModelElement).name == 'Group']

		if (group === null) {
			val compiledShapes = shapes.map[compileOutPatternElement].join('\n')
			val movables = movableElements.join(' ')
			movableElements.clear
			return '''
				<!-- «debugLocation» -->
				<g id="«name.normalizeRuleName»"«IF !movables.empty» movable="«movables»"«ENDIF»>
					«compiledShapes»
					<!-- constraints -->
					«FOR c : constraints»
						«c.compileConstraintGroup»
					«ENDFOR»
				</g>
			'''
		}

		if (shapes.size != 1) {
			val children = group.bindings.findFirst[propertyName == 'children']
			if (children === null) {
				throw new UnsupportedOperationException('''Group must have a children property at «debugLocation»''')
			}
			if (!(children.value instanceof SequenceExp)) {
				throw new UnsupportedOperationException('''expected a Sequence of children at «debugLocation», got «children.value.class.simpleName»''')
			}
			val compiledChildren = (children.value as SequenceExp).elements.filter(VariableExp).map[referredVariable].filter(OutPatternElement).map[compileOutPatternElement].join("\n")
			val movables = movableElements.join(',')
			movableElements.clear

			val attributes = group.bindingsToAttributes
			return '''
			<!-- «debugLocation» -->
			<g id="«name.normalizeRuleName»" «attributes»«IF !movables.empty» movable="«movables»"«ENDIF»>
				«compiledChildren»
				<!-- constraints -->
				«FOR c : constraints»
					«c.compileConstraintGroup»
				«ENDFOR»
			</g>
			'''
		}
		else {
			val attributes = group.bindingsToAttributes
			val movables = movableElements.join(',')
			movableElements.clear
			return '''
			<!-- «debugLocation» -->
			<g id="«name.normalizeRuleName»" «attributes»«IF !movables.empty» movable="«movables»"«ENDIF»>
				«group.compileInlineXML»
				<!-- constraints -->
				«FOR c : constraints»
					«c.compileConstraintGroup»
				«ENDFOR»
			</g>
			'''
		}
	}

	private dispatch def primitiveValue(OclExpression it) {
		throw new UnsupportedOperationException('''attribute value at «debugLocation» is not a primitive value, got «class.simpleName»''')
	}

	private dispatch def primitiveValue(PrimitiveExp it) {
		compileExpression(false).replace('"','')
	}

	private def isGroup(OutPatternElement it) {
		(type as OclModelElement).name == 'Group'
	}

	private def bindingsToAttributes(OutPatternElement outPattern) {
		val varName = if (outPattern.isGroup) '___group___' else '''«outPattern.varName».elem'''

		// bindings that are of primitive type are directly encoded using attributes of the SVG element
		val primitiveAttributes = outPattern.bindings.filter[!#{'content', 'children', 'actions'}.contains(propertyName)]
			.filter[value instanceof PrimitiveExp].map[
				switch propertyName {
					case 'mouseTransparent': '''pointer-events="none"'''
					case 'movable': {
						if ((value instanceof BooleanExp) && (value as BooleanExp).booleanSymbol) {
							movableElements.add((eContainer as OutPatternElement).varName)
							''''''
						}
						else if ((value instanceof StringExp)) {
							movableElements.add((value as StringExp).stringSymbol)
							''''''
						}
						else {
							'''"pointer-events"="none"'''
						}
					}
					default: '''«propertyName»="«value.primitiveValue»"'''
				}
			].join(' ')
		// bindings that are not of primitive type are processed inside the constraint element using JS
		outPattern.bindings.filter[!#{'content', 'children', 'actions'}.contains(propertyName)]
			.filter[!(value instanceof PrimitiveExp)].forEach[
				attributesInitializers.add('''this.«varName».setAttribute("«propertyName»", «value.compileExpression(false)»)''')
			]
		// special actions binding that contains mapping to add event listeners
		outPattern.bindings.filter[propertyName == "actions"].forEach[
			val value = it.value
			switch (value) {
				SetExp: {
					attributesInitializers.addAll(
						value.elements.filter(StringExp).map[
							compileActionString
						]
					)
				}
				default: throw new UnsupportedOperationException('''actions binding must be a Set at «debugLocation»''')
			}

			/*
			let outline = s.context.svg.getElementById(s.___id___ + ".outline")
						outline.addEventListener("click", clickHandler);
			*/
		]
		primitiveAttributes
	}

	private def String compileActionString(StringExp it) {
		val parts = stringSymbol.split(' ')
		if (parts.size != 3) {
			throw new UnsupportedOperationException('''action string must have 3 parts at «debugLocation»''')
		}
		val event = parts.get(0)
		val target = parts.get(1)
		val listener = parts.get(2)

		'''s.context.svg.getElementById(s.___id___ + ".«target»").addEventListener("«event»", «listener»)'''
	}

	private dispatch def compileRule(MatchedRule it) {
		throw new UnsupportedOperationException('''Cannot compile non lazy rule "«name»" at «debugLocation»''')
	}

	private def normalizeRuleName(String it) {
		return parameters.lowercaseRules ? toLowerCase : it
	}

	private def String compileOutPatternElement(OutPatternElement it) {
		if (processedOutPatterns.contains(it)) return ""

		processedOutPatterns.add(it)
		val attributes = bindingsToAttributes
		val tagName = type.toSVGElement
		var String inlineXML = ""
		var String children = ""
		var String contentValue = ""
		if (bindings.findFirst[propertyName == 'content'] !== null) {
			val content = bindings.findFirst[propertyName == 'content']
			if (content.value instanceof StringExp) {
				inlineXML = compileInlineXML
			}
			else if (content.value instanceof NavigationOrAttributeCallExp) {
				contentValue = '''content-value="param(«(content.value as NavigationOrAttributeCallExp).name»)"'''
				inlineXML = "default"
			}
			else {
				attributesInitializers.add('''this.«varName».elem.innerHTML = («content.value.compileExpression(false)»);''')
			}
		}
		if (bindings.findFirst[propertyName == 'children'] !== null) {
			val c = bindings.findFirst[propertyName == 'children'].value
			if (c instanceof SequenceExp) {
				children = c.elements.filter(VariableExp).map[referredVariable].filter(OutPatternElement).map[compileOutPatternElement].join('\n')
			}
			else {
				throw new UnsupportedOperationException('''expected a Sequence of children at «debugLocation», got «c.class.simpleName»''')
			}
		}
		if (inlineXML.empty && children.empty) {
			'''<«tagName» id=".«varName»" «attributes»/>'''
		}
		else {
			'''
			<«tagName» id=".«varName»" «contentValue» «attributes» >
				«inlineXML»
				«children»
			</«tagName»>
			'''
		}
	}

	private dispatch def toSVGElement(OclType it) {
		throw new UnsupportedOperationException('''cannot find corresponding SVG tag for «it» at «debugLocation»''')
	}


	private dispatch def toSVGElement(OclModelElement it) {
		if (model.name != 'SVG') {
			throw new UnsupportedOperationException('''cannot find corresponding SVG tag at «debugLocation», metamodel is not SVG''')
		}
		if (SVGMMtoSVGTag.containsKey(name)) {
			return SVGMMtoSVGTag.get(name)
		}
		else {
			throw new UnsupportedOperationException('''cannot find corresponding SVG tag for «name» at «debugLocation»''')
		}
	}

	private def String compileInlineXML(OutPatternElement it) {
		val content = bindings.findFirst[propertyName == 'content']?.value
		if (content === null) return ""

		switch content {
			StringExp: {
				val contentSVG = '''
				<?xml version="1.0" encoding="UTF-8"?>
				<data>
					«content.stringSymbol»
				</data>'''
				return contentSVG.processXML
			}
			default: throw new UnsupportedOperationException('''expected string for property "content" at «debugLocation», got «content.class.simpleName»''')
		}
	}

	private def compileConstraintGroup(OutPatternElement it) {
		val srcName = (outPattern.rule as MatchedRule).inPattern.elements.get(0).varName
		val cstrs = bindings.findFirst[propertyName == 'expressions']?.value as SequenceExp
		if (cstrs === null) { // compile constraints with label
			'''
				«FOR b : bindings»
					<constraints name="«b.propertyName»">
						{
							let «srcName» = this;
							«suggestedVariables.clear»
							«FOR init : attributesInitializers»
								«init.SVGSanitize»;
							«ENDFOR»
							«attributesInitializers.clear»
							«val constraints = b.value.compileExpression(true)»
							«FOR s : suggestedVariables»
							«s»;
							«ENDFOR»
							«IF parameters.legacyTCSVGConstraints»
								«constraints.SVGSanitize».add();
							«ELSE»
								return «constraints.SVGSanitize»;
							«ENDIF»
					}
					</constraints>
				«ENDFOR»
			'''
		}
		else { // old way
			'''
				<constraints>
					«FOR c : cstrs.elements»
						{
							let «srcName» = this;
							«FOR init : attributesInitializers»
								«init.SVGSanitize»;
							«ENDFOR»
							«attributesInitializers.clear»
							«val constraints = c.compileExpression(true).SVGSanitize»
							«FOR s : suggestedVariables»
							«s»;
							«ENDFOR»
							«IF parameters.legacyTCSVGConstraints»
								«constraints».add();
							«ELSE»
								return «constraints»;
							«ENDIF»
						}
					«ENDFOR»
				</constraints>
			'''
		}
	}


}