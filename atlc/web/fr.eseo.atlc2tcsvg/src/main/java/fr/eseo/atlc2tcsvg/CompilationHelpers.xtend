package fr.eseo.atlc2tcsvg

import com.google.common.base.Charsets
import com.google.common.io.CharStreams
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Paths
import org.eclipse.m2m.atl.common.ATL.Helper
import org.eclipse.m2m.atl.common.ATL.LocatedElement
import org.eclipse.m2m.atl.common.OCL.OclExpression
import org.eclipse.m2m.atl.common.OCL.OclFeatureDefinition
import org.eclipse.m2m.atl.common.OCL.Operation

class CompilationHelpers {
	
	static def SVGSanitize(String it) {
		replaceAll("&(?!((#(x[a-fA-F0-9]+)|[0-9]+)|quot|amp|apos|lt|gt);)", "&amp;").
		replace("<", "&lt;")
	}

	static def readFile(String path) {
		if (Files.exists(Paths.get(path))) {
			val contentBuilder = new StringBuilder
			Files.lines(Paths.get(path)).forEach[contentBuilder.append(it).append("\n")]
			contentBuilder.toString
		}
		else {
			val fin = ClassLoader.getSystemClassLoader().getResourceAsStream(path)
			CharStreams.toString(new InputStreamReader(fin, Charsets.UTF_8))
		}
	}

	static dispatch def debugLocation(OclExpression it) {
		'''«eResource.URI.lastSegment» - «location»'''
	}

	static dispatch def debugLocation(LocatedElement it) {
		'''«eResource.URI.lastSegment» - «location»'''
	}
	
	static dispatch def boolean hasConstraintsTag(LocatedElement it) {
		commentsBefore.findFirst[matches("^--\\s+@constraints")] !== null
	}

	static dispatch def boolean hasConstraintsTag(Operation it) {
		((eContainer as OclFeatureDefinition).eContainer as Helper).hasConstraintsTag
	}
}