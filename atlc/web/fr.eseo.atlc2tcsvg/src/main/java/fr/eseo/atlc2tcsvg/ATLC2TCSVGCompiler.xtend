/****************************************************************
 *  Copyright (C) 2020 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atlc2tcsvg

import com.beust.jcommander.JCommander
import com.beust.jcommander.ParameterException
import com.google.common.base.Stopwatch
import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import fr.eseo.atlc.constraints.ConstraintsPackage
import fr.eseo.jatl.atltypeinference.ATLTypeInference
import fr.eseo.jatl.atltypeinference.XMLConcreteSyntaxVariableResolver
import java.io.FileWriter
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchEvent
import java.nio.file.WatchKey
import java.nio.file.WatchService
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.Set
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xcore.XcoreStandaloneSetup
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import org.eclipse.m2m.atl.common.ATL.ATLPackage
import org.eclipse.m2m.atl.common.ATL.Helper
import org.eclipse.m2m.atl.common.ATL.Module
import org.eclipse.m2m.atl.common.ATL.Rule
import org.eclipse.m2m.atl.common.OCL.Attribute
import org.eclipse.m2m.atl.common.OCL.OCLPackage
import org.eclipse.m2m.atl.common.OCL.OclFeatureDefinition
import org.eclipse.m2m.atl.common.OCL.OclModelElement
import org.eclipse.m2m.atl.common.OCL.Operation
import org.eclipse.m2m.atl.common.Problem.Problem
import org.eclipse.m2m.atl.core.emf.EMFModel
import org.eclipse.m2m.atl.core.emf.EMFModelFactory
import org.eclipse.m2m.atl.engine.parser.AtlParser
import org.eclipse.m2m.atl.emftvm.compiler.AtlResourceFactoryImpl
import org.eclipse.xtend.lib.annotations.Accessors

import static extension fr.eseo.atlc2tcsvg.CompilationHelpers.*

class ATLC2TCSVGCompiler {
	static val String version = "v0.1.3"
	ResourceSet rs

	@Accessors
	Parameters parameters

	// BiMap<String, EPackage> metamodels = HashBiMap.create
	BiMap<String, Resource> metamodels = HashBiMap.create

	@Accessors
	Module mainModule

	WatchService watcher
	BiMap<WatchKey, Path> keys
	Set<Path> watchedResources
	Set<Path> safeWatchedResources

	BiMap<Operation, String> functionNames = HashBiMap.create

	extension ATLTypeInference atlType

	ExecutorService threadPool = Executors.newCachedThreadPool
//	ExecutorService threadPool = Executors.newSingleThreadExecutor

	var String compiledRules = ""
	var String compiledHelpers = ""

	def static void main(String[] args) {
		val param = new Parameters
		val argParser = JCommander.newBuilder.addObject(param).build
		try {
			argParser.parse(args)
		}
		catch (ParameterException e) {
			println('''Error: «e.message»''')
			argParser.usage
			System.exit(1)
		}
		if (param.help) {
			argParser.usage
		}
		else if (param.showVersion) {
			println('''ATLC2TCSVG «ATLC2TCSVGCompiler.version»''')
		}
		else {
			try {
				val compiler = new ATLC2TCSVGCompiler(param)
				compiler.compile
			}
			catch (Exception e) {
				println('''Error: «e.message»''')
				if (param.verbose) {
					e.printStackTrace
				}
				System.exit(1)
			}
		}
	}

	new(Parameters params) {
		this.parameters = params
		checkParameters

		initResource

		if (parameters.daemon || parameters.metamodels.filter[it.endsWith("xcore")].size > 0) {
			XcoreStandaloneSetup.doSetup
		}

		initCompiler

		if (parameters.daemon) {
			if (parameters.outputPath === null) {
				println('''Error cannot use daemon and stdout at the same time.''')
				System.exit(1)
			}

			this.watcher = FileSystems.^default.newWatchService
			this.keys = HashBiMap.create
			this.watchedResources = new HashSet
			this.safeWatchedResources = new HashSet

			registerAll
		}
	}

	def checkParameters() {
		if (parameters.html) {
			if (	!parameters.embededScriptPath.empty
				||	!parameters.embededStylePath.empty
				||	!parameters.embededSVGPath.empty
				||	!parameters.localStripts.empty
				||	!parameters.styleURL.empty
			) {
				println('Warning: HTML mode enabled, all arguments related to file inclusion or remote scripts are ignored')
			}
		}
	}

	private def initCompiler() {
		loadTransformation

		parameters.metamodels.forEach[loadMetamodel(it)]
		metamodels.put('Constraints', ConstraintsPackage.eINSTANCE.eResource)

		// user did not provide the SVG metamodel
		// we fallback to internal one
		if (!metamodels.containsKey('SVG')) {
			println('Info: falling back to internal SVG metamodel')
			val svgModel = ClassLoader.getSystemClassLoader().getResourceAsStream('SVG.ecore')
			val svgResource = rs.createResource(URI.createURI('SVG.ecore'))
			svgResource.load(svgModel, Collections.EMPTY_MAP)
			metamodels.put('SVG', svgResource)
		}

		setupATLTyping

		if (parameters.verbose) {
			atlType.check
		}
	}

	private def resetCompiler() {
		metamodels.clear
		mainModule = null
		atlType = null

		functionNames.clear

		compiledHelpers = ""
		compiledRules = ""
	}

	private def setupATLTyping() {
		val oclLib = ClassLoader.getSystemClassLoader().getResourceAsStream("OCLLibrary.atl")
		val oclLibraryResource = rs.createResource(URI.createURI("OCLLibrary.atl"))
		oclLibraryResource.load(oclLib, Collections.EMPTY_MAP)

		val atolLib = ClassLoader.getSystemClassLoader().getResourceAsStream("ATOLLibrary.atl")
		val atolLibraryResource = rs.createResource(URI.createURI("ATOLLibrary.atl"))
		atolLibraryResource.load(atolLib, Collections.EMPTY_MAP)

		val commonModels =
			#{
				"Ecore"			-> #[EcorePackage.eINSTANCE.eResource],
				"OCLLibrary"	-> #[
										oclLibraryResource,
										atolLibraryResource
									],
				"ATL"			-> #[
									OCLPackage.eINSTANCE.eResource,
									ATLPackage.eINSTANCE.eResource
								],
				"Constraints"	-> #[],
				"IN"			-> #[mainModule.eResource]
			}
		val metamodels = new HashMap(commonModels)
		this.metamodels.filter[$1 != 'Constraints'].forEach[k, v| metamodels.put(k, #[v])]

		atlType = new ATLTypeInference(
			metamodels
		)
		XMLConcreteSyntaxVariableResolver.register(atlType)
	}

	private def registerAll() {
		parameters.inputPath.registerResource(false)

		parameters.metamodels.forEach[parseMetamodelPath.value.split(';').forEach[registerResource(false)]] // remove part before =
		parameters.embededScriptPath.forEach[registerResource(true)]
		parameters.embededStylePath.forEach[registerResource(true)]
		parameters.embededSVGPath.forEach[registerResource(true)]
	}

	private def registerResource(String path, boolean isSafe) {
		val file = Paths.get(path)
		watchedResources.add(file.fileName)
		if (isSafe) safeWatchedResources.add(file.fileName)
		val dir = file.parent ?: Paths.get('.')
		if (!keys.containsValue(dir)) {
			val key = dir.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY)
			keys.put(key, dir)
		}
	}

	private def initResource() {
		rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put("atl", new AtlResourceFactoryImpl)
		rs.resourceFactoryRegistry.extensionToFactoryMap.put('ecore', new EcoreResourceFactoryImpl)
	}

	def loadTransformation() {
		val parser = AtlParser.^default
		val modelFactory = new EMFModelFactory
		val pbs = modelFactory.newModel(parser.problemMetamodel) as EMFModel
		try {
			rs.loadOptions.put("problems", pbs)
				
			val transfo = rs.getResource(URI.createURI(parameters.inputPath), true)
			val fileName = parameters.inputPath.split('/').last

			transfo.errors.forEach[
				System.err.println('''error in «fileName»:«line»-«column» - «message»''')
			]
			transfo.warnings.forEach[
				System.err.println('''warning in «fileName»:«line»-«column» - «message»''')
			]
			if (!transfo.errors.empty || 
				!pbs.resource.contents.filter(Problem).filter[severity.toString == "error"].empty) {
				throw new UnsupportedOperationException('''File «fileName» contains syntax errors.''')
			}
			mainModule = transfo.contents.get(0) as Module
		}
		catch (Exception e) {
			System.err.println('''Error reading input file "«parameters.inputPath»": «e.message»''')
			pbs.resource.contents.filter(Problem).forEach[
				println('''«severity»: «location» - «description»''')
			]
			System.exit(1)
		}
	}

	private def parseMetamodelPath(String path) {
		val parts = path.split("=")
		var MMname = ""
		var MMPath = path
		if (parts.size > 1) {
			MMname = parts.get(0)
			MMPath= parts.drop(1).join('=')
		}
		MMname -> MMPath
	}

	def loadMetamodel(String path) {
		try {
			val MMInfo = path.parseMetamodelPath

			val resources = MMInfo.value.split(';').map[rs.getResource(URI.createURI(it), true)]
			resources.forEach[EcoreUtil.resolveAll(it)]
			val r = resources.get(0)
			resources.drop(1).forEach[r.contents.addAll(it.contents)]

			val actualMMName = MMInfo.key
			val ePackage = EcoreUtil.getObjectByType(r.getContents(), EcorePackage.Literals.EPACKAGE) as EPackage
			metamodels.put(actualMMName.empty ? ePackage.name : actualMMName, r)
		} catch (Exception e) {
			System.err.println('''Error loading metamode file "«path»"''')
			if (parameters.verbose) {
				e.printStackTrace
			}
			System.exit(1)
		}
	}

	private def compileDaemon() {
		val stopwatch = Stopwatch.createUnstarted
		compileOnce

		println('''
			Watched resources :
			«FOR r : watchedResources»
				- «r»
			«ENDFOR»
		''')

		for (;;) {
			println('''waiting for modification''')

			var WatchKey key
			try {
				key = watcher.take
			} catch (InterruptedException e) {
				return
			}

			val dir = keys.get(key)
			if (dir === null) {
				println('''WatchKey not registered, exiting''')
				return
			}

			// sleep to ensure file is correctly saved and avoid multiple notification on the same file
			Thread.sleep(50)

			for (event : key.pollEvents) {
				// TODO: detect which file has been modified and only recompile parts that depends on this file
				if (event.kind == StandardWatchEventKinds.ENTRY_MODIFY) {
					val ev = event as WatchEvent<Path>
					val filename = ev.context
					println('''modification detected on «filename»''')
					if (watchedResources.contains(filename)) {
						if (safeWatchedResources.contains(filename)) {
							println('''change in a safe file, reusing cache''')
							stopwatch.reset.start
							compileOnce
							stopwatch.stop
							println('''compilation done in «stopwatch.elapsed(TimeUnit.MILLISECONDS)»ms''')
						}
						else {
							println('''change in the metamodel or ATL, reset compiler''')
							resetCompiler
							println('''init compiler''')
							stopwatch.reset.start
							initCompiler
							stopwatch.stop
							println('''init done in «stopwatch.elapsed(TimeUnit.MILLISECONDS)»ms''')

							println('''recompiling''')
							stopwatch.reset.start
							compileOnce
							stopwatch.stop
							println('''compilation done in «stopwatch.elapsed(TimeUnit.MILLISECONDS)»ms''')
						}
					}
					else {
						println('''file is not watched, skiping''')
					}
				}
			}
			val valid = key.reset
			if (!valid) {
				keys.remove(key)
				println('''one of the watched directories disapeared, exiting''')
				return
			}
		}
	}

	private def void compileOnce() {
		registerAllHelpersNames
		val res = compileModule(mainModule)

		if (parameters.outputPath !== null) {
			try (val outFile = new FileWriter(parameters.outputPath))
				outFile.write(res.toString)
		}
		else {
			println(res)
		}
	}

	def compile() {
		if 	(parameters.daemon) {
			compileDaemon
		}
		else {
			compileOnce
		}
		threadPool.shutdown()
	}

	private def compileModule(Module it) {
		var Iterable<Future<String>> helpersFutures
		var Iterable<Future<String>> rulesFutures
		val baseURL = if (!parameters.hostname.empty) {
			'''«parameters.protocol»://«parameters.hostname»/«parameters.baseURL»'''
		} else {
			parameters.baseURL
		}
		if (compiledRules.empty) {
			rulesFutures = elements.filter(Rule).map[threadPool.submit(new ATLCCompilationUnit(it,this,atlType))]
		}
		if (compiledHelpers.empty) {
			helpersFutures = elements.filter(Helper).map[threadPool.submit(new ATLCCompilationUnit(it,this,atlType))]
		}
		if (compiledHelpers.empty) {
			compiledHelpers = helpersFutures.map[get].join.SVGSanitize
		}
		if (compiledRules.empty) {
			compiledRules = rulesFutures.map[get].join
		}
		// this compiler does not support calling rules (does it makes sense in TCSVG anyway ?)
		// we register all Rules name so that they can be silently ignored
		val output = '''
				<svg «IF parameters.html»display="none"«ENDIF» xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:c="http://myconstraints">
					<script>
						«IF !parameters.html»
							«readFile('OCLLibrary.js').SVGSanitize»
						«ENDIF»

						«compiledHelpers»
					</script>
					<defs>
						«compiledRules»
					</defs>

					«IF !parameters.html»
						«FOR svg : parameters.embededSVGPath»
						«readFile(svg)»
						«ENDFOR»

						«FOR url : parameters.styleURL»
							<link xmlns="http://www.w3.org/1999/xhtml" rel="stylesheet" href="«url»" type="text/css"/>
						«ENDFOR»
						«FOR css : parameters.embededStylePath»
							<style>
								«readFile(css).SVGSanitize»
							</style>
						«ENDFOR»

						<script href="«baseURL»«parameters.cassowaryJSPath»"/>
						<script href="«baseURL»«parameters.tcsvgJSPath»"/>
						«FOR src : parameters.localStripts»
							<script href="«src»"/>
						«ENDFOR»
						«FOR script : parameters.embededScriptPath»
							<script>
								«readFile(script).SVGSanitize»
							</script>
						«ENDFOR»
					«ENDIF»
				</svg>
			'''
		if (parameters.html) {
			'''
				import { OCLLibrary as OCL } from '@aof/OCLLibrary.js'

				// todo: could this be added to tcsvg directly and not polute the global namescape ?
				globalThis.OCLLibrary = OCL

				// requires JQuery node so that helpers are accessibles in rule
				export function insertTemplatesInto(node) {
					node.append(templates)
				}

				const templates = `
					«output»
				`
			'''
		}
		else {
			'''
				<?xml version="1.0" encoding="utf-8"?>
				«output»
			'''
		}
	}

	def isSourceElement(EObject it) {
		// val res = eResource.contents.get(0)
		if (this.metamodels.inverse.containsKey(eResource)) {
			val mmName = this.metamodels.inverse.get(eResource)
			return this.mainModule.inModels.filter[metamodel.name == mmName].size > 0
		}

		return false
	}


	private def void registerAllHelpersNames() {
		mainModule.elements.filter(Helper).forEach[
			it.definition.feature.getJSFunctionName
		]
	}

	dispatch def String getJSFunctionName(Operation it) {
		functionNames.computeIfAbsent(it) [
			val definition = eContainer as OclFeatureDefinition
			val functionName = if (definition.context_ !== null) {
				val context = definition.context_.context_
				switch context {
					OclModelElement: '''«context.model.name»__«context.name»__«name»'''
					default: '''«context.class.simpleName»__«name»'''
				}
			}
			else {
				'''«name»'''
			}
			if (functionNames.containsValue(functionName)) {
				var i = 2
				while (functionNames.containsValue('''«functionName»_«i»''')) i++
				'''«functionName»_«i»'''
			}
			else {
				functionName
			}
		]
	}

	dispatch def String getJSFunctionName(Attribute it) {
		val definition = eContainer as OclFeatureDefinition
		if (definition.context_ !== null) {
			val context = definition.context_.context_
			switch context {
				OclModelElement: '''__attribute__«context.model.name»__«context.name»__«name»'''
				default: '''__attribute__«context.class.simpleName»__«name»'''
			}
		}
		else {
			'''__attribute__«name»'''
		}
	}

}
