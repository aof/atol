'use strict'

function isString(value) {
    return (typeof value === 'string' || value instanceof String)
}

// https://stackoverflow.com/a/32538867/4780219
function isIterable(value) {
    if (value == null) {
        return false
    }
    return typeof value[Symbol.iterator] === 'function'
}

var OCLLibrary = {
    // methods for String & Sequence
    size(value) {
        if (isString(value) || Array.isArray(value)) {
            return value.length
        }
        else if (value instanceof Set) {
            return value.size
        }
        else if (value) {
            return 1
        }
        else {
            return undefined
        }
    },

    indexOf(source, find) {
        if (Array.isArray(source) || (isString(source) && isString(find))) {
            const pos = source.indexOf(find)
            return (pos == -1) ? -1 : pos + 1
        }
        else if (source instanceof Set) {
            let pos = 1
            for (const e of source) {
                if (find == e) {
                    return pos
                }
                pos++
            }
            return -1
        }
        else if (source) {
            return source == find ? 1 : -1
        }
        else {
            return undefined
        }
    },

    includes(collection, element) {
        if (Array.isArray(collection)) {
            return collection.includes(element)
        }
        else if (collection instanceof Set) {
            return collection.has(element)
        }
        else if (collection) {
            return collection == element
        }
        else {
            return undefined
        }
    },

    excludes(collection, element) {
        let res = this.includes(collection, element)
        if (res == undefined) {
            return undefined
        }
        else {
            return !res
        }
    },

    count(collection, element) {
        if (Array.isArray(collection) || collection instanceof Set) {
            let cpt = 0
            for (const e of collection) {
                if (e == element) {
                    cpt++
                }
            }
            return cpt
        }
        else if (collection) {
            return collection == element ? 1 : 0
        }
        else {
            return undefined
        }
    },

    includesAll(collection, elements) {
        if (Array.isArray(collection) || collection instanceof Set) {
            if (Array.isArray(elements)) {
                return elements.every((e) => this.includes(collection, e))
            }
            else if (elements instanceof Set) {
                for (const e of elements) {
                    if (!this.includes(collection, e)) {
                        return false
                    }
                }
                return true
            }
            else {
                return undefined
            }
        }
        else if (collection && elements) {
            if (this.size(elements) != 1) {
                return false
            }
            else {
                return collection == this.at(elements, 1)
            }
        }
        else {
            return undefined
        }
    },

    excludesAll(collection, elements) {
        if (Array.isArray(collection) || collection instanceof Set) {
            if (Array.isArray(elements)) {
                return elements.every((e) => this.excludes(collection, e))
            }
            else if (elements instanceof Set) {
                for (const e of elements) {
                    if (!this.excludes(collection, e)) {
                        return false
                    }
                }
                return true
            }
            else {
                return undefined
            }
        }
        else if (collection && elements) {
            return !this.includes(elements, collection)
        }
        else {
            return undefined
        }
    },

    isEmpty(collection) {
        const res = this.size(collection)
        if (res == undefined) {
            return undefined
        }
        else {
            return res == 0
        }
    },

    notEmpty(collection) {
        const res = this.size(collection)
        if (res == undefined) {
            return undefined
        }
        else {
            return res != 0
        }
    },

    sum(collection) {
        if (Array.isArray(collection)) {
            return collection.reduce((acc, val) => acc + val, 0)
        }
        else if (collection instanceof Set) {
            let s = 0
            for (const e of collection) {
                s = s + e
            }
            return s
        }
        else if (collection) {
            return collection
        }
        else {
            return undefined
        }
    },

    //TODO asXX operations should do nothing if source is already of the correct type
    // problem, we cannot differenciate from non ordered and ordered
    asBag(collection) {
        if (Array.isArray(collection)) {
            return collection
        }
        else if (collection instanceof Set) {
            return Array.from(collection)
        }
        else if (collection) {
            return [collection]
        }
        else {
            return undefined
        }
    },

    asSequence(collection) {
        if (Array.isArray(collection)) {
            return collection
        }
        else if (collection instanceof Set) {
            return Array.from(collection)
        }
        else if (collection) {
            return [collection]
        }
        else {
            return undefined
        }
    },

    asSet(collection) {
        if (Array.isArray(collection)) {
            return new Set(collection)
        }
        else if (collection instanceof Set) {
            return collection
        }
        else if (collection) {
            return new Set([collection])
        }
        else {
            return undefined
        }
    },

    asOrderedSet(collection) {
        if (Array.isArray(collection)) {
            return new Set(collection)
        }
        else if (collection instanceof Set) {
            return collection
        }
        else if (collection) {
            return new Set([collection])
        }
        else {
            return undefined
        }
    },

    // Array operations
    union(lhs, rhs) {
        if (Array.isArray(lhs)) {
            if (rhs instanceof Set) {
                return lhs.concat(Array.from(rhs))
            }
            else {
                return lhs.concat(rhs)
            }
        }
        else if (lhs instanceof Set) {
            let union = new Set(lhs)
            for (let e of rhs) {
                union.add(e)
            }
            return union
        }
        else if (lhs instanceof Map) {
            let union = new Map(lhs)
            for (let e of rhs) {
                union.set(e[0], e[1])
            }
            return union
        }
        else if (lhs && rhs) {
            return this.union([lhs], rhs)
        }
        else {
            return undefined
        }
    },

    flatten(collection) {
        if (Array.isArray(collection)) {
            return collection.flat()
        }
        else if (collection) {
            return [collection]
        }
        else {
            return undefined
        }
    },

    append(collection, element) {
        if (Array.isArray(collection)) {
            let res = collection.slice()
            res.push(element)
            return res
        }
        else if (collection) {
            return [collection, element]
        }
        else {
            return undefined
        }
    },

    prepend(collection, element) {
        if (Array.isArray(collection)) {
            return [element].concat(collection)
        }
        else if (collection) {
            return [element, collection]
        }
        else {
            return undefined
        }
    },

    insertAt(collection, index, element) {
        if (Array.isArray(collection)) {
            const start = collection.slice(0, index-1)
            const end = collection.slice(index-1)
            return start.concat([element]).concat(end)
        }
        else if (collection) {
            return this.insertAt([collection], index, element)
        }
        else {
            return undefined
        }
    },

    subSequence(collection, start, end) {
        if (Array.isArray(collection)) {
            return collection.slice(start - 1, end)
        }
        else {
            return undefined
        }
    },
    
    at(collection, idx) {
        if (Array.isArray(collection)) {
            if (idx < 1 || idx > collection.size) {
                return undefined
            }
            return collection[idx - 1]
        }
        else if (collection instanceof Set) {
            let i = 1
            for (const e of collection) {
                if (i == idx) {
                    return e
                }
                i++
            }
            return undefined
        }
        else if (collection) {
            return collection
        }
        else {
            return undefined
        }
    },

    first(collection) {
        return this.at(collection, 1)
    },

    last(collection) {
        return this.at(collection, this.size(collection))
    },

    including(collection, key, val) {
        if (val && collection instanceof Map) {
            let res = new Map(collection)
            res.set(key, val)
            return res
        }
        else if (Array.isArray(collection)) {
            return this.append(collection, key)
        }
        else if (collection instanceof Set) {
            let set = new Set(collection)
            set.add(key)
            return set
        }
        else if (collection) {
            return [collection, key]
        }
        else {
            return undefined
        }
    },

    excluding(collection, element) {
        if (Array.isArray(collection)) {
            return this.reject(collection, (e) => e == element)
        }
        else if (collection instanceof Set) {
            let set = new Set(collection)
            set.delete(element)
            return set
        }
        else if (collection) {
            return collection == element ? [] : [collection]
        }
        else {
            return undefined
        }
    },

    // Set operations

    intersection(lhs, rhs) {
        if (lhs instanceof Set && rhs instanceof Set) {
            let inter = new Set()
            for (let e of rhs) {
                if (lhs.has(e)) {
                    inter.add(e)
                }
            }
            return inter
        }
        else {
            return undefined
        }
    },

    //TODO change compiler to use this operation when src is a Set
    // operator -
    difference(lhs, rhs) {
        if (lhs instanceof Set && rhs instanceof Set) {
            let diff = new Set(lhs)
            for (const e of rhs) {
                if (lhs.has(e)) {
                    diff.delete(e)
                }
            }
            return diff
        }
        else {
            return undefined
        }
    },

    symmetricDifference(lhs, rhs) {
        if (lhs instanceof Set && rhs instanceof Set) {
            let diff = new Set(lhs)
            for (const e of rhs) {
                if (diff.has(e)) {
                    diff.delete(e)
                }
                else {
                    diff.add(e)
                }
            }
            return diff
        }
        else {
            return undefined
        }
    },

    // ordered set operations

    subOrderedSet(set, lower, upper) {
        if (set instanceof Set) {
            let res = new Set()
            let values = Array.from(set)
            for (let val of values.slice(lower - 1, upper)) {
                res.add(val)
            }
            return res
        }
        else {
            return undefined
        }
    },


    // Iterating operations

    exists(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(collection)
        }

        for (const e of collection) {
            if (lambda(e)) {
                return true
            }
        }
        return false
    },

    forAll(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(collection)
        }
        
        for (const e of collection) {
            if (!lambda(e)) {
                return false
            }
        }
        return true
    },

    isUnique(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return true
        }
        
        let values = new Set()
        for (const e of collection) {
            let val = lambda(e)
            if (values.has(val)) {
                return false
            }
            else {
                values.add(val)
            }
        }
        return true
    },

    any(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(collection) ? collection : undefined
        }
        
        for (const e of collection) {
            if (lambda(e)) {
                return e
            }
        }
        return undefined
    },

    one(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(collection)
        }
        
        let found = false
        for (const e of collection) {
            if (lambda(e)) {
                if (found) {
                    return false
                }
                else {
                    found = true
                }
            }
        }
        return found
    },
    
    collect(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return [lambda(collection)]
        }
        
        if (Array.isArray(collection)) {
            return collection.map(lambda)
        }
        else /* Set */ {
            let res = new Set
            for (const e of collection) {
                    res.add(lambda(e))
            }
            return res
        }
    },

    select(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(collection) ? [collection] : []
        }
        
        if (Array.isArray(collection)) {
            return collection.filter(lambda)
        }
        else /* Set */ {
            let res = new Set
            for (const e of collection) {
                if (lambda(e)) {
                    res.add(e)
                }
            }
            return res
        }
    },

    reject(collection, lambda) {
        return this.select(collection, (e) => !lambda(e))
    },

    iterate(collection, seed, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return lambda(seed, collection)
        }
        if (Array.isArray(collection)) {
            return collection.reduce(lambda, seed)
        }
        else {
            return Array.from(collection).reduce(lambda, seed)            
        }
    },

    sortBy(collection, lambda) {
        if (!collection) {
            return undefined
        }

        if (!isIterable(collection)) {
            return [collection]
        }
        
        let arr = Array.from(collection)
        arr = arr.sort((l, r) => lambda(l) < lambda(r) ? -1 : 1)
        if (Array.isArray(collection)) {
            return arr
        }
        else {
            return new Set(arr)
        }
    },

    zipWith(left, right, lambda) {
        if (!left || !right) {
            return undefined
        }

        if (isIterable(left) && !isIterable(right)) {
            return this.zipWith(left, [right], lambda)
        } else if (!isIterable(left) && isIterable(right)) {
            return this.zipWith([left], right, lambda)
        } else if (!isIterable(left) && !isIterable(right)) {
            return this.zipWith([left], [right], lambda)
        }

        let set = (left instanceof Set || right instanceof Set)
        left = Array.from(left)
        right = Array.from(right)
        
        const length = Math.min(left.length, right.length)
        const res = left.slice(0, length).map((v, idx) => lambda(v, right[idx]))

        if (set) {
            let s = new Set
            for (const e of res) {
                s.add(e)
            }
            return s
        }
        else {
            return res
        }
    },


    // OCLAny operations
    oclIsUndefined(val) {
        if (val == "") {
            return false
        }
        return val ? false : true
    },
    
    oclIsKindOf(val, type) {
        //TODO implement oclIsKindOf
        throw "unsupported operation"
    },

    oclIsTypeOf(val, type) {
        //TODO
        throw "unsupported operation"
    },
    
    toString(val) {
        return val?.toString()
    },
    
    oclType(val) {
        //TODO implement oclType
        throw "unsupported operation"
    },

    debug(src, s) {
        console.log(`${s}: ${src}`)
        return src
    },

    refSetValue(src, name, val) {
        if (src && name) {
            src[name] = val
            return src
        }
        else {
            return undefined
        }
    },

    refGetValue(src, name) {
        if (src && name) {
            return src[name]
        }
        else {
            return undefined
        }
    },

    refImmediateComposite(src) {
        //TODO not implemented
        throw "unsupported operation"
    },

    refInvokeOperation(src, opName, args) {
        if (src && opName) {
            return src[opName](...args)
        }
        else {
            return undefined
        }
    },

    // Map operations
    get(map, key) {
        if (map instanceof Map) {
            return map.get(key)
        }
        else {
            return undefined
        }
    },

    getKeys(map) {
        if (map instanceof Map) {
            return map.keys()
        }
        else {
            return undefined
        }
    },

    getValues(map) {
        if (map instanceof Map) {
            return map.values()
        }
        else {
            return undefined
        }
    },

    // Strings methods
    concat(string1, string2) {
        if (isString(string1) && isString(string2)) {
            return string1 + string2
        }
        else {
            return undefined
        }
    },
    
    substring(string, lower, upper) {
        if (isString(string)) {
            if (lower > upper || lower < 1) {
                return undefined
            }
            else {
                return string.substring(lower - 1, upper)
            }
        }
    },

    toInteger(string) {
        if (string) {
            return parseInt(string)
        }
        else {
            return undefined
        }
    },

    toReal(string) {
        if (string) {
            return parseFloat(string)
        }
        else {
            return undefined
        }
    },

    toUpper(string) {
        if (isString(string)) {
            return string.toUpperCase()
        }
        else {
            return undefined
        }
    },

    toLower(string) {
        if (isString(string)) {
            return string.toLowerCase()
        }
        else {
            return undefined
        }
    },

    toSequence(string) {
        if (isString(string)) {
            return Array.from(string)
        }
        else {
            return undefined
        }
    },

    trim(string) {
        if (isString(string)) {
            return string.trim()
        }
        else {
            return undefined
        }
    },

    startsWith(string, prefix) {
        if (isString(string) && isString(prefix)) {
            return string.startsWith(prefix)
        }
        else {
            return undefined
        }
    },

    endsWith(string, prefix) {
        if (isString(string)) {
            return string.endsWith(prefix)
        }
        else {
            return undefined
        }
    },

    lastIndexOf(string, find) {
        if (isString(string) && isString(find)) {
            const pos = string.lastIndexOf(find)
            return (pos == -1) ? -1 : pos + 1
        }
        else {
            return undefined
        }
    },

    split(string, pattern) {
        if (isString(string) && isString(pattern)) {
            return string.split(pattern)
        }
        else {
            return undefined
        }
    },

    replaceAll(string, find, replace) {
        return this.regexReplaceAll(string, find, replace)
    },

    regexReplaceAll(string, find, replace) {
        if (isString(string) && isString(find) && isString(replace)) {
            const regexp = new RegExp(find, 'g')
            return string.replaceAll(regexp, replace)
        }
        else {
            return undefined
        }
    },




    // Numerical
    max(left, right) {
        const res = Math.max(left, right)
        return isNaN(res) ? undefined : res
    },

    min(left, right) {
        const res = Math.min(left, right)
        return isNaN(res) ? undefined : res
    },

    abs(value) {
        const res = Math.abs(value)
        return isNaN(res) ? undefined : res
    },

    div(num, denum) {
        const res = Math.floor(num / denum)
        return isNaN(res) ? undefined : res
    },

    mod(left, right) {
        const res = left % right
        return isNaN(res) ? undefined : res
    },

    floor(num) {
        const res = Math.floor(num)
        return isNaN(res) ? undefined : res
    },

    round(num) {
        const res = Math.round(num)
        return isNaN(res) ? undefined : res
    },

    cos(num) {
        const res = Math.cos(num)
        return isNaN(res) ? undefined : res
    },

    sin(num) {
        const res = Math.sin(num)
        return isNaN(res) ? undefined : res
    },

    tan(num) {
        const res = Math.tan(num)
        return isNaN(res) ? undefined : res
    },

    acos(num) {
        const res = Math.acos(num)
        return isNaN(res) ? undefined : res
    },

    asin(num) {
        const res = Math.asin(num)
        return isNaN(res) ? undefined : res
    },

    toDegree(angle) {
        const res = angle * (180 / Math.PI)
        return isNaN(res) ? undefined : res
    },

    toRadian(angle) {
        const res = angle * (Math.PI / 180)
        return isNaN(res) ? undefined : res
    },

    exp(num) {
        const res = Math.exp(num)
        return isNaN(res) ? undefined : res
    },

    log(num) {
        const res = Math.log(num)
        return isNaN(res) ? undefined : res
    },

    sqrt(num) {
        const res = Math.sqrt(num)
        return isNaN(res) ? undefined : res
    }
}