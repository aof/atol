#!/bin/bash

set -eu

echo "Building atlc2tcsvg"

pushd ../../../..
    ./gradlew :atlc:web:fr.eseo.atlc2tcsvg:build
popd

echo "Building docker image"
pushd ..
    docker build -t registry.kher.nl/aof/atol/atlc2tcsvg:latest -f docker/Dockerfile .
    docker build -t registry.kher.nl/aof/atol/atlc2tcsvg:devcontainer -f docker/Dockerfile.dev-container .
popd

