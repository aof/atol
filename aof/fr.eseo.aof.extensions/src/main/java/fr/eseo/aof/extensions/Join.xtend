/****************************************************************
 *  Copyright (C) 2023 IMT Atlantique 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.aof.extensions

import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.impl.operation.Operation
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver

class Join extends Operation<String> {

    val IBox<String> sourceBox
	val String glue
    val String base = ""

    def updateResult() {
        result.set(0, sourceBox.reduce[$0+glue+$1] ?: base)
    }

	new(IBox<String> sourceBox, String glue) {
		this.sourceBox = sourceBox
		this.glue = glue
		updateResult()
		registerObservation(sourceBox, new DefaultObserver<String> {
			override added(int index, String element) {
				updateResult()
			}
			override moved(int newIndex, int oldIndex, String element) {
				updateResult()
			}
			override removed(int index, String element) {
				updateResult()
			}
			override replaced(int index, String newElement, String oldElement) {
				updateResult()
			}
		})
	}

	override isOptional() {
		false
	}

	override isSingleton() {
		true
	}

	override isOrdered() {
		true
	}

	override isUnique() {
		true
	}

	override getResultDefautElement() {
		base
	}

//	def static void main(String[] args) {
//		val bs = AOFFactory.INSTANCE.createSequence(1, 2, 3, 4, 5, 6)
//		bs.inspect("bs: ")
//		val cs = new Sum(bs).result
//		cs.inspect("cs: ")
//		new BoxFuzzer(bs, [BoxFuzzer.rand.nextInt], [
//			SortedBy.assertEquals("", bs.reduce[$0+$1]?:0, cs.get(0))
//		])
//	}
}