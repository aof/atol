/****************************************************************
 *  Copyright (C) 2020 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atlc.example.curriculum.importer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider
import fr.eseo.atlc.example.curriculum.Course
import fr.eseo.atlc.example.curriculum.CurriculumFactory
import fr.eseo.atlc.example.curriculum.CurriculumPackage
import fr.eseo.atlc.example.curriculum.Institute
import java.io.PrintWriter
import java.util.ArrayList
import java.util.Collections
import java.util.HashSet
import java.util.Set
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.emfjson.jackson.annotations.EcoreIdentityInfo
import org.emfjson.jackson.databind.EMFContext
import org.emfjson.jackson.databind.deser.ReferenceEntry
import org.emfjson.jackson.module.EMFModule
import org.emfjson.jackson.resource.JsonResourceFactory
import org.emfjson.jackson.utils.ValueReader
import org.emfjson.jackson.utils.ValueWriter

class Main {
	val rs = new ResourceSetImpl
	val Resource in

	val reqs = #{"ECSE 202","ECSE 205","MATH 262","MATH 263","COMP 250","ECSE 200","ECSE 222","ECSE 223","FACC 100","CCOM 206","ECSE 206","ECSE 210","ECSE 211","ECSE 324","COMP 251","ECSE 310","ECSE 321","ECSE 325","MATH 240","FACC 250"}
	val naturalScience = #{"ATOC 214","ATOC 215","ATOC 219","BIOL 200","BIOL 215","CHEM 203","ENVR 200","EPSC 201","EPSC 203","EPSC 210","ESYS 200","MIMM 211","PHYS 214","PHYS 224","PHYS 230","PHYS 253","PHYS 260"}
	val impactTechno = #{"ANTH 212","BTEC 502","CIVE 469","ECON 225","ECON 347","ENVR 201","GEOG 200","GEOG 203","GEOG 205","GEOG 302","MGPO 440","PHIL 343","RELG 270","SOCI 235","SOCI 312","URBP 201"}
	val humanities = #{"ARCH 528","BUSA 465","CLAS 203","ENVR 203","ENVR 400","FACC 220","FACC 500","FACC 501","HISP 225","HISP 226","INDR 294","INTG 201","INTG 202","MATH 338","MGCR 222","MGCR 352","ORGB 321","ORGB 423"}
	val technicalComp = #{"COMP 424","ECSE 335","ECSE 412","ECSE 416","ECSE 420","ECSE 421","ECSE 422","ECSE 424","ECSE 428","ECSE 429","ECSE 439"}

	val keep = new HashSet<String>

	def static void main(String[] args) {
		new Main()
	}

	def setup() {
		val mapper = new ObjectMapper
		val module = new EMFModule
		module.configure(EMFModule.Feature.OPTION_USE_ID, true)
		module.setIdentityInfo(new EcoreIdentityInfo("_id", new ValueReader<Object, String>() {
			override readValue(Object value, DeserializationContext context) {
				value.toString
			}
		}, new ValueWriter<EObject, Object>() {
			override writeValue(EObject value, SerializerProvider context) {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
		}))
		module.setReferenceDeserializer(new JsonDeserializer<ReferenceEntry>() {
			override ReferenceEntry deserialize(JsonParser parser, DeserializationContext ctxt) {
				val parent = EMFContext.getParent(ctxt);
				val reference = EMFContext.getReference(ctxt);

				new ReferenceEntry.Base(parent, reference, parser.text, 'http://eris.eseo.fr/ATLC/Example/Curriculum#//Course')
			}
		});
		
		mapper.registerModule(module)

		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"json", 
			new JsonResourceFactory(mapper)
		)
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"xmi",
			new XMIResourceFactoryImpl
		)
		rs.packageRegistry.put(
			CurriculumPackage.eNS_URI,
			CurriculumPackage.eINSTANCE
		)
	}


	new() {
		setup

		in = rs.getFileResource("../fr.eseo.atlc.example.curriculum/scripts/results_nice.json")
		val courses = in.contents as EList<?> as EList<Course>
		EcoreUtil.resolveAll(in)
		courses.cleanup
		courses.cleanup

		keep.addAll(reqs)
//		keep.addAll(naturalScience)
//		keep.addAll(impactTechno)
//		keep.addAll(humanities)
//		keep.addAll(technicalComp)

//		new PrintWriter("../fr.eseo.atlc.example.curriculum/scripts/existing_codes.txt", "UTF-8") => [
//			it.println('''«FOR c : courses SEPARATOR '\n'»«c.code.toLowerCase.replace(' ', '-')»«ENDFOR»''')
//			it.close()
//		]

		courses.removeIf[!keep.contains(code)]

		courses.forEach[
			val toRemove = new ArrayList<Object>

			requisites.forEach[
				if (!courses.contains(target)) {
					toRemove.add(it)
					target.requisitedBy.remove(it)
				}
			]
			requisites.removeAll(toRemove)
			toRemove.clear

			requisitedBy.forEach[
				if (!courses.contains(source)) {
					toRemove.add(it)
					source.requisites.remove(it)
				}
			]
			requisitedBy.removeAll(toRemove)
			toRemove.clear

			corequisites.forEach[
				if (!keep.contains(target.code)) {
					toRemove.add(it)
					target.corequisitedBy.remove(it)
				}
			]
			corequisites.removeAll(toRemove)
			toRemove.clear

			corequisitedBy.forEach[
				if (!courses.contains(source)) {
					toRemove.add(it)
					source.corequisites.remove(it)
				}
			]
			corequisitedBy.removeAll(toRemove)
			toRemove.clear

			restrictions.forEach[
				if (!keep.contains(target.code)) {
					toRemove.add(it)
					target.restrictedBy.remove(it)
				}
			]
			restrictions.removeAll(toRemove)
			toRemove.clear

			restrictedBy.forEach[
				if (!courses.contains(source)) {
					toRemove.add(it)
					source.restrictions.remove(it)
				}
			]
			restrictedBy.removeAll(toRemove)
			toRemove.clear
		]

		val institute = CurriculumFactory.eINSTANCE.createInstitute
		institute.name = "McGill"
		institute.courses.addAll(courses)

		val resSave = rs.createResource(URI.createFileURI("../fr.eseo.atlc.example.curriculum/scripts/results.xmi"))
		resSave.contents.add(institute)
		resSave.save(Collections.EMPTY_MAP);

		var writer = new PrintWriter("../fr.eseo.atlc.example.curriculum/scripts/results.plantuml", "UTF-8");
		writer.println(institute.toPlantuml);
		writer.close();

		writer = new PrintWriter("../fr.eseo.atlc.example.curriculum/scripts/results.dot", "UTF-8");
		writer.println(institute.toDot);
		writer.close();
	}
	
	def getFileResource(ResourceSet rs, String file) {
		rs.getResource(URI.createFileURI(file), true)
	}

	def cleanup(EList<Course> courses) {
		courses.forEach[
			val toRemove = new ArrayList<Object>
			requisites.forEach[
				if (target.eIsProxy || source == target) {
					toRemove.add(it)
					target.requisitedBy.remove(it)
				}
			]
			requisites.removeAll(toRemove)
			toRemove.clear

			corequisites.forEach[
				if (target.eIsProxy || source == target) {
					toRemove.add(it)
					target.corequisitedBy.remove(it)
				}
			]
			corequisites.removeAll(toRemove)
			toRemove.clear

			restrictions.forEach[
				if (target.eIsProxy || source == target) {
					toRemove.add(it)
					target.restrictedBy.remove(it)
				}
			]
			restrictions.removeAll(toRemove)
			toRemove.clear
		]
	}

	def toPlantuml(Institute it) {
		'''@startuml
«FOR cs : #{reqs, humanities, technicalComp, naturalScience, impactTechno} »
«IF !courses.filter[cs.contains(code)].empty»
package «cs.packageName» <<Rectangle>> {
	«FOR c : courses.filter[cs.contains(code)]»
		object "«c.code»" as «c.id» «c.color(false)» {
			title = "«c.title»"
			credits = «c.credits»
			terms = "«c.terms.join(', ')»"
		}
	«ENDFOR»
}
«ENDIF»
«ENDFOR»
«FOR c :courses»
«FOR r : c.requisites»
	«c.id» --> «r.target.id»
«ENDFOR»
«FOR r : c.corequisites»
	«c.id» .> «r.target.id»
«ENDFOR»
«FOR r : c.restrictions»
	«c.id» --* «r.target.id»
«ENDFOR»
«ENDFOR»
@enduml'''
	}

	def toDot(Institute it) {
		'''digraph G {
	«FOR cs : #{reqs, humanities, technicalComp, naturalScience, impactTechno} »
		subgraph cluster_«cs.packageName» {
			label = "«cs.packageName»";
			«FOR c : courses.filter[cs.contains(code)]»
				«c.id» [label="«c.code»", style=filled, fillcolor="«c.color(true)»"];
			«ENDFOR»
		}
	«ENDFOR»
	«FOR c :courses»
	«FOR r : c.requisites»
		«c.id» -> «r.target.id»;
	«ENDFOR»
	«FOR r : c.corequisites»
		«c.id» -> «r.target.id» [style=dotted, rank=same];
	«ENDFOR»
	«FOR r : c.restrictions»
		«c.id» ->  «r.target.id» [color=red];
	«ENDFOR»
	«ENDFOR»
}
'''
	}

	def color(Course it, boolean dot) {
		val fall = "B21224"
		val winter = "047AB2"
		switch terms.size {
			case 0: if (dot) 'white' else ''
			case 2: if (dot) '''#«winter»:#«fall»''' else '''#«winter»/«fall»'''
			default:
				if (terms.contains("winter")) {
					'#'+winter
				}
				else {
					'#'+fall
				}
		}
	}

	def packageName(Set<String> it) {
		switch it {
			case it == reqs: "required"
			case it == humanities: "humanities"
			case it == technicalComp: "technologies"
			case it == naturalScience: "naturalSciences"
			case it == impactTechno: "technologicalImpact"
			default: ""
		}
	}

	def group(Course it) {
		switch code {
			case reqs.contains(it): 0
			case humanities.contains(it): 1
			case technicalComp.contains(it): 2
			case naturalScience.contains(it): 3
			case impactTechno.contains(it): 4
		}
	}

	def id(Course it) {
		code.replace(' ', '_')
	}

}