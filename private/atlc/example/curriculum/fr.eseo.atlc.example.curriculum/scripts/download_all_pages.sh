#!/bin/bash

for c in `cat existing_codes.txt`
do
    echo "Downloading ${c}"
    curl -s "https://mcgill.ca/study/2018-2019/courses/${c}" -o "raw_pages/$c.html"
done
