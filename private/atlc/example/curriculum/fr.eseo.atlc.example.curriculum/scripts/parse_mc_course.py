#!/bin/env python3

from urllib import request, error
from lxml import etree 
import re
import json
import sys
import os

debug = 0

def parse_mc_course(code):
    base_url = 'https://mcgill.ca/study/2018-2019/courses/'
    code = code.lower()
    doc = None

    try:
        fp = request.urlopen(base_url + code)
        parser = etree.HTMLParser()
        doc = etree.parse(fp, parser)
        fp.close()
    except error.HTTPError as e:
        if debug:
            print("Error while getting document (error %s)" % e.code)
        return None

    print('Parsing %s' % code)

    xpath_title = '//h1[@id="page-title"]'
    xpath_desciption = '/html/body/div[5]/div[2]/div[3]/div[2]/div/div/div/div/div/div/div[2]/p[1]'
    xpath_terms = '//p[@class="catalog-terms"]'
    xpath_notes = '//ul[@class="catalog-notes"]'

    title_raw = doc.xpath(xpath_title)[0].text
    desc_raw = doc.xpath(xpath_desciption)[0].text
    terms_raw = doc.xpath(xpath_terms)[0].text

    try:
        notes_elmnt = doc.xpath(xpath_notes)[0]
    except IndexError as e:
        notes_elmnt = None

    code_re  = re.compile('([A-Z0-9]{4} \d{3})')
    title_re = re.compile('([A-Z0-9]{4} \d{3})\s+(([^(]|\([^\d])+)\s*(\((\d(\.\d+)?) credits\))?')
    title_res = title_re.search(title_raw)
    code = title_res.group(1)
    title = title_res.group(2).strip()
    credits = title_res.group(5)

    description = desc_raw.replace('\\n', '').strip()

    terms = []
    if "Fall" in terms_raw:
        terms.append("fall")
    if "Winter" in terms_raw:
        terms.append("winter")
    if "Summer" in terms_raw:
        terms.append("summer")

    requisites = []
    restrictions = []
    corequisites = []
    if not notes_elmnt is None:
        for li in notes_elmnt.getchildren():
            p = li.getchildren()[0]
            if p.text is None or "Prerequisite" in p.text:
                for a in p.getchildren():
                    if a.text is not None and code_re.match(a.text) and a.text != code:
                        requisites.append({'eClass': 'http://eris.eseo.fr/ATLC/Example/Curriculum#//Requisite', '_id': '%s %s' % (code, a.text),'source': code, 'target': a.text})

            elif 'Restriction' in p.text:
                for a in p.getchildren():
                    if a.text is not None and code_re.match(a.text) and a.text != code:
                        restrictions.append({'eClass': 'http://eris.eseo.fr/ATLC/Example/Curriculum#//Restriction', '_id': '%s %s' % (code, a.text),'source': code, 'target': a.text})

            elif 'Corequisite' in p.text:
                for a in p.getchildren():
                    if a.text is not None and code_re.match(a.text) and a.text != code:
                        corequisites.append({'eClass': 'http://eris.eseo.fr/ATLC/Example/Curriculum#//Corequisite', '_id': '%s %s' % (code, a.text),'source': code, 'target': a.text})


    result = {
        'eClass': 'http://eris.eseo.fr/ATLC/Example/Curriculum#//Course',
        'title': title,
        'code': code,
        '_id': code,
        'credits': credits,
        'description': description,
        'terms': terms,
        'restrictions': restrictions,
        'requisites': requisites,
        'corequisites': corequisites
    }

    return result


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("usage: %s course_code" % (sys.argv[0]))
        sys.exit(1)

    code = sys.argv[1] 
    debug = os.environ.get('MC_DEBUG', 0)

    print(json.dumps(parse_mc_course(code)))
