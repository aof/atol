/****************************************************************
 *  Copyright (C) 2020 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atlc.example.curriculum.transfo

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import fr.eseo.aof.extensions.AOFExtensions
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.ExpressionGroup
import fr.eseo.atlc.example.curriculum.Course
import fr.eseo.atlc.example.curriculum.CurriculumFactory
import fr.eseo.atlc.example.curriculum.CurriculumPackage
import fr.eseo.atlc.example.curriculum.Institute
import fr.eseo.atlc.example.curriculum.Semester
import fr.eseo.atol.gen.AbstractRule
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Cassowary
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Choco
import fr.eseo.atol.javafx.JFX.Figure
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Random
import javafx.application.Application
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.layout.Pane
import javafx.scene.shape.Line
import javafx.scene.shape.Rectangle
import javafx.scene.text.Text
import javafx.stage.Stage
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IOne

import static extension fr.eseo.aof.exploration.OCLByEquivalence.*
import static extension fr.eseo.atol.gen.JFXUtils.*
import static extension fr.eseo.atol.rules.A.*

class CurriculumGenerator extends Application implements AOFExtensions {
	val IBox<Node> nodes
	val IBox<ExpressionGroup> constraints
	var Constraints2Cassowary c2cas
	var Constraints2Choco c2cho
	val r = new Random

	val root = new Pane

	val transfo = new Course2JFX

	val Institute institute

	val hideLine = AOFFactory.INSTANCE.createOne(false)

	val BiMap<EObject, Node> transformationCache = HashBiMap.create

	val Map<Rectangle, Semester> semesterCache = new HashMap

	val defaultSemester = #{
		0 -> #{"ECSE 202", "ECSE 205", "MATH 262", "MATH 263"},
		1 -> #{"COMP 250", "ECSE 200", "ECSE 222", "ECSE 223", "FAC 100"},
		2 -> #{"CCOM 206", "ECSE 206", "ECSE 210", "ECSE 211", "ECSE 324"},
		3 -> #{"COMP 251", "ECSE 310", "ECSE 321", "ECSE 325", "MATH 240", "FACC 250"},
		4 -> #{"ECSE 308", "ECSE 331", "ECSE 353", "ECSE 427"},
		5 -> #{"ECSE 425", "ECSE 444", "ECSE 456"},
		6 -> #{"ECSE 457", "FACC 300", "FACC 400"}
	}

	def static void main(String[] args) {
		launch(args)
	}

	new() {
		extension val JFX = transfo.JFXMM
		extension val CR = transfo.CurriculumMM
		// Init meta model
		val rs = new ResourceSetImpl

		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"xmi",
			new XMIResourceFactoryImpl
		)
		rs.packageRegistry.put(
			CurriculumPackage.eNS_URI,
			CurriculumPackage.eINSTANCE
		)

		// create a resource
		val NbSemesters = 5
		val resource = rs.getFileResource("data.xmi");
		institute = resource.contents.get(0) as Institute
		institute.semesters.addAll(
			(0..<NbSemesters).map[ n |
				CurriculumFactory.eINSTANCE.createSemester => [
					number = n
					term = if (n%2 == 0) 'fall' else 'winter'
				]
			]
		)

		//TODO: sum() operation is not available in ATOL compiler yet
		//TODO: should be moved to a preprocessing refining number
		institute.semesters.forEach[s |
			s._totalCredits.bind(
//				new Sum(s._courses.credits, 0.0f, [$0+$1], [$0-$1]).result
				s._courses.credits.sumFloats
			)
		]

		// assign each course to its suggested semester
		//TODO: should be moved to a preprocessing refining number
		// c.semesterNumber <- c.institute.semesters.indexOf(c.semester)
		institute.courses.forEach[c |
			c._semesterNumber.bind(
				c._semester.collect([s |
					if (s !== null)
						s.number
					else
						0
				], [n |
					institute.semesters.filter[number == n].get(0)
				])
			)
			c.semester = institute.semesters.get(0)
//			defaultSemester.filter[k, v|
//				v.contains(c.code)
//			].forEach[k, v|
//				c.semesterNumber = k
//			]
		]

		val courses_pairs = institute._courses.collect[it -> transfo.Course(it).g]
		val courses = courses_pairs.collect[value]
		val semesters_pairs = institute._semesters.collect[it -> transfo.Semester(it).g]
		val semesters = semesters_pairs.collect[value]

		// TODO: make this incremental (e.g., by changing TupleRule.trace into a BiMap)
		courses_pairs.forEach[p |
			p.value.nodes.filter(Rectangle).forEach[
				transformationCache.put(p.key, it)
			]
		]
		semesters_pairs.forEach[p |
			p.value.nodes.filter(Rectangle).forEach[
				transformationCache.put(p.key, it)
				semesterCache.put(it, p.key)
			]
		]

		val figures = semesters.concat(courses).closure[
			it._children
		]
		nodes = figures.collectMutable[
			it?._nodes ?: AbstractRule.emptySequence
		]//.inspect("nodes: ")
		constraints = figures.collectMutable[
			it?._constraints ?: AbstractRule.emptySequence
		]//.inspect("constraints: ")

		c2cho = new Constraints2Choco(transfo.JFXMM, transfo.CurriculumMM)
		c2cho.defaultLowerBound = 0
		c2cho.defaultUpperBound = 6
		c2cho.apply(constraints.select[solver == "choco"].collect[it as Constraint])
		c2cho.solve

		c2cas = new Constraints2Cassowary(transfo.JFXMM, transfo.CurriculumMM)
		c2cas.apply(constraints.select[solver == "cassowary"].collect[it as Constraint])
		c2cas.solve

		nodes.filter[_movable.get].forEach[
			onPress
			onDrag
		]
		nodes.filter[_hideable.get].forEach[
			visibleProperty.toBox.bind(hideLine)
		]
	}

	// target element accessor for ListPatterns
	def static g(List<Object> l) {
		l.get(0) as Figure
	}


	override start(Stage stage) throws Exception {
		// Setup GUI stuff
		val scene = new Scene(root, 800, 800);
		stage.setScene(scene);
		stage.setTitle("Curriculum");
		stage.show();

		//Quit app on escape keypress
		scene.addEventHandler(KeyEvent.KEY_PRESSED, [KeyEvent t |
			switch t.getCode {
				case ESCAPE: {
					stage.close
				}
				case SPACE: {
//					c2cas.debug
					c2cho.debug
				}
				case H: hideLine.toggle
				case R: {
					val maxX = scene.width * 0.9
					val maxY = 600 * 0.9
					randomizePositions(maxX, maxY)
				}
				case D: {
					val courses = institute.courses.last
					if (courses !== null) {
						val c = courses
						c.requisitedBy.forEach[source.requisites.remove(it)]
						c.corequisitedBy.forEach[source.corequisites.remove(it)]
						c.semester = null
						EcoreUtil.delete(c)

						c2cho.solve
					}
				}
				default: {}
			}
		]);

		root.children.toBox.bind(
				nodes.select(Arrow).collect[it as Node]
			.concat(
				nodes.select(Rectangle).collect[it as Node]
			).concat(
				nodes.select(Text).collect[it as Node]
			).concat(
				nodes.select(Line).collect[it as Node]
			)
		)
		root.style = "-fx-background-color: #ffffff;"

		randomizePositions(scene.width * 0.9, 600 * 0.9)
	}

	def randomizePositions(double maxX, double maxY) {
		for (n : nodes) {
			switch (n) {
				Rectangle: {
					c2cas.suggestValue(n.xProperty, r.nextDouble * maxX)
					c2cas.suggestValue(n.yProperty, r.nextDouble * maxY)
				}
			}
		}
	}

	def toggle(IOne<Boolean> it) {
		set(!get)
	}

	var dx = 0.0
	var dy = 0.0

	def onPress(Node it) {
		onMousePressed = [e |
			val t = e.target
			switch t {
				Rectangle: {
					dx = e.x - t.x
					dy = e.y - t.y
					e.consume
				}
			}
		]
	}

	def onDrag(Node it) {
		onMouseDragged = [e |
			val t = e.target
			switch t {
				Rectangle:
					switch (e.button) {
						case MouseButton.PRIMARY: {
							val smNodes = semesterCache.filter[k, v | k.contains(e.x, e.y)]
							if (!smNodes.empty) {
								val p = smNodes.entrySet.get(0)
								val s = p.value
								val c = transformationCache.inverse.get(t) as Course //TODO; check if exists elsewhere
								extension val CR = transfo.CurriculumMM

								if (c.semester != s) {
									c2cho.suggest(c._semesterNumber, s.number)
									if (!c2cho.solve) {
										println("No solution found, ignoring suggestion")
									}
								}
								c2cas.suggestValue(t.xProperty, e.x - dx)
								c2cas.suggestValue(t.yProperty, e.y - dy)
							}
							e.consume
						}
						default: {}
					}
				}
		]
	}
}