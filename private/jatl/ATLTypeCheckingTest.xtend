/****************************************************************
 *  Copyright (C) 2021 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/
 
 package fr.eseo.jatl.atltypeinference.tests

import fr.eseo.jatl.atltypeinference.ATLTypeChecking
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.m2m.atl.common.ATL.ATLPackage
import org.eclipse.m2m.atl.common.OCL.Attribute
import org.eclipse.m2m.atl.common.OCL.OCLPackage
import org.eclipse.m2m.atl.common.OCL.Operation
import org.eclipse.m2m.atl.emftvm.compiler.AtlResourceFactoryImpl
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameter
import org.junit.runners.Parameterized.Parameters
import org.junit.Test

/*
 * TODO:
 *	- ATL compatibility ideas (also for ATOL
 *		- specify that calling a lazy rule without further navigation results in the first element
 *			- but any navigation on its result if to access other target elements
 *			- an explicit resolve could also do that to replace resolveTemp
 *	- transform this class into a JUnit test class
 *	- handle generics in some way (see OCLLibrary for some commented examples)
 *	- add a pseudo OclModelElement-based type (e.g., INFER!PLEASE) to infer type
 *		- or use an annotation
 */
@RunWith(Parameterized)
class ATLTypeCheckingTest {
	@Parameters(name = "{0}")
	def static main() {
		ATLTypeInferenceTest.main
	}

	def static <E> E trustMe(Object it) {
		it as E
	}

	@Parameter(0)
	public String name
	@Parameter(1)
	public Map<String, List<Resource>> models
	@Parameter(2)
	public Iterable<Resource> extraBuiltinLibraries

	@Test
	def void test() {
		println("**** " + name)
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put("atl", new AtlResourceFactoryImpl)

		//  Map<String, List<Resource>>
		val commonModels = #{
			"Ecore"			-> #[EcorePackage.eINSTANCE.eResource],
			"OCLLibrary"	-> #[
									extraBuiltinLibraries,
									#[
										rs.getResource(URI.createFileURI(
											"../fr.eseo.jatl.atltypeinference/OCLLibrary.atl"
										), true)
									]
								].flatten,
			"ATL"			-> #[
								OCLPackage.eINSTANCE.eResource,
								ATLPackage.eINSTANCE.eResource
							]
		}
		val Map<String, List<Resource>> allModels = trustMe(models.union(commonModels))
//		println("Without propagation:")
//		allModels.check(false)
//		println("With propagation:")
		allModels.check(true)
	}

	def static check(Map<String, List<Resource>> models, boolean withPropagation) {
		extension val ti = new ATLTypeChecking(models, withPropagation, true)
		println(everythingTypeChecks)
		printStatus

		for(i : 0..2) {
			for(it : context.constraints) {
				propagate(it)
			}
		}

		println("*** AFTER PROPAGATION ***")
		printStatus

		for(attr : Attribute.allInstancesFrom("IN")) {
			println('''Attribute «attr.name»: «
				attr.initExpression.inferredType.asString
			»''')
		}

		for(op : Operation.allInstancesFrom("IN")) {
			println('''Operation «op.name»: «
				op.body.inferredType.asString
			»''')
		}
	}
}
