/****************************************************************
 *  Copyright (C) 2021 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/
 package fr.eseo.jatl.atltypeinference.tests

import fr.eseo.jatl.ATLType.ATLTypePackage
import fr.eseo.jatl.ATLType.Type
import fr.eseo.jatl.atltypeinference.ATLTypeChecking
import java.util.Collections
import org.eclipse.m2m.atl.common.OCL.OCLFactory
import org.eclipse.m2m.atl.common.OCL.OCLPackage
import org.junit.Test

import static org.junit.Assert.assertEquals

import static extension org.eclipse.m2m.atl.common.OCL.OCLFactory.*
import static extension org.eclipse.m2m.atl.common.OCL.OCLPackage.*

class ATLTypeCheckingSolverTests {
	extension val OCLPackage = OCLPackage.eINSTANCE
	extension val OCLFactory = OCLFactory.eINSTANCE
	extension val ATLTypeChecking atc = new ATLTypeChecking(Collections.emptyMap, true, true)
	extension val ATLTypePackage atlt = ATLTypePackage.eINSTANCE

	def void testLE(Type l, Type r, boolean expected) {
		val msg = '''«l.showType» <= «r.showType»: «l <= r»'''
		println("	" + msg)
		assertEquals(msg, expected, l <= r)
	}

	val BagType = atlt.bagType.asATLType
	val CollectionType = atlt.collectionType.asATLType
	val IntegerType = atlt.integerType.asATLType
	val OclAnyType = atlt.oclAnyType.asATLType
	val IfExp = ifExp.asATLType
	val OclExpression = oclExpression.asATLType

	@Test
	def void basicTests() {
		println("*** Basic")
		testLE(BagType, BagType, true)
		testLE(atlt.bagType.asATLType(OclAnyType), atlt.bagType.asATLType(OclAnyType), true)
		testLE(atlt.bagType.asATLType(IntegerType), atlt.bagType.asATLType(OclAnyType), true)
		testLE(atlt.bagType.asATLType(OclAnyType), atlt.bagType.asATLType(IntegerType), false)
		testLE(CollectionType, CollectionType, true)
		testLE(BagType, CollectionType, true)
		testLE(CollectionType, BagType, false)
		testLE(IfExp, OclExpression, true)
		testLE(IfExp, BagType, false)
		testLE(BagType, IfExp, false)
	}	

	def newTypeVar(String name) {
		return (createVariableExp => [
			it.location = name
		]).inferredType
	}

	@Test
	def void singleVarPropagationUpperReductionTest() {
		println("*** Upper reduction")
		val tva = newTypeVar("a")
		tva.operator_LE(OclExpression)
		tva.operator_LE(IfExp)
		printStatus
	}

	@Test
	def void singleVarPropagationUpperAugmentationTest() {
		println("*** Upper augmentation")
		val tva = newTypeVar("a")
		tva.operator_LE(IfExp)
		tva.operator_LE(OclExpression)
		printStatus
	}

	@Test
	def void singleVarPropagationLowerReductionTest() {
		println("*** Lower reduction")
		val tva = newTypeVar("a")
		tva.operator_GE(IfExp)
		tva.operator_GE(OclExpression)
		printStatus
	}

	@Test
	def void singleVarPropagationLowerAugmentationTest() {
		println("*** Lower augmentation")
		val tva = newTypeVar("a")
		tva.operator_GE(OclExpression)
		tva.operator_GE(IfExp)
		printStatus
	}

	@Test
	def void twoVarLEPropagationTests() {
		println("*** Two-variable LE")
		val tva = newTypeVar("a")
		val tvb = newTypeVar("b")
		println(tva.showType)
		tva.operator_LE(OclExpression)
		tvb.operator_LE(IfExp)
		tvb.operator_LE(tva)
		printStatus
	}

	@Test
	def void twoVarEQPropagationTests() {
		println("*** Two-variable EQ")
		val tva = newTypeVar("a")
		val tvb = newTypeVar("b")
		println(tva.showType)
		tva.operator_LE(OclExpression)
		tvb.operator_LE(IfExp)
		tvb.operator_EQ(tva)
		printStatus
	}
}