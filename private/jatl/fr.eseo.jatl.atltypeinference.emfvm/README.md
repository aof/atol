Attempt at running the new version of ATLTypeInference on classical ATL.
Needs some modifications, because classical ATL does not support:
- operations in the context of Maps
- operations in the context of Tuples
...

With EMFTVM, the problem is different (with launch config), and may be solvable:
- could not find Ecore (because metamodels are apparently not considered as source models)