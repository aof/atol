/****************************************************************
 *  Copyright (C) 2021 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/
 
 package fr.eseo.jatl.atltypeinference

import fr.eseo.jatl.ATL2Java
import fr.eseo.jatl.ATL2Java.Metamodel
import fr.eseo.jatl.ATLType.ATLTypeFactory
import fr.eseo.jatl.ATLType.ATLTypePackage
import fr.eseo.jatl.ATLType.Constraint
import fr.eseo.jatl.ATLType.EQ
import fr.eseo.jatl.ATLType.Feature
import fr.eseo.jatl.ATLType.FixedType
import fr.eseo.jatl.ATLType.LE
import fr.eseo.jatl.ATLType.LT
import fr.eseo.jatl.ATLType.OclAnyType
import fr.eseo.jatl.ATLType.Operation
import fr.eseo.jatl.ATLType.Property
import fr.eseo.jatl.ATLType.TupleType
import fr.eseo.jatl.ATLType.Type
import fr.eseo.jatl.ATLType.TypeVariable
import fr.eseo.jatl.OCL2Java
import fr.eseo.jatl.OCLLibrary
import java.util.ArrayList
import java.util.Collection
import java.util.HashSet
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.m2m.atl.common.ATL.ATLPackage
import org.eclipse.m2m.atl.common.OCL.BagType
import org.eclipse.m2m.atl.common.OCL.BooleanType
import org.eclipse.m2m.atl.common.OCL.CollectionType
import org.eclipse.m2m.atl.common.OCL.IntegerType
import org.eclipse.m2m.atl.common.OCL.OCLPackage
import org.eclipse.m2m.atl.common.OCL.OclExpression
import org.eclipse.m2m.atl.common.OCL.OclModelElement
import org.eclipse.m2m.atl.common.OCL.OclType
import org.eclipse.m2m.atl.common.OCL.OrderedSetType
import org.eclipse.m2m.atl.common.OCL.SequenceType
import org.eclipse.m2m.atl.common.OCL.SetType
import org.eclipse.m2m.atl.common.OCL.StringType
import org.eclipse.m2m.atl.common.OCL.VariableDeclaration
import org.eclipse.m2m.atl.common.Problem.ProblemPackage
import org.eclipse.xtend.lib.annotations.Data

//@EcoreHelpers(packages = #[
//	ATLTypePackage
//])

@ATL2Java(libraries = #[
	"src/fr/eseo/jatl/atltypeinference/ATLTypeChecking.atl",
	"src/fr/eseo/jatl/atltypeinference/ATLTypingHelpers.atl"
], metamodels = #[
	@Metamodel(name = "ATL", ePackages =#[ATLPackage, OCLPackage]),
	@Metamodel(name = "Ecore", ePackages = #[EcorePackage]),
	@Metamodel(name = "Problem", ePackages = #[ProblemPackage]),
	@Metamodel(name = "ATLType", ePackages = #[ATLTypePackage])
])
@Data
//class ATLTypeInference extends ATLTypeInferenceMetamodels implements OCLLibrary {
class ATLTypeChecking extends ATLTypeInferenceMetamodels implements OCLLibrary {
	override getExcludedMessagePrefixes() {
		#{
		}
	}

	def name(Object it) {
		switch it {
			Map<?, ?>: get("name") as String
			EStructuralFeature: name
			EClassifier: name
			Class<?>: simpleName
			default: throw new UnsupportedOperationException("name")
		}
	}

	def Type asATLType(EClass it, Type...typeParameters) {
		val ret = ATLTypeFactory.eINSTANCE.createFixedType
		ret.type = it
		ret.name = name
		if(typeParameters.length > 0) {
			ret.name = '''«ret.name»<«typeParameters.map[e | e.showType].join(", ")»>'''
		}
		ret.typeParameters.addAll(typeParameters)
		ret
	}

	def Type asATLType(OclType it) {
		if(it instanceof org.eclipse.m2m.atl.common.OCL.TupleType) {
			return ATLTypeFactory.eINSTANCE.createTupleType => [ret |
				ret.parts.addAll(attributes.map[a |
					ATLTypeFactory.eINSTANCE.createTuplePart => [
						name = a.name
						type = ATLTypeFactory.eINSTANCE.createTypeVariable
						a.type.asATLType.operator_EQ(type)
					]
				])
			]
		}
		val ret = ATLTypeFactory.eINSTANCE.createFixedType
		ret.type = switch it {
			BooleanType: ATLTypePackage.eINSTANCE.booleanType
			IntegerType: ATLTypePackage.eINSTANCE.integerType
			StringType: ATLTypePackage.eINSTANCE.stringType
			OclModelElement:
				EClass.allInstancesFrom(model.name).findFirst[e |
					e.name == name
				]
			CollectionType: {
				ret.typeParameters.add(elementType.asATLType)
				switch it {
					BagType: ATLTypePackage.eINSTANCE.bagType
					OrderedSetType: ATLTypePackage.eINSTANCE.orderedSetType
					SetType: ATLTypePackage.eINSTANCE.setType
					SequenceType: ATLTypePackage.eINSTANCE.sequenceType
					CollectionType: ATLTypePackage.eINSTANCE.collectionType
					default: throw new IllegalStateException
				}
			}
			OclType: ATLTypePackage.eINSTANCE.oclType
			default: throw new UnsupportedOperationException('''asATLType for «it»''')
		}
		ret.name = ret.type.name
		if(ret.typeParameters.length > 0) {
			ret.name = '''«ret.name»<«ret.typeParameters.map[e | e.showType].join(", ")»>'''
		}
		ret
	}

	def Type asATLType(Class<?> it) {
		val ret = ATLTypeFactory.eINSTANCE.createFixedType
		ret.type = switch it {
			case Boolean: ATLTypePackage.eINSTANCE.booleanType
			case Integer: ATLTypePackage.eINSTANCE.integerType
			case String: ATLTypePackage.eINSTANCE.stringType
			case Class: ATLTypePackage.eINSTANCE.oclType
			case OclAnyType: ATLTypePackage.eINSTANCE.oclAnyType
			case Collection: ATLTypePackage.eINSTANCE.collectionType
			case List: ATLTypePackage.eINSTANCE.sequenceType
			case Object: ATLTypePackage.eINSTANCE.oclAnyType
			default: throw new UnsupportedOperationException('''asATLType for «it»''')
		}
		ret.name = ret.type.name
		ret
	}

	val context = ATLTypeFactory.eINSTANCE.createContext

	def boolean <(EClass l, EClass b) {
		l.EAllSuperTypes.contains(b)
	}

	val oclAnyType = ATLTypePackage.eINSTANCE.oclAnyType

	def dispatch boolean <=(FixedType l, FixedType r) {
			r == oclAnyType
		||	(
				(l.type == r.type || l.type.EAllSuperTypes.contains(r.type))
			&&
				l.typeParameters.length == r.typeParameters.length
			&&
				l.typeParameters.zip(r.typeParameters).forall[p |
					p.key <= p.value
				]
			)
	}

	def dispatch boolean <=(FixedType l, Type r) {
		switch r {
			FixedType: l <= r
			TypeVariable: r.lowerBounds.forall[b |
				b <= r
			]
			default: throw new IllegalStateException
		}
	}

	def dispatch boolean <=(TypeVariable l, Type r) {
		switch r {
			FixedType: l.upperBounds.forall[b |
				b <= r
			]
			TypeVariable: true // TODO
			default: throw new IllegalStateException
		}
	}

	val boolean propagate
	val boolean showChanges

	def println(TypeVariable it, String msg) {
		if(showChanges) {
			println('''	«msg»: «asString»''')
		}
	}

	def makeEQ(TypeVariable it, Type t) {
//		val ret = lowerBounds.forall[b |
//			b <= t
//		] && upperBounds.forall[b |
//			t <= b
//		]
		if(propagate) {
			it.println("Before")
			lowerBounds.clear
			lowerBounds.add(t)
			upperBounds.clear
			upperBounds.add(t)
			it.println("After")
		}
		true	// must return true for all constraints to be added
	}

	val variables = new HashSet<TypeVariable>
	def add(Type it) {
		if(it instanceof TypeVariable) {
			variables.add(it)
		}
	}

	val resolvedOperations = new HashSet<Operation>
	val resolvedProperties = new HashSet<Property>

	def getContextType(Feature it) {
		val ctxt = it.context
		switch ctxt {
			TypeVariable:	if(
									ctxt.lowerBounds.length == 1
								&&
									ctxt.lowerBounds == ctxt.upperBounds
							) {
								// TODO: should bounds be FixedTypes in the metamodel,
								(ctxt.lowerBounds.get(0) as FixedType).type
							}
			FixedType: ctxt.type
			default: throw new IllegalStateException
		}
	}

	def asATLType(EClassifier it) {
		switch it {
			EClass: asATLType(#[])
			EDataType: instanceClass.asATLType
			default: throw new IllegalStateException
		}
	}
	def void propagate(Constraint it) {
		for(v : new ArrayList(variables)) {
			// TODO: consider helpers
			val op = v.typedOperation
			if(op !== null && !resolvedOperations.contains(op)) {
				if(op.context instanceof TupleType) {
					// TODO
				} else {
					val ctxtType = op.contextType
					if(ctxtType !== null) {
						// TODO: what about argumentTypes?
						val opName = OCL2Java.operatorNames.get(op.name) ?: op.name
						println("ready for operation lookup: " + opName)
						val ops = ctxtType.eAllOperations.filter[name == opName]
						if(ops.length == 1) {
							resolvedOperations.add(op)
							ops.get(0).EType.asATLType.operator_EQ(op.type)
						}
					}
				}
			} else {
				val prop = v.typedProperty
				if(prop !== null && !resolvedProperties.contains(prop)) {
					val ctxt = prop.context
					if(ctxt instanceof TupleType) {
						// even if we cannot resolve it now, we won't ever, so stop considering it
						resolvedProperties.add(prop)
						val part = ctxt.parts.findFirst[name == prop.name]
						if(part !== null) {
								part.type.operator_EQ(prop.type)
						}
					} else {
						val ctxtType = prop.contextType
						if(ctxtType !== null) {
							println("ready for property lookup: " + prop.name)
							val props = ctxtType.eAllStructuralFeatures.filter[name == prop.name]
							if(props.length == 1) {
								resolvedProperties.add(prop)
								props.get(0).EType.operator_EQ(prop.type)
							}
						}
					}
				}
			}
		}
		propagate_
	}

	def dispatch propagate_(LE it) {
		val l = left
		val r = right
		switch l {
			TypeVariable: switch r {
				TypeVariable: {
					if(propagate) {
						l.println("Left before")
						l.println("Right before")
						l.upperBounds
						r.lowerBounds
						l.println("Left after")
						l.println("Right after")
					}
				}
				FixedType:
					l.makeLE(r)
				default: throw new IllegalStateException
			}
			FixedType: switch r {
				TypeVariable:
					r.makeGE(l)
				FixedType:
					println("TODO: check <=")
				default: throw new IllegalStateException
			}
			default: throw new IllegalStateException
		}
	}

	def dispatch propagate_(EQ it) {
		val l = types.get(0)
		val r = types.get(1)
		switch l {
			TypeVariable: switch r {
				TypeVariable: {
					if(propagate) {
						l.println("Left before")
						r.println("Right before")

						val upperBounds = new ArrayList<Type>
						for(lub : l.upperBounds) {
							val b = r.upperBounds.findFirst[it <= lub || lub <= it]
							if(b === null) {
								upperBounds.add(lub)
							} else if(b <= lub) {
								upperBounds.add(b)
							} else {
								upperBounds.add(lub)
							}
						}
						for(rub : r.upperBounds) {
							val b = upperBounds.findFirst[it <= rub || rub <= it]
							if(b === null) {
								upperBounds.add(rub)
							} else  {
								// already handled in the previous loop
							}
						}
						val lowerBounds = new ArrayList<Type>
						for(llb : l.lowerBounds) {
							val b = r.lowerBounds.findFirst[it <= llb || llb <= it]
							if(b === null) {
								lowerBounds.add(llb)
							} else if(b <= llb) {
								lowerBounds.add(llb)
							} else {
								lowerBounds.add(b)
							}
						}
						for(rlb : r.lowerBounds) {
							val b = lowerBounds.findFirst[it <= rlb || rlb <= it]
							if(b === null) {
								lowerBounds.add(rlb)
							} else  {
								// already handled in the previous loop
							}
						}
						replaceAll(l.upperBounds, upperBounds)
						replaceAll(r.upperBounds, upperBounds)
						replaceAll(l.lowerBounds, lowerBounds)
						replaceAll(r.lowerBounds, lowerBounds)

						l.println("Left after")
						r.println("Right after")
					}
				}
				FixedType:
					l.makeEQ(r)
				default: throw new IllegalStateException
			}
			FixedType: switch r {
				TypeVariable:
					r.makeEQ(l)
				FixedType:
					println("TODO: check ==")
				default: throw new IllegalStateException
			}
			default: throw new IllegalStateException
		}
	}

	def operator_EQ(Type l, Type r) {
		val constraint = ATLTypeFactory.eINSTANCE.createEQ
		constraint.types.add(l)
		constraint.types.add(r)
		context.constraints.add(constraint)

		l.add
		r.add

		if(showChanges) {
			println('''Adding: «constraint.asString»''')
		}
		constraint.propagate

		true	// must return true for all constraints to be added
	}

	def replaceAll(Collection<Type> it, Collection<Type> o) {
//		println("A" + it.map[showType] + " " + o.map[showType])
		clear
//		println("B" + it.map[showType])
		addAll(o)
//		println("C" + it.map[showType])
	}

	def makeLE(TypeVariable l, Type r) {
		if(propagate) {
			l.println("Before")
			val upperBounds = new ArrayList
			var comparable = false
			for(b : l.upperBounds) {
				if(r <= b) {
					// reducing bound
					upperBounds.add(r)
					comparable = true
				} else {
					// keeping bound
					upperBounds.add(b)
					if(b <= r) {
						comparable = true
					}
				}
			}
			if(!comparable) {
				println("	not comparable")
				upperBounds.add(r)
			}
			l.upperBounds.replaceAll(upperBounds)
			l.println("After")
		}
		true	// must return true for all constraints to be added
	}

	def makeGE(TypeVariable l, Type r) {
		if(propagate) {
			l.println("Before")
			val lowerBounds = new ArrayList
			var comparable = false
			for(b : l.lowerBounds) {
				if(b <= r) {
					// reducing bound
					lowerBounds.add(r)
					comparable = true
				} else {
					// keeping bound
					lowerBounds.add(b)
					if(r <= b) {
						comparable = true
					}
				}
			}
			if(!comparable) {
				println("	not comparable")
				lowerBounds.add(r)
			}
			l.lowerBounds.replaceAll(lowerBounds)
			l.println("After")
		}
		true	// must return true for all constraints to be added
	}

	def operator_LE(Type l, Type r) {
		val constraint = ATLTypeFactory.eINSTANCE.createLE
		constraint.left = l
		constraint.right = r
		context.constraints.add(constraint)

		l.add
		r.add

		if(showChanges) {
			println('''Adding: «constraint.asString»''')
		}
		constraint.propagate

		true	// must return true for all constraints to be added
	}

	def dispatch showType(TupleType it) {
		'''TupleType(«parts.map['''«name»:«type.showType»'''].join(",")»)'''
	}

	def dispatch showType(TypeVariable it) {
		val te = typedExpression as OclExpression
		if(te !== null) {
			'''typeof(«te.class.simpleName»@«te.location»)'''
		} else {
			val tv = typedVariable as VariableDeclaration
			if(tv !== null) {
				'''varType(«tv.varName»@«tv.location»)'''
			} else {
				val tp = typedProperty
				if(tp !== null) {
					'''propType(«tp.context.showType», «tp.name»)'''
				} else {
					val to = typedOperation
					if(to !== null) {
						'''opType(«to.context.showType».«to.name»(«
							to.argumentTypes.map[showType].join(",")
						»))'''	// TODO: to.argumentTypes?
					} else {
						'''TypeVar@«hashCode»'''
					}
				}
			}
		}
	}

	def dispatch showType(FixedType it) {
		'''«name»'''
	}


	def asString(Constraint it) {
		switch it {
			EQ:
				'''«types.get(0).showType» == «types.get(1).showType»'''
			LT:
				'''«left.showType» < «right.showType»'''
			LE:
				'''«left.showType» <= «right.showType»'''
		}
	}

	def dispatch asString(FixedType it) {
		showType
	}

	def dispatch asString(TypeVariable it) {
		'''«lowerBounds.map[showType]» <= «showType» <= «upperBounds.map[showType]»'''
	}

	def operator_GE(Type l, Type r) {
		operator_LE(r, l)
	}

	def Type type(Property it) {
		getType
	}

	def Type type(Operation it) {
		getType
	}

	def Type elementType(Type it) {
		null	// TODO
	}

	def printStatus() {
		println("	Variables:")
		variables.forEach[
			println("		" + asString)
		]
		println("	Constraints:")
		context.constraints.forEach[
			println("		" + asString)
		]
	}
}
