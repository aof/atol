/****************************************************************
 *  Copyright (C) 2020 ESEO, Université d'Angers 
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *    - Frédéric Jouault
 *    - Théo Le Calvar
 *
 *  version 1.0
 *
 *  SPDX-License-Identifier: EPL-2.0
 ****************************************************************/

package fr.eseo.atol.atlc.geometry2javafx

import fr.eseo.atlc.constraints.CompositeExp
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.VariableExp
import geometry.L1Circle
import geometry.L2Circle
import geometry.Line
import geometry.Text
import java.util.Random
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import org.eclipse.emf.ecore.EObject
import org.eclipse.papyrus.aof.core.IBox
import org.junit.Test

import static org.junit.Assert.assertEquals

class TestGeometry2JFX {
	val transfo = new Geometry2JavaFX
	val r = new Random
	val color = "0xffffffff"
	val color2 = "0x00ff00ff"
	
	static def L1Circle createL1Circle(String fill, String stroke, Boolean movable) {
		Geometry.L1Circle.newInstance => [
			it.fill = fill
			it.stroke = stroke
			it.movable = movable
		]
	}
	static def L2Circle createL2Circle(String fill, String stroke, Boolean movable) {
		Geometry.L2Circle.newInstance => [
			it.fill = fill
			it.stroke = stroke
			it.movable = movable
		]
	}
	static def geometry.Rectangle createRectangle(String fill, String stroke, Boolean movable) {
		Geometry.Rectangle.newInstance => [
			it.fill = fill
			it.stroke = stroke
			it.movable = movable
		]
	}
	static def Text createText(String text, String fill, String stroke, Boolean movable) {
		Geometry.Text.newInstance => [
			it.text = text
			it.fill = fill
			it.stroke = stroke
			it.movable = movable
		]
	}
	static def Line createLine(String fill, String stroke, Boolean movable) {
		Geometry.Line.newInstance => [
			it.fill = fill
			it.stroke = stroke
			it.movable = movable
		]
	}
	
	def void assertSameEMF(EObject expected, EObject o) {
		assertEquals("same class", expected.eClass, o.eClass)
		expected.eClass.EAllAttributes.forEach[
			assertEquals(expected.eGet(it), o.eGet(it))
		]
		expected.eClass.EAllReferences.forEach[
			assertSameEMF(expected.eGet(it) as EObject, o.eGet(it) as EObject)
		]
	}
	
	@Test
	def void testCircleL1() {
		val movable = r.nextBoolean
		
		val circle = createL1Circle(color, color, movable)
		val jfxCircle = transfo.Circle(circle).t

		assertEquals(color, jfxCircle.fill.toString)
		assertEquals(color, jfxCircle.stroke.toString)
		assertEquals(movable, transfo.JFXMM._movable(jfxCircle).get)
		
		circle.stroke = color2
		assertEquals(color2, jfxCircle.stroke.toString)
		
		circle.fill = color2
		assertEquals(color2, jfxCircle.fill.toString)
	}

	@Test
	def void testCircleL2() {
		val movable = r.nextBoolean
		
		val circle = createL2Circle(color, color, movable)
		val jfxCircle = transfo.Circle(circle).t

		assertEquals(color, jfxCircle.fill.toString)
		assertEquals(color, jfxCircle.stroke.toString)
		assertEquals(movable, transfo.JFXMM._movable(jfxCircle).get)
		
		circle.stroke = color2
		assertEquals(color2, jfxCircle.stroke.toString)
		
		circle.fill = color2
		assertEquals(color2, jfxCircle.fill.toString)
	}

	@Test
	def void testCircleRadius() {
		val circle = createL1Circle(color, color, true)
		val circle2 = createL1Circle(color, color, true)
		val circleRadius = Geometry.CircleRadius.newInstance => [
			it.circle = circle
		]
		
		val radius = transfo.CircleRadius(circleRadius).t as VariableExp
		val jfxCircle = transfo.Circle(circle).t
		val jfxCircle2 = transfo.Circle(circle2).t
		
		assertEquals("source of the radius", jfxCircle, (radius.source as IBox<Circle>).get(0))
		assertEquals("property name", "radius", radius.propertyName)
		assertEquals("is constant", false, radius.isConstant)
		assertEquals("is vector", true, radius.isVector)
		
		circleRadius.circle = circle2
		assertEquals("source of the radius should change", jfxCircle2, (radius.source as IBox<Circle>).get(0))
	}

	@Test
	def void testCircleCenter() {
		val circle = createL1Circle(color, color, true)
		val circle2 = createL1Circle(color, color, true)
		val circleCenter = Geometry.CircleCenter.newInstance => [
			it.circle = circle
		]
		
		val centerX = transfo.CircleCenter(circleCenter).x as VariableExp
		val centerY = transfo.CircleCenter(circleCenter).y as VariableExp
		val jfxCircle = transfo.Circle(circle).t
		val jfxCircle2 = transfo.Circle(circle2).t
		
		assertEquals("source of the centerX", jfxCircle, (centerX.source as IBox<Circle>).get(0))
		assertEquals("property name", "centerX", centerX.propertyName)
		assertEquals("is constant", false, centerX.isConstant)
		assertEquals("is vector", true, centerX.isVector)
		
		assertEquals("source of the centerY", jfxCircle, (centerY.source as IBox<Circle>).get(0))
		assertEquals("property name", "centerY", centerY.propertyName)
		assertEquals("is constant", false, centerY.isConstant)
		assertEquals("is vector", true, centerY.isVector)
		
		circleCenter.circle = circle2
		assertEquals("source of the centerX should change", jfxCircle2, (centerX.source as IBox<Circle>).get(0))
		assertEquals("source of the centerY should change", jfxCircle2, (centerY.source as IBox<Circle>).get(0))
	}
	
	@Test
	def void testRectangle() {
		val movable = r.nextBoolean
		
		val rect = createRectangle(color, color, movable)
		val jfxRect = transfo.Rectangle(rect).t
		
		assertEquals("stroke",color, jfxRect.stroke.toString)
		assertEquals("fill", color, jfxRect.fill.toString)
		assertEquals("movable", movable, transfo.JFXMM._movable(jfxRect).get)
		
		rect.stroke = color2
		assertEquals("stroke after update",color2, jfxRect.stroke.toString)
		
		rect.fill = color2
		assertEquals("fill after update", color2, jfxRect.fill.toString)
	}
	
	@Test
	def void testRectangleTopLeft() {
		val rect = createRectangle(color, color, false)
		val rect2 = createRectangle(color2, color2, false)
		val rectCorner = Geometry.RectangleTopLeft.newInstance => [
			rectangle = rect
		]
		val jfxRect = transfo.Rectangle(rect).t
		val pointX = transfo.Point(rectCorner).x as VariableExp
		val pointY = transfo.Point(rectCorner).y as VariableExp
		
		assertEquals("x source", jfxRect, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("x property name", "x", pointX.propertyName)
		assertEquals("x is vector", true, pointX.isVector)
		assertEquals("x is constant", false, pointX.isConstant)
		
		assertEquals("y source", jfxRect, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("y property name", "y", pointY.propertyName)
		assertEquals("y is vector", true, pointY.isVector)
		assertEquals("y is constant", false, pointY.isConstant)
		
		rectCorner.rectangle = rect2
		
		val jfxRect2 = transfo.Rectangle(rect2).t
		
		assertEquals("x source", jfxRect2, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("y source", jfxRect2, (pointY.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testRectangleSize() {
		val rect = createRectangle(color, color, false)
		val rect2 = createRectangle(color2, color2, false)
		val rectCorner = Geometry.RectangleSize.newInstance => [
			rectangle = rect
		]
		val jfxRect = transfo.Rectangle(rect).t
		val pointX = transfo.Point(rectCorner).x as VariableExp
		val pointY = transfo.Point(rectCorner).y as VariableExp
		
		assertEquals("x source", jfxRect, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("x property name", "width", pointX.propertyName)
		assertEquals("x is vector", true, pointX.isVector)
		assertEquals("x is constant", false, pointX.isConstant)
		
		assertEquals("y source", jfxRect, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("y property name", "height", pointY.propertyName)
		assertEquals("y is vector", true, pointY.isVector)
		assertEquals("y is constant", false, pointY.isConstant)
		
		rectCorner.rectangle = rect2
		
		val jfxRect2 = transfo.Rectangle(rect2).t
		
		assertEquals("x source updated", jfxRect2, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("y source updated", jfxRect2, (pointY.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testRectangleBottomLest() {
		val rect = createRectangle(color, color, false)
		val rect2 = createRectangle(color2, color2, false)
		val rectCorner = Geometry.RectangleBottomLeft.newInstance => [
			rectangle = rect
		]
		val jfxRect = transfo.Rectangle(rect).t
		val pointX = transfo.Point(rectCorner).x as VariableExp
		val pointY = transfo.Point(rectCorner).y as CompositeExp
		var left = pointY.arguments.get(0) as VariableExp
		var right = pointY.arguments.get(1) as VariableExp
		
		assertEquals("x source", jfxRect, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("x property name", "x", pointX.propertyName)
		assertEquals("x is vector", true, pointX.isVector)
		assertEquals("x is constant", false, pointX.isConstant)
		
		assertEquals("y.left source", jfxRect, (left.source as IBox<Rectangle>).get(0))
		assertEquals("y.left property name", "y", left.propertyName)
		assertEquals("y.left is vector", true, left.isVector)
		assertEquals("y.left is constant", false, left.isConstant)
		
		assertEquals("y.right source", jfxRect, (right.source as IBox<Rectangle>).get(0))
		assertEquals("y.right property name", "height", right.propertyName)
		assertEquals("y.right is vector", true, right.isVector)
		assertEquals("y.right is constant", false, right.isConstant)
		
		rectCorner.rectangle = rect2
		
		val jfxRect2 = transfo.Rectangle(rect2).t
		left = pointY.arguments.get(0) as VariableExp
		right = pointY.arguments.get(1) as VariableExp
		
		assertEquals("x source updated", jfxRect2, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("y.left source updated", jfxRect2, (left.source as IBox<Rectangle>).get(0))
		assertEquals("y.right source updated", jfxRect2, (right.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testRectangleTopRight() {
		val rect = createRectangle(color, color, false)
		val rect2 = createRectangle(color2, color2, false)
		val rectCorner = Geometry.RectangleTopRight.newInstance => [
			rectangle = rect
		]
		val jfxRect = transfo.Rectangle(rect).t
		val pointX = transfo.Point(rectCorner).x as CompositeExp
		val pointY = transfo.Point(rectCorner).y as VariableExp
		var left = pointX.arguments.get(0) as VariableExp
		var right = pointX.arguments.get(1) as VariableExp
		
		assertEquals("x.left source", jfxRect, (left.source as IBox<Rectangle>).get(0))
		assertEquals("x.left property name", "x", left.propertyName)
		assertEquals("x.left is vector", true, left.isVector)
		assertEquals("x.left is constant", false, left.isConstant)
		
		assertEquals("x.right source", jfxRect, (right.source as IBox<Rectangle>).get(0))
		assertEquals("x.right property name", "width", right.propertyName)
		assertEquals("x.right is vector", true, right.isVector)
		assertEquals("x.right is constant", false, right.isConstant)
		
		assertEquals("y source", jfxRect, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("y property name", "y", pointY.propertyName)
		assertEquals("y is vector", true, pointY.isVector)
		assertEquals("y is constant", false, pointY.isConstant)
		
		rectCorner.rectangle = rect2
		
		val jfxRect2 = transfo.Rectangle(rect2).t
		left = pointX.arguments.get(0) as VariableExp
		right = pointX.arguments.get(1) as VariableExp
		
		assertEquals("y source updated", jfxRect2, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("x.left source updated", jfxRect2, (left.source as IBox<Rectangle>).get(0))
		assertEquals("x.right source updated", jfxRect2, (right.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testRectangleBottomRight() {
		val rect = createRectangle(color, color, false)
		val rect2 = createRectangle(color2, color2, false)
		val rectCorner = Geometry.RectangleBottomRight.newInstance => [
			rectangle = rect
		]
		val jfxRect = transfo.Rectangle(rect).t
		val pointX = transfo.Point(rectCorner).x as CompositeExp
		val pointY = transfo.Point(rectCorner).y as CompositeExp
		var leftX = pointX.arguments.get(0) as VariableExp
		var rightX = pointX.arguments.get(1) as VariableExp
		var leftY = pointY.arguments.get(0) as VariableExp
		var rightY = pointY.arguments.get(1) as VariableExp
		
		assertEquals("x.left source", jfxRect, (leftX.source as IBox<Rectangle>).get(0))
		assertEquals("x.left property name", "x", leftX.propertyName)
		assertEquals("x.left is vector", true, leftX.isVector)
		assertEquals("x.left is constant", false, leftX.isConstant)
		
		assertEquals("x.right source", jfxRect, (rightX.source as IBox<Rectangle>).get(0))
		assertEquals("x.right property name", "width", rightX.propertyName)
		assertEquals("x.right is vector", true, rightX.isVector)
		assertEquals("x.right is constant", false, rightX.isConstant)
		
		assertEquals("y.left source", jfxRect, (leftY.source as IBox<Rectangle>).get(0))
		assertEquals("y.left property name", "y", leftY.propertyName)
		assertEquals("y.left is vector", true, leftY.isVector)
		assertEquals("y.left is constant", false, leftY.isConstant)
		
		assertEquals("y.right source", jfxRect, (rightY.source as IBox<Rectangle>).get(0))
		assertEquals("y.right property name", "height", rightY.propertyName)
		assertEquals("y.right is vector", true, rightY.isVector)
		assertEquals("y.right is constant", false, rightY.isConstant)
		
		rectCorner.rectangle = rect2
		
		val jfxRect2 = transfo.Rectangle(rect2).t
		leftX = pointX.arguments.get(0) as VariableExp
		rightX = pointX.arguments.get(1) as VariableExp
		leftY = pointY.arguments.get(0) as VariableExp
		rightY = pointY.arguments.get(1) as VariableExp
		
		assertEquals("x.left source updated", jfxRect2, (leftX.source as IBox<Rectangle>).get(0))
		assertEquals("x.right source updated", jfxRect2, (rightX.source as IBox<Rectangle>).get(0))
		assertEquals("y.left source updated", jfxRect2, (leftY.source as IBox<Rectangle>).get(0))
		assertEquals("y.right source updated", jfxRect2, (rightY.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testLine() {
		val movable = r.nextBoolean
		
		val l = createLine(color, color, movable)
		val jfxL = transfo.Line(l).t
		
		assertEquals("stroke",color, jfxL.stroke.toString)
		assertEquals("fill", color, jfxL.fill.toString)
		assertEquals("movable", movable, transfo.JFXMM._movable(jfxL).get)
		
		l.stroke = color2
		assertEquals("stroke after update",color2, jfxL.stroke.toString)
		
		l.fill = color2
		assertEquals("fill after update", color2, jfxL.fill.toString)
	}
	
	@Test
	def void testLineStart() {
		val l1 = createLine(color, color, false)
		val l2 = createLine(color2, color2, false)
		val lStart = Geometry.LineStart.newInstance => [
			line = l1
		]
		val jfxL = transfo.Line(l1).t
		val pointX = transfo.Point(lStart).x as VariableExp
		val pointY = transfo.Point(lStart).y as VariableExp
		
		assertEquals("x source", jfxL, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("x property name", "startX", pointX.propertyName)
		assertEquals("x is vector", true, pointX.isVector)
		assertEquals("x is constant", false, pointX.isConstant)
		
		assertEquals("y source", jfxL, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("y property name", "startY", pointY.propertyName)
		assertEquals("y is vector", true, pointY.isVector)
		assertEquals("y is constant", false, pointY.isConstant)
		
		lStart.line = l2
		
		val jfxL2 = transfo.Line(l2).t
		
		assertEquals("x source updated", jfxL2, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("y source updated", jfxL2, (pointY.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testLineEnd() {
		val l1 = createLine(color, color, false)
		val l2 = createLine(color2, color2, false)
		val lEnd = Geometry.LineEnd.newInstance => [
			line = l1
		]
		val jfxL = transfo.Line(l1).t
		val pointX = transfo.Point(lEnd).x as VariableExp
		val pointY = transfo.Point(lEnd).y as VariableExp
		
		assertEquals("x source", jfxL, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("x property name", "endX", pointX.propertyName)
		assertEquals("x is vector", true, pointX.isVector)
		assertEquals("x is constant", false, pointX.isConstant)
		
		assertEquals("y source", jfxL, (pointY.source as IBox<Rectangle>).get(0))
		assertEquals("y property name", "endY", pointY.propertyName)
		assertEquals("y is vector", true, pointY.isVector)
		assertEquals("y is constant", false, pointY.isConstant)
		
		lEnd.line = l2
		
		val jfxL2 = transfo.Line(l2).t
		
		assertEquals("x source updated", jfxL2, (pointX.source as IBox<Rectangle>).get(0))
		assertEquals("y source updated", jfxL2, (pointY.source as IBox<Rectangle>).get(0))
	}
	
	@Test
	def void testLineCenter() {
		val l1 = createLine(color, color, false)
		val lCenter = Geometry.LineCenter.newInstance => [
			line = l1
		]
		val jfxL = transfo.Line(l1).t
		val pointX = transfo.Point(lCenter).x as CompositeExp
		val pointY = transfo.Point(lCenter).y as CompositeExp
		var leftX = pointX.arguments.get(0) as CompositeExp
		var rightX = pointX.arguments.get(1) as DoubleExp
		var leftY = pointY.arguments.get(0) as CompositeExp
		var rightY = pointY.arguments.get(1) as DoubleExp
		
		assertEquals("/", pointX.operatorName)
		assertEquals("/", pointY.operatorName)
		assertEquals(2.0, rightX.value, 0E-5)
		assertEquals(2.0, rightY.value, 0E-5)
		
		assertEquals("+", leftX.operatorName)
		val leftXLeft = leftX.arguments.get(0) as VariableExp
		assertEquals(jfxL, (leftXLeft.source as IBox<Line>).get(0))
		assertEquals("startX", leftXLeft.propertyName)
		assertEquals(true, leftXLeft.isVector)
		assertEquals(false, leftXLeft.isConstant)
		
		val leftXRight = leftX.arguments.get(1) as VariableExp
		assertEquals(jfxL, (leftXRight.source as IBox<Line>).get(0))
		assertEquals("endX", leftXRight.propertyName)
		assertEquals(true, leftXRight.isVector)
		assertEquals(false, leftXRight.isConstant)
		
		assertEquals("+", leftY.operatorName)
		val leftYLeft = leftY.arguments.get(0) as VariableExp
		assertEquals(jfxL, (leftYLeft.source as IBox<Line>).get(0))
		assertEquals("startY", leftYLeft.propertyName)
		assertEquals(true, leftYLeft.isVector)
		assertEquals(false, leftYLeft.isConstant)
		
		val leftYRight = leftY.arguments.get(1) as VariableExp
		assertEquals(jfxL, (leftYRight.source as IBox<Line>).get(0))
		assertEquals("endY", leftYRight.propertyName)
		assertEquals(true, leftYRight.isVector)
		assertEquals(false, leftXRight.isConstant)
	}
	
}