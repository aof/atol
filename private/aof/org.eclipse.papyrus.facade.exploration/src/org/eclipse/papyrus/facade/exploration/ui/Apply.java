package org.eclipse.papyrus.facade.exploration.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.papyrus.facade.exploration.Facade;
import org.eclipse.papyrus.facade.exploration.FacadeModel;
import org.eclipse.papyrus.facade.exploration.UMLRTFacade;
import org.eclipse.papyrus.infra.core.listenerservice.IPapyrusListener;
import org.eclipse.papyrus.uml.tools.listeners.PapyrusStereotypeListener;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;


public class Apply implements IObjectActionDelegate {

    private ISelection selection;
    private IWorkbenchPart activePart;

    /**
     * Constructor for Action1.
     */
    public Apply() {
        super();
    }

    /**
     * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
     */
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
        this.activePart = targetPart;
    }

    /**
     * @see IActionDelegate#run(IAction)
     */
    public void run(IAction action) {
        Resource sourceResource = (Resource)((StructuredSelection)selection).getFirstElement();

        // activate stereotype notifications... TODO: avoid doing it if already done (e.g., by Papyrus?)
        final IPapyrusListener pl = new PapyrusStereotypeListener();
		sourceResource.eAdapters().add(new EContentAdapter() {
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				pl.notifyChanged(notification);
			}
		});

        ResourceSet resourceSet = sourceResource.getResourceSet();
        Resource targetResource = resourceSet.createResource(URI.createURI(sourceResource.getURI().toString() + "-transformed.xmi"));
//        System.out.println("Hello");

        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();

        IProject project = workspaceRoot.getProject("ATLI");
        Facade facade =
                new UMLRTFacade();

        Resource sourceReferenceResource = resourceSet.getResource(URI.createURI("ClassDiagram"), false);
        facade.addSource(sourceResource);

        facade.addTarget(new FacadeModel(targetResource));

        facade.init();
    }

    /**
     * @see IActionDelegate#selectionChanged(IAction, ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        this.selection = selection;
    }

}
