package org.eclipse.papyrus.facade.exploration;

import static org.eclipse.papyrus.facade.exploration.Utils.create;
import static org.eclipse.papyrus.facade.exploration.Utils.createOne;
import static org.eclipse.papyrus.facade.exploration.Utils.createPropertyBox;
import static org.eclipse.papyrus.facade.exploration.Utils.findClass;
import static org.eclipse.papyrus.facade.exploration.Utils.inspectProperty;
import static org.eclipse.papyrus.facade.exploration.Utils.select;
import static org.eclipse.papyrus.facade.exploration.Utils.set;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBinaryFunction;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IObserver;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IPair;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver;
import org.eclipse.papyrus.aof.core.impl.utils.cache.IBinaryCache;
import org.eclipse.papyrus.aof.core.impl.utils.cache.IUnaryCache;
import org.eclipse.papyrus.aof.core.impl.utils.cache.WeakKeysWeakValuesBinaryCache;
import org.eclipse.papyrus.aof.core.impl.utils.cache.WeakKeysWeakValuesUnaryCache;
import org.eclipse.papyrus.aof.emf.EMFFactory;
import org.eclipse.papyrus.facade.exploration.Rule.Bindings;
import org.eclipse.papyrus.facade.exploration.obsolete.HashBinaryCache;
import org.eclipse.papyrus.facade.exploration.obsolete.HashUnaryCache;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;


// DONE: precompute guards that are in bindings in rules so that they are not recomputed all the time
// but would be unnecessary if bindings were not in a method, but parameters to the rule constructor
// => actually using a cache in boxOutput and lift does the trick (TODO: do we need a cache for isA? currently only called once per type)
public class UMLRTFacade extends Facade {

	public void applyStereotype(Element e, String stereotypeName) {
		if(e.eContainer() == null) {
			if(e instanceof PackageableElement) {
				tempModel.getPackagedElements().add((PackageableElement)e);
			} else {
				throw new UnsupportedOperationException("cannot apply stereotype on detached element " + e);
			}
		}
		Stereotype s = e.getApplicableStereotype(stereotypeName);
		e.applyStereotype(s);
	}

	@Override
	public void init() {
		ResourceSet rs = new ResourceSetImpl();
		URI uri = URI.createURI(UMLRTFacade.class.getResource("umlrtmetamodel.ecore").toString());
		final Resource umlRTMM = rs.getResource(uri, true);
		final Resource targetMM = umlRTMM;

		final UMLPackage UML = UMLPackage.eINSTANCE;

		addRule(new Rule<Model, EObject>(this, "Model2Model", UML.getModel(), findClass(targetMM, "Model")) {
			public void bindContents(Bindings bindings, Model source, EObject target) {

				IBox<PackageableElement> source_packageElement = createPropertyBox(source, "packagedElement");
//				source_packageElement.inspect("source_packagedElement: ");
//				IUnaryFunction<PackageableElement, PackageableElement> id = getId();
//				source_packageElement = apply(source_packageElement, id);
//				IBox<EObject> target_classes = createPropertyBox(target, "classes");
//
//				inspectProperty(target_classes, "\ttarget classes: ", "name");

//				IBox<? extends EObject> source_classes = select(source_packageElement, UML.getClass_());
				IBox<Class> source_classes = select(source_packageElement, Class.class);
//				source_classes = (IBox<Class>)(IBox)source_packageElement;
//				source_classes = select(source_classes, "UMLRealTime::Capsule");
//				inspectProperty(source_classes, "is capsule: ", "name");
//				source_classes = select(source_classes, Class.class);

//				inspectProperty(source_classes, "source classes: ", "name");
//				path(source_classes, UML.getClass_(), "name").inspect("source class names: ");

//				path(source_classes, hasStereotypeAccessor("UMLRealTime::Capsule")).inspect("\tisCapsule: ");

//				IBox<EObject> transformedClasses = transform(source_classes);
//				inspectType(inspectProperty(transformedClasses, "transformed classes: ", "name"), "transformed class types: ");
				//target_classes.bind(transformedClasses);
				bindings.add(target, "classes", source_classes, UML.getClass_());

//				inspectProperty(path(source_classes, isCapsule).inspect("hasstereo: ").apply(getIf(findClass(targetMM, "Capsule"), findClass(targetMM, "PassiveClass"))), "types: ", "name");

/*
				self.packageElement->select(e |
					e.oclIsKindOf(Package)
				)->select(e |
					e.hasStereotype(ProtocolContainer)
				)->collect(e |
					e.packageElement
				)->select(e |
						e.oclIsKindOf(Collaboration)
				)->select(e |
					e.hasStereotype(Protocol)
				)
*/
				// TODO: closure of packagedElement to traverse all subPackages?
				bindings.add(target, "protocols",
//					.collect((IUnaryFunction)Utils.getId())	// used to transform OrderedSet into Sequence so that moves are correctly propagated
//					transform(	// this (and distinct below) is now done in Rule.Bindings
						inspectProperty(selectStereotype(select(
							selectStereotype(select(source_packageElement, Package.class), "UMLRealTime::ProtocolContainer")
								.asOption() // we only look at the first package with stereotype ProtocolContainer so that the following collectBox can be bidir TODO: document
								.collectMutable(EMFFactory.INSTANCE, UML.getPackage(), "packagedElement"),
							Collaboration.class
						), "UMLRealTime::Protocol"), "Protocols: ", "name")
//					).distinct()
				, UML.getCollaboration());
			}
		});

		addRule(new Rule<Class, EObject>(this, "Class2Capsule", UML.getClass_(), findClass(targetMM, "Capsule"), "UMLRealTime::Capsule") {
			public void bindContents(Bindings bindings, Class source, EObject target) {
				bindEntityContents(bindings, source, target);

				// target.parts <- source.ownedAttribute->select(e | e.hasStereotype(CapsulePart)
				bindings.add(target, "parts", source, "ownedAttribute",
					hasStereotypeAccessor("UMLRealTime::CapsulePart")
				);
				bindings.add(target, "ports", source, "ownedAttribute",
					hasStereotypeAccessor("UMLRealTime::RTPort")
				);
				bindings.add(target, "connectors", source, "ownedConnector",
					hasStereotypeAccessor("UMLRealTime::RTConnector")
				);
			}
		});

		addRule(new Rule<Property, EObject>(this, "Property2Part", UML.getProperty(), findClass(targetMM, "CapsulePart"), "UMLRealTime::CapsulePart") {
			@Override
			public void bindContents(Bindings bindings, Property source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				bindings.add("type", target, source);
			}
		});

		addRule(new Rule<Port, EObject>(this, "Port", UML.getPort(), findClass(targetMM, "Port"), "UMLRealTime::RTPort") {
			@Override
			public void bindContents(Bindings bindings, Port source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				bindings.add("type", target, source);

				bindings.add(target, "redefines", source, "redefinedPort");
			}
		});

		addRule(new Rule<Connector, EObject>(this, "Connector", UML.getConnector(), findClass(targetMM, "Connector"), "UMLRealTime::RTConnector") {
			@Override
			public void bindContents(Bindings bindings, Connector source, EObject target) {
				bindNamedElementContents(bindings, source, target);

//DONE: fixed bug in UMLRT metamodel: ends is not a container => ConnectorEnds are dangling
				bindings.add(target, "ends", source, "end");
			}
		});

		addRule(new Rule<ConnectorEnd, EObject>(this, "ConnectorEnd", UML.getConnectorEnd(), findClass(targetMM, "ConnectorEnd")) {	// TODO: owner.hasStereotype(RTConnector)
			@Override
			public void bindContents(Bindings bindings, ConnectorEnd source, EObject target) {
				bindings.add("role", target, source);
			}
		});

		addRule(new Rule<Property, EObject>(this, "Property2Attribute", UML.getProperty(), findClass(targetMM, "Attribute")) {
			@Override
			public void bindContents(Bindings bindings, Property source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				bindings.add("type", target, source);
				// TODO: if the type is not a DataType, then this is an error in the source model
//				, new IUnaryFunction<EObject, Boolean>() {
//					@Override
//					public Boolean apply(EObject a) {
//						return a instanceof DataType;
//					}
//				});
			}
		});

		addRule(new Rule<Collaboration, EObject>(this, "Collaboration2Protocol", UML.getCollaboration(), findClass(targetMM, "Protocol"), "UMLRealTime::Protocol") {
			@Override
			public void bindContents(Bindings bindings, Collaboration source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// TODO: but unused in our sample => obtain better sample (+ corresponding target)
//				bind(target, "inSignals", 
//				bind(target, "outSignals", 
//				bind(target, "inOutSignals", 
			}
		});

		addRule(new Rule<StateMachine, EObject>(this, "StateMachine", UML.getStateMachine(), findClass(targetMM, "StateMachine"), "UMLRealTimeStateMach::RTStateMachine") {
			@Override
			public void bindContents(Bindings bindings, StateMachine source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				EObject region = create(targetMM, "CompositeState");
				set(target, "top", region);

				IBox<Region> source_region = createPropertyBox(source, "region");

				// TODO: add asOpt before CollectBox so that it can work in reverse (but then can only transform a StateMachine if it has a single region)
				IBox<Vertex> source_vertices = source_region.collectMutable(EMFFactory.INSTANCE, UML.getRegion(), "subvertex");

//				bindings.add(region, "initial", select(select(source_vertices, Pseudostate.class), UML.getPseudostate(), "kind", PseudostateKind.INITIAL_LITERAL), UML.getPseudostate());
				bindings.add(region, "initial", select(select(source_vertices, Pseudostate.class), propertyEquals("kind", PseudostateKind.INITIAL_LITERAL)), UML.getPseudostate());

				// TODO: write corresponding rules
//				bindings.add(region, "deepHistory", select(select(source_vertices, Pseudostate.class), UML.getPseudostate(), "kind", PseudostateKind.DEEP_HISTORY_LITERAL), UML.getPseudostate());
//				bindings.add(region, "junctionPoints", select(select(source_vertices, Pseudostate.class), UML.getPseudostate(), "kind", PseudostateKind.JUNCTION_LITERAL), UML.getPseudostate());
//				bindings.add(region, "choicePoints", select(select(source_vertices, Pseudostate.class), UML.getPseudostate(), "kind", PseudostateKind.CHOICE_LITERAL), UML.getPseudostate());
				bindings.add(region, "substates", select(source_vertices, State.class), UML.getState());
				bindings.add(region, "transitions", source_region.collectMutable(EMFFactory.INSTANCE, UML.getRegion(), "transition"), UML.getTransition());
			}
		});
//*
		addRule(new Rule<Class, EObject>(this, "Class2Passive", UML.getClass_(), findClass(targetMM, "PassiveClass")
//					, NOT.compose(OR.compose(ISA(StateMachine.class), hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs())
					, NOT.compose(ComposableBinaryFunction.mergeInputs(OR.compose(UMLRTFacade.<Class>ISA(StateMachine.class), hasStereotypeAccessor("UMLRealTime::Capsule"))))
//					, ISA(StateMachine.class).or(hasStereotypeAccessor("UMLRealTime::Capsule")).not()
//					, hasStereotypeAccessor("UMLRealTime::Capsule").or(ISA(StateMachine.class)).not()
				) {
			public void bindContents(Bindings bindings, Class source, EObject target) {
				bindEntityContents(bindings, source, target);
			}
		});
/**/
		addRule(new Rule<State, EObject>(this, "State", UML.getState(), findClass(targetMM, "State"), "UMLRealTimeStateMach::RTState") {
			@Override
			public void bindContents(Bindings bindings, State source, EObject target) {
				bindNamedElementContents(bindings, source, target);
			}
		});
//*
		addRule(new Rule<Pseudostate, EObject>(this, "Initial", UML.getPseudostate(), findClass(targetMM, "InitialPoint"), "UMLRealTimeStateMach::RTPseudostate", propertyEquals("kind", PseudostateKind.INITIAL_LITERAL)) {
			@Override
			public void bindContents(Bindings bindings, Pseudostate source, EObject target) {
				bindNamedElementContents(bindings, source, target);
			}
		});
/**/
		addRule(new Rule<Transition, EObject>(this, "Transition", UML.getTransition(), findClass(targetMM, "Transition")) {
			@Override
			public void bindContents(Bindings bindings, Transition source, EObject target) {
				bindNamedElementContents(bindings, source, target);
				bindings.add(target, "sourceVertex", source, "source");
				bindings.add(target, "targetVertex", source, "target");

				bindings.add(target, "redefines", source, "redefinedTransition");
			}
		});

		addRule(new Rule<Operation, EObject>(this, "Operation", UML.getOperation(), findClass(targetMM, "Operation")) {
			@Override
			public void bindContents(Bindings bindings, Operation source, EObject target) {
				bindNamedElementContents(bindings, source, target);
			}
		});

//		addRule(new Rule(DataType, ))

		final Model sourceModel = (Model) source.getContents().get(0);
		tempModel = create(sourceModel.eClass());
		for(Profile profile : sourceModel.getAllAppliedProfiles()) {
			tempModel.applyProfile(profile);
		}

		// TODO: would need the same on the source Resource if there were multiple possible rules for a given target element
		// TODO: optimize: elements that are temporarily removed, then readded undergo bindings.disable then bindings.enable...
		//      this would not be an issue if there were not so many temporarily removed elements (mostly because of the way zip works?)
		// TODO: while working on this, it appeared that all elements, even the Model, get removed then added back !!!! this is too much
		//      "temporary" changes

		// This approach is not possible because contents binding happens before the element is added to the model
		// therefore, the solution must be to disable before that.... or maybe we could combine with this approach, but make an initial
		// disable of siblings when creating a new sibling..... in Facade.fowardAPply?
		Adapter bindingDisabler = new EContentAdapter() {
			@Override
			protected void addAdapter(Notifier notifier) {
				super.addAdapter(notifier);
				if(notifier instanceof EObject) {
					Bindings bindings = UMLRTFacade.this.bindingsByTransformedElement.get(notifier);
					if(bindings != null) {
						if(siblings.get(notifier).size() > 1) {
							for(EObject sibling : siblings.get(notifier)) {
								if(sibling != notifier) {
									System.out.println("Disabling bindings for " + Inspect.toString(sibling, null));
									Bindings siblingBindings = UMLRTFacade.this.bindingsByTransformedElement.get(sibling);
									siblingBindings.disable();
								}
							}
							System.out.println("Enabling bindings for " + Inspect.toString(notifier, null));
							bindings.enable();
						}
//						if(((EObject)notifier).eClass().getName().equals("Model")) {
//							System.out.println("Model enabled!");
//						}
					}
				}
			}

//			@Override
//			protected void removeAdapter(Notifier notifier) {
//				super.removeAdapter(notifier);
//				if(notifier instanceof EObject) {
//					Bindings bindings = UMLRTFacade.this.bindingsByTransformedElement.get(notifier);
//					if(bindings != null) {
//						if(siblings.get(notifier).size() > 1) {
//							System.out.println("Disabling bindings for " + Inspect.toString(notifier, null));
//							bindings.disable();
//						}
////						if(((EObject)notifier).eClass().getName().equals("Model")) {
////							System.out.println("Model disabled!");
////						}
//					}
//				}
////				Facade.forwardApply
//			}
		};

		// TODO: listen to Resource.getContents?
		EObject targetModel = transform(createOne(sourceModel), UML.getModel()).get(0);
		target.getResource().getContents().add(targetModel);
//		target.getResource()
		targetModel
		.eAdapters().add(bindingDisabler);
	}

	private Model tempModel;

//	private void bindRedefinableElementContents(EObject source, EObject target) {
//		bindNamedElementContents(source, target);
//	}

	private void bindNamedElementContents(Bindings bindings, EObject source, EObject target) {
		// source -> target
//		createPropertyBox(target, "name").bind(createPropertyBox(source, "name"));
		bindings.add("name", target, source);
		// target -> source
//		createPropertyBox(source, "name").bind(createPropertyBox(target, "name"));
//		bind("name", source, target);
	}

	// Now in Rule.Bindings
//	private void bind(String propertyName, EObject target, EObject source, IUnaryFunction<EObject, Boolean> passiveFilter) {
//		bind(target, propertyName, source, propertyName, passiveFilter);
//	}
//
//	private void bibind(String propertyName, EObject target, EObject source) {
//		bind(propertyName, target, source, null);
////		bind(propertyName, source, target, null);
//	}
//
//	private void bind(String propertyName, EObject target, EObject source) {
//		bind(propertyName, target, source, null);
//	}
//
//	private void bind(EObject target, String targetPropertyName, IBox source) {
//		IBox tgt = createPropertyBox(target, targetPropertyName);
//		IBox src = transform(source);
//		src = src.asBox(tgt);
//		tgt.bind(src);
//	}
//
//	private void bibind(EObject target, String targetPropertyName, IBox source) {
//		IBox targetBox = createPropertyBox(target, targetPropertyName);
//		IBox sourceBox = transform(source);
////		System.out.println("source: " + sourceBox);
////		System.out.println("target: " + targetBox);
//		targetBox.bind(sourceBox);
////		System.out.println("source: " + sourceBox);
////		System.out.println("target: " + targetBox);
////		sourceBox.bind(targetBox);
////		System.out.println("source: " + sourceBox);
////		System.out.println("target: " + targetBox);
//	}
//
//	private void bind(EObject target, String targetPropertyName, EObject source, String sourcePropertyName) {
//		bind(target, targetPropertyName, source, sourcePropertyName, null);
//	}
//
//	private void bind(EObject target, String targetPropertyName, EObject source, String sourcePropertyName, IUnaryFunction<EObject, Boolean> passiveFilter) {
//		IBox<EObject> src = createPropertyBox(source, sourcePropertyName);
//		if(passiveFilter != null) {
//			src = src.select(passiveFilter);
//		}
//		if(source.eClass().getEStructuralFeature(sourcePropertyName) instanceof EReference) {
//			src = transform(src);
//		}
//		IBox<EObject> tgt = createEPropertyBox(target, targetPropertyName);
//		src = src.asBox(tgt);
//		tgt.bind(src);
//	}
//
//	private <A> void bindWithActiveFilter(EObject target, String targetPropertyName, EObject source, String sourcePropertyName, IUnaryFunction<?, IOne<Boolean>> activeFilter) {
//		IBox<EObject> src = createPropertyBox(source, sourcePropertyName);
////inspectProperty(src, "src1: ", "name", true);
//		if(activeFilter != null) {
//			src = src.select(path(src, (IUnaryFunction<EObject, IOne<Boolean>>)activeFilter)).inspect(sourcePropertyName + ": ");
////inspectProperty(src, "src2: ", "name", true);
//		}
//		if(source.eClass().getEStructuralFeature(sourcePropertyName) instanceof EReference) {
//			src = transform(src);
////inspectProperty(src, "src3: ", "name", true);
//		}
//		IBox<EObject> tgt = createPropertyBox(target, targetPropertyName);
//		src = src.asBox(tgt);
//		tgt.bind(src);
//	}

	private void bindEntityContents(Bindings bindings, EObject source, EObject target) {
		bindNamedElementContents(bindings, source, target);

//		createPropertyBox(target, "attributes").bind(transform(createPropertyBox(source, "ownedAttribute")));
//		get(target, "attributes").bind(select(path((IBox<Element>)createPropertyBox(source, "ownedAttribute"), hasStereotypeAccessor("UMLRealTime::Part"))));
		bindings.add(target, "attributes", source, "ownedAttribute", (IUnaryFunction)ISA(Port.class).or(hasStereotypeAccessor("UMLRealTime::CapsulePart")).not());
//			not part and not port

//		operations
//		bind(target, "operations", source, "ownedOperation");
		// TODO: concurrent modification issue when switching a Class from PassiveClass to Capsule because the very same target Operation
		// is moved into a different container, and therefore removed from the first, which is still bound by a bidir binding to the source, which
		// gets modified... => we should disable bindings of disabled classes...

//		behavior
		// TODO: belongs in bindEntityContents, but until rule disabling works, must be moved to Class2Capsule for tests to pass
		bindings.add(target, "behaviour", source, "classifierBehavior", hasStereotypeAccessor("UMLRealTimeStateMach::RTStateMachine"));
	}

//	public static IBox<Boolean> and(IBox<Boolean> a, IBox<Boolean> b) {
//		return zipWith(a, b, new IBinaryFunction<Boolean, Boolean, Boolean>() {
//			@Override
//			public Boolean apply(Boolean a, Boolean b) {
//				return a && b;
//			}
//		});
//	}
//
//	public static IBox<Boolean> not(IBox<Boolean> a) {
//		return a.collect(new IUnaryFunction<Boolean, Boolean>() {
//			@Override
//			public Boolean apply(Boolean a) {
//				return !a;
//			}
//		});
//	}

	// Global TODO for what comes below: add cache where appropriate (possibly nowhere, but probably at least in propertyEquals)

	private static final boolean useWeakCache = true;

	// interface in Java 8
	public static abstract class ComposableBinaryFunction<A, B, C> implements IBinaryFunction<A, B, C> {
		// default method in Java 8
		public <V1, V2> ComposableBinaryFunction<V1, V2, C> compose(final IUnaryFunction<? super V1, ? extends A> beforeFirst, final IUnaryFunction<? super V2, ? extends B> beforeSecond) {
			return new ComposableBinaryFunction<V1, V2, C>() {
				@Override
				public C apply(V1 a, V2 b) {
					return ComposableBinaryFunction.this.apply(beforeFirst.apply(a), beforeSecond.apply(b));
				}
			};
		}

		// default method in Java 8
		public <V> ComposableUnaryFunction<V, C> composeWithMergedInputs(final IUnaryFunction<V, A> beforeFirst, final IUnaryFunction<V, B> beforeSecond) {
			return mergeInputs(this.compose(beforeFirst, beforeSecond));
		}

		// only to be called if A=B
		// default method in Java 8
		// unsafe, prefer the static version
		public <D extends A> ComposableUnaryFunction<D, C> mergeInputs() {
/*
			return new ComposableUnaryFunction<D, C>() {
				@Override
				public C apply(D a) {
					return ComposableBinaryFunction.this.apply((A)a, (B)a);
				}
			};
/*/
			return mergeInputs((ComposableBinaryFunction<? super D, ? super D, C>)this);
/**/
		}

		public static <D, C> ComposableUnaryFunction<D, C> mergeInputs(final ComposableBinaryFunction<? super D, ? super D, C> f) {
			return new ComposableUnaryFunction<D, C>() {
				@Override
				public C apply(D a) {
					return f.apply(a, a);
				}
			};
		}

		public ComposableBinaryFunction<IBox<A>, IBox<B>, IBox<C>> lift() {
			final ComposableBinaryFunction<A, B, C> self = this;
			return new ComposableBinaryFunction<IBox<A>, IBox<B>, IBox<C>>() {
				private IBinaryCache<IBox<A>, IBox<B>, IBox<C>> cache =
					useWeakCache ?
						new WeakKeysWeakValuesBinaryCache<IBox<A>, IBox<B>, IBox<C>>()
					:
						new HashBinaryCache<IBox<A>, IBox<B>, IBox<C>>()
				;
				@Override
				public IBox<C> apply(IBox<A> a, IBox<B> b) {
					IBox<C> ret = cache.get(a, b);
					if(ret == null) {
						ret = (IBox<C>)a.zipWith(b, self);
						cache.put(a, b, ret);
					}
					return ret;
				}
			};
		}

		public ComposableBinaryFunction<IOne<A>, IOne<B>, IOne<C>> oldLift() {
			return new ComposableBinaryFunction<IOne<A>, IOne<B>, IOne<C>>() {
/*
				// This version lets some entries disappear because the Pair is collectable
				private Map<IPair<IOne<A>, IOne<B>>, WeakReference<IOne<C>>> cache = new WeakHashMap<IPair<IOne<A>,IOne<B>>, WeakReference<IOne<C>>>();

				private IOne<C> getCached(IOne<A> a, IOne<B> b) {
					IPair<IOne<A>, IOne<B>> input = AOFFactory.INSTANCE.createPair(a,  b);
					WeakReference<IOne<C>> r = cache.get(input);
					IOne<C> ret = r == null ? null : r.get();
					return ret;
				}

				private void putCache(IOne<A> a, IOne<B> b, IOne<C> c) {
					cache.put(AOFFactory.INSTANCE.createPair(a, b), new WeakReference(c));
				}
/*/
				private Map<IOne<A>, Map<IOne<B>, WeakReference<IOne<C>>>> cache = new WeakHashMap<IOne<A>, Map<IOne<B>,WeakReference<IOne<C>>>>();

				private IOne<C> getCached(IOne<A> a, IOne<B> b) {
					Map<IOne<B>, WeakReference<IOne<C>>> cache2 = cache.get(a);
					if(cache2 == null) {
						return null;
					} else {
						WeakReference<IOne<C>> ret = cache2.get(b);
						if(ret == null) {
							return null;
						} else {
							return ret.get();
						}
					}
				}

				private void putCache(IOne<A> a, IOne<B> b, IOne<C> c) {
					Map<IOne<B>, WeakReference<IOne<C>>> cache2 = cache.get(a);
					if(cache2 == null) {
						cache2 = new WeakHashMap<IOne<B>, WeakReference<IOne<C>>>();
						cache.put(a,  cache2);
					}
					cache2.put(b, new WeakReference<IOne<C>>(c));
				}
/**/
				@Override
				public IOne<C> apply(final IOne<A> a, final IOne<B> b) {
					IOne<C> ret = getCached(a, b);
					if(ret == null) {
						ret = AOFFactory.INSTANCE.createOne(ComposableBinaryFunction.this.apply(a.get(0), b.get(0)));
						final IOne<C> ret_ = ret;
						IObserver observer = new SingletonObserver() {
	//						@Override
	//						public void created(Iterable elements) {
	//							ret.add(0, ComposableBinaryFunction.this.apply(a.get(0), b.get(0)));
	//						}
	
							@Override
							public void added(int index, Object element) {
								ret_.add(index, ComposableBinaryFunction.this.apply(a.get(index), b.get(index)));
							}
	
							@Override
							public void replaced(int index, Object oldElement,
									Object newElement) {
								ret_.set(index, ComposableBinaryFunction.this.apply(a.get(index), b.get(index)));
							}
						};
						a.addObserver(observer);
						b.addObserver(observer);
						putCache(a, b, ret);
					}
					return ret;
				}
			};
		}
	}

	// This version lets some entries disappear because the Pair is collectable before either key is
	public static class FaultyBinaryCache<K1, K2, V> implements IBinaryCache<K1, K2, V> {
		private Map<IPair<K1, K2>, WeakReference<V>> cache = new WeakHashMap<IPair<K1,K2>, WeakReference<V>>();

		public V get(K1 key1, K2 key2) {
			WeakReference<V> ret = cache.get(AOFFactory.INSTANCE.createPair(key1, key2));
			return ret == null ? null : ret.get();
		}

		public void put(K1 key1, K2 key2, V value) {
			cache.put(AOFFactory.INSTANCE.createPair(key1, key2), new WeakReference<V>(value));
		}
	}

	// interface in Java 8
	public static abstract class ComposableUnaryFunction<A, B> implements IUnaryFunction<A, B> {
		// default method in Java 8
		public <V> ComposableUnaryFunction<V, B> compose(final IUnaryFunction<V, A> before) {
			return new ComposableUnaryFunction<V, B>() {
				@Override
				public B apply(V a) {
					return ComposableUnaryFunction.this.apply(before.apply(a));
				}
			};
		}

		public <V1, V2> ComposableBinaryFunction<V1, V2, B> compose(final IBinaryFunction<V1, V2, A> before) {
			return new ComposableBinaryFunction<V1, V2, B>() {
				@Override
				public B apply(V1 a, V2 b) {
					return ComposableUnaryFunction.this.apply(before.apply(a, b));
				}
			};
		}

		public ComposableUnaryFunction<A, IBox<B>> boxOutput() {
/* With Java 8:
					return AOFFactory.INSTANCE::createOne;
//OR
//					return (a) -> AOFFactory.INSTANCE.createOne(a);
/*/
			return new ComposableUnaryFunction<A, IBox<B>>() {

				// TODO: use my Cache? apparently no need for WeakReference
				private Map<A, IOne<B>> cache = new WeakHashMap<A, IOne<B>>();

				@Override
				public IOne<B> apply(A a) {
					IOne<B> ret = cache.get(a);
					if(ret == null) {
						ret = AOFFactory.INSTANCE.createOne(ComposableUnaryFunction.this.apply(a));
						ret.addObserver(new Utils.TaggingObserver(ComposableUnaryFunction.this + "(" + Inspect.toString(a, null) + ").boxOutput"));
						cache.put(a,  ret);
					}
					return ret;
				}
			};
/**/
		}

		public ComposableUnaryFunction<IBox<A>, IBox<B>> lift() {
			final ComposableUnaryFunction<A, B> self = this;
			return new ComposableUnaryFunction<IBox<A>, IBox<B>>() {
				private IUnaryCache<IBox<A>, IBox<B>> cache =
					useWeakCache ?
						new WeakKeysWeakValuesUnaryCache<IBox<A>, IBox<B>>()
					:
						new HashUnaryCache<IBox<A>, IBox<B>>()
				;
				@Override
				public IBox<B> apply(IBox<A> a) {
					IBox<B> ret = cache.get(a);
					if(ret == null) {
						ret = (IOne<B>)a.collect(self);
						cache.put(a,  ret);
					}
					return ret;
				}
			};
		}

/*
		public ComposableUnaryFunction<IOne<A>, IOne<B>> lift() {
			return new ComposableUnaryFunction<IOne<A>, IOne<B>>() {
				private Map<IOne<A>, WeakReference<IOne<B>>> cache = new WeakHashMap<IOne<A>, WeakReference<IOne<B>>>();

				@Override
				public IOne<B> apply(final IOne<A> a) {
					WeakReference<IOne<B>> r = cache.get(a);
					IOne<B> ret = r == null ? null : r.get();
					if(ret == null) {
						ret = AOFFactory.INSTANCE.createOne(ComposableUnaryFunction.this.apply(a.get(0)));
						final IOne<B> ret_ = ret;
						IObserver<A> observer = new SingletonObserver<A>() {
		//						@Override
		//						public void created(Iterable<A> elements) {
		////							Iterator<A> i = elements.iterator();
		////							ret.add(0, ComposableUnaryFunction.this.apply(i.next()));
		////							if(i.hasNext()) {
		////								throw new IllegalStateException();
		////							}
		//							ret.add(0, ComposableUnaryFunction.this.apply(a.get(0)));
		//						}
		
							@Override
							public void added(int index, A element) {
								ret_.add(index, ComposableUnaryFunction.this.apply(a.get(index)));
							}
		
							@Override
							public void replaced(int index, A oldElement,
									A newElement) {
								ret_.replace(index, ComposableUnaryFunction.this.apply(a.get(index)));
							}
						};
						a.addObserver(observer);
						cache.put(a,  new WeakReference(ret));
					}
					return ret;
				}
			};
		}
*/
/*
		public ComposableUnaryFunction<IBox<A>, IBox<B>> lift(final IConstraints constraints) {
			return new ComposableUnaryFunction<IBox<A>, IBox<B>>() {
				private Map<IBox<A>, WeakReference<IBox<B>>> cache = new WeakHashMap<IBox<A>, WeakReference<IBox<B>>>();

				@Override
				public IBox<B> apply(final IBox<A> a) {
					WeakReference<IBox<B>> r = cache.get(a);
					IBox<B> ret = r == null ? null : r.get();
					if(ret == null) {
						ret = AOFFactory.INSTANCE.createBox(constraints, ComposableUnaryFunction.this.apply(a.get(0)));
						final IBox<B> ret_ = ret;
						IObserver<A> observer = new SingletonObserver<A>() {
		//						@Override
		//						public void created(Iterable<A> elements) {
		////							Iterator<A> i = elements.iterator();
		////							ret.add(0, ComposableUnaryFunction.this.apply(i.next()));
		////							if(i.hasNext()) {
		////								throw new IllegalStateException();
		////							}
		//							ret.add(0, ComposableUnaryFunction.this.apply(a.get(0)));
		//						}
		
							@Override
							public void added(int index, A element) {
								ret_.add(index, ComposableUnaryFunction.this.apply(a.get(index)));
							}
		
							@Override
							public void replaced(int index, A oldElement,
									A newElement) {
								ret_.replace(index, ComposableUnaryFunction.this.apply(a.get(index)));
							}
						};
						a.addObserver(observer);
						cache.put(a,  new WeakReference(ret));
					}
					return ret;
				}
			};
		}
*/
	}

	public static <A> Predicate<A> isA(final java.lang.Class<? extends A> c) {
/* With Java 8:
		return c::isInstance;
/*/
		return new Predicate<A>() {
			@Override
			public Boolean apply(A o) {
					return c.isInstance(o);
			}

			public String toString() {
				return "isA_" + c.getSimpleName();
			}
		};
/**/
	}

	public Predicate<EObject> isA(final EClass c) {
/* With Java 8:
		return c::isInstance;
/*/
		return new Predicate<EObject>() {
			@Override
			public Boolean apply(EObject o) {
					return c.isInstance(o);
			}
		};
/**/
	}

	public static abstract class SingletonObserver<A> extends DefaultObserver<A> {
		@Override
		public void removed(int index, Object element) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void moved(int newIndex, int oldIndex, Object element) {
			throw new UnsupportedOperationException();
		}
	}


	public static <A> MutablePredicate<A> ISA(final java.lang.Class<? extends A> c) {
/* With Java 8:
		return TODO;
/*/
		Predicate<A> f = isA(c);
		return asMutablePredicate(f.boxOutput());
/**/
	}

	public static <A> ComposableUnaryFunction<A, Boolean> valueEquals(final A other) {
		return new ComposableUnaryFunction<A, Boolean>() {
			@Override
			public Boolean apply(A a) {
				if(other == null) {
					return a == null;
				} else {
					return other.equals(a);
				}
			}
		};
	}

//	private static final IOne<Boolean> FALSE = AOFFactory.INSTANCE.createOne(false);

	public static ComposableUnaryFunction<EObject, IBox<Boolean>> propertyEquals(final String propertyName, final Object value) {
/* With Java 8:
		return TODO;
/*/
		// TODO: document that (&check that it is currently true) all functions used in collectBox must have a cache (so that the same box, with its registered observers, is returned each time)
		// => i.e., make sure it is document in AOF
		// alternatively CollectBox should probably cache the result of its collector
		return new ComposableUnaryFunction<EObject, IBox<Boolean>>() {
//			protected final IOne<Boolean> FALSE = AOFFactory.INSTANCE.createOne(false);
			private IUnaryCache<EObject, IOne<Boolean>> cache = new WeakKeysWeakValuesUnaryCache<EObject, IOne<Boolean>>();
			@Override
			public IOne<Boolean> apply(EObject a) {
				if(a == null) {
					return FALSE;
				} else {
					IOne<Boolean> ret = cache.get(a);
					if(ret == null) {
							ret = (IOne<Boolean>)createPropertyBox(a, propertyName).collect(valueEquals(value));
							cache.put(a, ret);
					}
					return ret;
				}
			}
		};
/**/
	}

//	private static ComposableUnaryFunction<StateMachine, IOne<Boolean>> IS_A_STATE_MACHINE = isA(StateMachine.class).boxOutput();
/*
	static {
		// test
		not.compose(or.compose(isA(StateMachine.class), isA(Class.class)).mergeInputs()).boxOutput();
		not.lift().compose(or.lift().compose(isA(StateMachine.class).boxOutput(), hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs());

//		And now, we try to do:
//		(x) -> !((x instanceof StateMachine) || x.hasStereotype("UMLRealTime::Capsule"))

//		not.compose(or.compose(isA(StateMachine.class), hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs()).boxOutput();
		NOT.compose(OR.compose(IS_A_STATE_MACHINE, hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs());
		NOT.compose(OR.compose(ISA(StateMachine.class), hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs());

		ISA(StateMachine.class).or(hasStereotypeAccessor("UMLRealTime::Capsule")).not();

		MutablePredicate<Element> isasm = ISA(StateMachine.class);
		hasStereotypeAccessor("").or(isasm);

//		hasStereotypeAccessor("").or(ISA(StateMachine.class));
	}
*/
//	public ComposableUnaryFunction<Object, IOne<Boolean>> getPassiveClassPredicate() {
//		return NOT.compose(OR.compose(ISA(StateMachine.class), hasStereotypeAccessor("UMLRealTime::Capsule")).mergeInputs());
//	}




/*
	public static abstract class Guard<A> extends ComposableUnaryFunction<A, Boolean> {
		public Guard<A> not() {
			return not.compose(this);
		}

		public <B> IBinaryFunction<A, B, Boolean> and(Guard<B> f) {
			return and.compose(this, f);
		}

		public <B> IBinaryFunction<A, B, Boolean> or(Guard<B> f) {
			return or.compose(this, f);
		}
	}

	static {
		Guard<EObject> isAStateMachine = null;
		Guard<EObject> isCapsule = null;

		isAStateMachine.or(isCapsule).mergeInputs().not();
	}
*/
}
