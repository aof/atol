package org.eclipse.papyrus.facade.exploration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;

public class ModelFuzzer {

	private Resource modelToFuzz;
	private Random random;

	public ModelFuzzer(Resource modelToFuzz, long seed) {
		this.modelToFuzz = modelToFuzz;
		this.random = new Random(seed);
	}

	public static enum ManyReferenceChangeKind {
		ADD_EXISTING, ADD_NEW, REMOVE, MOVE, CLEAR;
	}
/*
	public static enum SingleReferenceChangeKind {
		SET_EXISTING, SET_NEW, SET_NULL, UNSET;
	}

	public static enum ManyAttributeChangeKind {
		ADD, REMOVE, MOVE, CLEAR;
	}

	public static enum SingleAttributeChangeKind {
		SET, SET_NULL, UNSET;
	}
*/
	public static enum ManyChangeKind {
		ADD, REMOVE, MOVE,
		SET // REPLACE
		// 2 still unsupported by AOF:
		// 	ADD_MANY
		// 	CLEAR	
		;
	}

	public static enum SingleChangeKind {
		SET, SET_NULL, UNSET;
	}

//	public static class Change {
//		public EStructuralFeature feature;
//		public ChangeKind kind;
//	}
//
//	private List<Change> possibleChanges(EClass eClass) {
//		return null;
//	}

	private Object getPossibleValue(EStructuralFeature feature, List<EObject> allContents) {
		Object ret = null;
		if(feature instanceof EAttribute) {
			EClassifier type = feature.getEType();
			Class<?> instanceClass = type.getInstanceClass();
			if(type instanceof EEnum) {
				List<EEnumLiteral> literals = ((EEnum)type).getELiterals();
				ret = literals.get(random.nextInt(literals.size())).getInstance();
			} else if(instanceClass.equals(Integer.class) || instanceClass.equals(Integer.TYPE)) {
				ret = random.nextInt();
			} else if(instanceClass.equals(String.class)) {
				ret = "" + random.nextInt();
			} else if(instanceClass.equals(Double.class) || instanceClass.equals(Double.TYPE)) {
				ret = random.nextDouble();
			} else if(instanceClass.equals(Boolean.class) || instanceClass.equals(Boolean.TYPE)) {
				ret = random.nextBoolean();
			} else {
				throw new UnsupportedOperationException("Cannot get a value of type " + type.getInstanceClass());
			}
		} else {
			if(random.nextBoolean()) {
				// add existing
				List<EObject> candidates = new ArrayList<EObject>();
				for(EObject o : allContents) {
					if(feature.getEType().isInstance(o)) {
						candidates.add(o);
					}
				}
				if(candidates.size() > 0) {
					ret = candidates.get(random.nextInt(candidates.size()));
				} else {
					ret = null;
				}
			} else {
				// add new
				EClass eClass = (EClass)feature.getEType();
				// TODO: any non abstract subtype
				List<EClass> subtypes = getSubtypes(eClass);
				eClass = subtypes.get(random.nextInt(subtypes.size()));
				ret = eClass.getEPackage().getEFactoryInstance().create(eClass);
			}
		}
		return ret;
	}

	private Map<EClass, List<EClass>> subtypesByEClass = new HashMap<EClass, List<EClass>>();

	// & self
	private List<EClass> getSubtypes(EClass eClass) {
		List<EClass> ret = subtypesByEClass.get(eClass);
		if(ret == null) {
			ret = new ArrayList<EClass>();
			for(EClassifier e : eClass.getEPackage().getEClassifiers()) {
				if(e instanceof EClass) {
					EClass ec = (EClass)e;
					if(ec.getEAllSuperTypes().contains(eClass) && !ec.isAbstract()) {
						ret.add(ec);
					}
				}
			}
			if(!eClass.isAbstract()) {
				ret.add(eClass);
			}
			subtypesByEClass.put(eClass, ret);
		}
		return ret;
	}

	private Map<EClass, List<EStructuralFeature>> featuresByEClass = new HashMap<EClass, List<EStructuralFeature>>();
	private Collection<String> excludedFeatures = Arrays.asList(
			"templateParameter", "ownedTemplateSignature",	"owningTemplateParameter", // would require extra constraints + unused in case study
			"annotatedElement"	// behaves like containment|container, but is not one


			// UMLRT features that are not transformed
			, "owner", "conjugate", "multiplicity"
	);

	private List<EStructuralFeature> getFeatures(EClass eClass) {
		List<EStructuralFeature> ret = featuresByEClass.get(eClass);
		if(ret == null) {
			ret = new ArrayList<EStructuralFeature>();
			for(EStructuralFeature feature : eClass.getEAllStructuralFeatures()) {
				if(feature.isChangeable() && !feature.isDerived() && !excludedFeatures.contains(feature.getName())) {
					ret.add(feature);
				}
			}
		}
		return ret;
	}

	// and self
	private List<EObject> allContainers(EObject eo) {
		List<EObject> ret = new ArrayList<EObject>();
		ret.add(eo);
		while(eo.eContainer() != null) {
			ret.add(eo.eContainer());
			eo = eo.eContainer();
		}
		return ret;
	}

	// containment in current model, or in targetmodel... TODO: move this out of fuzzer
	private boolean isContainment(EReference ref) {
		return ref.isContainment() || "classifierBehavior".equals(ref.getName());
	}

	private boolean wouldAddContainmentCycle(EObject eo, EStructuralFeature feature, Object newValue_) {
		if(feature instanceof EReference) {
			EObject newValue = (EObject)newValue_;
			EReference ref = (EReference)feature;
			if(isContainment(ref) && allContainers(eo).contains(newValue)) {
				return true;
			}
			if(ref.isContainer() && newValue == eo) {
				return true;
			}
		}
		return false;
	}

	public void performOneChange() {
		List<EObject> allContents = new ArrayList<EObject>();
		for(Iterator<EObject> i = modelToFuzz.getAllContents() ; i.hasNext() ; ) {
			EObject eo = i.next();
			if(getFeatures(eo.eClass()).size() > 0) {
				allContents.add(eo);
			}
		}
		int index = random.nextInt(allContents.size());
		EObject eo = allContents.get(index);
		List<EStructuralFeature> features = getFeatures(eo.eClass());
		EStructuralFeature feature = features.get(random.nextInt(features.size()));
		if(feature.isMany()) {
			EList<Object> value = (EList<Object>)eo.eGet(feature);
			int n = value.size();
			ManyChangeKind kind = ManyChangeKind.values()[random.nextInt(ManyChangeKind.values().length)];
			trace(eo, feature, kind, value);
			switch(kind) {
			case ADD:
				Object newValue = getPossibleValue(feature, allContents);
				System.out.println("\t\tnewValue = " + Inspect.toString(newValue, null));
				if(eo == newValue) {
					System.out.println("debug");
				}
				if(newValue == null) {
					System.out.println("\t\tINVALID (because no possible value):");
				} else if(feature.isUnique() && value.contains(newValue)) {
					System.out.println("\t\tINVALID (because already in set):");
				} else if(wouldAddContainmentCycle(eo, feature, newValue)) {
					System.out.println("\t\tINVALID (because would add containment cycle):");
				} else {
					value.add(random.nextInt(n + 1), newValue);
				}
				break;
			case REMOVE:
				if(n > 0) {
					value.remove(random.nextInt(n));
				} else {
					value.clear();
					System.out.println("\t\tCONVERTED to clear (because of empty collection):");
				}
				break;
//			case CLEAR:
//				value.clear();
//				break;
			case MOVE:
				if(n > 0) {
					value.move(random.nextInt(n), random.nextInt(n));
				} else {
					System.out.println("\t\tINVALID (because of empty collection):");
				}
				break;
			case SET:
				newValue = getPossibleValue(feature, allContents);
				System.out.println("\t\tnewValue = " + Inspect.toString(newValue, null));
				if(eo == newValue) {
					System.out.println("debug");
				}
				if(newValue == null) {
					System.out.println("\t\tINVALID (because no possible value):");
				} else if(feature.isUnique() && value.contains(newValue)) {
					System.out.println("\t\tINVALID (because already in set):");	// TODO: unless we replace that one
				} else if(wouldAddContainmentCycle(eo, feature, newValue)) {
					System.out.println("\t\tINVALID (because would add containment cycle):");
				} else if(value.size() == 0) {
					System.out.println("\t\tINVALID (because empty collection => nothing to replace):");
				} else {
					value.set(random.nextInt(n), newValue);
				}
				break;
			default:
				throw new UnsupportedOperationException("TODO: add support for " + kind);
			}
		} else {
			SingleChangeKind kind = SingleChangeKind.values()[random.nextInt(SingleChangeKind.values().length)];
			trace(eo, feature, kind, eo.eGet(feature));
			switch(kind) {
			case SET:
				Object newValue = getPossibleValue(feature, allContents);
				System.out.println("\t\tnewValue = " + Inspect.toString(newValue, null));
				if(wouldAddContainmentCycle(eo, feature, newValue)) {
					System.out.println("\t\tINVALID (because would add containment cycle):");
				} else {
					if(feature.getName().startsWith("base_")) {
						// TODO: handle this more generically
						// stereotypes must be unset then set because of the way PapyrusStereotypeListener works
						// (i.e., it does not "notify the old value")
						eo.eUnset(feature);
					}
					eo.eSet(feature, newValue);
				}
				break;
			case SET_NULL:
				Class<?> instanceClass = feature.getEType().getInstanceClass();
				if(instanceClass != null && instanceClass.isPrimitive()) {
					eo.eUnset(feature);
					System.out.println("\t\tCONVERTED to UNSET (because of primitive type):");
				} else {
					eo.eSet(feature, null);
				}
				break;
			case UNSET:
				eo.eUnset(feature);
				break;
			default:
				throw new UnsupportedOperationException("TODO: add support for " + kind);
			}
		}
	}

	private void trace(EObject eo, EStructuralFeature feature, Enum kind, Object value) {
//		if(value instanceof Collection) {
//			System.out.println(value.getClass());
//		}
		System.out.println("\t" + Inspect.toString(eo, null) + "." + feature.getName() + ": " + kind + " (oldValue: " + Inspect.toString(value, null) + ")");
	}
}
