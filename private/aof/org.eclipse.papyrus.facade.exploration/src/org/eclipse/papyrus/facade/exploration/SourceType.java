package org.eclipse.papyrus.facade.exploration;

import org.eclipse.emf.ecore.EObject;

// would only be useful for unidirectional transformations (possibly with bidirectional bindings)
public interface SourceType {

	boolean isInstance(EObject element);
}
