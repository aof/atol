package org.eclipse.papyrus.facade.exploration;

import static org.eclipse.papyrus.facade.exploration.Utils.inspectProperty;
import static org.eclipse.papyrus.facade.exploration.Utils.path;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBinaryFunction;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IObserver;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IPair;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.ListDelegate;
import org.eclipse.papyrus.aof.core.impl.One;
import org.eclipse.papyrus.aof.core.impl.operation.CollectBox;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.papyrus.facade.exploration.Rule.Bindings;
import org.eclipse.papyrus.facade.exploration.UMLRTFacade.ComposableBinaryFunction;
import org.eclipse.papyrus.facade.exploration.UMLRTFacade.ComposableUnaryFunction;
import org.eclipse.papyrus.facade.exploration.Utils.TaggingObserver;
import org.eclipse.papyrus.uml.tools.listeners.PapyrusStereotypeListener;
import org.eclipse.uml2.uml.Element;

public abstract class Facade {

	protected Resource source;
	protected FacadeModel target;
	private List<Rule<EObject, EObject>> rules = new ArrayList<Rule<EObject, EObject>>();

	public void addSource(Resource resource) {
		this.source = resource;
	}

	public void addTarget(FacadeModel facadeModel) {
		this.target = facadeModel;
	}

	public abstract void init();

	public abstract void applyStereotype(Element e, String stereotypeName);

/*
	protected Rule<EObject, EObject> getRuleFor(EObject source) {
		for(Map.Entry<Class, Rule<EObject, EObject>> entry : rules.entrySet()) {
			if(entry.getKey().isInstance(source)) {
				return entry.getValue();
			}
		}
//		System.out.println("Rule not found for " + source);

		return null;
	}
*/
/*
	protected EObject transform(EObject source) {
		for(Rule<EObject, EObject> rule : rules) {
			if(rule.getType().isInstance(source) && entry.getValue().getGuard() == null) {
				return entry.getValue().apply(source);
			}
		}
//		System.out.println("Rule not found for " + source);

		return null;
	}
/**/
//	private final static IBox<Boolean> FALSE = AOFFactory.INSTANCE.createOne(false, false);	// why do we need to set initial?
/*
	public static class Transform<E, R> extends Operation<R> {

		private IBox<E> sourceBox;

		public Transform(IBox<E> sourceBox) {
			this.sourceBox = sourceBox;
			for (E element : sourceBox) {
				if (selector.apply(element)) {
					getResult().append(element);
				}
			}
			registerObservation(sourceBox, new SourceObserver());
			registerObservation(getResult(), new ResultObserver());
		}

		@Override
		public boolean isOptional() {
			return sourceBox.isOptional();
		}

		@Override
		public boolean isSingleton() {
			return sourceBox.isSingleton();
		}

		@Override
		public boolean isOrdered() {
			return sourceBox.isOrdered();
		}

		@Override
		public boolean isUnique() {
			return sourceBox.isUnique();
		}
	}
*/

	public static <A> IUnaryFunction<Boolean, A> condition(final A ifTrue, final A ifFalse) {
		return new IUnaryFunction<Boolean, A>() {
			@Override
			public A apply(Boolean a) {
				if(a) {
					return ifTrue;
				} else {
					return ifFalse;
				}
			}
		};
	}
/*
	static {
		IBox<EObject> source = AOFFactory.INSTANCE.createOrderedSet();
		UMLRTFacade.MutablePredicate<EObject> guardA = null;
		UMLRTFacade.MutablePredicate<EObject> guardB = null;

		(
			path(source, guardA).collect(condition("A", null))
		).zipWith(
			path(source, guardB).collect(condition("B", null)),
			new IBinaryFunction<String, String, String>() {
				@Override
				public String apply(String a, String b) {
					if(b == null) {
						return a;
					} else if(a == null) {
						return b;
					} else {
						throw new IllegalStateException("two rules matching the same element");
					}
				}
			}
		);
	}
*/

	// TODO: make non-static to avoid memory leak?
	// or make "constant" that does not remember its observers
	protected static final IOne<Boolean> FALSE =
/*
		AOFFactory.INSTANCE.createOne(false);
/*/
		new One<Boolean>() {	// constant
			{
				setDelegate(new ListDelegate<Boolean>());
				clear(false);
			}

			@Override
			public void addObserver(IObserver<Boolean> observer) {
				// no need for a constant
			}

			@Override
			public void removeObserver(IObserver<Boolean> observer) {
				// no need for a constant
			}
		};
/**/
	private final IOne<Boolean> TRUE = AOFFactory.INSTANCE.createOne(true);
	{
		TRUE.addObserver(new Utils.TaggingObserver("TRUE"));
		FALSE.addObserver(new Utils.TaggingObserver("FALSE"));
	}

	// DONE: optimize by only checking types and guards for rules that may match (would need type propagation)
	// impact of adding sourceType: on a given version of a transformation (UMLRTFacade) we had 2433 boxes, and only 929 after using sourceType
	// TODO: make sourceType optional by not crashing if it is null?
	protected <A extends EObject> IBox<EObject> transform(IBox<A> source, EClass sourceType) {
		// TODO: sort rules so that those without guard come last? OR use explicit guards and no default rule mechanism
/*
		IBox<EObject> ret = AOFFactory.INSTANCE.createBox(source);
		for(final Rule<EObject, EObject> rule : this.rules) {
			IBox<EObject> v = (IBox)select(source, rule.getType());
			if(rule.getGuard() != null) {
				v = v.select(
					path(v, rule.getGuard())
						.inspect("\tpresence for rule " + rule + ": ")
				);
				inspectProperty(v, "\tselection for rule " + rule + ": ", "name");
			}
			ret = union(ret, v.collectRefs(rule));
		}
		if(source.isSingleton()) {
			//ret = ret.first();
		} else if(source.isUnique()) {
			ret = ret.distinct();
		}
		return ret;
/*/
		source.inspect("source: ");
		IBox<Rule<EObject, EObject>> rules = null;
		for(final Rule<EObject, EObject> rule : this.rules) {
			if(rule.getSourceType().isSubTypeOf(sourceType)) {
				IBox<Rule<EObject, EObject>> partialRules;
				if(rule.getGuard() == null && source.length() == 1) {
					partialRules = source.collect(new IUnaryFunction<A, Rule<EObject, EObject>>() {
						@Override
						public Rule<EObject, EObject> apply(A a) {
							if(rule.getSourceType().isInstance(a)) {
								return rule;
							} else {
								return null;
							}
						}
					}).inspect("immutable selection for rule " + rule.toString() + ": ");
				} else {
					partialRules = path(source, new IUnaryFunction<A, IBox<Boolean>>() {
						@Override
						public IBox<Boolean> apply(A a) {
							if(rule.getSourceType().isInstance(a)) {
								if(rule.getGuard() == null) {
									return TRUE;
								} else {
									return rule.getGuard().apply(a);
								}
							} else {
								return FALSE;
							}
						}
					})
					.inspect("mutable selection for rule " + rule.toString() + ": ")
					.collect(new IUnaryFunction<Boolean, Rule<EObject, EObject>>() {
						@Override
						public Rule<EObject, EObject> apply(Boolean a) {
							if(a) {
								return rule;
							} else {
								return null;
							}
						}
					});
				}
				rules = (rules == null) ? partialRules : rules.zipWith(partialRules,
					new IBinaryFunction<Rule<EObject, EObject>, Rule<EObject, EObject>, Rule<EObject, EObject>>() {
						@Override
						public Rule<EObject, EObject> apply(Rule<EObject, EObject> a, Rule<EObject, EObject> b) {
							if(a != null) {
								return a;
							} else {
								return b;
							}
						}
					}
	/* This unzipper is probably unnecessary since reverse propagation on elements will induce recomputation in forward direction anyway
					,
					new IUnaryFunction<Rule<EObject, EObject>, IPair<Rule<EObject, EObject>, Rule<EObject, EObject>>>() {
						@Override
						public IPair<Rule<EObject, EObject>, Rule<EObject, EObject>> apply(Rule<EObject, EObject> a) {
	//						if(a == )
	//						if(a != null) {
	//							return a;
	//						} else {
	//							return b;
	//						}
							return null;
						}
					}
	*/
				).inspect("zipping with previous rules for rule " + rule + ": ");
			}
		}
		if(rules == null) {
			// TODO: or subtypes
			throw new IllegalStateException("No rule can match elements of type " + sourceType.getName());
		}
inspectProperty(source, "elements: ", "name");
rules.inspect("rules: ");
		IBox<IPair<A, Rule<EObject, EObject>>> sourceAndRules = source.zip(rules, true);
		IBox<EObject> ret = sourceAndRules.selectMutable(
			sourceAndRules.collect(
				new IUnaryFunction<IPair<A, Rule<EObject, EObject>>, Boolean>() {
					@Override
					public Boolean apply(IPair<A, Rule<EObject, EObject>> a) {
						// we test the type (TODO: and guard) a second time to ignore temporary misalignment
						Rule r = a.getRight();
						EObject element = a.getLeft();
						if(r != null && r.getSourceType().isInstance(element)) {
							return true;
						}
						return false;
					}
				}
			)
		).collectTo(
			(IUnaryFunction)forwardApply,
			(IUnaryFunction)reverseApply
		);
		ret.inspect("transformed: ");
		if(source.isUnique() && !ret.isUnique()) {
			ret = ret.distinct();
		}
		return ret;
/**/
//		return new Transform(source).getResult();
//		IBox<A> src = source;
//		for(final Rule<EObject, EObject> rule : this.rules) {
//			IPair<IBox<A>, IBox<A>> p = source.split(isOfType(rule.getType()));
//			p.getFirst();
//			src = p.getSecond();
//		}
//		return null;
	}

	// define globaly (and above all not inline) to make sure the cache can be leveraged
	// TODO: was static, check if this changes anything
	public final IUnaryFunction<IPair<EObject, Rule<EObject, EObject>>, EObject> forwardApply = new IUnaryFunction<IPair<EObject, Rule<EObject, EObject>>, EObject>() {
		@Override
		public EObject apply(IPair<EObject, Rule<EObject, EObject>> a) {
			EObject source = a.getLeft();
			Rule<EObject, EObject> rule = a.getRight();
			
			List<EObject> targets = targetsPerSource.get(source);
			if(targets == null) {
				targets = new ArrayList<EObject>();
				targetsPerSource.put(source, targets);
			}
			for(EObject sibling : targets) {
				bindingsByTransformedElement.get(sibling).disable();
			}

			EObject target = rule.forwardApply(source);
			Bindings bindings = new Bindings(Facade.this, true);
			bindingsByTransformedElement.put(target, bindings);
			rule.bindContents(bindings, source, target);

			targets.add(target);
			siblings.put(target, targets);

			return target;
		}
	};

	// TODO: privatize... once client code currently in UMLRTFacade is moved in Facade
	protected Map<EObject, Bindings> bindingsByTransformedElement = new HashMap<EObject, Rule.Bindings>();
	protected Map<EObject, List<EObject>> targetsPerSource = new HashMap<EObject, List<EObject>>();
	// targets with same source
	protected Map<EObject, List<EObject>> siblings = new HashMap<EObject, List<EObject>>();

	public final IUnaryFunction<EObject, IPair<EObject, Rule<EObject, EObject>>> reverseApply = new IUnaryFunction<EObject, IPair<EObject, Rule<EObject, EObject>>>() {
		@Override
		public IPair<EObject, Rule<EObject, EObject>> apply(EObject target) {
			if(target == null) {
				return AOFFactory.INSTANCE.createPair(null, null);
			} else {
				// we assume all rules have distinct target types (or we would need reverse guards) => this is checked in addRule
				Rule<EObject, EObject> rule = (Rule)ruleByTargetType.get(new EMFType(target.eClass()));
				if(rule != null) {
					EObject source = rule.reverseApply(target);
					Bindings bindings = new Bindings(Facade.this, false);
					// NO, because we will never have two source elements for a given target element (see above assumption about distinct target types for rules)
//					bindingsByTransformedElement.put(source, bindings);
					rule.bindContents(bindings, source, target);
					return AOFFactory.INSTANCE.createPair(source, rule);
				} else {
					throw new UnsupportedOperationException("No rule to transform target element " + target);
				}
			}
		}
	};

	private final Map<String, Rule<?, ?>> ruleByName = new HashMap<String, Rule<?, ?>>();
	private final Map<Type, Rule<?, ?>> ruleByTargetType = new HashMap<Type, Rule<?, ?>>();

	protected <C extends EObject> void addRule(Rule<C, EObject> r) {
//		r.setFacade(this);
		rules.add((Rule<EObject, EObject>)r);
		ruleByName.put(r.toString(), r);
		if(ruleByTargetType.containsKey(r.getTargetType())) {
			throw new IllegalStateException("Rule " + r + " has the same return type as already registered rule " + ruleByTargetType.get(r.getTargetType()));
		} else {
			ruleByTargetType.put(r.getTargetType(), r);
		}
	}

	public Rule getRule(String name) {
		return ruleByName.get(name);
	}

	public static Predicate<Boolean> not =
/* With Java 8:
			(a) -> !a;
/*/
			new Predicate<Boolean>() {
				@Override
				public Boolean apply(Boolean a) {
					return !a;
				}
			};
/**/

	public static ComposableBinaryFunction<Boolean, Boolean, Boolean> and =
/* With Java 8:
			Boolean::logicalAnd;
//OR
//			(a, b) -> a && b;
/*/
			new ComposableBinaryFunction<Boolean, Boolean, Boolean>() {
				@Override
				public Boolean apply(Boolean a, Boolean b) {
					return a && b;
				}
			};
/**/

	public static ComposableBinaryFunction<Boolean, Boolean, Boolean> or =
/* With Java 8:
			Boolean::logicalOr;
//OR
//			(a, b) -> a || b;
/*/
			new ComposableBinaryFunction<Boolean, Boolean, Boolean>() {
				@Override
				public Boolean apply(Boolean a, Boolean b) {
					return a || b;
				}
			};
/**/

	protected static ComposableUnaryFunction<IBox<Boolean>, IBox<Boolean>> NOT = not.lift();
	public static ComposableBinaryFunction<IBox<Boolean>, IBox<Boolean>, IBox<Boolean>> AND = and.lift();
	public static ComposableBinaryFunction<IBox<Boolean>, IBox<Boolean>, IBox<Boolean>> OR = or.lift();

	// B should be Boolean or IOne<Boolean>
	public static abstract class Predicate<A> extends ComposableUnaryFunction<A, Boolean> {

		public Predicate<A> not() {
			return asPredicate(not.compose(this));
		}

		public Predicate<A> and(IUnaryFunction<? super A, Boolean> f) {
			return asPredicate(and.compose(this, f).mergeInputs());
		}

		public <B> Predicate<A> or(IUnaryFunction<? super A, Boolean> f) {
			return asPredicate(or.compose(this, f).mergeInputs());
		}
	}

	public static <A> Predicate<A> asPredicate(final IUnaryFunction<?super A, Boolean> f) {
		return new Predicate<A>() {
			@Override
			public Boolean apply(A a) {
				return f.apply(a);
			}
		};
	}

	private Map<String, UMLRTFacade.MutablePredicate<Element>> hasStereotypeAccessorCache = new HashMap<String, UMLRTFacade.MutablePredicate<Element>>();

	protected MutablePredicate<Element> hasStereotypeAccessor(final String stereotypeName) {
		MutablePredicate<Element> ret = hasStereotypeAccessorCache.get(stereotypeName);
//		MutablePredicate<Element> ret = r == null ? null : r.get();
//		ret = null;// uncomment to force recomputation (cache used to make transfo behave differently and serializePipe crash, but why?)
		// was notably triggering ConcurrentModificationException like any property used similarly in rule guard (but there may have been another issue)
		if(ret == null) {
			ret = new MutablePredicate<Element>() {
				// TODO: should we use an AOF Cache like EMFFeatureAccessor? why?
				private Map<Element, IOne<Boolean>> cache = new HashMap<Element, IOne<Boolean>>();

				@Override
				public IOne<Boolean> apply(final Element a) {
					IOne<Boolean> ret = cache.get(a);
					if(ret == null) {
	//					if(a != null)
	//						System.out.println(get(a, "name") + ": " + (a.getAppliedStereotype(stereotypeName) != null));
							ret = AOFFactory.INSTANCE.createOne(
								(a == null) ?
										false	// because of null default value
									:
										a.getAppliedStereotype(stereotypeName) != null
							);
							ret.addObserver(new TaggingObserver("hasStereotype(" + stereotypeName + ")"));
						// this inspect is very useful for pipes visualization because this box is a root box
						ret.inspect(Inspect.toString(a, null) + ".hasStereotype(" + stereotypeName + ")");
						final IBox<Boolean> aret = ret;
						if(a != null) {
							a.eAdapters().add(new AdapterImpl() {
								public void notifyChanged(Notification notification) {
									switch(notification.getEventType()) {
									case PapyrusStereotypeListener.APPLIED_STEREOTYPE:
									case PapyrusStereotypeListener.MODIFIED_STEREOTYPE:
									case PapyrusStereotypeListener.UNAPPLIED_STEREOTYPE:	// TODO: do nothing if not "our" stereotype
										aret.add(0, a.getAppliedStereotype(stereotypeName) != null);
										break;
									default:
										// ignore
										break;
									}
								}
							});
						}
						cache.put(a, ret);
					}
					return ret;
				}
			};
			hasStereotypeAccessorCache.put(stereotypeName, ret);
		}

		return ret;
	}

	public static <A> MutablePredicate<A> asMutablePredicate(final IUnaryFunction<?super A, IBox<Boolean>> f) {
		if(f instanceof MutablePredicate) {
			return (MutablePredicate<A>)f;
		} else {
			return new MutablePredicate<A>() {
				@Override
				public IBox<Boolean> apply(A a) {
					return f.apply(a);
				}
			};
		}
	}

	public static abstract class MutablePredicate<A> extends ComposableUnaryFunction<A, IBox<Boolean>> {

		public MutablePredicate<A> not() {
			return asMutablePredicate(NOT.compose(this));
		}

		public MutablePredicate<A> and(IUnaryFunction<? super A, IBox<Boolean>> f) {
			return asMutablePredicate(AND.compose(this, f).mergeInputs());
		}

		public MutablePredicate<A> or(IUnaryFunction<? super A, IBox<Boolean>> f) {
			return asMutablePredicate(OR.compose(this, f).mergeInputs());
		}
	}


	@SuppressWarnings("unchecked")
	public <A extends Element> IBox<A> selectStereotype(IBox<? extends Element> target, final String stereotypeName) {
//		System.out.println("select stereo " + stereotypeName);
		return (IBox<A>)target.selectMutable(
/*
			new IUnaryFunction<Element, Boolean>() {
				@Override
				public Boolean apply(Element a) {
					System.out.println(get(a, "name") + " => " + passiveGet(a.getApplicableStereotypes(), "qualifiedName"));
					System.out.println(a.getAppliedStereotype(stereotypeName));
					return a.getAppliedStereotype(stereotypeName) != null;
				}
			}
/*/
			new CollectBox<Element, Boolean>((IBox<Element>)target, null,
				hasStereotypeAccessor(stereotypeName)
			).getResult()//.inspect("has stereo: ")
/**/
		);
	}

}
