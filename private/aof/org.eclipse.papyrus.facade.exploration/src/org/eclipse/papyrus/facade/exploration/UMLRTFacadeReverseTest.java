package org.eclipse.papyrus.facade.exploration;

import static org.eclipse.papyrus.facade.exploration.Utils.create;
import static org.eclipse.papyrus.facade.exploration.Utils.get;
import static org.eclipse.papyrus.facade.exploration.Utils.set;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Model;
import org.junit.Test;

// Reverse synchronization tests
public class UMLRTFacadeReverseTest extends UMLRTFacadeTest {

	@Test
	public void testAddCapsule() {
		EObject modelRT = umlRTSample.getContents().get(0);
		Collection classes = get(modelRT, "classes");
		addElement(classes, "Capsule", true);
	}

	@Test
	public void testAddPassiveClass() {
		EObject modelRT = umlRTSample.getContents().get(0);
		Collection classes = get(modelRT, "classes");
		addElement(classes, "PassiveClass", true);
	}

	protected EObject addElement(Collection c, String className, boolean setName) {
		EObject element = create(findTargetEClass(className));
		if(setName) {
			set(element, "name", "TEST");
		}
		System.out.println("Adding " + className);
		c.add(element);
		check();
		return element;
	}

	@Test
	public void testCapsuleRenaming() {
		org.eclipse.uml2.uml.Class c = findClass("Top");
		EObject capsule = findTargetCapsule("Top");

		set(capsule, "name", "Test");

		assertEquals(get(capsule, "name"), c.getName());
	}

	@Test
	public void testReverseRemoveCapsule() {
		reverseRemoveEntity("SubPartClass", false);
	}

	@Test
	public void testReverseRemoveCapsuleThenPutBack() {
		List<List> preSnapshot = takeSnapshot();
		reverseRemoveEntity("SubPartClass", true);
		List<List> postSnapshot = takeSnapshot();
		compareSnapshots(preSnapshot, postSnapshot);
		assertEquals(preSnapshot, postSnapshot);
	}

	// TODO: THIS FAILS until rule disabling works 
//*
	@Test
	public void testSnapshotIdenticalAfterMovingPassiveBackAndForth() {
		// we need to first put the boxes in a state they can go back to by moving Passive around once
		// because the result cannot be the same because we do not remember the original index
		moveEntity("Passive", 0, true);	// create a state to which we can come back

		List<List> preSnapshot = takeSnapshot();
		int oldIndex = moveEntity("Passive", 0, false);	// fails with 1 or 0 but not with 2
		List<List> middleSnapshot = takeSnapshot();
		moveEntity("Passive", oldIndex, false);	// fails with 1 or 0 but not with 2
		List<List> postSnapshot = takeSnapshot();

		Model m = (Model)umlRTProfileSample.getContents().get(0);
		System.out.println(Utils.passiveGet(m, "packagedElement", "name"));

		compareSnapshots(preSnapshot, middleSnapshot);
		compareSnapshots(preSnapshot, postSnapshot);
		assertEquals(preSnapshot, postSnapshot);
//		moveEntity("Passive", 1);
//		moveEntity("Passive", 2);
	}
/**/

//* Used to Fail AND make all subsequent tests fail BUT now works!
	@Test
	public void testMovingPassiveAround() {
		moveEntity("Passive", 0, false);
		moveEntity("Passive", 1, false);
		moveEntity("Passive", 2, false);
	}
/**/

	@Test
	public void testMovingPassiveAround2() {
		List<List> preSnapshot = takeSnapshot();
		moveEntity("Passive", 0, true);	// create a state to which we can come back
		List<List> middleSnapshot = takeSnapshot();

		compareSnapshots(preSnapshot, middleSnapshot);

		moveEntity("Passive", 3, true);
		List<List> postSnapshot = takeSnapshot();
//		moveEntity("Passive", 3, true);
	}

	protected int moveEntity(String entityName, int toIndex, boolean andBack) {
		EObject model = findOne(umlRTSample, findTargetEClass("Model"));
		EObject entity = findOne(umlRTSample, findTargetEClass("Entity"), "name", entityName);
		EList c = get(model, "classes");
		System.out.println("Moving entity " + entityName + " to " + toIndex);
		int oldIndex = c.indexOf(entity);
		c.move(toIndex, oldIndex);
		check();
		if(andBack) {
			System.out.println("Moving entity " + entityName + " back to " + oldIndex);
			c.move(oldIndex, toIndex);
		}
		check();
		return oldIndex;
	}

	public void reverseRemoveEntity(String entityName, boolean thenPutBack) {
		EObject model = findOne(umlRTSample, findTargetEClass("Model"));
		EObject entity = findOne(umlRTSample, findTargetEClass("Entity"), "name", entityName);
		List c = get(model, "classes");
		int index = c.indexOf(entity);
		System.out.println("Removing entity " + entityName);
		c.remove(entity);
		check();
		if(thenPutBack) {	// TODO: then append
			System.out.println("Putting back entity " + entityName);
			c.add(index, entity);
			check();
		}
	}

	// issue was in formula in SelectMutables.ResultObserver.removed
	@Test
	public void testMovePassiveThenRemoveSubPartClassThenUndo() {
//		List<List> preSnapshot = takeSnapshot();
		int oldIndex = moveEntity("Passive", 0, false);	// passes with 0 and 1
//		List<List> middleSnapshot = takeSnapshot();
//		compareSnapshots(preSnapshot, middleSnapshot);
		reverseRemoveEntity("SubPartClass", true);	// target removal FAILS to remove in source
		// the following line was not necessary for the crash to occur
//		moveEntity("Passive", oldIndex, false);
		check();
	}

	// Remark: testMovePassiveThenRemoveSubPartClassThenUndo implies this test ONLY in interactive mode
	// (because of the way UNDO works, undoing even secondary actions, not user-triggered ones),
	// therefore we must add it explicitly here
	// issue is with opt.asOne not working in reverse: it attempts to replace in empty opt
	@Test
	public void testRemovePartTypeThenUndo() {
		EObject subTop = findOne(umlRTSample, findTargetEClass("Entity"), "name", "SubTop");
		List<EObject> parts = get(subTop, "parts");
		EObject right = parts.get(0);
		EObject v = get(right, "type");
		set(right, "type", null);
		// The following fails with IndexOutOfBoundsException because opt.asOne results in replace
		// on the one into a replace on the opt, which is empty
		set(right, "type", v);
	}

	// TODO: THIS FAILS until we establish bindings in the correct direction
	@Test
	public void testAddingInitializedCapsule() {
		EObject c = create(findTargetEClass("Capsule"));
		String name = "TEST";
		set(c, "name", name);

		EObject model = findOne(umlRTSample, findTargetEClass("Model"));
		EList classes = get(model, "classes");
		classes.add(c);

		assertEquals(name, get(c, "name"));
		check();
	}

	// Remark: with original umlrtmetamodel, Model.top is a containment, which, in interactive mode, makes moving any Capsule into setting it to top...
	// after "fixing" the metamodel, moving works correctly

	// tests move in selectwithPredicate
	@Test
	public void testMovingProtocols() {
		EObject modelRT = umlRTSample.getContents().get(0);
		EList protocols = get(modelRT, "protocols");
		addElement(protocols, "Protocol", false);
		protocols.move(1, 0);
	}

	// tests remove in selectwithPredicate
	@Test
	public void testRemovingProtocols() {
		EObject modelRT = umlRTSample.getContents().get(0);
		EList protocols = get(modelRT, "protocols");
		EObject element = addElement(protocols, "Protocol", true);
		protocols.remove(element);
	}

	@Test
	public void fuzzingTest() {
		ModelFuzzer fuzzer = new ModelFuzzer(this.umlRTSample, 2);
		int i = 0;
		while(true) {
			System.out.println("Change number " + ++i);
			fuzzer.performOneChange();
			fullCheck();
		}
	}

	// no null constraint
	@Test
	public void testSettingRedefinesToNullOnPort() {
		EObject e = findOne(umlRTSample, findTargetEClass("Port"), "name", "p2X");
		set(e, "redefines", null);
		fullCheck();
	}

	// failure (because not backpropagated + invalid anyway)
	@Test
	public void testSettingRedefinesToNullOnPassiveClass() {
		EObject e = findOne(umlRTSample, findTargetEClass("PassiveClass"));
		EObject s = findOne(umlRTSample, findTargetEClass("State"), "name", "Started");
		set(e, "redefines", s);
		fullCheck();
	}

	@Test
	public void testChangingBehavior() {
		EObject e = findOne(umlRTSample, findTargetEClass("Capsule"), "name", "SubPartClass");
		EObject sm = findOne(umlRTSample, findTargetEClass("StateMachine"), "name", "PartClassSM");
		set(e, "behaviour", sm);
		fullCheck();
	}

	// Other possible tests (discovered by fuzzying)
	// InitialPoint.incomingTransitions should always be empty, adding something to it does not synchronize...
}
