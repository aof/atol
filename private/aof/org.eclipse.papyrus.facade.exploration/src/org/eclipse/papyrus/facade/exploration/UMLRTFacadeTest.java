package org.eclipse.papyrus.facade.exploration;

/*
 *	- Fa�ade' manually generated code example
 *	- discuss Java 8 lambdas rewriting...
 */


import static org.eclipse.papyrus.facade.exploration.Utils.passiveGet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.DynamicEObjectImpl;
import org.eclipse.emf.ecore.impl.EAnnotationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.impl.URIMappingRegistryImpl;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IObserver;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.BaseDelegate;
import org.eclipse.papyrus.aof.core.impl.Box;
import org.eclipse.papyrus.aof.core.impl.operation.Bind;
import org.eclipse.papyrus.aof.core.impl.operation.Collect;
import org.eclipse.papyrus.aof.core.impl.operation.CollectBox;
import org.eclipse.papyrus.aof.core.impl.operation.Distinct;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.papyrus.aof.core.impl.operation.Operation;
import org.eclipse.papyrus.aof.core.impl.operation.ZipWith;
import org.eclipse.papyrus.aof.core.impl.utils.FactoryObserver;
import org.eclipse.papyrus.aof.emf.impl.FeatureDelegate;
import org.eclipse.papyrus.facade.exploration.Utils.TaggingObserver;
import org.eclipse.papyrus.infra.core.listenerservice.IPapyrusListener;
import org.eclipse.papyrus.uml.tools.listeners.PapyrusStereotypeListener;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.internal.impl.AnyReceiveEventImpl;
import org.eclipse.uml2.uml.internal.impl.AssociationImpl;
import org.eclipse.uml2.uml.internal.impl.CallEventImpl;
import org.eclipse.uml2.uml.internal.impl.ClassImpl;
import org.eclipse.uml2.uml.internal.impl.CollaborationImpl;
import org.eclipse.uml2.uml.internal.impl.ComponentImpl;
import org.eclipse.uml2.uml.internal.impl.ConnectorEndImpl;
import org.eclipse.uml2.uml.internal.impl.ConnectorImpl;
import org.eclipse.uml2.uml.internal.impl.GeneralizationImpl;
import org.eclipse.uml2.uml.internal.impl.LiteralIntegerImpl;
import org.eclipse.uml2.uml.internal.impl.LiteralStringImpl;
import org.eclipse.uml2.uml.internal.impl.LiteralUnlimitedNaturalImpl;
import org.eclipse.uml2.uml.internal.impl.ModelImpl;
import org.eclipse.uml2.uml.internal.impl.OperationImpl;
import org.eclipse.uml2.uml.internal.impl.PackageImpl;
import org.eclipse.uml2.uml.internal.impl.PortImpl;
import org.eclipse.uml2.uml.internal.impl.ProfileApplicationImpl;
import org.eclipse.uml2.uml.internal.impl.PropertyImpl;
import org.eclipse.uml2.uml.internal.impl.PseudostateImpl;
import org.eclipse.uml2.uml.internal.impl.RegionImpl;
import org.eclipse.uml2.uml.internal.impl.StateImpl;
import org.eclipse.uml2.uml.internal.impl.StateMachineImpl;
import org.eclipse.uml2.uml.internal.impl.TransitionImpl;
import org.eclipse.uml2.uml.internal.impl.TriggerImpl;
import org.eclipse.uml2.uml.internal.operations.ProfileOperations;
import org.eclipse.uml2.uml.internal.resource.UMLResourceFactoryImpl;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.junit.After;
import org.junit.Before;

public class UMLRTFacadeTest {

	private static URI getPluginURI(java.lang.Class<?> c) {
		String url = c.getResource(c.getSimpleName() + ".class").toString();
		url = url.replaceAll("![^!]*$", "!");
//		String plugin = url.replaceAll("^.*/plugins/([^_-]*)[_-].*$", "$1");	// launched form Eclipse
//		plugin = plugin.replaceAll("^.*/libs/([^_-]*)[_-].*$", "$1");			// launched from command line
//		pluginToURL.put(plugin, url);
		return URI.createURI(url + "/");
	}

	private static void init() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, new UMLResourceFactoryImpl());
		//EPackage.Registry.INSTANCE.put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		// To enable usage of UML 2.4 models from Luna and beyond, the two following lines are necessary
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/UML", UMLPackage.eINSTANCE);
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/Types", TypesPackage.eINSTANCE);

		URI uri = getPluginURI(UMLResourcesUtil.class);
//		System.out.println(uri);
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));

//		defineProfiles("NOCOMMIT/uml-rt.profile.uml");
//		defineProfiles("NOCOMMIT/uml-rt-sm.profile.uml");
	}

	public static void main(String[] args) throws Exception {
		UMLRTFacadeTest t = new UMLRTFacadeTest();
		t.setUp();
		t.manualTest();
	}

	protected static final String CAPSULE = "UMLRealTime::Capsule";

	public void manualTest() throws IOException {
		umlRTSample.save(new HashMap<String, Object>());
//		umlRTProfileSample.setURI(URI.createFileURI("NOCOMMIT/sample-reserialized.uml"));
//		umlRTProfileSample.save(new HashMap<>());

//		applyStereotype("Passive", CAPSULE);
//		check();

//		, 0
//		, 0, 0, 1, 2, 3, 4, 3, 2, 1, 0
//		testMove("Passive"
//			, 1, 0
//		);

/*
		while(true) {
			testMove("Passive"
				, new Random().nextInt(8)
			);
			if(false) {	// to avoid unreachable code error for what comes after the enclosing while loop
				break;
			}
		}
/**/

//		testApplyProfile(umlRTProfileSample, true);
//		testRemove();
//		check();

		umlRTSample.setURI(URI.createFileURI("NOCOMMIT/sample-UMLRT-2.xmi"));
		umlRTSample.save(new HashMap<String, Object>());

		System.out.println("Number of boxes: " + boxes.size());
		serializePipes();

		System.out.println("Number of boxes: " + boxes.size());

		// Trying unloading to check if there is a memory leak
		Inspect.enable = true;
		// TODO: unloading the source model should not be necessary for gc to happen
		umlRTProfileSample.unload();
		umlRTSample.unload();
//		CollectBox.resetCache();
//		javaSet(null, CollectTo.class, "cache", new Cache());
		facade = null;
		List<WeakReference<Box>> rBoxes = rBoxes();
		boxes = null;
		System.gc();
		List<WeakReference<Box>> nonCollectedBoxes = new ArrayList<WeakReference<Box>>();
		for(WeakReference<Box> rBox : rBoxes) {
			if(rBox.get() != null) {
				nonCollectedBoxes.add(rBox);
			}
		}
//		boxes = nonCollectedBoxes;

		System.out.println("Number of boxes after gc: " + nonCollectedBoxes.size());
//		while(true) {
//			if(false) break;
//		}
	}

	private List<WeakReference<Box>> rBoxes() {
		List<WeakReference<Box>> rBoxes = new ArrayList<WeakReference<Box>>();
		for(Box box : boxes) {
			rBoxes.add(new WeakReference(box));
		}
		return rBoxes;
	}

	private final static String OPERATION_PACKAGE = "org.eclipse.papyrus.aof.core.impl.operation.";

	// TODO: fix pipes serialization by aligning it to newest AOF version
	// TODO: name predicates of SelectWithPredicate when they correspond to known functions (e.g., isOfType in Utils)
	private void serializePipes() throws IOException {
		PrintStream out = new PrintStream("NOCOMMIT/transfo.plantuml");
		out.println("@startuml");
		out.println("hide empty description");

		final String BIDIR_ARROW = "-->";			// corresponds to an Observer
		final String UNIDIR_ARROW = "-[#blue]->";	// no corresponding Observer
		final String ROOT_BOX_COLOR = "lightgreen";	// or TERMINAL_BOX

		Map<Object, String> functionNames = new HashMap<Object, String>();
		functionNames.put(facade.or, "or");
		functionNames.put(facade.OR, "OR");
		functionNames.put(facade.and, "and");
		functionNames.put(facade.AND, "AND");
		functionNames.put(facade.not, "not");
		
		int id = 0;
		for(Box<?> box :  boxes) {
			BaseDelegate delegate = javaGet(box, Box.class, "delegate");

			String boxId = getBoxId(box);
			String boxName = box.getConstraints().toString();
			String boxColor = "orange";

			// no way to represent boxes as rectangles (i.e., Object Nodes) => make them orange
			if(delegate instanceof FeatureDelegate) {
				EStructuralFeature f = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "feature");
				EObject o = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "object");
				boxName += "=" + Inspect.toString(o, null) + "." + f.getName();
				// DONE: highlight these "root" boxes
				boxColor = ROOT_BOX_COLOR;
			}

			// using observers only to retrieve arcs is not enough because some operations do not observe their result
			// (e.g., ZipWith without unzipper, CollectImmutables without reverseCollector, CollectBox (not anymore, only with unidir version))
			// => but we have the information in the operations themselves (resultBox)
			// => Therefore we should gather all unique operations while traversing the boxes, then serialize the operations with their arcs
			// without relying on observers.
			// BUT we can also just handle the special cases => see specific "Zip" and "CollectImmutables" handling below

			for(IObserver observer : box.getObservers()) {
				String observerClassName = observer.getClass().getName();
				boolean unknownObserver = false;

				// unwrap SilentObserver
				if(observerClassName.equals(OPERATION_PACKAGE + "Operation$SilentObserver")) {	// necessary because SilentObserver is private
					observer = javaGet(observer, "wrappedObserver");
					observerClassName = observer.getClass().getName();
				}
//				System.out.println("\t" + observer);

				if(observerClassName.startsWith(OPERATION_PACKAGE)) {
					//  + "Distinct$ResultObserver"

					// There is now CollectBox.InnerObserver that points to its SilentObserver
//					if(observer.getClass().getDeclaredFields().length > 1) {
//						throw new IllegalStateException("Found an AOF observer with more than one field");
//					}
/*
					if(observerClassName.startsWith(operationPackage + "Bind$")) {
						Bind b = ;
						out.println("\t\tbind: " + b + "." + observer.getClass().getEnclosingClass());
					} else {
						Operation op = javaGet(observer, "this$0");
						out.println("\t\toperation: " + op);
					}
*/
					Object op = javaGet(observer, "this$0");	// remark: Bind does not extend Operation
					String opName = op.getClass().getSimpleName();
					String opId = opName + "_" + op.hashCode();

					String label = observer.getClass().getSimpleName().replaceAll("Observer$", "");

					// testing toString
//					if(op instanceof Bind) {
//						System.out.println(toString(op));
//						System.out.println(observer);
//						System.out.println(toString(observer));
//						System.exit(0);
//					}

					if(op instanceof CollectBox) {
						Object collector = javaGet(op, "collector");
						if(collector.getClass().getName().equals("org.eclipse.papyrus.aof.emf.impl.EMFMetaClass$PropertyAccessor")) {
							EStructuralFeature feature = javaGet(collector, "feature");
							opName += "(" + feature.getEContainingClass().getName() + "." + feature.getName() + ")";							
						}
//						System.out.println(collector);
						// DONE: this is redundant with ResultObserver for bidir version
//						if(label.equals("Source")) {	// only do it once (i.e., not for each InnerBoxObserver)
//							Box resultBox = javaGet(op, Operation.class, "resultBox");
//							out.println(opId + UNIDIR_ARROW + getBoxId(resultBox) + " : Result");
//						}
					} else if(op instanceof Inspect) {
						opName += "(" + javaGet(op, "label") + ")";
					} else if(op instanceof ZipWith) {
						//System.out.println("\t" + observer);
						if(label.equals("Right")) {	// only do it once (i.e., not for Result and not for Left, which may not exist if leftRightDepency)
							if(javaGet((ZipWith)op, ZipWith.class, "unzipper") == null) {
								// no unzipper => no reverse synchronization => no ResultObserver
								Box resultBox = javaGet((ZipWith)op, Operation.class, "resultBox");
								out.println(opId + UNIDIR_ARROW + getBoxId(resultBox) + " : Result");
							}
							if(javaGet((ZipWith)op, ZipWith.class, "leftRightDependency")) {
								Box leftBox = javaGet((ZipWith)op, ZipWith.class, "leftBox");
								out.println(getBoxId(leftBox) + UNIDIR_ARROW + opId + " : Left");
							}
						}
					} else if(op instanceof Collect) {
						if(javaGet(op, "inverseCollector") == null) {
							// no inverseCollector => no reverse synchronization => no ResultObserver
							Box resultBox = javaGet((Collect)op, Operation.class, "resultBox");
							out.println(opId + UNIDIR_ARROW + getBoxId(resultBox) + " : Result");
						}
					}

					out.println("state \"" + opName + "\" as " + opId);

					if("Result".equals(label)) {
						out.println(opId + BIDIR_ARROW + boxId + " : " + label);
					} else {
						out.println(boxId + BIDIR_ARROW + opId + " : " + label);
					}
//				} else if(observer.getClass().getEnclosingMethod().getDeclaringClass()) {
				} else if(observer.getClass().getEnclosingMethod() != null) {
					Method context = observer.getClass().getEnclosingMethod().getDeclaringClass().getEnclosingMethod();
					if(context.getName().equals("lift") && context.getDeclaringClass().getSimpleName().equals("ComposableBinaryFunction")) {
						String opName = "BiComp";
						String opId = opName + id++;

						Box a = javaGet(observer, "val$a");
						if(box == a) {	// only do it once (i.e., not for b)
							String aId = getBoxId(a);

							Box b = javaGet(observer, "val$b");
							String bId = getBoxId(b);
	
							Object function = javaGet(javaGet(observer, "this$2"), "this$1");
							if(functionNames.containsKey(function)) {
								opName += " " + functionNames.get(function);
							}
	
							out.println("state \"" + opName + "\" as " + opId);
	
							Box ret = javaGet(observer, "val$ret_");
							String retId = getBoxId(ret);
	
							out.println(aId + UNIDIR_ARROW + opId + " : Left");
							out.println(bId + UNIDIR_ARROW + opId + " : Right");
							out.println(opId + UNIDIR_ARROW + retId + " : Result");
						}
					} else if(context.getName().equals("lift") && context.getDeclaringClass().getSimpleName().equals("ComposableUnaryFunction")) {
						String opName = "UComp";
						String opId = opName + id++;

						Object function = javaGet(javaGet(observer, "this$2"), "this$1");
						if(functionNames.containsKey(function)) {
							opName += " " + functionNames.get(function);
						}

						out.println("state \"" + opName + "\" as " + opId);

						Box a = javaGet(observer, "val$a");
						String aId = getBoxId(a);

						Box ret = javaGet(observer, "val$ret_");
						String retId = getBoxId(ret);

						out.println(aId + UNIDIR_ARROW + opId + " : Source");
						out.println(opId + UNIDIR_ARROW + retId + " : Result");
						// not simple if boxes contains WeakReferences instead of plain Boxes
						if(!boxes.contains(ret)) {
							System.out.println("unknown box");
						}
					} else {
						unknownObserver = true;
					}
				} else if(observer instanceof TaggingObserver){
					boxName = ((TaggingObserver)observer).tag;
					boxColor = ROOT_BOX_COLOR;
				} else {
					unknownObserver = true;
				}
				if(unknownObserver) {
					System.out.println("\t" + observer + " : " +
							observer.getClass()
//							observer.getClass().getEnclosingMethod()
//						observer.getClass().getEnclosingMethod().getDeclaringClass().getEnclosingMethod()
					);
					for(Field f : observer.getClass().getDeclaredFields()) {
						System.out.println("'\t\t" + f);
					}
				}
			}
			out.println("state \"" + boxName + "\" as " + boxId + " #" + boxColor);
		}
		out.println("@enduml");
		out.close();

	}

	private String getBoxId(Box box) {
		return "box_" + box.hashCode();
	}

	Resource umlRTProfileSample;
	Resource umlRTSample;
	Facade facade;

	private void registerToString(IUnaryFunction toString, Class...classes) {
		for(Class c : classes) {
			Inspect.registerToString(c, toString);
		}
	}

	@After
	public void tearDown() {
		umlRTProfileSample.unload();
		umlRTSample.unload();
		// no need to reinit boxes because apparently a new object is created by junit for each separate test
	}

	@Before
	public void setUp() throws IOException {
		Inspect.enable = false;

		// TODO: automate registration of a toString function on all subclasses
		// as a workaround of Inspect not looking at parent classes
		registerToString(Utils.get("name", true),
				ClassImpl.class, CollaborationImpl.class, ModelImpl.class,
				AssociationImpl.class, PackageImpl.class, PropertyImpl.class,
				PortImpl.class, StateMachineImpl.class, RegionImpl.class,
				PseudostateImpl.class, TransitionImpl.class, StateImpl.class,
				ConnectorImpl.class, ConnectorEndImpl.class, AnyReceiveEventImpl.class,
				OperationImpl.class, ProfileApplicationImpl.class, GeneralizationImpl.class,
				LiteralUnlimitedNaturalImpl.class, CallEventImpl.class, LiteralStringImpl.class,
				TriggerImpl.class, ComponentImpl.class, LiteralIntegerImpl.class,

				// Ecore
				EAnnotationImpl.class
		);
		//Inspect.registerToString(clazz, toString);
		Inspect.registerToString(DynamicEObjectImpl.class,
			new IUnaryFunction<EObject, String>() {
				@Override
				public String apply(EObject a) {
					EStructuralFeature f = a.eClass().getEStructuralFeature("name");
					String ret = "";
					if(f != null) {
						ret += a.eGet(f);
					}
					ret += ":" + a.eClass().getName();
					return ret;
				}
			}
		);

		registerToString(new IUnaryFunction<Object, String>() {
			@Override
			public String apply(Object a) {
				return UMLRTFacadeTest.this.toString(a);
			}
		}, Bind.class, Distinct.class, Object.class);

//		else if(object instanceof Collection<?>) {
//			Collection<Object> c = (Collection<Object>)object;
//			StringBuffer sb = new StringBuffer("[");
//			for(Object o : c) {
//				if(sb.length() > 1) {
//					sb.append(", ");
//				}
//				sb.append(toString(o, null));
//			}
//			sb.append(']');
//			return sb.toString();
//		}


		init();
		ResourceSet rs = new ResourceSetImpl();

		umlRTProfileSample = load(rs, "NOCOMMIT/sample.uml");

		// Activate stereotype notifications
		//	is StereotypeElementListener a better (but possibly newer) alternative?
		// TODO: move to Facade
		final IPapyrusListener pl = new PapyrusStereotypeListener();
		umlRTProfileSample.eAdapters().add(new EContentAdapter() {
			public void notifyChanged(Notification notification) {
				super.notifyChanged(notification);
				pl.notifyChanged(notification);
			}
		});
		


		facade = new UMLRTFacade();
		facade.addSource(umlRTProfileSample);

		umlRTSample = rs.createResource(URI.createFileURI("NOCOMMIT/sample-UMLRT.xmi"));
		facade.addTarget(new FacadeModel(umlRTSample));
		Box.factoryObserver = new FactoryObserver<IBox<?>>() {
			@Override
			public void added(int index, IBox<?> element) {
				boxes.add((Box<?>)element);
			}
		};
		facade.init();
		Box.factoryObserver = null;	// warning, we miss boxes added by tests (e.g., because new rule activations happen)
		System.gc();
	}

	protected List<Box> boxes = new ArrayList<Box>();

	protected List<List> takeSnapshot() {
		List<List> ret = new ArrayList();
		for(IBox b : boxes) {
			List c = new ArrayList();
			for(Object v : b) {
				c.add(v);
			}
			ret.add(c);
		}
		return ret;
	}

	private static String toString(Iterable i) {
		StringBuffer ret = new StringBuffer("[");
		for(Object o : i) {
			if(ret.length() > 1) {
				ret.append(", ");
			}
			ret.append(Inspect.toString(o, null));
		}
		ret.append(']');
		return ret.toString();
	}

	protected void compareSnapshots(List<List> pre, List<List> post) {
		if(pre.size() != post.size()) {
			throw new IllegalStateException("the two snapshots to compare have different sizes");
		}
		System.out.println("Comparing snapshots [" + pre.size() + "]:");
		for(int i = 0 ; i < pre.size() ; i++) {
			List preL = pre.get(i);
			List postL = post.get(i);
			if(!preL.equals(postL)) {
				System.out.println("\t[" + i + "]:\t" + toString(preL) + " != " + toString(postL));
				BaseDelegate delegate = javaGet(boxes.get(i), Box.class, "delegate");
				String s = delegate.toString();
				if(delegate instanceof FeatureDelegate) {
					EStructuralFeature f = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "feature");
					EObject o = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "object");
					s = "Delegate for feature " + f.getName() + " of " + Inspect.toString(o, null);
				}
				System.out.println("\t\t" + s);
			}
		}
		System.out.println(".");
	}

	protected static <A, C> A javaGet(C o, String fieldName) {
		return javaGet(o, (Class<C>)o.getClass(), fieldName);
	}

	protected static <A, C> A javaGet(C o, Class<? super C> c, String fieldName) {
		try {
			Field f = c.getDeclaredField(fieldName);
			f.setAccessible(true);
			return (A)f.get(o);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static <C> void javaSet(C o, Class<? super C> c, String fieldName, Object value) {
		try {
			Field f = c.getDeclaredField(fieldName);
			f.setAccessible(true);
			f.set(o, value);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected org.eclipse.uml2.uml.Class findClass(String name) {
		return findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getClass_(), "name", name);
	}

	protected void applyStereotype(String className, String stereotypeName) {
		changeStereotype(className, stereotypeName, true);
	}

	protected void removeStereotype(String className, String stereotypeName) {
		changeStereotype(className, stereotypeName, false);
	}

	private void changeStereotype(String className, String stereotypeName, boolean apply) {
		org.eclipse.uml2.uml.Class c = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getClass_(), "name", className);
		Stereotype stereotype = c.getApplicableStereotype(stereotypeName);
		if(apply) {
			applyStereotype(c, stereotype);
		} else {
			removeProfile(c, stereotype);
		}
	}

	protected EObject findTargetCapsule(String className) {
		org.eclipse.uml2.uml.Class c = findClass(className);
		Rule<EObject, EObject> rule = facade.getRule("Class2Capsule");
		EObject capsule = (EObject)AOFFactory.INSTANCE.createOne(AOFFactory.INSTANCE.createPair(c, rule)).collectTo((IUnaryFunction)facade.forwardApply).get(0);
		return capsule;
	}

	protected EClass findTargetEClass(String className) {
		// TODO: get access to target metamodel without resorting to such ends
		EObject capsule = findTargetCapsule("Top");
		return findOne(capsule.eClass().eResource(), capsule.eClass().eClass(), "name", className);
	}

	private static void applyStereotype(org.eclipse.uml2.uml.Class c, Stereotype s) {
		System.out.println("Applying stereotype " + s.getName() + " to class " + c.getName());
		c.applyStereotype(s);	// takes some time
	}

	protected void check() {
		if(true) {
			fullCheck();
			return;
		}
		Model model = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getModel());

		EObject modelRT = umlRTSample.getContents().get(0);

		EList<PackageableElement> packagedElements = model.getPackagedElements();

		Collection<?> actualOut = passiveGet(modelRT, "classes", "name");
		Collection<?> actualOutTypes = passiveGet(modelRT, "classes", "#type", "name");

		EObject expectedModelRT = recomputeModelRT();
		Collection<?> expectedOut = passiveGet(expectedModelRT, "classes", "name");
		Collection<?> expectedOutTypes = passiveGet(expectedModelRT, "classes", "#type", "name");

		System.out.println("actual out:\t" + actualOut);
		System.out.println("actual types:\t" + actualOutTypes);
		System.out.println("expected:\t" + expectedOut);
		System.out.println("expected types:\t" + expectedOutTypes);
		assertEquals(expectedOut, actualOut);
		assertEquals(expectedOutTypes, actualOutTypes);
	}

	private Resource recomputeFacade() {
		Facade recomputationFacade = new UMLRTFacade();
		recomputationFacade.addSource(umlRTProfileSample);

		ResourceSet rs = new ResourceSetImpl();
		Resource recomputedUMLRTSample = rs.createResource(URI.createFileURI("NOCOMMIT/sample-UMLRT-check.xmi"));
		recomputationFacade.addTarget(new FacadeModel(recomputedUMLRTSample));
		recomputationFacade.init();

		return recomputedUMLRTSample;
	}

	private static void removeProfile(org.eclipse.uml2.uml.Class c, Stereotype s) {
		System.out.println("Removing stereotype " + s.getName() + " from class " + c.getName());
		c.unapplyStereotype(s);
	}

	protected int testMove(String className, Integer...indices) {
		Model model = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getModel());
		EObject c = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getClass_(), "name", className);

		EObject modelRT = umlRTSample.getContents().get(0);

		EList<PackageableElement> packagedElements = model.getPackagedElements();

		Object inBefore = passiveGet(model, "packagedElement", "name");
		Object outBefore = passiveGet(modelRT, "classes", "name");

		if(indices.length == 0) {
			indices = new Integer[] {1, 0};
			System.out.println("Warning: no indices specified, defaulting to " + Arrays.asList(indices));
		}
		int oldIndex = packagedElements.indexOf(c);
		for(int index : indices) {
			move(packagedElements, c, index);

			System.out.println("IN before:\t" + inBefore);
			System.out.println("OUT before:\t" + outBefore);
			System.out.println("IN after:\t" + passiveGet(model, "packagedElement", "name"));
			Collection<?> actualOut = passiveGet(modelRT, "classes", "name");
			System.out.println("OUT after:\t" + actualOut);

			Collection<?> expectedOut = recomputeTargetClasses(packagedElements);

			System.out.println("expected:\t" + expectedOut);
			assertEquals(expectedOut, actualOut);
		}
		return oldIndex;
	}

	private EObject recomputeModelRT() {
		Resource recomputedUMLRTSample = recomputeFacade();
		return recomputedUMLRTSample.getContents().get(0);
	}

	private Collection<?> recomputeTargetClasses(List<PackageableElement> packagedElements) {
/*
		// Obsolete partial recomputation (obsolete because now PassiveClass rule also contributes to the collection
		return Utils.asList(

			Utils.get(AOFFactory.INSTANCE.createOrderedSet(packagedElements.toArray(new Element[0]))
			.select(new IUnaryFunction<Element, Boolean>() {
				@Override
				public Boolean apply(Element a) {
					if(a instanceof org.eclipse.uml2.uml.Class) {
						if(a.getAppliedStereotype(CAPSULE) != null) {
							return true;
						}
					}
					return false;
				}
			}), "name")
		);
/*/
		// Improved solution (but more costly): recompute the whole transformation AFTER the changes
		EObject modelRT = recomputeModelRT();
		return passiveGet(modelRT, "classes", "name");
/**/
	}

	protected static void move(EList<PackageableElement> packagedElements, EObject c, int newPos) {
		System.out.println("Moving " + c.eClass().getName() + " " + Utils.get(c, "name") + " to position " + newPos);
		packagedElements.move(newPos, packagedElements.indexOf(c));
	}

	protected static <A> A findOne(Resource resource, EClass type) {
		return findOne(resource, type, null, null);
	}

	protected static <A> A findOne(Resource resource, EClass type, String propertyName, Object value) {
		Collection<EObject> c = find(resource, type, propertyName, value);
		if(c.isEmpty()) {
			return null;
		} else {
			return (A)c.iterator().next();
		}
	}

	protected static Collection<EObject> find(Resource resource, EClass type, String propertyName, Object value) {
		Collection<EObject> ret = new ArrayList<EObject>();

		for(Iterator<EObject> i = resource.getAllContents() ; i.hasNext() ; ) {
			EObject eo = i.next();
			if(type == null || type.isInstance(eo)) {
				if(propertyName == null) {
					ret.add(eo);
				} else {
					EStructuralFeature feature = eo.eClass().getEStructuralFeature(propertyName);

					if(feature != null && (value == null ? eo.eGet(feature) == null : value.equals(eo.eGet(feature)))) {
						ret.add(eo);
					}
				}
			}
		}

		return ret;
	}

	private static Resource load(ResourceSet rs, String path) throws IOException {
//		Resource ret = rs.createResource(createFileURI(path));
//		Map<String, Object> options = new HashMap<>();
//		ret.load(options);
		Resource ret = rs.getResource(createFileURI(path), true);
		System.out.println("Loaded: " + ret);
		return ret;
	}

	private static URI createFileURI(String path) {
		return URI.createFileURI(new File(path).getAbsolutePath());
	}

	private static void defineProfiles(String uri) {
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.getResource(URI.createURI(uri), true);
		for(Iterator<EObject> i = r.getAllContents() ; i.hasNext() ; ) {
			EObject eo = i.next();
			if(eo instanceof Profile) {
				Profile profile = (Profile)eo;
				System.out.println("profile: " + eo + " defined: " + profile.isDefined());
				if(!profile.isDefined()) {
					ProfileOperations.define(profile);
					// we now need to register the definition, otherwise it won't be there anymore once the profile is loaded again in another resource set
					if(profile.isDefined()) {
						EPackage definition = profile.getDefinition();
						EPackage.Registry.INSTANCE.put(definition.getNsURI(), definition);
						Resource rd = rs.createResource(URI.createURI("test.ecore"));

						// plus we need to detach it from its owning EAnnotation, otherwise it will be found to be a different parent EAnnotation than expected when applying stereotypes
						//((Collection)definition.eContainer().eGet(definition.eContainingFeature())).clear();
						// but because we also need the definition to be in a resource, we can simply move it in one
						rd.getContents().add(definition);
					}
				}
			}
		}	
	}

	protected void fullCheck() {
		fullCheck(null);
	}

	protected void fullCheck(String msg) {
		Resource newTargetModel = this.recomputeFacade();
		assertTrue(msg, compare("/contents", newTargetModel.getContents(), umlRTSample.getContents(), new HashSet<EObject>()));
	}

	private boolean compare(String path, Collection c1, Collection c2, Set<EObject> traversedElements) {
		if(c1.size() != c2.size()) {
			System.out.println("Size difference at " + path + ": " + Inspect.toString(c1, null) + " != " + Inspect.toString(c2, null));
			return false;
		}
		boolean ret = true;
		int i = 0;
		for(Iterator i1 = c1.iterator(), i2 = c2.iterator() ; i1.hasNext() && i2.hasNext() ; ) {
			Object e1 = i1.next();
			Object e2 = i2.next();
			String newPath = path + "[" + i + "]";
			boolean r;
			if(e1 == null || e2 == null) {
				ret &= r = e1 == e2;
				if(!r) {
					System.out.println("Existence difference at " + newPath + ": " + Inspect.toString(e1, null) + " != " + Inspect.toString(e2, null));
				}
			} else if(e1 instanceof EObject && e2 instanceof EObject) {
				ret &= compare(newPath, (EObject)e1, (EObject)e2, traversedElements);
			} else {
				ret &= r = e1.equals(e2);
				if(!r) {
					System.out.println("Equality difference at " + newPath + ": " + Inspect.toString(e1, null) + " != " + Inspect.toString(e2, null));
				}
			}
			i++;
		}

		return ret;
	}

	private boolean compare(String path, EObject eo1, EObject eo2, Set<EObject> traversedElements) {
		if(traversedElements.contains(eo1) || traversedElements.contains(eo2)) {
			return true;
		}

		traversedElements.add(eo1);
		traversedElements.add(eo2);

		// TODO: compare eClasses once the metamodel is loaded only once
//		if(eo1.eClass() != eo2.eClass()) {
		if(!eo1.eClass().getName().equals(eo2.eClass().getName())) {
			System.out.println("Type difference at " + path + ": " + Inspect.toString(eo1, null) + " != " + Inspect.toString(eo2, null));
			return false;
		}
		boolean ret = true;
		for(EStructuralFeature feature : eo1.eClass().getEAllStructuralFeatures()) {
			Object v1 = eo1.eGet(feature);
//			Object v2 = eo2.eGet(feature);	// TODO: once we have the same metamodel
			Object v2 = eo2.eGet(eo2.eClass().getEStructuralFeature(feature.getName()));
			String newPath = path + "/" + feature.getName();
			boolean r;
			if(v1 == null) {
				ret &= r = v2 == null;
				if(!r) {
					System.out.println("Existence difference at " + newPath + ": " + Inspect.toString(v1, null) + " != " + Inspect.toString(v2, null));
				}
			} else if(feature instanceof EAttribute) {
				if(v1 instanceof EEnumLiteral && v2 instanceof EEnumLiteral) {	// TODO: useless once same metamodel is used
					ret &= r = ((EEnumLiteral)v1).getLiteral().equals(((EEnumLiteral)v2).getLiteral());
				} else {
					ret &= r = v1.equals(v2);
				}
				if(!r) {
					System.out.println("Equality difference at " + newPath + ": " + Inspect.toString(v1, null) + " != " + Inspect.toString(v2, null));
				}
			} else if(feature.isMany()) {
				ret &= compare(newPath, (Collection)v1, (Collection)v2, traversedElements);
			} else {
				ret &= compare(newPath, (EObject)v1, (EObject)v2, traversedElements);
			}
		}

		return ret;
	}

	// to be registered in Eclipse debugger as a DetailedFormater for java.lang.Object:
	// org.eclipse.papyrus.aof.core.impl.operation.Inspect.class.getDeclaredMethod("toString", new Class[] {Object.class, org.eclipse.papyrus.aof.core.IUnaryFunction.class}).invoke(null, this, null)
	// Note the use of reflection to call the right toString method even for boxes
	// or, now that toString(object) exsists:
	// org.eclipse.papyrus.aof.core.impl.operation.Inspect.toString(this)
	// TODO: improve this
	public String toString(Object o) {
		boolean showBoxValue = !true;
		StringBuffer ret = new StringBuffer();
		if(o instanceof Bind) {
			ret.append(toString(javaGet(o, "leftBox")));
			ret.append(":=:");
			ret.append(toString(javaGet(o, "rightBox")));
		} else if(o instanceof Operation){
			Operation op = (Operation)o;
			String opName = op.getClass().getSimpleName();
			if("Zip".equals(opName) || "ZipWith".equals(opName)) {
				ret.append(toString(javaGet((ZipWith)op, ZipWith.class, "leftBox")));
				ret.append(".");
				ret.append(opName);
				ret.append("(");
				ret.append(toString(javaGet((ZipWith)op, ZipWith.class, "rightBox")));
				ret.append(")");
			} else {
				ret.append(toString(javaGet(op, "sourceBox")));
				ret.append(".");
				ret.append(opName);
				if("CollectBox".equals(opName)) {
					Object collector = javaGet(op, "collector");
					ret.append("(");
					ret.append(toString(collector));
					ret.append(")");
				}
			}
		} else if("org.eclipse.papyrus.aof.emf.impl.EMFMetaClass$PropertyAccessor".equals(o.getClass().getName())) {
			EStructuralFeature feature = javaGet(o, "feature");
			ret.append(feature.getEContainingClass().getName());
			ret.append(".");
			ret.append(feature.getName());
		} else if(o.getClass().getEnclosingMethod() != null && "hasStereotypeAccessor".equals(o.getClass().getEnclosingMethod().getName())) {
			String stereotypeName = javaGet(o, "val$stereotypeName");
			ret.append("hasStereotype(");
			ret.append(stereotypeName);
			ret.append(")");
		} else if(o instanceof BaseDelegate<?>){
			BaseDelegate delegate = (BaseDelegate)o;
			ret.append(delegate.getClass().getSimpleName() + " of ");
			ret.append(toString(javaGet(delegate, BaseDelegate.class, "delegator")));
		} else if(o instanceof Box){
			Box box = (Box)o;
			BaseDelegate delegate = javaGet(box, Box.class, "delegate");

			if(delegate instanceof FeatureDelegate) {
				EStructuralFeature f = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "feature");
				EObject eo = javaGet((FeatureDelegate)delegate, FeatureDelegate.class, "object");
				ret.append(Inspect.toString(eo, null) + "." + f.getName());
				if(showBoxValue) {
					ret.append("=");
					ret.append(Inspect.toString(box, null));
					ret.append("\n");
				}
			} else {
				for(IObserver observer : ((Box<?>)o).getObservers()) {
					String observerClassName = observer.getClass().getName();
					if(observerClassName.equals(OPERATION_PACKAGE + "Operation$SilentObserver")) {	// necessary because SilentObserver is private
						observer = javaGet(observer, "wrappedObserver");
						observerClassName = observer.getClass().getName();
					}
					String label = observer.getClass().getSimpleName().replaceAll("Observer$", "");
					if("Result".equals(label)) {
						Object op = javaGet(observer, "this$0");	// remark: Bind does not extend Operation
						ret.append(toString(op));
					}
				}
				if(ret.length() == 0) {
					ret.append(Inspect.toString(box, null));
				} else if(showBoxValue) {
					ret.append("=");
					ret.append(Inspect.toString(box, null));
					ret.append("\n");
				}
			}
		} else if(o instanceof IObserver<?>){
			IObserver<?> observer = (IObserver)o;
			String observerClassName = observer.getClass().getName();
			if(observerClassName.equals(OPERATION_PACKAGE + "Operation$SilentObserver")) {	// necessary because SilentObserver is private
				ret.append("_");
				observer = javaGet(observer, "wrappedObserver");
				observerClassName = observer.getClass().getName();
			}
			Object op = javaGet(observer, "this$0");	// remark: Bind does not extend Operation
			ret.append(observer.getClass().getSimpleName());
			ret.append(" of ");
			ret.append(toString(op));
		} else {
			ret.append(o.toString());
		}
		return ret.toString();
	}
}
