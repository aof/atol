package org.eclipse.papyrus.facade.exploration;

//would only be useful for unidirectional transformations (possibly with bidirectional bindings)
public interface TargetType<C> {

	C newInstance();
}
