package org.eclipse.papyrus.facade.exploration;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.EditingDomain;

public class FacadeModel {
    private Resource resource;
    private EditingDomain editingDomain;

    public FacadeModel(Resource resource) {
            this(resource, null);
    }

    public FacadeModel(Resource resource, EditingDomain editingDomain) {
            this.resource = resource;
            this.editingDomain = editingDomain;
    }

    public Resource getResource() {
            return this.resource;
    }

    public EditingDomain getEditingDomain() {
            return this.editingDomain;
    }

    public boolean hasEditingDomain() {
            return this.editingDomain != null;
    }
}
