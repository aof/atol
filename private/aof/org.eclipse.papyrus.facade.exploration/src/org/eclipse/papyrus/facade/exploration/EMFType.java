package org.eclipse.papyrus.facade.exploration;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class EMFType<T> implements Type<T> {

	private EClass eClass;

	public EMFType(EClass eClass) {
		this.eClass = eClass;
	}

	@Override
	public boolean isInstance(EObject element) {
		return this.eClass.isInstance(element);
	}

	@Override
	public T newInstance() {
		return (T)this.eClass.getEPackage().getEFactoryInstance().create(this.eClass);
	}

	public boolean equals(Object other) {
		return (other instanceof EMFType) && ((EMFType)other).eClass == this.eClass;
	}

	public int hashCode() {
		return this.eClass.hashCode() * 10 + 1;
	}

	// TODO: because of this, we should probably simply directly use EClass everywhere in Rule and Facade instead of Type
	// otherwise isSubTypeOf(Type) would become uselessly complex
	@Override
	public boolean isSubTypeOf(EClass sourceType) {
		return sourceType.isSuperTypeOf(this.eClass);
	}
}
