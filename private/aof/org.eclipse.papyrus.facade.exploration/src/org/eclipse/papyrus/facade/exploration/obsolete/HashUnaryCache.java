package org.eclipse.papyrus.facade.exploration.obsolete;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.papyrus.aof.core.impl.utils.cache.IUnaryCache;

public class HashUnaryCache<K, V> implements IUnaryCache<K, V> {
	private Map<K, V> cache = new HashMap<K, V>();

	public V get(K key) {
		return cache.get(key);
	}

	public void put(K key, V value) {
		cache.put(key, value);
	}
}