package org.eclipse.papyrus.facade.exploration.ui;

import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.papyrus.facade.exploration.FacadeModel;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class Select implements IObjectActionDelegate {

    static Queue<FacadeModel> selectedResources = new LinkedList<FacadeModel>();
    private ISelection selection;

    public Select() {
            // TODO Auto-generated constructor stub
    }

    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
            // TODO Auto-generated method stub

    }

    public void run(IAction action) {
            Object selectedElement = ((StructuredSelection)selection).getFirstElement();
            FacadeModel incrementalModel = null;
            if(selectedElement instanceof EObject) {
                    incrementalModel = new FacadeModel(((EObject)selectedElement).eResource());
            } else if(selectedElement instanceof IGraphicalEditPart) {
                    IGraphicalEditPart gep = (IGraphicalEditPart)selectedElement;
                    incrementalModel = new FacadeModel(gep.resolveSemanticElement().eResource(), gep.getEditingDomain());
            }
            selectedResources.add(incrementalModel);
    }

    public void selectionChanged(IAction action, ISelection selection) {
            this.selection = selection;
    }

}
