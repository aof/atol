package org.eclipse.papyrus.facade.exploration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IFactory;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.operation.CollectBox;
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver;
import org.eclipse.papyrus.aof.emf.EMFFactory;

public class Utils {

	static IFactory emf = EMFFactory.INSTANCE;

	protected static <A> IUnaryFunction<A, A> getId() {
		return (IUnaryFunction<A, A>)new IUnaryFunction<Object, Object>() {
			@Override
			public Object apply(Object a) {
				return a;
			}
		};
	}

	protected static <A extends EObject> IBox<A> createEPropertyBox(EObject target, String propertyName) {
		return (IBox<A>)createPropertyBox(target, propertyName);
	}

	public static <A> IBox<A> createPropertyBox(EObject target, String propertyName) {
		EStructuralFeature property = target.eClass().getEStructuralFeature(propertyName);
		if(property == null) {
			throw new IllegalArgumentException("property " + propertyName + " not found on " + target);
		}
		return emf.createPropertyBox(target, property);
	}

	public static <A> A create(Resource targetMM, String className) {
		EClass targetClass = findClass(targetMM, className);
		return (A)create(targetClass);
	}

	protected static <A> A create(EClass targetClass) {
		EObject target = targetClass.getEPackage().getEFactoryInstance().create(targetClass);
		return (A)target;
	}

	protected static EClass findClass(Resource resource, String name) {
		for(EObject eo : getAllContents(resource)) {
			if(eo instanceof EClass && ((EClass)eo).getName().equals(name)) {
				return (EClass)eo;
			}
		}
		return null;
	}

	protected static Iterable<EObject> getAllContents(final Resource resource) {
		return new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return resource.getAllContents();
			}
		};
	}

//	private static class GetFunction<A> implements IUnaryFunction<EObject, A> {
//		private String propertyName;
//
//		public GetFunction(String propertyName) {
//			this.propertyName = propertyName;
//		}
//
//		@Override
//		public A apply(EObject a) {
//			return get(a, this.propertyName);
//		}
//	}

	protected static <A> IBox<A> get(IBox<? extends EObject> target, final String propertyName) {
		return target.collectTo(
/*
			new GetFunction(propertyName)
/*/
			new IUnaryFunction<EObject, A>() {
				@Override
				public A apply(EObject a) {
					return get(a, propertyName);
				}
			}
/**/
		);
	}

	public static <A> Collection<A> passiveGet(Collection<? extends EObject> eos, String...propertyNames) {
		return passiveGetInternal(eos, propertyNames);
	}

	public static <A> Collection<A> passiveGet(EObject eo, String...propertyNames) {
		Collection<EObject> ret = new ArrayList<EObject>();
		ret.add(eo);
		return passiveGetInternal(ret, propertyNames);
	}

	private static <A> Collection<A> passiveGetInternal(Collection<?> in, String...propertyNames) {
		Collection<?> ret = in;
		for(String propertyName : propertyNames) {
			Collection<Object> newRet = new ArrayList<Object>();
			for(EObject o : (Collection<EObject>)ret) {
				Object v;
				if("#type".equals(propertyName)) {
					v = o.eClass();
				} else {
					v = o.eGet(o.eClass().getEStructuralFeature(propertyName));
				}
				if(v instanceof Collection) {
					newRet.addAll((Collection<?>)v);
				} else {
					newRet.add(v);
				}
			}
			ret = newRet;
		}
		return (Collection<A>)ret;
	}

	public static <A> IBox<A> path(IBox<? extends EObject> target, EClass type, final String propertyName) {
		return target.collectMutable(emf, type, type.getEStructuralFeature(propertyName));
	}

	protected static <T> T get(EObject target, String propertyName) {
		EStructuralFeature property = target.eClass().getEStructuralFeature(propertyName);
		if(property == null) {
			throw new IllegalArgumentException("property " + propertyName + " not found on " + target);
		}
		return (T)target.eGet(property);
	}

	protected static EObject set(EObject target, String propertyName, Object value) {
		EStructuralFeature property = target.eClass().getEStructuralFeature(propertyName);
		if(property == null) {
			throw new IllegalArgumentException("property " + propertyName + " not found on " + target);
		}
		target.eSet(property, value);
		return target;
	}

	public static <A, C extends A> UMLRTFacade.Predicate<A> isOfType(final Class<C> type) {
		return new UMLRTFacade.Predicate<A>() {
			@Override
			public Boolean apply(A a) {
				return type.isInstance(a);
			}
		};
	}

	// and B super C | C extends B
	public static <A, C extends A> IBox<C> select(IBox<A> target, final Class<C> type) {
		IUnaryFunction<A, Boolean> selector = isOfType(type);
		return (IBox<C>)target.select(selector);
	}

	public static <A> IBox<A> select(IBox<A> target, final EClassifier type) {
		return (IBox<A>)target.select(new IUnaryFunction<Object, Boolean>() {
			@Override
			public Boolean apply(Object a) {
				return type.isInstance(a);
			}
		});
	}

	// This is only used to tag the box for serializePipes
	public static class TaggingObserver extends DefaultObserver {
		public final String tag;
		public TaggingObserver(String tag) {
			this.tag = tag;
		}

		@Override
		public void added(int index, Object element) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void removed(int index, Object element) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void replaced(int index, Object oldElement, Object newElement) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void moved(int newIndex, int oldIndex, Object element) {
			// TODO Auto-generated method stub
			
		}
	}

	public static <A, B> IBox<B> path(IBox<A> target, IUnaryFunction<? super A, ? extends IBox<B>> f) {
		return path(target, null, f);
	}

	public static <A, B> IBox<B> path(IBox<A> target, A aDefault, IUnaryFunction<? super A, ? extends IBox<B>> f) {
		return new CollectBox<A, B>((IBox<A>)target, aDefault, f).getResult();
	}

//	private static class GetType<A> implements IUnaryFunction<EObject, EClass> {
//		@Override
//		public EClass apply(EObject a) {
//			return a.eClass();
//		}
//	}

	protected static IBox<EClass> getType(IBox<? extends EObject> target) {
		return target.collectTo(
/*
			new GetType()
/*/
			new IUnaryFunction<EObject, EClass>() {
				@Override
				public EClass apply(EObject a) {
					return a.eClass();
				}
			}
/**/
		);
	}

	// IUnaryFunction<? super A, IOne<Boolean>>
	protected static <A> IBox<A> select(IBox<A> target, IUnaryFunction predicate) {
		return target.selectMutable(path(target, predicate));
	}

	protected static IBox<Object> select(IBox<Object> target, final Class<?> type, final Object value) {
		return filter(target, type, value, true);
	}

	protected static IBox<Object> reject(IBox<Object> target, final Class<?> type, final Object value) {
		return filter(target, type, value, false);
	}

	protected static IBox<Object> filter(IBox<Object> target, final Class<?> type, final Object value, final boolean invert) {
		return target.selectMutable(target.collect(new IUnaryFunction<Object, Boolean>() {
			@Override
			public Boolean apply(Object a) {
				if(a == null) {
					return (value != null) ^ invert;
				} else {
					return (!a.equals(value)) ^ invert;
				}
			}
		}).inspect("bool"));
	}

	public static IBox<EObject> select(IBox<? extends EObject> target, final EClass type, final String propertyName, final Object value) {
		return filter(target, type, propertyName, value, true);
	}

	public static IBox<EObject> reject(IBox<? extends EObject> target, final EClass type, final String propertyName, final Object value) {
		return filter(target, type, propertyName, value, false);
	}

	protected static IBox<EObject> filter(IBox<? extends EObject> target, final EClass type, final String propertyName, final Object value, final boolean invert) {
		return (IBox<EObject>)target.inspect("source: ").selectMutable(path(target, type, propertyName).inspect("values: ").collect(new IUnaryFunction<Object, Boolean>() {
			@Override
			public Boolean apply(Object a) {
				if(a == null) {
					return (value != null) ^ invert;
				} else {
					return (!a.equals(value)) ^ invert;
				}
			}
		}).inspect("bool"));
	}

	public static <A extends EObject, B extends EObject> IBox<B> inspectProperty(IBox<A> target, String label, final String propertyName) {
		return inspectProperty(target, label, propertyName, false);
	}

	public static <A extends EObject> IUnaryFunction<A, String> get(final String propertyName, final Boolean andType) {
/*
		return new IUnaryFunction<A, String>() {
			@Override
			public String apply(A a) {
				if(a == null) {
					return "null";
				} else {
					return "" + get(a, propertyName) + (andType ? ":" + a.eClass().getName() : "");
				}
			}
		};
/*/
		return new IUnaryFunction<A, String>() {
			@Override
			public String apply(EObject a) {
				if(a == null) {
					return "null";
				} else {
					EStructuralFeature f = a.eClass().getEStructuralFeature(propertyName);
					String ret = "";
					if(f != null) {
						ret += a.eGet(f) + ":";
					}
					if(andType) {
						ret += a.eClass().getName();
					}
					return ret;
				}
			}
		};
/**/
	}

	public static <A extends EObject, B extends EObject> IBox<B> inspectProperty(IBox<A> target, String label, final String propertyName, final boolean andType) {
		return (IBox<B>)target.inspect(label, get(propertyName, andType));
	}

	public static <A extends EObject, B extends EObject> IBox<B> inspectType(IBox<A> target, String label) {
		return (IBox<B>)target.inspect(label, new IUnaryFunction<EObject, String>() {
			@Override
			public String apply(EObject a) {
				if(a == null) {
					return "null";
				} else {
					return a.eClass().getName();
				}
			}
		});
	}

	protected static <A, B> IBox<B> apply(IBox<A> target, IUnaryFunction<? super A, ? extends B> function) {
		return target.collect(function);
	}

	protected static <A> IUnaryFunction<Boolean, A> getIf(final A thenValue, final A elseValue) {
		return new IUnaryFunction<Boolean, A>() {
			@Override
			public A apply(Boolean a) {
				if(a) {
					return thenValue;
				} else {
					return elseValue;
				}
			}
		};
	}

	protected static <A> IBox<A> createOne(A element) {
		return AOFFactory.INSTANCE.createOne(element);
	}

/* obsolete now that zipWith is in IBox, and actually it is IBox.zip, which is implemented in terms of ZipWith
	protected static <A, B, C> IBox<C> zipWith(IBox<A> first, IBox<B> second, final IBinaryFunction<A, B, C> f) {
		return first.zip(second).collect(new IUnaryFunction<IPair<A, B>, C>() {
			@Override
			public C apply(IPair<A, B> p) {
				return f.apply(p.getFirst(), p.getSecond());
			}
		});
	}
*/

	// unidirectional!
	protected static <C, A extends C, B extends C> IBox<C> union(IBox<A> a, IBox<B> b) {
		IBox<IBox> l = AOFFactory.INSTANCE.createSequence();
		l.add(a);
		l.add(b);
		return path(l, emf.createSequence(), (IUnaryFunction)getId());	// flatten
	}

	public static <A> List<A> asList(IBox<A> a) {
		List<A> ret = new ArrayList<A>();
		for(A e : a) {
			ret.add(e);
		}
		return ret;
	}
}
