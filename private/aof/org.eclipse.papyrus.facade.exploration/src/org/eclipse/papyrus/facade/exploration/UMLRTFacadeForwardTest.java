package org.eclipse.papyrus.facade.exploration;

import static org.eclipse.papyrus.facade.exploration.Utils.create;
import static org.eclipse.papyrus.facade.exploration.Utils.get;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;
import org.junit.Test;

// Incrementality tests
public class UMLRTFacadeForwardTest extends UMLRTFacadeTest {
	@Test
	public void testRemoveCapsule() {
		removeClass("SubPartClass");
	}

	@Test
	public void testRemovePassive() {
		removeClass("Passive");
	}

	public void removeClass(String className) {
		Model model = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getModel());
		org.eclipse.uml2.uml.Class passive = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getClass_(), "name", className);
		System.out.println("Removing class " + className);
		model.getPackagedElements().remove(passive);
		check();
	}

	@Test
	public void testMoveCapsuledPassiveTo0() {
		testApplyStereotype();
		testMovePassiveTo0();
	}

	@Test
	public void testMovePassiveTo0() {
		testMove("Passive", 0);
	}

	@Test
	public void testMoveCapsuledPassiveTo1() {
		testApplyStereotype();
		testMovePassiveTo1();
	}

	@Test
	public void testMovePassiveTo1() {
		List<List> preSnapshot = takeSnapshot();
		testMove("Passive", 1);
		List<List> postSnapshot = takeSnapshot();
		compareSnapshots(preSnapshot, postSnapshot);
	}

	@Test
	public void testMovePassiveTo7AndBack() {
		movePassiveAndBack(7);
	}

	@Test
	public void testMovePassiveTo6AndBack() {
		movePassiveAndBack(6);
	}

	@Test
	public void testMovePassiveTo5AndBack() {
		movePassiveAndBack(5);
	}

	@Test
	public void testMovePassiveTo4AndBack() {
		movePassiveAndBack(4);
	}

	@Test
	public void testMovePassiveTo3AndBack() {
		movePassiveAndBack(3);
	}

	@Test
	public void testMovePassiveTo2AndBack() {
		movePassiveAndBack(2);
	}

	// TODO: THIS FAILS until rule disabling works 
	@Test
	public void testMovePassiveTo1AndBack() {
		movePassiveAndBack(1);
	}

/* Fail for 1
	@Test
	public void testMovePassiveClassToAllAndBack() {
		moveToAllAndBack("Passive");
	}
/**/

/* No fail
	@Test
	public void testMoveSubPartClassToAllAndBack() {
		moveToAllAndBack("SubPartClass");
	}
/**/

	protected void moveToAllAndBack(String className) {
		for(int i = 0 ; i < 8 ; i++) {
			moveAndBack(className, i);			
		}
	}

	protected void movePassiveAndBack(int toIndex) {
		moveAndBack("Passive", toIndex);
	}

	protected void moveAndBack(String className, int toIndex) {
//		Object prev = get(findClass("PartClass"), "classifierBehavior");
		List<List> preSnapshot = takeSnapshot();
		int oldIndex = testMove(className, toIndex);
		List<List> middleSnapshot = takeSnapshot();
		testMove(className, oldIndex);
		List<List> postSnapshot = takeSnapshot();
		compareSnapshots(preSnapshot, middleSnapshot);
		compareSnapshots(preSnapshot, postSnapshot);
//		System.out.println(prev + " -> " + get(findClass("PartClass"), "classifierBehavior"));
		assertEquals(preSnapshot, postSnapshot);
	}

	@Test
	public void testMovePassiveAround() {
		int oldIndex = testMove("Passive", 0);
		testMove("Passive", 1);
		testMove("Passive", 2);
	}

	@Test
	public void testMoveCapsuledPassiveTo1Then0() {
		testApplyStereotype();
		testMovePassiveTo1Then0();
	}

	@Test
	public void testMovePassiveTo1Then0() {
		testMove("Passive", 1, 0);
	}

	@Test
	public void testCapsuleName() {
		org.eclipse.uml2.uml.Class c = findClass("Top");
		EObject capsule = findTargetCapsule("Top");
		assertEquals(c.getName(), get(capsule, "name"));
	}

	@Test
	public void testClassRenaming() {
		org.eclipse.uml2.uml.Class c = findClass("Top");
//TODO: find what this crashes		EObject capsule = AOFFactory.INSTANCE.createOne(c).collectRefs((IUnaryFunction<org.eclipse.uml2.uml.Class, EObject>)facade.getRule("Class2Capsule")).get(0);
		EObject capsule = findTargetCapsule("Top");

		c.setName("Test");

		assertEquals(c.getName(), get(capsule, "name"));
	}

	// TODO: add checks, so far only detects exceptions
	@Test
	public void testMoveProtocol() {
		org.eclipse.uml2.uml.Package p = findOne(umlRTProfileSample, UMLPackage.eINSTANCE.getPackage(), "name", "MyProt");
		Collaboration newProt = create(UMLPackage.eINSTANCE.getCollaboration());
		newProt.setName("newProt");
		p.getPackagedElements().add(newProt);
//		System.out.println(newProt.getApplicableStereotypes());
		Stereotype stereotype = newProt.getApplicableStereotype("UMLRealTime::Protocol");
		newProt.applyStereotype(stereotype);
		move(p.getPackagedElements(), newProt, 0);
		move(p.getPackagedElements(), newProt, 1);
	}

	@Test
	public void testApplyStereotype() {
		applyStereotype("Passive", CAPSULE);
		check();
	}

	@Test
	public void testApplyThenUnapplyStereotype() {
		List<List> preSnapshot = takeSnapshot();
		applyStereotype("Passive", CAPSULE);
		removeStereotype("Passive", CAPSULE);
		List<List> postSnapshot = takeSnapshot();
		compareSnapshots(preSnapshot, postSnapshot);
		assertEquals(preSnapshot, postSnapshot);
		check();
	}

	@Test
	public void testUnapplyThenApplyStereotype() {
		List<List> preSnapshot = takeSnapshot();
		removeStereotype("SubPartClass", CAPSULE);
		applyStereotype("SubPartClass", CAPSULE);
		List<List> postSnapshot = takeSnapshot();
		compareSnapshots(preSnapshot, postSnapshot);
		assertEquals(preSnapshot, postSnapshot);
		check();
	}

	// TODO replace by test based on object identity rather than on accessing Cache.sources, which does not exist anymore
//	@Test
//	public void testCache() {
//		applyStereotype("Passive", CAPSULE);
//		Collection c = javaGet(CollectTo.getCache(), "sources");
//		int preCacheSize = c.size();
//		removeStereotype("Passive", CAPSULE);
//		int postCacheSize = c.size();
//		check();
//		assertEquals(preCacheSize, postCacheSize);
//		System.out.println("Cache size: " + preCacheSize);
//	}

	@Test
	public void fuzzingTest() {
		// seed = 2 fails @36 on RTState.base_State unset Started:State
		// seed = 1 fails @155 on Port.associationEnd <- new ExtensionEnd
		// Older:
		// seed = 5 fails @675 on testRemoveRTPortStereotype (p1)
		// seed = 1 fails @464 on testRemoveRTPortStereotype (p2)
		// seed = 0 fails @947 on OutOfMemoryError
		ModelFuzzer fuzzer = new ModelFuzzer(this.umlRTProfileSample, 2);
		int i = 0;
		while(true) {
			System.out.println("Change number " + ++i);
			fuzzer.performOneChange();
		//	fullCheck();
		}
	}

	private UMLPackage UML = UMLPackage.eINSTANCE;

	@Test
	public void testUnsettingPseudostateState() {
		Pseudostate ps = findOne(umlRTProfileSample, UML.getPseudostate());
		ps.eUnset(UML.getPseudostate_State());
		fullCheck();
	}

	@Test
	public void testRemoveSubVertex() {
		Region region = findOne(umlRTProfileSample, UML.getRegion());
		List<Vertex> subvertices = region.getSubvertices();
		subvertices.remove(0);
		fullCheck();
	}

	// actually reproduce bug identified by fuzzer (currently ok here) & add check
	// see testUnapplyCapsulePartStereotypeOnLeft
//	@Test
//	public void testRemoveRTPortStereotype() {
//		Port p = findOne(umlRTProfileSample, UML.getPort(), "name", "p2");
//		System.out.println(p);
//		p.unapplyStereotype(p.getApplicableStereotype("UMLRealTime::RTPort"));
//	}

	// only triggered by setting base_Property, and not by unapplying
	// => apparently a bug in PapyrusStereotypeListener ! BUT also because we do not {un,}apply and
	// instead directly play with base_.* feature
	// from now on ModelFuzzer unsets such features before setting them
//	@Test
//	public void testUnapplyCapsulePartStereotypeOnLeft() {
//		Property p = findOne(umlRTProfileSample, UML.getProperty(), "name", "left");
//		EObject capsulePart = findOne(umlRTProfileSample, null, "base_Property", p);
//		System.out.println(capsulePart);
////		p.unapplyStereotype(p.getApplicableStereotype("UMLRealTime::CapsulePart"));
//		set(capsulePart, "base_Property", UML.getEFactoryInstance().create(UML.getPort()));
//		fullCheck();
//	}

	// TODO: try to reproduce this bug found by fuzzing with seed = 2...
	@Test
	public void testRemovingPartClassSM() {
//		org.eclipse.uml2.uml.Class subPartClass = findOne(umlRTProfileSample, UML.getClass_(), "name", "SubPartClass");
//		subPartClass.setClassifierBehavior(null);
		StateMachine subPartClassSM = findOne(umlRTProfileSample, UML.getStateMachine(), "name", "SubPartClassSM");
		StateMachine partClassSM = findOne(umlRTProfileSample, UML.getStateMachine(), "name", "PartClassSM");
		System.out.println(Inspect.toString(subPartClassSM, null) + " " + Inspect.toString(partClassSM, null));
		subPartClassSM.setClassifierBehavior(partClassSM);
		fullCheck();
	}

//	@Test
//	public void testAssociationOwnedEnd() {
//		Association association = findOne(umlRTProfileSample, UML.getAssociation());
//		Port p1 = findOne(umlRTProfileSample, UML.getPort(), "name", "p1");
//		Property top = findOne(umlRTProfileSample, UML.getProperty(), "name", "top");
//		List<Property> ownedEnd = association.getOwnedEnds();
//		ownedEnd.set(ownedEnd.indexOf(top), p1);
//	}
	@Test
	public void testRemovingRTPort() {
//		Port p1 = findOne(umlRTProfileSample, UML.getPort(), "name", "p1");
//		p1.unapplyStereotype(p1.getApplicableStereotype("UMLRealTime::RTPort"));
		org.eclipse.uml2.uml.Class partClass = findOne(umlRTProfileSample, UML.getClass_(), "name", "PartClass");
		partClass.getOwnedAttributes().remove(0);
		fullCheck();
	}

	@Test
	public void testRemovingRTState() {
		State state = findOne(umlRTProfileSample, UML.getState(), "name", "Started");
		state.unapplyStereotype(state.getApplicableStereotype("UMLRealTimeStateMach::RTState"));
		fullCheck();
	}
}
