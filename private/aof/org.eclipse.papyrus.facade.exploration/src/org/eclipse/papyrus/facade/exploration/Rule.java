package org.eclipse.papyrus.facade.exploration;

import static org.eclipse.papyrus.facade.exploration.Utils.createPropertyBox;
import static org.eclipse.papyrus.facade.exploration.Utils.select;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.aof.core.IBinding;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.One;
import org.eclipse.papyrus.aof.core.utils.Functions;
import org.eclipse.uml2.uml.Element;

public abstract class Rule<S extends EObject, T extends EObject> {

	// TODO: rename into forwardApply and forget about implementing IUnaryFunction
	public T forwardApply(S source) {
		T target = targetType.newInstance();
//		this.bindContents(source, target);
		return target;
	}

	public S reverseApply(T target) {
		S source = sourceType.newInstance();
		if(this.stereotype != null) {
			// TODO: problem is that stereotype can only be applied once element has been attached to model
			facade.applyStereotype((Element)source, stereotype);
		}
//		this.bindContents(source, target);
		return source;
	}

	public static class Bindings {

		private Facade facade;
		private boolean sourceToTarget;

		private Collection<IBinding<?>> bindings = new ArrayList<IBinding<?>>();
		private boolean disabled = false;

		public Bindings(Facade facade, boolean sourceToTarget) {
			this.facade = facade;
			this.sourceToTarget = sourceToTarget;
		}

		public void add(String propertyName, EObject targetElement, EObject sourceElement) {
			this.add(targetElement, propertyName, sourceElement, propertyName, null);
		}

		public void add(EObject targetElement, String targetPropertyName, EObject sourceElement, String sourcePropertyName) {
			this.add(targetElement, targetPropertyName, sourceElement, sourcePropertyName, null);
		}

		public void add(EObject targetElement, String targetPropertyName, EObject sourceElement, String sourcePropertyName, IUnaryFunction<?, IBox<Boolean>> mutablePredicate) {
			IBox source = createPropertyBox(sourceElement, sourcePropertyName);

			if(mutablePredicate != null) {
				source = select(source, (IUnaryFunction<EObject, IBox<Boolean>>)mutablePredicate);//.inspect(sourcePropertyName + ": ");
	//inspectProperty(src, "src2: ", "name", true);
			}

			EClass sourceType = null;
			EStructuralFeature sourceProperty = sourceElement.eClass().getEStructuralFeature(sourcePropertyName);
			if(sourceProperty instanceof EReference) {
				sourceType = (EClass)sourceProperty.getEType();
			}
			this.add(targetElement, targetPropertyName, source, sourceType);
		}

		public void add(EObject targetElement, String targetPropertyName, IBox source, EClass sourceType) {
			IBox target = createPropertyBox(targetElement, targetPropertyName);
			// TODO: check actual source type? (was source before class Bindings was introduced)
			EStructuralFeature targetFeature = targetElement.eClass().getEStructuralFeature(targetPropertyName);
			if(targetFeature instanceof EReference) {
				source = facade.transform(source, sourceType);
			}
			// We add a filter to avoid setting invalid values in the target model properties
			// this is necessary for two reasons:
			// - misalignment can potentially result in wrong values
			// - client code may put the source model in a temporary invalid state
			// Plus, invalid models should probably not crash the live transfo
			// TODO: document this precisely
			// BUT this creates other errors
			final EClassifier targetType = targetFeature.getEType();
/*
			source = source.select(new IUnaryFunction<Object, Boolean>() {
				@Override
				public Boolean apply(Object a) {
					return a == null || targetType.isInstance(a);
				}
			});
/*/
//			source = select(source, targetFeature.getEType());
			// select would create problems with opt and one
			// TODO: consider putting this inside the if enclosing the call to transform because it should not be need for primitive types
			if(target instanceof One<?>) {
				source = source.collect(new IUnaryFunction<Object, Object>() {
					@Override
					public Object apply(Object a) {
						if(targetType.isInstance(a)) {
							return a;
						} else {
							return null;
						}
					}
				}, Functions.identity());
			} else {
				source = select(source, targetType);
			}
/**/
			source = source.asBox(target);
			if(this.sourceToTarget) {
//				source = source.asBox(target);
				this.bindings.add(target.bind(source));
			} else {
//				target = target.asBox(source);
				this.bindings.add(source.bind(target));
			}
		}

		public void enable() {
			if(disabled) {
				disabled = false;
				for(IBinding<?> b : this.bindings) {
					b.enable();
				}
			} else {
				System.out.println();
			}
		}

		public void disable() {
			if(!disabled) {
				disabled = true;
				for(IBinding<?> b : this.bindings) {
					b.disable();
				}
			} else {
				System.out.println();
			}
		}

		// debug
		public String toString() {
			StringBuffer ret = new StringBuffer();

			for(IBinding binding : bindings) {
				ret.append(binding.getLeft() + " :=: " + binding.getRight());
				ret.append('\n');
			}

			return ret.toString();
		}
	}

	public abstract void bindContents(Bindings bindings, S source, T target);

	private String name;
	private Type<S> sourceType;
	private Type<T> targetType;
	private String stereotype;
	private IUnaryFunction<? super S, IBox<Boolean>> guard;
	private Facade facade;

	// DONE: extracted stereotype from guard so that at least this kind of source element aspect can be automatically reversed
	// but it is still the responsibility of the user to make sure the guard does not need further reversal
	// alternatively, hasStereotypeAccessor could be bidirectional (setting to false (resp. true) resulting in unapplying (resp. applying))...

	public Rule(Facade facade, String name, EClass sourceType, EClass targetType) {
		this(facade, name, sourceType, targetType, null, null);
	}

	public Rule(Facade facade, String name, EClass sourceType, EClass targetType, String stereotype) {
		this(facade, name, sourceType, targetType, stereotype, null);
	}

	public Rule(Facade facade, String name, final EClass sourceType, final EClass targetType, IUnaryFunction<? super S, IBox<Boolean>> guard) {
		this(facade, name, sourceType, targetType, null, guard);
	}

	// @param sourceType must be either a java.lang.Class, an EClass, or a String
	public Rule(Facade facade, String name, final EClass sourceType, final EClass targetType, String stereotype, IUnaryFunction<? super S, IBox<Boolean>> guard) {
		this.name = name;
		this.sourceType = new EMFType<S>(sourceType);
		this.targetType = new EMFType<T>(targetType);
		this.stereotype = stereotype;
		this.facade = facade;
		if(stereotype != null) {
			if(guard == null) {
				this.guard = (IUnaryFunction)facade.hasStereotypeAccessor(stereotype);
			} else {
				this.guard = facade.AND.compose(guard, (IUnaryFunction)facade.hasStereotypeAccessor(stereotype)).mergeInputs();
			}
		} else {
			this.guard = guard;
		}
	}

	public Type<S> getSourceType() {
		return this.sourceType;
	}

	public Type<T> getTargetType() {
		return this.targetType;
	}

	public IUnaryFunction<? super S, IBox<Boolean>> getGuard() {
		return this.guard;
	}

	public String toString() {
		return this.name;
	}

//	public void setFacade(Facade facade) {
//		this.facade = facade;
//	}
}
