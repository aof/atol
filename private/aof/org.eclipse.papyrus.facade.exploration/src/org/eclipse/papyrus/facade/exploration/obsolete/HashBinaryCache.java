package org.eclipse.papyrus.facade.exploration.obsolete;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.papyrus.aof.core.impl.utils.cache.IBinaryCache;

// could use a single HashMap<IPair<K1, K2>, V>
public class HashBinaryCache<K1, K2, V> implements IBinaryCache<K1, K2, V> {
	private Map<K1, Map<K2, V>> cache = new HashMap<K1, Map<K2,V>>();

	public V get(K1 key1, K2 key2) {
		Map<K2, V> cache2 = cache.get(key1);
		if(cache2 == null) {
			return null;
		} else {
			return cache2.get(key2);
		}
	}

	public void put(K1 key1, K2 key2, V value) {
		Map<K2, V> cache2 = cache.get(key1);
		if(cache2 == null) {
			cache2 = new HashMap<K2, V>();
			cache.put(key1, cache2);
		}
		cache2.put(key2, value);
	}
}