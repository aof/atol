package org.eclipse.papyrus.facade.exploration;

import org.eclipse.emf.ecore.EClass;

public interface Type<T> extends SourceType, TargetType<T> {

	boolean isSubTypeOf(EClass sourceType);

}
