package org.eclipse.papyrus.facade.exploration.myaoftests;

import static org.eclipse.papyrus.facade.exploration.Utils.inspectProperty;
import static org.eclipse.papyrus.facade.exploration.Utils.path;
import static org.eclipse.papyrus.facade.exploration.Utils.reject;
import static org.eclipse.papyrus.facade.exploration.Utils.select;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IFactory;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.emf.EMFFactory;
import org.junit.Test;

// Some tests in this class may end up in aof.core.tests or aof.emf.tests
public class MyAOFTests {

	private final static IFactory aof = EMFFactory.INSTANCE;

//	@Test
//	public void bibind() {
//		IBox<Integer> a = aof.createSequence(1, 2, 3, 4, 5).inspect("a: ");
//		IBox<Integer> b = aof.createSequence();
//		b.inspect("b: ");
//		a.bind(b);
//		b.bind(a);
//	}

	@Test
	public void move() {
		IBox<Integer> a = aof.createOrderedSet(1, 2);
		IBox<Integer> b = a.collect(new IUnaryFunction<Integer, Integer>() {
			@Override
			public Integer apply(Integer a) {
				return a + 1;
			}
		}).inspect("after collect: ").select(new IUnaryFunction<Integer, Boolean>() {
			@Override
			public Boolean apply(Integer a) {
				return a < 3;
			}
		}).inspect("b: ");
		IBox<Integer> target = aof.createOrderedSet();
		target.inspect("target: ").bind(b.asBox(target));
		a.move(1, 0);
		a.move(0, 1);
	}

/*
	@Test
	public void moveJ8() {
		IBox<Integer> a = aof.createOrderedSet(1, 2, 3);
		IBox<Integer> b = a.select(x ->
			x < 3
		).inspect(
			"after select1: "
		).collect(x ->
			x + 1
		).inspect(
			"after collect: "
		).select(x ->
			x < 3
		).inspect("b: ");
		IBox<Integer> target = aof.createOrderedSet();
		target.inspect("target: ").bind(b);
		a.move(1, 0);
		a.move(0, 1);
	}
*/

/*
	@Test
	public void moveJ8_Mutables() {
		IBox<Integer> a = aof.createOrderedSet(1, 2);
		IBox<Integer> b = path(a, 0, x -> aof.createOne(x + 1)).inspect("after collect: ");
		b = b.select(path(b, 0, x -> aof.createOne(x < 3))).inspect("b: ");
		IBox<Integer> target = aof.createOrderedSet();
		target.inspect("target: ").bind(b);
		a.move(1, 0);
		a.move(0, 1);
	}
*/

	@Test
	public void moveEMF() {
		EClass eClass = EcorePackage.eINSTANCE.getEClass();
		EClass eAttribute = EcorePackage.eINSTANCE.getEAttribute();
		IBox classifiers = aof.createPropertyBox(eClass.getEPackage(), "eClassifiers");
		IBox<EObject> attributes =
			inspectProperty(
				reject(
					inspectProperty(
//						select(
//							(IBox)select(
								inspectProperty(
										select(
											classifiers
											, eClass
										)
										, "classes: ", "name"
								)
//								, eClass, "name", "EClass"
//							)
								.collectMutable(aof, eClass, "eStructuralFeatures")
//							, eAttribute
//						)
						, "attributes: ", "name"
					)
					, eAttribute, "name", "iD"
				)
				, "attributes|name<>'iD': ", "name"
			);

		IBox<EObject> target =
//			aof.createSequence()
			aof.createOrderedSet()
		;
		inspectProperty(target, "target: ", "name");
		target.bind(attributes.asBox(target));
		eClass.getEStructuralFeatures().move(0, 1);
		eClass.getEStructuralFeatures().move(1, 0);
	}

	@Test
	public void moveEMFSimplified() {
		EClass eClass = EcorePackage.eINSTANCE.getEClass();
		EClass eAttribute = EcorePackage.eINSTANCE.getEAttribute();

		IBox<EObject> classifiers = aof.createPropertyBox(eClass.getEPackage(), "eClassifiers");

		IBox<EObject> attributes =
			reject(
				(IBox)path(
					select(
						classifiers
//						, eClass
						, eClass, "name", "EClass"
					),
					eClass, "eStructuralFeatures"
				),
				eAttribute, "name", "iD"
			);

		IBox<EObject> target = aof.createOrderedSet();
		inspectProperty(target, "target: ", "name");

		target.bind(attributes.asBox(target));

		System.out.println("Moving 0 to 1");
		eClass.getEStructuralFeatures().move(0, 1);
		System.out.println("Moving 1 to 0");
		eClass.getEStructuralFeatures().move(1, 0);
	}
}
