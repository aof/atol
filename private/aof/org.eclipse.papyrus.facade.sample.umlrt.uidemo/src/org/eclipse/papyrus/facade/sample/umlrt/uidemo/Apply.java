/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt.uidemo;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.papyrus.facade.Facade;
import org.eclipse.papyrus.facade.sample.umlrt.UMLRTFacade;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;


public class Apply implements IObjectActionDelegate {

    private ISelection selection;
    private IWorkbenchPart activePart;

    /**
     * Constructor for Action1.
     */
    public Apply() {
        super();
    }

    /**
     * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
     */
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
        this.activePart = targetPart;
    }

    /**
     * @see IActionDelegate#run(IAction)
     */
    public void run(IAction action) {
        Resource sourceResource = (Resource)((StructuredSelection)selection).getFirstElement();
        ResourceSet resourceSet = sourceResource.getResourceSet();

        // TODO: load targetMM in specific ResourceSet so that it does not show up in editor?
        Resource targetMMResource = resourceSet.getResource(sourceResource.getURI().trimSegments(1).appendSegment("umlrtmetamodel.ecore"), true);

        Resource targetResource = resourceSet.createResource(sourceResource.getURI().appendSegment("-transformed.xmi"));

        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();

        Facade facade = new UMLRTFacade(sourceResource, targetResource, targetMMResource);
    }

    /**
     * @see IActionDelegate#selectionChanged(IAction, ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        this.selection = selection;
    }

}
