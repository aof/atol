/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt.tests;

import static org.eclipse.papyrus.aof.lang.Helpers.findEClass;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.impl.URIMappingRegistryImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IPair;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.papyrus.aof.core.impl.utils.Equality;
import org.eclipse.papyrus.facade.Facade.PropagationListener;
import org.eclipse.papyrus.facade.sample.umlrt.UMLRTFacade;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.junit.Before;
import org.junit.BeforeClass;

public class UMLRTFacadeTest implements PropagationListener {

	// Absolute path seems necessary for (relative path) profiles to load correctly (only necessary to load the source model here)
	protected final static URI resourcesPath =
//		URI.createFileURI(new File("resources_NOCOMMIT/").getAbsolutePath())
		URI.createURI(UMLRTFacadeTest.class.getResource("UMLRTFacadeTest.class").toString().replaceFirst("/bin/.*", "/resources_NOCOMMIT/"))
	;

	// EMF Initialization
	private static void initEMF() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
	}

	// UML Initialization
	private static URI getPluginURI(java.lang.Class<?> c) {
		String url = c.getResource(c.getSimpleName() + ".class").toString();
		url = url.replaceAll("![^!]*$", "!");
		return URI.createURI(url + "/");
	}

	private static void initUML() {
		initEMF();

		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
		//EPackage.Registry.INSTANCE.put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		// To enable usage of UML 2.4 models from Luna and beyond, the two following lines are necessary
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/UML", UMLPackage.eINSTANCE);
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/Types", TypesPackage.eINSTANCE);

		URI uri = getPluginURI(UMLResourcesUtil.class);
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));
	}

	protected static UMLPackage UML = UMLPackage.eINSTANCE;

	// Initialization
	@BeforeClass
	public static void init() {
		initUML();

		ResourceSet rs = new ResourceSetImpl();
		targetMetamodel = rs.getResource(resourcesPath.appendSegment("umlrtmetamodel.ecore"), true);
		forAllContents(targetMetamodel, EPackage.class, new IConsumer<EPackage>() {
			@Override
			public void accept(EPackage pack) {
				EPackage.Registry.INSTANCE.put(pack.getNsURI(), pack);
			}
		});

		Inspect.enable = false;
		Inspect.registerToString(EObject.class, toString("name", true));
		Inspect.registerToString(Collection.class, new IUnaryFunction<Collection, String>() {
			@Override
			public String apply(Collection a) {
				StringBuffer ret = new StringBuffer();
				ret.append("[");
				for(Object  o : a) {
					if(ret.length() > 1) {
						ret.append(", ");
					}
					ret.append(Inspect.toString(o));
				}
				ret.append("]");
				return ret.toString();
			}
		});
	}

	protected static Resource targetMetamodel;

	// Running

	protected Resource sourceModel;
	protected Resource targetModel;

	public Resource getSourceModel() {
		return this.sourceModel;
	}

	public Resource getTargetModel() {
		return this.targetModel;
	}

	public IPair<Resource, Resource> run() {
		ResourceSet rs = new ResourceSetImpl();

		// Absolute path seems necessary for (relative path) profiles to load correctly
//		sourceModel = rs.getResource(URI.createFileURI(new File(resourcesPath + "sample.uml").getAbsolutePath()), true);
		sourceModel = rs.getResource(resourcesPath.appendSegment("sample.uml"), true);

		targetModel = transform();

		return AOFFactory.INSTANCE.createPair(sourceModel, targetModel);
	}

	// Returns a new target model on each run
	public Resource transform() {
		ResourceSet rs = new ResourceSetImpl();

		Resource targetModel = rs.createResource(resourcesPath.appendSegment("sample-UMLRT.xmi"));

		new UMLRTFacade(sourceModel, targetModel, targetMetamodel).setPropagationListener(this);;

		return targetModel;
	}

	// Tests

	@Before
	public void initialRun() {
		run();
	}

/*	// Uncomment to save the targetModel after each test (mostly useful when running a single test)
	@After
	public void save() throws IOException {
		this.targetModel.save(Collections.emptyMap());
	}
/**/

	// Utils

	protected Resource check() {
		return check(null);
	}

	protected Resource check(String msg) {
		Resource newTargetModel = this.transform();
		StringBuffer message = new StringBuffer();
		boolean comparisonResult = compare(newTargetModel, this.targetModel, message);
		assertTrue((msg == null ? "" : msg + "\n") + message.toString(), comparisonResult);
		return newTargetModel;
	}

	protected boolean compare(Resource expected, Resource actual, StringBuffer message) {
		return compare("/contents", expected.getContents(), actual.getContents(), new HashSet<EObject>(), message);
	}

	private boolean compare(String path, Collection<?> c1, Collection<?> c2, Set<EObject> traversedElements, StringBuffer message) {
		if(c1.size() != c2.size()) {
			message.append("Size difference at " + path + ": " + Inspect.toString(c1, null) + " != " + Inspect.toString(c2, null));
			message.append("\n");
			return false;
		}
		boolean ret = true;
		int i = 0;
		for(Iterator<?> i1 = c1.iterator(), i2 = c2.iterator() ; i1.hasNext() && i2.hasNext() ; ) {
			Object e1 = i1.next();
			Object e2 = i2.next();
			String newPath = path + "[" + i + "]";
			boolean r;
			if(e1 == null || e2 == null) {
				ret &= r = e1 == e2;
				if(!r) {
					message.append("Existence difference at " + newPath + ": " + Inspect.toString(e1, null) + " != " + Inspect.toString(e2, null));
					message.append("\n");
				}
			} else if(e1 instanceof EObject && e2 instanceof EObject) {
				ret &= compare(newPath, (EObject)e1, (EObject)e2, traversedElements, message);
			} else {
				ret &= r = e1.equals(e2);
				if(!r) {
					message.append("Equality difference at " + newPath + ": " + Inspect.toString(e1, null) + " != " + Inspect.toString(e2, null));
					message.append("\n");
				}
			}
			i++;
		}

		return ret;
	}

	private boolean compare(String path, EObject eo1, EObject eo2, Set<EObject> traversedElements, StringBuffer message) {
		if(traversedElements.contains(eo1) || traversedElements.contains(eo2)) {
			return true;
		}

		traversedElements.add(eo1);
		traversedElements.add(eo2);

		// We could compare eClasses if the metamodel was loaded only once
//		if(eo1.eClass() != eo2.eClass()) {
		if(!eo1.eClass().getName().equals(eo2.eClass().getName())) {
			message.append("Type difference at " + path + ": " + Inspect.toString(eo1, null) + " != " + Inspect.toString(eo2, null));
			message.append("\n");
			return false;
		}
		boolean ret = true;
		for(EStructuralFeature feature : eo1.eClass().getEAllStructuralFeatures()) {
			Object v1 = eo1.eGet(feature);
//			Object v2 = eo2.eGet(feature);	// only works if the metamodel is load only once
			Object v2 = eo2.eGet(eo2.eClass().getEStructuralFeature(feature.getName()));
			String newPath = path + "/" + feature.getName();
			boolean r;
			if(v1 == null || v2 == null) {
				ret &= r = v1 == v2;
				if(!r) {
					message.append("Existence difference at " + newPath + ": " + Inspect.toString(v1, null) + " != " + Inspect.toString(v2, null));
					message.append("\n");
				}
			} else if(feature instanceof EAttribute) {
				if(v1 instanceof EEnumLiteral && v2 instanceof EEnumLiteral) {	// useless once metamodel is loaded only once
					ret &= r = ((EEnumLiteral)v1).getLiteral().equals(((EEnumLiteral)v2).getLiteral());
				} else {
					ret &= r = v1.equals(v2);
				}
				if(!r) {
					message.append("Equality difference at " + newPath + ": " + Inspect.toString(v1, null) + " != " + Inspect.toString(v2, null));
					message.append("\n");
				}
			} else if(feature.isMany()) {
				ret &= compare(newPath, (Collection<?>)v1, (Collection<?>)v2, traversedElements, message);
			} else {
				ret &= compare(newPath, (EObject)v1, (EObject)v2, traversedElements, message);
			}
		}

		return ret;
	}

	public static interface IConsumer<E> {
		void accept(E e);
	}

	protected <C extends EObject> void forAllSourceElement(java.lang.Class<C> type, IConsumer<? super C> c) {
		forAllContents(this.sourceModel, type, c);
	}

	public static <C extends EObject> void forAllContents(Resource resource, java.lang.Class<C> type, IConsumer<? super C> c) {
		for(EObject e : getAllContents(resource)) {
			if(type.isInstance(e)) {
				c.accept((C)e);
			}
		}
	}

	public static <C extends EObject> void forAllContents(Resource resource, EClass type, IConsumer<? super C> c) {
		for(EObject e : getAllContents(resource)) {
			if(type.isInstance(e)) {
				c.accept((C)e);
			}
		}
	}

	protected void forAllTargetElement(String className, IConsumer<EObject> c) {
		EClass type = findEClass(targetMetamodel, className);
		for(EObject e : getAllContents(this.targetModel)) {
			if(type.isInstance(e)) {
				c.accept(e);
			}
		}
	}

	protected <A extends EObject> A findOne(Resource resource, Class<A> type) {
		return findOne(resource, type, null, null);
	}

	protected <A extends EObject> A findOne(Resource resource, Class<A> type, final String propertyName, final Object value) {
		final Object[] ret = new Object[1];
		forAllContents(resource, type, new IConsumer<A>() {
			@Override
			public void accept(A e) {
				if(ret[0] == null) {
					if((propertyName == null) || (has(e, propertyName) && Equality.optionalEquals(get(e, propertyName), value))) {
						ret[0] = e;
					}
				}
			}
		});
		return (A)ret[0];
	}

	protected <A extends EObject> A findOne(Resource resource, EClass type) {
		return findOne(resource, type, null, null);
	}

	protected <A extends EObject> A findOne(Resource resource, EClass type, final String propertyName, final Object value) {
		final Object[] ret = new Object[1];
		forAllContents(resource, type, new IConsumer<A>() {
			@Override
			public void accept(A e) {
				if(ret[0] == null) {
					if((propertyName == null) || (has(e, propertyName) && Equality.optionalEquals(get(e, propertyName), value))) {
						ret[0] = e;
					}
				}
			}
		});
		return (A)ret[0];
	}

	// EMF Utils

	protected static EStructuralFeature getFeature(EObject eo, String featureName) {
		return eo.eClass().getEStructuralFeature(featureName);
	}

	protected static boolean has(EObject eo, String featureName) {
		return getFeature(eo, featureName) != null;
	}

	protected static <A> A get(EObject eo, String featureName) {
		return (A)eo.eGet(getFeature(eo, featureName));
	}

	protected static <A> A get(EObject eo, EStructuralFeature feature) {
		return (A)eo.eGet(feature);
	}

	protected static void add(EObject eo, String featureName, Object value) {
		Collection<Object> c = get(eo, featureName);
		c.add(value);
	}

	protected static void set(EObject eo, String featureName, Object value) {
		set(eo, getFeature(eo, featureName), value);
	}

	protected static void set(EObject eo, EStructuralFeature feature, Object value) {
		eo.eSet(feature, value);
	}

	protected static <A extends EObject> A create(EClass eClass) {
		return (A)eClass.getEPackage().getEFactoryInstance().create(eClass);
	}

	protected static Iterable<EObject> getAllContents(final Resource resource) {
		// We iterate over a copy of the resource contents. Otherwise, tests that add elements would trigger
		// ConcurrentModificationExceptions
		List<EObject> ret = new ArrayList<EObject>();
		for(Iterator<EObject> i = resource.getAllContents() ; i.hasNext() ; ) {
			ret.add(i.next());
		}
		return ret;
	}

	@Override
	public void reversePropagationBlocked(EObject targetElement, String targetPropertyName, Object blockedElement) {
		System.out.println("Warning: could not propagate erroneous target model change: insertion of " + Inspect.toString(blockedElement) + " in " + Inspect.toString(targetElement) + "." + Inspect.toString(targetPropertyName));
	}

	protected static <A extends EObject> IUnaryFunction<A, String> toString(final String propertyName, final Boolean andType) {
		return new IUnaryFunction<A, String>() {
			@Override
			public String apply(EObject a) {
				if(a == null) {
					return "null";
				} else {
					EStructuralFeature f = a.eClass().getEStructuralFeature(propertyName);
					String ret = "";
					if(f != null) {
						ret += a.eGet(f) + ":";
					}
					if(andType) {
						ret += a.eClass().getName();
					}
					return ret;
				}
			}
		};
	}
}
