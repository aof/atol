/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt.tests;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.Test;

public class UMLRTFacadeFowardTest extends UMLRTFacadeTest {

	// First, test without change propagation
	@Test
	public void testResultEqualityForTwoRuns() {
		check();
	}

	// Compare to expected target... no propagation
	@Test
	public void compareToExpectedTarget() {
		ResourceSet rs = new ResourceSetImpl();
		Resource expectedTarget = rs.getResource(resourcesPath.appendSegment("sample-UMLRT.expected.xmi"), true);
		StringBuffer message = new StringBuffer();
		boolean comparisonResult = compare(expectedTarget, this.targetModel, message);
		assertTrue("Does not match expected target:\n" + message, comparisonResult);
	}

	@Test
	public void testDirectNameChange() {
		testNameChange(false);
	}

	@Test
	public void testNameChangeSettingNullFirst() {
		testNameChange(true);
	}

	public void testNameChange(final boolean setNullFirst) {
		final int i[] = new int[1];
		forAllSourceElement(EObject.class, new IConsumer<EObject>() {
			@Override
			public void accept(EObject eo) {
				if(has(eo, "name")) {
					if(setNullFirst) {
						set(eo, "name", null);
						check();
					}
					set(eo, "name", "test" + i[0]++);
					check();
				}
			}
		});
		check();
	}

	@Test
	public void testPropertyAddition() {
		testElementAddition(Class.class, "ownedAttribute");
	}

	public <C extends EObject> void testElementAddition(final java.lang.Class<C> type, final String featureName) {
		testElementAddition(type, featureName, (EClass)null);
	}

	public <C extends EObject> void testElementAddition(final java.lang.Class<C> type, final String featureName, final EClass featureType) {
		testElementAddition(type, featureName, featureType, (IConsumer<EObject>)null);
	}

	public <C extends EObject> void testElementAddition(final java.lang.Class<C> type, final String featureName, final EClass featureType, final String stereotypeName) {
		testElementAddition(type, featureName, featureType, new IConsumer<EObject>() {
			@Override
			public void accept(EObject newValue) {
				if(stereotypeName != null) {
					Element element = (Element)newValue;
					element.applyStereotype(element.getApplicableStereotype(stereotypeName));
				}
			}
		});
	}

	public <C extends EObject> void testElementAddition(final java.lang.Class<C> type, final String featureName, final IConsumer<EObject> newElementTweaker) {
		testElementAddition(type, featureName, null, newElementTweaker);
	}

	public <C extends EObject> void testElementAddition(final java.lang.Class<C> type, final String featureName, final EClass featureType, final IConsumer<EObject> newElementTweaker) {
		forAllSourceElement(type, new IConsumer<EObject>() {
			@Override
			public void accept(EObject e) {
				EStructuralFeature feature = getFeature(e, featureName);
				EClass eClass = featureType == null ? (EClass)feature.getEType() : featureType;
				EObject newValue = create(eClass);
				if(feature.isMany()) {
					Collection<EObject> elements = get(e, feature);
					elements.add(newValue);
				} else {
					set(e, feature, newValue);
				}
				if(newElementTweaker != null) {
					newElementTweaker.accept(newValue);
				}
				check("Failed comparison after adding a " + eClass.getName() + " to " + featureName + " of " + type.getName() + " " + get(e, "name"));
			}
		});
	}

	@Test
	public void testStereotypeRemoval() {
		forAllSourceElement(Element.class, new IConsumer<Element>() {
			@Override
			public void accept(Element element) {
				for(Stereotype stereotype : element.getAppliedStereotypes()) {
					element.unapplyStereotype(stereotype);
					check("Failed comparison after unapplying stereotype " + stereotype.getQualifiedName() + " from " + element);
				}
			}
		});
	}

	@Test
	public void testStereotypeAddition() {
		stereotypeAddition(false);
	}

	@Test
	public void testStereotypeAdditionAndRemoval() {
		stereotypeAddition(true);
	}

	public void stereotypeAddition(final boolean andRemoval) {
		forAllSourceElement(Element.class, new IConsumer<Element>() {
			@Override
			public void accept(Element element) {
				for(Stereotype stereotype : element.getApplicableStereotypes()) {
					if(element.isStereotypeApplicable(stereotype)) {
						if(!element.isStereotypeApplied(stereotype)) {
							element.applyStereotype(stereotype);
							check("Failed comparison after applying stereotype " + stereotype.getQualifiedName() + " to " + Inspect.toString(element));
							if(andRemoval) {
								element.unapplyStereotype(stereotype);
								check("Failed comparison after applying then unapplying stereotype " + stereotype.getQualifiedName() + " from " + element);
							}
						}
					}
				}
			}
		});
	}

	@Test
	public void testPassiveToCapsuleAndBack() {
		Class passive = findOne(this.sourceModel, Class.class, "name", "Passive");
		Stereotype stereotype = passive.getApplicableStereotype("UMLRealTime::Capsule");
		passive.applyStereotype(stereotype);
		check("Failed comparison after applying Capsule to Class Passive");
		passive.unapplyStereotype(stereotype);
		check("Failed comparison after unapplying Capsule from Class Passive");
	}

	@Test
	public void testRemovePartClass() {
		Model model = findOne(this.sourceModel, Model.class);
		Class partClass = findOne(this.sourceModel, Class.class, "name", "PartClass");
		model.getPackagedElements().remove(partClass);
		check();
	}

	@Test
	public void testStateMachineAddition() {
		testElementAddition(Class.class, "classifierBehavior", UML.getStateMachine(), "UMLRealTimeStateMach::RTStateMachine");
	}

	@Test
	public void testRegionAddition() throws IOException {
		testElementAddition(State.class, "region", new IConsumer<EObject>() {
			@Override
			public void accept(EObject e) {
				add(e, "transition", create(UML.getTransition()));
			}
		});
		this.targetModel.save(Collections.emptyMap());
	}
}
