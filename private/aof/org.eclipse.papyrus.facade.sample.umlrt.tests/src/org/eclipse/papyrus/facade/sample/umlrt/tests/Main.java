/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt.tests;

import java.io.IOException;
import java.util.Collections;

public class Main {

	public static void main(String[] args) throws IOException {
		UMLRTFacadeTest.init();
		UMLRTFacadeTest test = new UMLRTFacadeTest();

		// saving target model
		test.run().getRight().save(Collections.emptyMap());
	}
}
