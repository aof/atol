/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt.tests;

import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.junit.Test;


public class UMLRTFacadeReverseTest extends UMLRTFacadeTest {

	@Test
	public void testNameChange() {
		int i = 0;
		for(EObject eo : getAllContents(this.targetModel)) {
			if(has(eo, "name")) {
				set(eo, "name", "test" + i++);
				check();
			}
		}
	}

	public void testElementAddition(final String className, final String featureName) {
		forAllTargetElement(className, new IConsumer<EObject>() {
			@Override
			public void accept(EObject c) {
				EStructuralFeature feature = getFeature(c, featureName);
				Collection<EObject> elements = get(c, feature);
				EClass eClass = (EClass)feature.getEType();
				elements.add(create(eClass));
				check("Failed comparison after adding a " + eClass.getName() + " to " + featureName + " of " + className +
						(has(c, "name") ? " " + get(c, "name") : "")
				);
			}
		});
	}

	@Test
	public void testAttributeAdditionToCapsule() {
		testElementAddition("Entity" , "attributes");
	}

	@Test
	public void testOperationAdditionToCapsule() {
		testElementAddition("Entity" , "operations");
	}

	@Test
	public void testPartAdditionToCapsule() {
		testElementAddition("Capsule" , "parts");
	}

	@Test
	public void testPortAddition() {
		testElementAddition("Capsule", "ports");
	}

	@Test
	public void testConnectorAddition() {
		testElementAddition("Capsule", "connectors");
	}

	@Test
	public void testProtocolAddition() {
		testElementAddition("Model", "protocols");
	}

	@Test
	public void testParameterAddition() {
		testElementAddition("Operation", "parameters");
	}
}
