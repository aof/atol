package org.eclipse.papyrus.aof.java8;

import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.Test;

public class UML2UMLRTFowardTest extends UML2UMLRTTest {
	@Test
	public void testNameChange() {
		int i[] = new int[1];
		forAllSourceElement(EObject.class, eo -> {
			if(has(eo, "name")) {
				set(eo, "name", "test" + i[0]++);
			}
		});
		check();
	}

	@Test
	public void testPropertyAddition() {
//		forAllSourceElement(Class.class, c -> {
//			c.getOwnedAttributes().add(create(UML.getProperty()));
//			check();
//		});
		testElementAddition(Class.class, "ownedAttribute");
	}

	public <C extends EObject> void testElementAddition(java.lang.Class<C> type, String featureName) {
		forAllSourceElement(type, (EObject c) -> {
			EStructuralFeature feature = getFeature(c, featureName);
			Collection<EObject> elements = get(c, feature);
			EClass eClass = (EClass)feature.getEType();
			elements.add(create(eClass));
			check("Failed comparison after adding a " + eClass.getName() + " to " + featureName + " of " + type.getName() + " " + get(c, "name"));
		});
	}

	// This will only succeed once mutable stereotype accessors are used in UML2UMLRT
	@Test
	public void testStereotypeRemoval() {
		forAllSourceElement(Element.class, element -> {
			for(Stereotype stereotype : element.getAppliedStereotypes()) {
				element.unapplyStereotype(stereotype);
				check("Failed comparison after unapplying stereotype " + stereotype.getQualifiedName() + " from " + element);
			}
		});
	}

	// This will only succeed once mutable stereotype accessors are used in UML2UMLRT
	// TODO: handle stereotype incompatibility
	@Test
	public void testStereotypeAddition() {
		forAllSourceElement(Element.class, element -> {
			for(Stereotype stereotype : element.getApplicableStereotypes()) {
				if(element.isStereotypeApplicable(stereotype)) {
					if(!element.isStereotypeApplied(stereotype)) {
						element.applyStereotype(stereotype);
						check("Failed comparison after applying stereotype " + stereotype.getQualifiedName() + " to " + element);					
					}
				}
			}
		});
	}
}
