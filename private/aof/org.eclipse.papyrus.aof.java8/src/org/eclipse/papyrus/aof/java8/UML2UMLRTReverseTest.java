package org.eclipse.papyrus.aof.java8;

import java.util.Collection;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.junit.Test;


public class UML2UMLRTReverseTest extends UML2UMLRTTest {

	@Test
	public void testNameChange() {
		int i = 0;
		for(EObject eo : getAllContents(targetModel)) {
			if(has(eo, "name")) {
				set(eo, "name", "test" + i++);
			}
		}
		check();
	}

	@Test
	public void testAttributeAdditionToCapsule() {
		testElementAddition("Capsule" , "attributes");
	}

	@Test
	public void testPartAdditionToCapsule() {
		testElementAddition("Capsule" , "parts");
	}

	public void testElementAddition(String className, String featureName) {
		forAllTargetElement(className, c -> {
			EStructuralFeature feature = getFeature(c, featureName);
			Collection<EObject> elements = get(c, feature);
			EClass eClass = (EClass)feature.getEType();
			elements.add(create(eClass));
			check("Failed comparison after adding a " + eClass.getName() + " to " + featureName + " of " + className + " " + get(c, "name"));
		});
	}

	@Test
	public void testPortAddition() {
		testElementAddition("Capsule", "ports");
	}
}
