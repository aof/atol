package org.eclipse.papyrus.aof.java8;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.impl.URIMappingRegistryImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IPair;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UML2UMLRTTest {

	// EMF Initialization
	private static void initEMF() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
	}

	// UML Initialization
	private static URI getPluginURI(java.lang.Class<?> c) {
		String url = c.getResource(c.getSimpleName() + ".class").toString();
		url = url.replaceAll("![^!]*$", "!");
		return URI.createURI(url + "/");
	}

	private static void initUML() {
		initEMF();

		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
		//EPackage.Registry.INSTANCE.put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		// To enable usage of UML 2.4 models from Luna and beyond, the two following lines are necessary
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/UML", UMLPackage.eINSTANCE);
		EPackage.Registry.INSTANCE.put("http://www.eclipse.org/uml2/4.0.0/Types", TypesPackage.eINSTANCE);

		URI uri = getPluginURI(UMLResourcesUtil.class);
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
		URIMappingRegistryImpl.INSTANCE.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));
	}

	protected static UMLPackage UML = UMLPackage.eINSTANCE;

	// Initialization
	@BeforeClass
	public static void init() {
		initUML();

		ResourceSet rs = new ResourceSetImpl();
		targetMetamodel = rs.getResource(URI.createFileURI("resources/umlrtmetamodel.ecore"), true);
	}

	private static Resource targetMetamodel;

	// Running

	protected Resource sourceModel;
	protected Resource targetModel;

	public IPair<Resource, Resource> run() {
		ResourceSet rs = new ResourceSetImpl();

		// Absolute path seems necessary for (relative path) profiles to load correctly
		sourceModel = rs.getResource(URI.createFileURI(new File("resources/sample.uml").getAbsolutePath()), true);

		targetModel = transform();

		return AOFFactory.INSTANCE.createPair(sourceModel, targetModel);
	}

	// Returns a new target model on each run
	public Resource transform() {
		ResourceSet rs = new ResourceSetImpl();

		Resource targetModel = rs.createResource(URI.createFileURI("resources/sample-UMLRT.xmi"));

		Model model = (Model)sourceModel.getContents().get(0);
		targetModel.getContents().add(new UML2UMLRT(targetMetamodel).Model2Model(model));

		return targetModel;
	}

	// Tests

	@Before
	public void initialRun() {
		run();
	}

/*	// Uncomment to save the targetModel after each test (mostly useful when running a single test)
	@After
	public void save() throws IOException {
		this.targetModel.save(Collections.emptyMap());
	}
/**/

	@Test
	public void testResultEqualityForTwoRuns() {
		check();
	}

	// Utils

	protected void check() {
		check(null);
	}

	protected void check(String msg) {
		Resource newTargetModel = this.transform();
		assertTrue(msg, compare(newTargetModel.getContents(), targetModel.getContents()));
	}

	private boolean compare(Collection c1, Collection c2) {
		if(c1.size() != c2.size()) {
			return false;
		}
		boolean ret = true;
		for(Iterator i1 = c1.iterator(), i2 = c2.iterator() ; i1.hasNext() && i2.hasNext() ; ) {
			Object e1 = i1.next();
			Object e2 = i2.next();
			if(e1 == null) {
				ret &= e2 == null;
			} else if(e1 instanceof EObject && e2 instanceof EObject) {
				ret &= compare((EObject)e1, (EObject)e2);
			} else {
				ret &= e1.equals(e2);
			}
		}

		return ret;
	}

	private boolean compare(EObject eo1, EObject eo2) {
		if(eo1.eClass() != eo2.eClass()) {
			return false;
		}
		boolean ret = true;
		for(EStructuralFeature feature : eo1.eClass().getEAllStructuralFeatures()) {
			Object v1 = eo1.eGet(feature);
			Object v2 = eo2.eGet(feature);
			if(v1 == null) {
				ret &= v2 == null;
			} else if(feature instanceof EAttribute) {
				ret &= v1.equals(v2);
			} else if(feature.isMany()) {
				ret &= compare((Collection)v1, (Collection)v2);
			} else {
				ret &= compare((EObject)v1, (EObject)v2);
			}
		}

		return ret;
	}

	@SuppressWarnings("unchecked")
	protected <C extends EObject> void forAllSourceElement(java.lang.Class<C> type, Consumer<? super C> c) {
		getAllContents(this.sourceModel).forEach(e -> {
			if(type.isInstance(e)) {
				c.accept((C)e);
			}
		});
	}

	protected void forAllTargetElement(String className, Consumer<EObject> c) {
		getAllContents(this.targetModel).forEach(e -> {
			if(e.eClass().getName().equals(className)) {
				c.accept(e);
			}
		});
	}

	// EMF Utils

	protected static EStructuralFeature getFeature(EObject eo, String featureName) {
		return eo.eClass().getEStructuralFeature(featureName);
	}

	protected static boolean has(EObject eo, String featureName) {
		return getFeature(eo, featureName) != null;
	}

	protected static <A> A get(EObject eo, String featureName) {
		return (A)eo.eGet(getFeature(eo, featureName));
	}

	protected static <A> A get(EObject eo, EStructuralFeature feature) {
		return (A)eo.eGet(feature);
	}

	protected static void set(EObject eo, String featureName, Object value) {
		eo.eSet(getFeature(eo, featureName), value);
	}

	protected static <A> A create(EClass eClass) {
		return (A)eClass.getEPackage().getEFactoryInstance().create(eClass);
	}

	protected static Iterable<EObject> getAllContents(final Resource resource) {
		return new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return resource.getAllContents();
			}
		};
	}
}
