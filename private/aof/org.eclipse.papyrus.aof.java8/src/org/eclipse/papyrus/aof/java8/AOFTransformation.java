package org.eclipse.papyrus.aof.java8;

import java.util.Iterator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.IBinding;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IFactory;
import org.eclipse.papyrus.aof.emf.EMFFactory;

public abstract class AOFTransformation {

	private Resource targetMetamodel;
	private IFactory factory = EMFFactory.INSTANCE;


	public AOFTransformation(Resource targetMetamodel) {
		this.targetMetamodel = targetMetamodel;
	}

	// AOF Utils

	// Rationale for this method: it is a shortcut for pure AOF
	//		sourceBox.select(e -> eClass.isInstance(e)).collect(factory, eClass, propertyName)
	// or, using helpers from this class:
	//		select(sourceBox, eClass).collect(factory, eClass, propertyName)
	// which saves:
	//	- factory
	//	- eClass repetition
	protected <A, B> IBox<B> selectCollect(IBox<A> sourceBox, EClass eClass, String propertyName) {
		return select(sourceBox, eClass).collectMutable(this.factory, eClass, propertyName);
	}

	// Rationale: saves on factory
	protected <A> IBox<A> get(EObject eo, String propertyName) {
		return this.factory.createPropertyBox(eo, propertyName);
	}

	// Rationale: this avoids casts in client code
	// We can suppress the "unchecked" warning because we are sure the cast is always correct
	@SuppressWarnings("unchecked")
	protected <A, C extends A> IBox<C> select(IBox<A> source, java.lang.Class<C> c) {
		return (IBox<C>)source.select(e -> c.isInstance(e));
	}

/*
	// Rationale: this is a shortcut similar to select, but it does not save much
	protected <A, C extends A> IBox<A> reject(IBox<A> source, java.lang.Class<C> c) {
		return source.select(e -> !c.isInstance(e));
	}
*/

	// Rationale: this avoids casts in client code
	// We do not suppress the "unchecked" warning because it is only correct if the method is used correctly
	protected <A, C extends A> IBox<C> select(IBox<A> source, EClass c) {
		return (IBox<C>)source.select(e -> c.isInstance(e));
	}

	// Rationale: we often need to call asBox, and it does not hurt to call it if it is not required
	protected <A> IBinding<A> bind(IBox<A> target, IBox<A> source) {
		return target.bind(source.asBox(target));
	}

	// EMF Utils

	// We do not suppress the "unchecked" warning because it is only correct if the method is used correctly
	protected <A extends EObject> A create(String className) {
		EClass targetClass = findClass(this.targetMetamodel, className);
		return (A)create(targetClass);
	}

	// We do not suppress the "unchecked" warning because it is only correct if the method is used correctly
	private static <A extends EObject> A create(EClass eClass) {
		EObject target = eClass.getEPackage().getEFactoryInstance().create(eClass);
		return (A)target;
	}

	private static EClass findClass(Resource resource, String name) {
		for(Iterator<EObject> i = resource.getAllContents() ; i.hasNext() ; ) {
			EObject eo = i.next();
			if(eo instanceof EClass && ((EClass)eo).getName().equals(name)) {
				return (EClass)eo;
			}
		}
		return null;
	}
}
