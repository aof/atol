package org.eclipse.papyrus.aof.java8;

import java.io.IOException;
import java.util.Collections;

public class Main {

	public static void main(String[] args) throws IOException {
		UML2UMLRTTest.init();
		UML2UMLRTTest test = new UML2UMLRTTest();

		// saving target model
		test.run().getRight().save(Collections.emptyMap());
	}
}
