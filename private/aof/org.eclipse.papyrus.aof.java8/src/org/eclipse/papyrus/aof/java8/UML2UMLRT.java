package org.eclipse.papyrus.aof.java8;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

/*
 * Some issues with this "pure" AOF compared to the rule-based version:
 * - one must call the right rule explicitly
 * 		- in some cases there are several applicable rules
 * 			=> we would need some kind of super-rules
 * 			=> this is what has been done for:
 * 				- Class2Entity (a super-rule for Class2Capsule and Class2PassiveClass)
 * 				- Property2X (a super-rule for Property2Part and Property2Attribute)
 * 			BUT this does not address mutable rule selection issues
 * 
 * Other issues:
 * - it is not possible to use this::MethodName expressions as collectors of collectRefs because their values are not constant
 * (i.e., each method seems to be wrapped by a different object each time it is referenced)
 * 		=> therefore we use Functions and not methods
 * - it is not possible to simply use Functions in fields because one Function would not be able to refer to another one
 * 		=> therefore we define methods, but need to cache their translations to functions (i.e., each rule corresponds to a method plus a field)
 */
public class UML2UMLRT extends AOFTransformation {

	public UML2UMLRT(Resource targetMetamodel) {
		super(targetMetamodel);
	}

	private UMLPackage UML = UMLPackage.eINSTANCE;

	// We must make sure to compute the IUnaryFunctions once otherwise they get wrapped by different objects when used several times
	public IUnaryFunction<Model, EObject> Model2Model = this::Model2Model;
	public IUnaryFunction<Class, EObject> Class2Entity = this::Class2Entity;
	public IUnaryFunction<Property, EObject> Property2Port = this::Property2Port;
	public IUnaryFunction<Property, EObject> Property2X = this::Property2X;
	public IUnaryFunction<Collaboration, EObject> Collaboration2Protocol = this::Collaboration2Protocol;

	public EObject Model2Model(Model source) {
		EObject target = create("Model");

		IBox<PackageableElement> source_packagedElements = get(source, "packagedElement");

		IBox<EObject> target_classes = get(target, "classes");

		// Getting rid of the unchecked cast would require a method like the following on IBox<E>:
		//		<C extends E> IBox<C> select(java.lang.Class<C>)
		// therefore we currently hide it in a select(Class) function
		// IBox<Class> source_classes = (IBox)source_packagedElements.select(e -> e instanceof Class);
		IBox<Class> source_classes = select(source_packagedElements, Class.class);

		target_classes.bind(
			source_classes.collectTo(Class2Entity)	// Class2Capsule | Class2Passive
		);

		get(target, "protocols").bind(
			// TODO:
			// - select only Collaborations with stereotype "UMLRealTime::Protocol"
			select(
				// TODO:
				// - select only Packages with stereotype "UMLRealTime::ProtocolContainer"
				// - traverse all subPackages recursively? would require a closure operation
				selectCollect(source_packagedElements, UML.getPackage(), "packagedElement"),
				Collaboration.class
			).distinct().collectTo(Collaboration2Protocol)
		);

		return target;
	}

	public EObject Class2Entity(Class source) {
		EObject target;

		IBox<Property> source_attributes = get(source, "ownedAttribute");

		// How to rewrite this, assuming we have a mutable hasStereotype accessor?
		if(source.getAppliedStereotype("UMLRealTime::Capsule") != null) {
			target = create("Capsule");

			get(target, "ports").bind(select(source_attributes, Port.class).collectTo(Property2Port));
		} else {
			target = create("PassiveClass");
		}

		bindNamedElement(source, target);

		get(target, "attributes").bind(source_attributes.select(e -> !(e instanceof Port)).collectTo(Property2X));

		return target;
	}

	// TODO: && hasStereotype("UMLRealTime::RTPort")
	public EObject Property2Port(Property source) {
		EObject target = create("Port");

		IBox<Collaboration> source_type = get(source, "type");
		bind(get(target, "type"), source_type.collectTo(Collaboration2Protocol));

		bindNamedElement(source, target);

		return target;
	}

	// Property2{Attribute,Part}
	public EObject Property2X(Property source) {
		EObject target;

		if(source.getAppliedStereotype("UMLRealTime::CapsulePart") != null) {
			target = create("CapsulePart");
		} else {
			target = create("Attribute");	
		}

		// TODO: DataType and not Class... but then would we need to rule a "super-rule"?
		IBox<Class> source_type = get(source, "type");
		bind(get(target, "type"), source_type.collectTo(Class2Entity));

		bindNamedElement(source, target);

		return target;
	}

	// TODO: if hasStereotype("UMLRealTime::Protocol")
	public EObject Collaboration2Protocol(Collaboration source) {
		EObject target = create("Protocol");

		bindNamedElement(source, target);

		return target;
	}

	// This corresponds to an abstract rule in ATL
	private void bindNamedElement(NamedElement source, EObject target) {
/*		// passive version
		UML2UMLRTTest.set(target, "name", source.getName());
/*/		// active version
		get(target, "name").bind(get(source, "name"));
/**/
	}
}
