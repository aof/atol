package fr.eseo.aof2.exploration.graph2uml

import com.google.common.collect.HashBiMap
import org.eclipse.emf.common.notify.Notification
import org.eclipse.emf.common.notify.impl.AdapterImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IMetaClass
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver
import org.eclipse.xtend.lib.annotations.Data

class DSL {
	@Data
	static class Binder {
		val boolean forward

		def <E> bind(IBox<E> a, IBox<E> b) {
			if(forward) {
				a.bind(b)
			} else {
				b.bind(a)
			}
		}

//		def <E> <=>(IBox<? extends E> a, IBox<? extends E> b) {
		def <E> <=>(IBox<E> a, IBox<E> b) {
//			(a as IBox<E>).bind(b.asBox(a) as IBox<E>)
			bind(a as IBox<E>, b.asBox(a) as IBox<E>)
		}
	}

	// DONE: using IMetaClass checks that S,T respectively correspond to s,t
	@Data
	static class Rule<S, T> {
		val IMetaClass<S> source
		val IMetaClass<T> target
		val (Binder, S,T)=>Object init
		val cache = HashBiMap.create

		// direct calls to apply & reverseApply only for non-box context (e.g., root element)
		def T apply(S s) {
			if(!cache.containsKey(s)) {
				val t = this.target.newInstance
				cache.put(s, t)
				this.init.apply(new Binder(true), s, t)
			}
			cache.get(s) as T
		}
		def S reverseApply(T t) {
			if(!cache.containsValue(t)) {
				val s = this.source.newInstance
				cache.put(s, t)
				this.init.apply(new Binder(false), s, t)
			}
			cache.inverse.get(t) as S
		}
	}

	def <S, T> IBox<T> apply(IBox<S> box, Rule<S, T> rule) {
		box.collect([s |
			rule.apply(s)
		],[t |
			rule.reverseApply(t)
		])
	}

	def <S, T> IBox<T> apply(IBox<S> box, Rule<? extends S, ? extends T>...rule) {
		box.collect([s |
			(rule.findFirst[it.source.isInstance(s)] as Rule<S, T>)
				?.apply(s)
		],[t |
			(rule.findFirst[it.target.isInstance(t)] as Rule<S, T>)
				?.reverseApply(t)
		]).select[it !== null]
	}

	def <E> ifThen(IBox<Boolean> box, E thenVal, E elseVal) {
		box.collect[
			if(it ?: false) {
				thenVal
			} else {
				elseVal
			}
		]
	}

	def <E> emptyOne() {
		IBox.ONE as IBox<E>
	}

	def _contents(Resource r) {
		val ret = AOFFactory.INSTANCE.createSequence()
		ret.assign(r.contents)
		val active = AOFFactory.INSTANCE.createOne(false)
		r.eAdapters.add(new AdapterImpl {
			override notifyChanged(Notification n) {
				if(n.getFeatureID(null) == Resource.RESOURCE__CONTENTS && !active.get) {
					active.set(true)
					switch(n.eventType) {
						case Notification.ADD: {
							ret.add(n.position, n.newValue as EObject)
						}
						case Notification.REMOVE: {
							ret.removeAt(n.position)
						}
						case Notification.MOVE: {
							ret.move(n.position, n.oldValue as Integer)
						}
						case Notification.ADD_MANY: {
							var i = n.position
							for(e : n.newValue as Iterable<EObject>) {
								ret.add(i++, e)
							}
						}
						case Notification.REMOVE_MANY: {
							for(e : n.newValue as Iterable<EObject>) {
								ret.remove(e)
							}
						}
						case Notification.SET: {
							ret.set(n.position, n.newValue as EObject)
						}
						default: {
							throw new RuntimeException('''WARNING: cannot deal with: «n»''')
						}
					}
					active.set(false)
				}
			}
		})
		ret.addObserver(new DefaultObserver<EObject> {
			override added(int index, EObject element) {
				if(!active.get) {
					active.set(true)
					r.contents.add(index, element)
					active.set(false)
				}
			}
			override moved(int newIndex, int oldIndex, EObject element) {
				if(!active.get) {
					active.set(true)
					r.contents.move(newIndex, oldIndex)
					active.set(false)
				}
			}
			override removed(int index, EObject element) {
				if(!active.get) {
					active.set(true)
					r.contents.remove(index)
					active.set(false)
				}
			}
			override replaced(int index, EObject newElement, EObject oldElement) {
				if(!active.get) {
					active.set(true)
					r.contents.set(index, newElement)
					active.set(false)
				}
			}
		})
		ret
	}
}
