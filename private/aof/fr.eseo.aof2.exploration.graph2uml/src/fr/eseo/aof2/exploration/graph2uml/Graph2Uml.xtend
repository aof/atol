package fr.eseo.aof2.exploration.graph2uml

import AOFGraph.AOFGraphPackage
import AOFGraph.Box
import AOFGraph.Operation
import AOFGraph.ParameterBox
import AOFGraph.SubGraph
import fr.eseo.aof2.exploration.graph2uml.DSL.Rule
import java.util.Collections
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.uml2.uml.Activity
import org.eclipse.uml2.uml.ActivityParameterNode
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.OpaqueAction
import org.eclipse.uml2.uml.resource.UMLResource

class Graph2Uml {
		
	
	extension DSL = new DSL
	extension UML = new UML
	extension AOF2 = new AOF2
	def static void main(String[] args) {
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"ecore",
			new EcoreResourceFactoryImpl
		)
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"*",
			new XMIResourceFactoryImpl
		)
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			UMLResource.FILE_EXTENSION,
			UMLResource.Factory.INSTANCE
		)

		rs.packageRegistry.put(AOFGraphPackage.eNS_URI, AOFGraphPackage.eINSTANCE)

		val sourceRes = rs.getResource(URI.createFileURI("collectM_example.xmi"), true)
		val s = sourceRes.contents.get(0) as SubGraph
		val targetRes = rs.createResource(URI.createFileURI("collectM_example.uml"))
		
		//Set default instance for  (ex abstract class) 
		AOF2.Parameter.defaultInstance = AOF2.ParameterBox.newInstance

		// apply transformation
		val model = new Graph2Uml().apply(sourceRes, targetRes)

		//test with EMF 
//		test(sourceRes, targetRes)
	
		targetRes.save(Collections.emptyMap)
//		sourceRes.URI = URI.createFileURI("collectM_example-reserialized.xmi")
//		sourceRes.save(Collections.emptyMap)
	}

//	def static test(Resource sourceRes, Resource targetRes) {.select(UML.OpaqueAction)
//		sourceRes.contents.add(AOF2.SubGraph.newInstance)
//		(targetRes.contents.get(0) as Model).packagedElements.get(0).name = "TOTO"
//	}
		
	def apply(Resource sourceRes, Resource targetRes) {
		val model = UML.Model.newInstance
		

		targetRes.contents.add(model)
//		model.packagedElements.addAll(sourceRes.contents.filter(EPackage).map[
//			ePackage2Package.apply(it)
//		])
		
		model._packagedElement.select(Activity).bind(
				sourceRes._contents
						.select(AOF2.SubGraph)
						.apply(AOFGraph2Activity)
						.asOrderedSet
		)
		model.packagedElements.get(0).name = "Main"
		model
	}


	new() {
		AOFGraph2ActivityRec = AOFGraph2Activity
	}

	var Rule<SubGraph, Activity> AOFGraph2ActivityRec
		// DONE: make <=> check type
	val Rule<SubGraph, Activity> AOFGraph2Activity =
		new Rule(AOF2.SubGraph, UML.Activity)[extension b, s, t |
			
//			t._ownedNode.select(UML.OpaqueAction).name<=>s._operations.opName
			t._ownedNode.select(UML.OpaqueAction) <=> s._operations.apply(Operation2OpaqueAction)
//			t._ownedNode.select(UML.ActivityParameterNode) <=> s._boxParameters.apply(ParameterBox2ActivityParameterNode)
//			t._ownedNode.select(UML.ActivityParameterNode) <=> s._outputs.apply(Outputs2ActivityParameterNode)
			t._ownedNode.select(UML.ActivityParameterNode) <=>
				s._outputs.apply(Outputs2ActivityParameterNode).concat(
				s._boxParameters.apply(ParameterBox2ActivityParameterNode))
			//cache global alors qu'il faut un cache / règle 
			t._ownedBehavior.select(UML.Activity) <=> s._operations.select(AOF2.CollectMutable).collector1.apply(AOFGraph2ActivityRec)
				
		]

	val Rule<Operation, OpaqueAction> Operation2OpaqueAction =
		new Rule(AOF2.Operation, UML.OpaqueAction)[extension b, s, t |
			
//			t._ownedNode.select(UML.OpaqueAction).name<=>s._operations.opName
//			t._ownedNode <=> s._operations.apply(Operation2OpaqueAction)
			t._name <=> s._opName
		]
	val Rule<Operation, Behavior> Operation2Behavior =
		new Rule(AOF2.Operation, UML.Activity)[extension b, s, t |
			
//			t._ownedNode.select(UML.OpaqueAction).name<=>s._operations.opName
//			t._ownedNode <=> s._operations.apply(Operation2OpaqueAction)
			t._name <=> s._opName
		]	
		
	val Rule<ParameterBox, ActivityParameterNode> ParameterBox2ActivityParameterNode =
		new Rule(AOF2.ParameterBox, UML.ActivityParameterNode)[extension b, s, t |
			
//			t._ownedNode.select(UML.OpaqueAction).name<=>s._operations.opName
//			t._ownedNode <=> s._operations.apply(Operation2OpaqueAction)
			t._name <=> s._name
		]
	val Rule<Box, ActivityParameterNode> Outputs2ActivityParameterNode =
		new Rule(AOF2.Box, UML.ActivityParameterNode)[extension b, s, t |
			
//			t._ownedNode.select(UML.OpaqueAction).name<=>s._operations.opName
//			t._ownedNode <=> s._operations.apply(Operation2OpaqueAction)
			t._name <=> s._inspectMessage
		]
}
