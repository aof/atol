package fr.eseo.aof2.exploration.graph2uml

import org.eclipse.uml2.uml.UMLPackage
import fr.eseo.aof.xtend.utils.AOFAccessors

class UML {
	@AOFAccessors.Field
	var UMLPackage p
}