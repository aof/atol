/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade.sample.umlrt;

import static org.eclipse.papyrus.aof.lang.Helpers.findEClass;
import static org.eclipse.papyrus.aof.lang.MutablePredicate.isInstanceOf;
import static org.eclipse.papyrus.aof.lang.MutablePredicate.isPropertyEmpty;
import static org.eclipse.papyrus.aof.lang.MutablePredicate.propertyEquals;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IMetaClass;
import org.eclipse.papyrus.aof.lang.MutablePredicate;
import org.eclipse.papyrus.aof.lang.Predicate;
import org.eclipse.papyrus.facade.Bindings;
import org.eclipse.papyrus.facade.Facade;
import org.eclipse.papyrus.facade.Rule;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

/**
 * {@link UMLRTFacade} is a sample {@link Facade} between the UMLRealTime profile and a specific UMLRT metamodel.
 * <p>
 * Beyond its functionality, this class is also intended as a tutorial.
 * Consequently, each rule, and each binding is commented.
 * It is not necessary for all Facade code to have such comments.
 * However, it is pedagogically useful here.
 * <p>
 * Each rule has a comment in the <a href="https://www.eclipse.org/atl/">ATL</a>-like syntax introduced in
 * the documentation of class {@link Rule}.
 * <p>
 * Each binding has a comment in OCL-like syntax, with notable differences being:
 * <ul>
 * 		<li>Not everything is statically typed.
 * 		<li>Let binds values to variables without <em>in</em> body.
 * 		<li>The binding operator <code>:=:</code> introduced in the documentation of class {@link Bindings}
 * 		is used in place of the equality operator to denote synchronization.
 * </ul>
 * These comments are meant to be read along with the Java source code, as a tutorial.
 * There are therefore not presented in the Javadoc documentation comments because this would defeat their
 * purpose.
 * <p>
 * Most bindings do not have a reverse predicate (see {@link Bindings}).
 * When it is present, it is to guard against illegal changes in target model.
 * This can happen because the target metamodel is too permissive (notably the
 * RedefinableElement::redefines property).
 * Another solution would be to improve the target metamodel, but this may not always be possible.
 * <p>
 * Most rules do not override the {@link Rule}<code>.reverseInit(Object)</code> method because their guard do not
 * require it. The Pseudostate rules are a notable exception.
 * <p> 
 * Rule ordering is important because the first rule that matches prevails.
 * <p>
 * Finally, although changes can be performed in either source or target model and be propagated either
 * way, the target model must be initially empty. This constraint may be relaxed in future versions.
 * 
 * @author Fr�d�ric Jouault
 *
 */
public class UMLRTFacade extends Facade {

	/**
	 * Creates a new UMLRTFacade.
	 * 
	 * @param source a source UML model with the UMLRealTime profile applied.
	 * @param target an empty target resource corresponding to the target model.
	 * @param targetMetamodel the target metamodel.
	 */
	public UMLRTFacade(Resource source, Resource target, Resource targetMetamodel) {
		super(source, target);

		UMLRT_Model = factory.getMetaClass(findEClass(targetMetamodel, "Model"));
		UMLRT_Capsule = factory.getMetaClass(findEClass(targetMetamodel, "Capsule"));
		UMLRT_Entity = factory.getMetaClass(findEClass(targetMetamodel, "Entity"));
		UMLRT_CapsulePart = factory.getMetaClass(findEClass(targetMetamodel, "CapsulePart"));
		UMLRT_Port = factory.getMetaClass(findEClass(targetMetamodel, "Port"));
		UMLRT_Connector = factory.getMetaClass(findEClass(targetMetamodel, "Connector"));
		UMLRT_ConnectorEnd = factory.getMetaClass(findEClass(targetMetamodel, "ConnectorEnd"));
		UMLRT_Attribute = factory.getMetaClass(findEClass(targetMetamodel, "Attribute"));
		UMLRT_StateMachine = factory.getMetaClass(findEClass(targetMetamodel, "StateMachine"));
		UMLRT_Protocol = factory.getMetaClass(findEClass(targetMetamodel, "Protocol"));
		UMLRT_CompositeState = factory.getMetaClass(findEClass(targetMetamodel, "CompositeState"));
		UMLRT_PassiveClass = factory.getMetaClass(findEClass(targetMetamodel, "PassiveClass"));
		UMLRT_SimpleState = factory.getMetaClass(findEClass(targetMetamodel, "SimpleState"));
		UMLRT_InitialPoint = factory.getMetaClass(findEClass(targetMetamodel, "InitialPoint"));
		UMLRT_ExitPoint = factory.getMetaClass(findEClass(targetMetamodel, "ExitPoint"));
		UMLRT_EntryPoint = factory.getMetaClass(findEClass(targetMetamodel, "EntryPoint"));
		UMLRT_JunctionPoint = factory.getMetaClass(findEClass(targetMetamodel, "JunctionPoint"));
		UMLRT_ChoicePoint = factory.getMetaClass(findEClass(targetMetamodel, "ChoicePoint"));
		UMLRT_DeepHistory = factory.getMetaClass(findEClass(targetMetamodel, "DeepHistory"));
		UMLRT_Transition = factory.getMetaClass(findEClass(targetMetamodel, "Transition"));
		UMLRT_Operation = factory.getMetaClass(findEClass(targetMetamodel, "Operation"));
		UMLRT_Parameter = factory.getMetaClass(findEClass(targetMetamodel, "Parameter"));

		this.init();
	}

	private void init() {
		/*	rule Model2Model {
		 *		from
		 *			source : UML::Model
		 *		to
		 *			target : UMLRT::Model (
		 *				target.classes :=: source.packagedElement->select(e | e.oclIsKindOf(UML::Class)),
		 *				target.protocols :=: 	self.packagedElement->select(e |
		 *											e.oclIsKindOf(UML::Package)
		 *										)->select(e |
		 *											e.isStereotypeApplied(UMLRealTime::ProtocolContainer)
		 *										)->first()->collect(e |
		 *											e.packagedElement
		 *										)->select(e |
		 *											e.oclIsKindOf(UML::Collaboration)
		 *										)->select(e |
		 *											e.isStereotypeApplied(UMLRealTime::Protocol)
		 *										) 
		 *			)
		 *	}
		 */
		addRule(new Rule<Model, EObject>(this, "Model2Model", UML_Model, UMLRT_Model) {
			public void bindContents(Bindings<Model, EObject> bindings, Model source, EObject target) {

				// let source_packagedElement = source.packagedElement
				IBox<PackageableElement> source_packagedElement = factory.createPropertyBox(source, "packagedElement");
				// let source_classes = source_packagedElement->select(e | e.oclIsKindOf(UML::Class))
				IBox<Class> source_classes = source_packagedElement.select(UML_Class);
				// target.classes :=: source_classes
				bindings.add(target, "classes", source_classes, UML_Class);

				// The previous binding could also be written with mutable predicate, but since it is not actually
				// mutable, that would be wasting boxes:
				// target.classes :=: source.packagedElement->select(e | e.oclIsKindOf(UML::Class))
//				bindings.add(target, "classes", source, "packagedElement", isInstanceOf(UML_Class));


				/*	let source_protocols = self.packagedElement->select(e |
				 *		e.oclIsKindOf(UML::Package)
				 *	)->select(e |
				 *		e.isStereotypeApplied(UMLRealTime::ProtocolContainer)
				 *	)->first()->collect(e |
				 *		e.packagedElement
				 *	)->select(e |
				 *			e.oclIsKindOf(UML::Collaboration)
				 *	)->select(e |
				 *		e.isStereotypeApplied(UMLRealTime::Protocol)
				 *	)
				 */
				IBox<Collaboration> source_protocols = source_packagedElement
					.select(UML_Package)
					.selectMutable(isStereotypeApplied(UMLRealTime_ProtocolContainer))
					.asOption() // we only look at the first package with stereotype ProtocolContainer
					.collectMutable(UML_Package, "packagedElement")
					.select(UML_Collaboration)
					.selectMutable(isStereotypeApplied(UMLRealTime_Protocol));

				// target.protocols :=: source_protocols
				bindings.add(target, "protocols", source_protocols, UML_Collaboration);
			}
		});

		/*	rule Class2Capsule extends Class2Entity {
		 *		from
		 *			source : UML::Class (
		 *				source.isStereotypeApplied(UMLRealTime::Capsule)
		 *			)
		 *		to
		 *			target : UMLRT::Capsule (
		 *				target.parts :=:	source.ownedAttribute->select(e |
		 *										e.isStereotypeApplied(UMLRealTime::CapsulePart)
		 *									),
		 *				target.ports :=:	source.ownedAttribute->select(e |
		 *										e.isStereotypeApplied(UMLRealTime::RTPort)
		 *									),
		 *				target.connectors :=:	source.ownedConnector->select(e |
		 *											e.isStereotypeApplied(UMLRealTime::RTConnector)
		 *										)
		 *			)
		 *	}
		 */
		addRule(new Rule<Class, EObject>(this, "Class2Capsule", UML_Class, UMLRT_Capsule, UMLRealTime_Capsule) {
			public void bindContents(Bindings<Class, EObject> bindings, Class source, EObject target) {
				bindEntityContents(bindings, source, target);

				// target.parts :=: source.ownedAttribute->select(e | e.isStereotypeApplied(UMLRealTime::CapsulePart))
				bindings.add(target, "parts", source, "ownedAttribute",
					isStereotypeApplied(UMLRealTime_CapsulePart)
				);
				// target.ports :=: source.ownedAttribute->select(e | e.isStereotypeApplied(UMLRealTime::RTPort))
				bindings.add(target, "ports", source, "ownedAttribute",
					isStereotypeApplied(UMLRealTime_RTPort)
				);
				// target.connectors :=: source.ownedConnector->select(e | e.isStereotypeApplied(UMLRealTime::RTConnector))
				bindings.add(target, "connectors", source, "ownedConnector",
					isStereotypeApplied(UMLRealTime_RTConnector)
				);
			}
		});

		/*	rule Property2Part extends NamedElement {
		 *		from
		 *			source : UML::Property (
		 *				source.isStereotypeApplied(UMLRealTime::CapsulePart) and
		 *				not source.oclIsKindOf(UML::Port)
		 *			)
		 *		to
		 *			target : UMLRT::CapsulePart (
		 *				target.type :=: source.type
		 *			)
		 *	}
		 */
		addRule(new Rule<Property, EObject>(this, "Property2Part", UML_Property, UMLRT_CapsulePart, UMLRealTime_CapsulePart, MutablePredicate.<Property>isInstanceOf(UML_Port).not()) {
			@Override
			public void bindContents(Bindings<Property, EObject> bindings, Property source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.type :=: source.type
				bindings.add("type", target, source);
			}
		});

		/*	rule Port extends NamedElement {
		 *		from
		 *			source : UML::Port (
		 *				source.isStereotypeApplied(UMLRealTime::RTPort)
		 *			)
		 *		to
		 *			target : UMLRT::Port (
		 *				target.type :=: source.type,
		 *				target.redefines->select(e |
		 *							e.oclIsKindOf(UMLRT::Port)
		 *						) :=: source.redefinedPort
		 *			)
		 *	}
		 */
		addRule(new Rule<Port, EObject>(this, "Port", UML_Port, UMLRT_Port, UMLRealTime_RTPort) {
			@Override
			public void bindContents(Bindings<Port, EObject> bindings, Port source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.type :=: source.type
				bindings.add("type", target, source);

				// target.redefines->select(e | e.oclIsKindOf(UMLRT::Port)) :=: source.redefinedPort
				bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_Port), source, "redefinedPort");
			}
		});

		/*	rule Connector extends NamedElement {
		 *		from
		 *			source : UML::Connector (
		 *				source.isStereotypeApplied(UMLRealTime::RTConnector)
		 *			)
		 *		to
		 *			target : UMLRT::Connector (
		 *				target.ends :=: source.ends
		 *			)
		 *	}
		 */
		addRule(new Rule<Connector, EObject>(this, "Connector", UML_Connector, UMLRT_Connector, UMLRealTime_RTConnector) {
			@Override
			public void bindContents(Bindings<Connector, EObject> bindings, Connector source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.ends :=: source.ends
				bindings.add(target, "ends", source, "end");
			}
		});

		/*	rule ConnectorEnd {
		 *		from
		 *			source : UML::ConnectorEnd
		 *		to
		 *			target : UMLRT::ConnectorEnd (
		 *				target.role :=: source.role
		 *			)
		 *	}
		 */
		addRule(new Rule<ConnectorEnd, EObject>(this, "ConnectorEnd", UML_ConnectorEnd, UMLRT_ConnectorEnd) {
			@Override
			public void bindContents(Bindings<ConnectorEnd, EObject> bindings, ConnectorEnd source, EObject target) {
				// target.role :=: source.role
				bindings.add("role", target, source);
			}
		});

		/*	rule Property2Attribute extends NamedElement {
		 *		from
		 *			source : UML::Property
		 *		to
		 *			target : UMLRT::Attribute (
		 *				target.type :=:	source.type->reject(e |
		 *									e.isStereotypeApplied(UMLRealTime::Capsule)
		 *								)
		 *			)
		 *	}
		 */
		addRule(new Rule<Property, EObject>(this, "Property2Attribute", UML_Property, UMLRT_Attribute) {
			@Override
			public void bindContents(Bindings<Property, EObject> bindings, Property source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.type :=: source.type->reject(e | e.isStereotypeApplied(UMLRealTime::Capsule))
				bindings.add("type", target, source, isStereotypeApplied(UMLRealTime_Capsule).not());
			}
		});

		/*	rule Collaboration2Protocol extends NamedElement {
		 *		from
		 *			source : UML::Collaboration (
		 *				source.isStereotypeApplied(UMLRealTime::Protocol)
		 *			)
		 *		to
		 *			target : UMLRT::Protocol (
		 *				target.redefines->select(e | e.oclIsKindOf(UMLRT::Protocol)) :=: source.redefinedClassifier
		 *			)
		 *	}
		 */
		addRule(new Rule<Collaboration, EObject>(this, "Collaboration2Protocol", UML_Collaboration, UMLRT_Protocol, UMLRealTime_Protocol) {
			@Override
			public void bindContents(Bindings<Collaboration, EObject> bindings, Collaboration source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.redefines->select(e | e.oclIsKindOf(UMLRT::Protocol)) :=: source.redefinedClassifier
				bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_Protocol), source, "redefinedClassifier");
			}
		});

		/*	rule StateMachine extends NamedElement {
		 *		from
		 *			source : UML::StateMachine (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTStateMachine)
		 *			)
		 *		to
		 *			target : UMLRT::StateMachine (
		 *				target.top :=: source.region,
		 *				target.redefines->select(e |
		 *							e.oclIsKindOf(UMLRT::StateMachine) or
		 *							e.oclIsKindOf(UMLRT_Entity)
		 *						) :=:	source.redefinedClassifier->reject(e |
		 *									e.oclIsKindOf(UML::Collaboration)
		 *								)
		 *			)
		 *	}
		 */
		addRule(new Rule<StateMachine, EObject>(this, "StateMachine", UML_StateMachine, UMLRT_StateMachine, UMLRealTimeStateMach_RTStateMachine) {
			@Override
			public void bindContents(final Bindings<StateMachine, EObject> bindings, StateMachine source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.top :=: source.region
				bindings.add(target, "top", source, "region");

				// target.redefines->select(e | e.oclIsKindOf(UMLRT::StateMachine) or e.oclIsKindOf(UMLRT_Entity)) :=: source.redefinedClassifier->reject(e | e.oclIsKindOf(UML::Collaboration))
				bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_StateMachine).or(Predicate.isInstanceOf(UMLRT_Entity)), source, "redefinedClassifier", isInstanceOf(UML_Collaboration).not());

			}
		});

		/*	rule TopRegion extends Region2CompositeType {
		 *		from
		 *			source : UML::Region
		 *		to
		 *			target : UMLRT::CompositeState
		 *	}
		 */
		addRule(new Rule<Region, EObject>(this, "TopRegion", UML_Region, UMLRT_CompositeState) {
			@Override
			public void bindContents(final Bindings<Region, EObject> bindings, Region source, EObject target) {
				bindCompositeTypeContents(bindings, source, target);
			}
		});

		/*	rule Class2Passive extends Class2Entity {
		 *		from
		 *			source : UML::Class (
		 *				not (
		 *					source.oclIsKindOf(UML::StateMachine) or
		 *					source.isStereotypeApplied(UMLRealTime::Capsule)
		 *				)
		 *			)
		 *		to
		 *			target : UMLRT::PassiveClass
		 *	}
		 */
		addRule(new Rule<Class, EObject>(this, "Class2Passive", UML_Class, UMLRT_PassiveClass,
					MutablePredicate.<Class>isInstanceOf(UML_StateMachine).or(isStereotypeApplied(UMLRealTime_Capsule)).not()
				) {
			public void bindContents(Bindings<Class, EObject> bindings, Class source, EObject target) {
				bindEntityContents(bindings, source, target);
			}
		});

		/*	rule SimpleState extends Vertex {
		 *		from
		 *			source : UML::State (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTState) and
		 *				source.region->isEmpty()
		 *			)
		 *		to
		 *			target : UMLRT::SimpleState (
		 *				target.redefines->select(e |
		 *							e.oclIsKindOf(UMLRT::SimpleState)
		 *						) :=: source.redefinedState
		 *			)
		 *	}
		 */
		addRule(new Rule<State, EObject>(this, "SimpleState", UML_State, UMLRT_SimpleState, UMLRealTimeStateMach_RTState, isPropertyEmpty("region")) {
			@Override
			public void bindContents(Bindings<State, EObject> bindings, State source, EObject target) {
				bindVertexContents(bindings, source, target);

				// target.redefines->select(e | e.oclIsKindOf(UMLRT::SimpleState)) :=: source.redefinedState
				bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_SimpleState), source, "redefinedState");
			}
		});

		/*	rule Initial extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::initial
		 *			)
		 *		to
		 *			target : UMLRT::InitialPoint
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "Initial", UML_Pseudostate, UMLRT_InitialPoint, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.INITIAL_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}
		});

		/*	rule ExitPoint extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::exitPoint
		 *			)
		 *		to
		 *			target : UMLRT::ExitPoint
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "ExitPoint", UML_Pseudostate, UMLRT_ExitPoint, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.EXIT_POINT_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}

			@Override
			public void reverseInit(Pseudostate source) {
				source.setKind(PseudostateKind.EXIT_POINT_LITERAL);
			}
		});

		/*	rule EntryPoint extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::entryPoint
		 *			)
		 *		to
		 *			target : UMLRT::EntryPoint
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "EntryPoint", UML_Pseudostate, UMLRT_EntryPoint, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.ENTRY_POINT_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}

			@Override
			public void reverseInit(Pseudostate source) {
				source.setKind(PseudostateKind.ENTRY_POINT_LITERAL);
			}
		});

		/*	rule JunctionPoint extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::junction
		 *			)
		 *		to
		 *			target : UMLRT::JunctionPoint
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "JunctionPoint", UML_Pseudostate, UMLRT_JunctionPoint, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.JUNCTION_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}

			@Override
			public void reverseInit(Pseudostate source) {
				source.setKind(PseudostateKind.JUNCTION_LITERAL);
			}
		});

		/*	rule ChoicePoint extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::choice
		 *			)
		 *		to
		 *			target : UMLRT::ChoicePoint
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "ChoicePoint", UML_Pseudostate, UMLRT_ChoicePoint, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.CHOICE_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}

			@Override
			public void reverseInit(Pseudostate source) {
				source.setKind(PseudostateKind.CHOICE_LITERAL);
			}
		});

		/*	rule DeepHistory extends Vertex {
		 *		from
		 *			source : UML::Pseudostate (
		 *				source.isStereotypeApplied(UMLRealTimeStateMach::RTPseudostate) and
		 *				source.kind = PseudostateKind::deepHistory
		 *			)
		 *		to
		 *			target : UMLRT::DeepHistory
		 *	}
		 */
		addRule(new Rule<Pseudostate, EObject>(this, "DeepHistory", UML_Pseudostate, UMLRT_DeepHistory, UMLRealTimeStateMach_RTPseudostate, propertyEquals("kind", PseudostateKind.DEEP_HISTORY_LITERAL)) {
			@Override
			public void bindContents(Bindings<Pseudostate, EObject> bindings, Pseudostate source, EObject target) {
				bindVertexContents(bindings, source, target);
			}

			@Override
			public void reverseInit(Pseudostate source) {
				source.setKind(PseudostateKind.DEEP_HISTORY_LITERAL);
			}
		});

		/*	rule Transition extends NamedElement {
		 *		from
		 *			source : UML::Transition
		 *		to
		 *			target : UMLRT::ExitPoint (
		 *				target.sourceVertex->reject(e |
		 *							e.oclIsKindOf(UMLRT::CompositeState)
		 *						) :=: source.source,
		 *				target.targetVertex->reject(e |
		 *							e.oclIsKindOf(UMLRT::CompositeState)
		 *						) :=: source.target,
		 *				target.redefines->select(e |
		 *							e.oclIsKindOf(UMLRT::Transition)
		 *						) :=: source.redefinedTransition
		 *			)
		 *	}
		 */
		addRule(new Rule<Transition, EObject>(this, "Transition", UML_Transition, UMLRT_Transition) {
			@Override
			public void bindContents(Bindings<Transition, EObject> bindings, Transition source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// target.sourceVertex->reject(e | e.oclIsKindOf(UMLRT::CompositeState)) :=: source.source
				bindings.add(target, "sourceVertex", Predicate.isInstanceOf(UMLRT_CompositeState).not(), source, "source");
				// target.targetVertex->reject(e | e.oclIsKindOf(UMLRT::CompositeState)) :=: source.target
				bindings.add(target, "targetVertex", Predicate.isInstanceOf(UMLRT_CompositeState).not(), source, "target");

				// target.redefines->select(e | e.oclIsKindOf(UMLRT::Transition)) :=: source.redefinedTransition
				bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_Transition), source, "redefinedTransition");
			}
		});

		/*	rule Operation extends NamedElement {
		 *		from
		 *			source : UML::Operation
		 *		to
		 *			target : UMLRT::Operation (
		 *				target.parameters :=:	source.ownedParameter->select(e |
		 *											e.direction = ParameterDirectionKind::in
		 *										),
		 *				target.returnType :=:	source_ownedParameter->select(e |
		 *											e.direction = ParameterDirectionKind::return
		 *										)->collect(e |
		 *											e.type
		 *										)
		 *			)
		 *	}
		 */
		addRule(new Rule<Operation, EObject>(this, "Operation", UML_Operation, UMLRT_Operation) {
			@Override
			public void bindContents(Bindings<Operation, EObject> bindings, Operation source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				// let source_ownedParamter = source.ownedParameter
				IBox<Parameter> source_ownedParameter = factory.createPropertyBox(source, "ownedParameter");

				// target.parameters :=: source_ownedParameter->select(e | e.direction = ParameterDirectionKind::in)
				bindings.add(target, "parameters", source_ownedParameter.selectMutable(propertyEquals("direction", ParameterDirectionKind.IN_LITERAL)), UML_Parameter);

				// target.returnType :=: source_ownedParameter->select(e | e.direction = ParameterDirectionKind::return)->collect(e | e.type)
				bindings.add(target, "returnType", source_ownedParameter.selectMutable(propertyEquals("direction", ParameterDirectionKind.RETURN_LITERAL)).<Classifier>collectMutable(UML_Parameter, "type"), UML_Classifier);
			}
		});

		/*	rule Parameter extends NamedElement {
		 *		from
		 *			source : UML::Parameter (
		 *				source.direction = ParameterDirectionKind::in
		 *			)
		 *		to
		 *			target : UMLRT::Operation (
		 *			)
		 *	}
		 */
		addRule(new Rule<Parameter, EObject>(this, "Parameter", UML_Parameter, UMLRT_Parameter, propertyEquals("direction", ParameterDirectionKind.IN_LITERAL)) {
			@Override
			public void bindContents(Bindings<Parameter, EObject> bindings, Parameter source, EObject target) {
				bindNamedElementContents(bindings, source, target);

				bindings.add("type", target, source);
			}
		});

		transformRootModel();
	}

	/*	abstract rule NamedElement {
	 *		from
	 *			source : UML::NamedElement
	 *		to
	 *			target : UMLRT::NamedElement (
	 *				target.name :=: source.name
	 *			)
	 *	}
	 */
	// target is NamedElement
	private <S extends NamedElement> void bindNamedElementContents(Bindings<S, EObject> bindings, S source, EObject target) {
		// target.name :=: source.name
		bindings.add("name", target, source);
	}

	/*	abstract rule Class2Entity extends NamedElement {
	 *		from
	 *			source : UML::Class
	 *		to
	 *			target : UMLRT::Entity (
	 *				target.attributes :=: 	source.ownedAttribute->reject(e |
	 *											e.oclIsKindOf(UML::Port) or
	 *											e.isStereotypeApplied(UMLRealTime::CapsulePart)
	 *										),
	 *				target.operations :=: source.ownedOperation,
	 *				target.behaviour :=: 	source.classifierBehavior->select(e |
	 *											e.isStereotypeApplied(UMLRealTimeStateMach::RTStateMachine)
	 *										),
	 *				target.redefines->select(e |
	 *							e.oclIsKindOf(UMLRT::Entity)
	 *						) :=:	source.redefinedClassifier->reject(e |
	 *									e.oclIsKindOf(UML::Collaboration)
	 *								)
	 *			)
	 *	}
	 */
	// target is Entity
	private <S extends Class> void bindEntityContents(Bindings<S, EObject> bindings, S source, EObject target) {
		bindNamedElementContents(bindings, source, target);

		// target.attributes :=: source.ownedAttribute->reject(e | e.oclIsKindOf(UML::Port) or e.isStereotypeApplied(UMLRealTime::CapsulePart))
		bindings.add(target, "attributes", source, "ownedAttribute", isInstanceOf(UML_Port).or(isStereotypeApplied(UMLRealTime_CapsulePart)).not());

		// target.operations :=: source.ownedOperation
		bindings.add(target, "operations", source, "ownedOperation");

		// target.behaviour :=: source.classifierBehavior->select(e | e.isStereotypeApplied(UMLRealTimeStateMach::RTStateMachine))
		bindings.add(target, "behaviour", source, "classifierBehavior", isStereotypeApplied(UMLRealTimeStateMach_RTStateMachine));

		// target.redefines->select(e | e.oclIsKindOf(UMLRT::Entity)) :=: source.redefinedClassifier->reject(e | e.oclIsKindOf(UML::Collaboration))
		bindings.add(target, "redefines", Predicate.isInstanceOf(UMLRT_Entity), source, "redefinedClassifier", isInstanceOf(UML_Collaboration).not());
	}

	/*	abstract rule Vertex extends NamedElement {
	 *		from
	 *			source : UML::Vertex
	 *		to
	 *			target : UMLRT::Entity (
	 *				target.incomingTransitions :=: source.incoming,
	 *				target.outgoingTransitions :=: source.outgoing
	 *			)
	 *	}
	 */
	private <S extends Vertex> void bindVertexContents(Bindings<S, EObject> bindings, S source, EObject target) {
		bindNamedElementContents(bindings, source, target);

		// We need these bindings because there is no opposite reference in the target metamodel.
		// target.incomingTransitions :=: source.incoming
		bindings.add(target, "incomingTransitions", source, "incoming");
		// target.outgoingTransitions :=: source.outgoing
		bindings.add(target, "outgoingTransitions", source, "outgoing");
	}

	/*	abstract rule Region2CompositeState extends NamedElement {
	 *		from
	 *			source : UML::Region
	 *		to
	 *			target : UMLRT::CompositeState (
	 *				target.initial :=:	source.subvertex->select(e |
	 *										e.oclIsKindOf(UML::Pseudostate)
	 *									)->select(e |
	 *										e.kind = PseudostateKind::initial
	 *									),
	 *				target.deepHistory :=:	source.subvertex->select(e |
	 *											e.oclIsKindOf(UML::Pseudostate)
	 *										)->select(e |
	 *											e.kind = PseudostateKind::deepHistory
	 *										),
	 *				target.junctionPoints :=:	source.subvertex->select(e |
	 *												e.oclIsKindOf(UML::Pseudostate)
	 *											)->select(e |
	 *												e.kind = PseudostateKind::junction
	 *											),
	 *				target.choicePoints :=:	source.subvertex->select(e |
	 *											e.oclIsKindOf(UML::Pseudostate)
	 *										)->select(e |
	 *											e.kind = PseudostateKind::choice
	 *										),
	 *				target.entryPoints :=:	source.subvertex->select(e |
	 *											e.oclIsKindOf(UML::Pseudostate)
	 *										)->select(e |
	 *											e.kind = PseudostateKind::entryPoint
	 *										),
	 *				target.exitPoints :=:	source.subvertex->select(e |
	 *											e.oclIsKindOf(UML::Pseudostate)
	 *										)->select(e |
	 *											e.kind = PseudostateKind::exitPoints
	 *										),
	 *				target.substates->reject(e |
	 *							e.oclIsKindOf(UMLRT::CompositeState)
	 *						) :=:	source.subvertex->select(e |
	 *									e.oclIsKindOf(UML::State)
	 *								),
	 *				target.transitions :=: source.transition
	 *			)
	 *	}
	 */
	// target is CompositeType
	private <S extends Region> void bindCompositeTypeContents(Bindings<S, EObject> bindings, S source, EObject target) {
		bindNamedElementContents(bindings, source, target);

		// let source_vertices = source.subvertex
		IBox<Vertex> source_vertices = factory.createPropertyBox(source, "subvertex");

		// let source_pseudoStates = source_vertices->select(e | e.oclIsKindOf(UML::Pseudostate))
		IBox<Pseudostate> source_pseudoStates = source_vertices.select(UML_Pseudostate);

		// target.initial :=: source_pseudoStates->select(e | e.kind = PseudostateKind::initial)
		bindings.add(target, "initial", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.INITIAL_LITERAL)), UML_Pseudostate);

		// target.deepHistory :=: source_pseudoStates->select(e | e.kind = PseudostateKind::deepHistory)
		bindings.add(target, "deepHistory", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.DEEP_HISTORY_LITERAL)), UML_Pseudostate);

		// target.junctionPoints :=: source_pseudoStates->select(e | e.kind = PseudostateKind::junction)
		bindings.add(target, "junctionPoints", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.JUNCTION_LITERAL)), UML_Pseudostate);

		// target.choicePoints :=: source_pseudoStates->select(e | e.kind = PseudostateKind::choice)
		bindings.add(target, "choicePoints", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.CHOICE_LITERAL)), UML_Pseudostate);

		// target.entryPoints :=: source_pseudoStates->select(e | e.kind = PseudostateKind::entryPoint)
		bindings.add(target, "entryPoints", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.ENTRY_POINT_LITERAL)), UML_Pseudostate);

		// target.exitPoints :=: source_pseudoStates->select(e | e.kind = PseudostateKind::exitPoints)
		bindings.add(target, "exitPoints", source_pseudoStates.selectMutable(propertyEquals("kind", PseudostateKind.EXIT_POINT_LITERAL)), UML_Pseudostate);

		// target.substates->reject(e | e.oclIsKindOf(UMLRT::CompositeState)) :=: source_vertices->select(e | e.oclIsKindOf(UML::State))
		bindings.add(target, "substates", Predicate.isInstanceOf(UMLRT_CompositeState).not(), source_vertices.select(UML_State), UML_State);

		// target.transitions :=: source.transition
		bindings.add(target, "transitions", source, "transition");
	}

	// IMetaClasses from target metamodel
	// EObject is the generic type parameter because we use dynamic EMF, and not generated code.
	private final IMetaClass<EObject> UMLRT_Model;
	private final IMetaClass<EObject> UMLRT_Capsule;
	private final IMetaClass<EObject> UMLRT_Entity;
	private final IMetaClass<EObject> UMLRT_CapsulePart;
	private final IMetaClass<EObject> UMLRT_Port;
	private final IMetaClass<EObject> UMLRT_Connector;
	private final IMetaClass<EObject> UMLRT_ConnectorEnd;
	private final IMetaClass<EObject> UMLRT_Attribute;
	private final IMetaClass<EObject> UMLRT_StateMachine;
	private final IMetaClass<EObject> UMLRT_Protocol;
	private final IMetaClass<EObject> UMLRT_CompositeState;
	private final IMetaClass<EObject> UMLRT_PassiveClass;
	private final IMetaClass<EObject> UMLRT_SimpleState;
	private final IMetaClass<EObject> UMLRT_InitialPoint;
	private final IMetaClass<EObject> UMLRT_ExitPoint;
	private final IMetaClass<EObject> UMLRT_EntryPoint;
	private final IMetaClass<EObject> UMLRT_JunctionPoint;
	private final IMetaClass<EObject> UMLRT_ChoicePoint;
	private final IMetaClass<EObject> UMLRT_DeepHistory;
	private final IMetaClass<EObject> UMLRT_Transition;
	private final IMetaClass<EObject> UMLRT_Operation;
	private final IMetaClass<EObject> UMLRT_Parameter;

	// Stereotypes from source profile
	private final String UMLRealTime_Capsule = "UMLRealTime::Capsule";
	private final String UMLRealTime_ProtocolContainer = "UMLRealTime::ProtocolContainer";
	private final String UMLRealTime_Protocol = "UMLRealTime::Protocol";
	private final String UMLRealTime_CapsulePart = "UMLRealTime::CapsulePart";
	private final String UMLRealTime_RTPort = "UMLRealTime::RTPort";
	private final String UMLRealTime_RTConnector = "UMLRealTime::RTConnector";
	private final String UMLRealTimeStateMach_RTStateMachine = "UMLRealTimeStateMach::RTStateMachine";
	private final String UMLRealTimeStateMach_RTState = "UMLRealTimeStateMach::RTState";
	private final String UMLRealTimeStateMach_RTPseudostate = "UMLRealTimeStateMach::RTPseudostate";
}
