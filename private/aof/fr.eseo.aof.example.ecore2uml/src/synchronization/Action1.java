package synchronization;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.uml2.uml.Model;

public class Action1 implements IObjectActionDelegate {

	public Action1() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run(IAction action) {
		EPackage s = (EPackage)((StructuredSelection)selection).getFirstElement();
		new Ecore2UML().apply(s.eResource(), s.eResource());
	}

	private ISelection selection;

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

}
