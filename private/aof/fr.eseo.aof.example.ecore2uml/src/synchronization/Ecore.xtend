package synchronization

import fr.eseo.aof.xtend.utils.AOFAccessors
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.papyrus.aof.core.IMetaClass

//@AOFAccessors(ePackage=#[
//	"org.eclipse.emf.ecore.EcorePackage"
//])
class Ecore {
	@AOFAccessors.Field
	var EcorePackage p

	// for testing purposes
	def static <E extends ENamedElement> E create(IMetaClass<E> type, String name) {
		val ret = type.newInstance
		ret.name = name
		ret as E
	}
}