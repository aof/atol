package synchronization

import fr.eseo.aof.xtend.utils.AOFAccessors
import org.eclipse.papyrus.aof.core.IMetaClass
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.UMLPackage

//@AOFAccessors(ePackage=#[
//	"org.eclipse.uml2.uml.UMLPackage"
//])
class UML {
	@AOFAccessors.Field
	var UMLPackage p

	// for testing purposes
	def static <E extends NamedElement> E create(IMetaClass<E> type, String name) {
		val ret = type.newInstance
		ret.name = name
		ret as E
	}
}