package synchronization

import java.util.Collections
import java.util.Set
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.ETypedElement
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IMetaClass
import org.eclipse.uml2.uml.AggregationKind
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.MultiplicityElement
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.VisibilityKind
import org.eclipse.uml2.uml.resource.UMLResource
import synchronization.DSL.Binder
import synchronization.DSL.Rule

import static extension synchronization.Ecore.create
import static extension synchronization.UML.create

// TODO: investigate all items marked as DONE with the team to see if we can come up with
// better solutions


// Can be put here, but better to put in a separate type for incremental compilation
//@AOFAccessors(ePackage=#[
//	"org.eclipse.uml2.uml.UMLPackage",
//	"org.eclipse.emf.ecore.EcorePackage"
//])
class Ecore2UML {
	extension DSL = new DSL
	extension UML = new UML
	extension Ecore = new Ecore

	def static void main(String[] args) {
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			"ecore",
			new EcoreResourceFactoryImpl
		)
		rs.resourceFactoryRegistry.extensionToFactoryMap.put(
			UMLResource.FILE_EXTENSION,
			UMLResource.Factory.INSTANCE
		)
		val sourceRes = rs.getResource(URI.createFileURI("ClassDiagram.ecore"), true)
		val s = sourceRes.contents.get(0) as EPackage

		val targetRes = rs.createResource(URI.createFileURI("ClassDiagram.uml"))
		val model = new Ecore2UML().apply(sourceRes, targetRes)
		val t = model.packagedElements.get(0) as Package

		// Perform changes to "test" propagation
		t.name = t.name + "2"
		s.EClassifiers.add(Ecore.EClass.create("c1"))
		s.EClassifiers.add(Ecore.EDataType.create("dt1"))
		t.packagedElements.add(
			UML.Enumeration.c(
				"name"->"enum1",
				"ownedLiteral"->#[UML.EnumerationLiteral.c(
					"name"->"el1"
				)]
			)
		)
		val uc = UML.Class.newInstance
		uc.name = "TESTBindingDirection"
		t.packagedElements.add(uc)
		sourceRes.contents.add(Ecore.EPackage.create("ep1"))
		sourceRes.contents.remove(2)
		sourceRes.contents.move(0, 1)
		model.packagedElements.add(UML.Package.create("p2"))
		sourceRes.contents.addAll(0, #[Ecore.EPackage.create("ep3"), Ecore.EPackage.create("ep4")])
		model.packagedElements.addAll(0, #[UML.Package.create("p5"), UML.Package.create("p6")])
		// TODO: improve error reporting when element not matched by a rule
		// (test by commenting out t._packagedElement.select(Package) <=> ...)
		// Remark: when explicitly specifying the generic parameters to apply,
		// one must make sure that the target of the binding has an appropriate select
		// Actually, specifying a target select is sometimes enough to avoid needing
		// specifying the generic parameters to apply
		t.packagedElements.add(UML.Package.create("p7"))
//		t.packagedElements.add(UML.Association.newInstance)
//		println(s)

		targetRes.save(Collections.emptyMap)

		sourceRes.URI = URI.createFileURI("ClassDiagram-reserialized.ecore")
		sourceRes.save(Collections.emptyMap)
	}

	// for testing purposes
	def static <E extends EObject> E c(IMetaClass<E> type, Pair<String,Object>...slots) {
		val ret = type.newInstance
		slots.forEach[
			val f = ret.eClass.getEStructuralFeature(key)
			ret.eSet(f, value)
		]
		ret as E
	}

	def apply(Resource sourceRes, Resource targetRes) {
		val model = UML.Model.newInstance
		targetRes.contents.add(model)
//		model.packagedElements.addAll(sourceRes.contents.filter(EPackage).map[
//			ePackage2Package.apply(it)
//		])
		model._packagedElement.select(Package).bind(
				sourceRes._contents
						.select(Ecore.EPackage)
						.apply(ePackage2Package)
						.asOrderedSet
		)

		model
	}

	// DONE: make <=> check type
	val Rule<EPackage, Package> ePackage2Package_ =
		new Rule(Ecore.EPackage, UML.Package)[extension b, s, t |
			t._name <=> s._name

			t._packagedElement.select(Classifier) <=>
				s._eClassifiers
					.<EClassifier, Classifier>apply(
						eClass2Class, eDataType2PrimitiveType,
						eEnum2Enumeration
//					).union(	// TODO: make union bidir
//						s._eClassifiers	// TODO: support associations between references that are in different packages
//							.select(EClass)
//							.eStructuralFeatures
//							.select(EReference)
//							.selectMutable[it._eOpposite.collect[it !== null].asOne(false)]
//							.collectMutable[r |
//								r?._eOpposite?.collect[#{r, it}] ?: emptyOne
//							]
//							.<Set<EReference>, PackageableElement>apply(eReferencePair2Association)
					)//.inspect('''«s.name».packagedElement=''')

			t._packagedElement.select(Package) <=>
							s._eSubpackages.apply(ePackage2Package)
		]
	val Rule<EPackage, Package> ePackage2Package = ePackage2Package_

	// DONE: change binding direction appropriately
	val Rule<EClass, Class> eClass2Class_ =
		new Rule(Ecore.EClass, UML.Class)[extension b, s, t |
			t._name <=> s._name

			// TODO: requires a collectBox that creates missing intermediate elements
//			t._generalization.general(UML.Generalization).select(Class) <=> s._eSuperTypes.apply(eClass2Class)//.inspect('''«s.name».superTypes=''')
			t._generalization.general.select(Class) <=> s._eSuperTypes.apply(eClass2Class)//.inspect('''«s.name».superTypes=''')

			t._isAbstract.asOne(false) <=> s._abstract

			t._ownedAttribute <=> s._eStructuralFeatures.apply(
								eAttribute2Property, eReference2Property
							)

			t._ownedOperation <=> s._eOperations.apply(eOperation2Operation)
		]
	val Rule<EClass, Class> eClass2Class = eClass2Class_

	val eAttribute2Property =
		new Rule(Ecore.EAttribute, UML.Property)[extension b, s, t |
			eStructuralFeature2Property(b, s, t)

//			(t._isUnique as IOne<Boolean>).clear(false)
//			t._isUnique.asOne(false) <=> s._unique

			t._type <=> s._eType.apply(eDataType2PrimitiveType)
		]

	val eReference2Property =
		new Rule(Ecore.EReference, UML.Property)[extension b, s, t |
			eStructuralFeature2Property(b, s, t)

			t._aggregation <=> s._containment.ifThen(
						AggregationKind.COMPOSITE_LITERAL,
						AggregationKind.NONE_LITERAL
					)

			// TODO: why don't we get a notification for assoc?
//			t._association.inspect("assoc=") <=> s._eOpposite
//								.select[it !== null]
//								.collect[#{s, it}]
//								.apply(
//									eReferencePair2Association
//								)

			t._type <=> s._eType.apply(eClass2Class)
		]

	val eReferencePair2Association =
		new Rule(new IMetaClass<Set<EReference>> {
			override getDefaultInstance() {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
			override <E> getPropertyAccessor(Object property) {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
			override isInstance(Object object) {
				object instanceof Set<?>
			}
			override isSubTypeOf(IMetaClass<?> that) {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
			override newInstance() {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
			override setDefaultInstance(Set<EReference> defaultInstance) {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
		}, UML.Association)[extension b, s, t |
			println(s)
			val i = s.iterator
			val first = i.next
			val second = i.next
			t._name <=> first._name.zipWith(second._name)['''A_«$0»_«$1»''']
		]

	def eStructuralFeature2Property(extension Binder b, EStructuralFeature s, Property t) {
		t._name <=> s._name

		// DONE: force isPublic to true
		t.visibility = VisibilityKind.PUBLIC_LITERAL

		t._isReadOnly.asOne(false) <=> s._changeable

		t._isDerived.asOne(false) <=> s._derived
	}

	def eTypedElement2MultiplicityElement(extension Binder b, ETypedElement s, MultiplicityElement t) {
		// TODO: make this work
		// e.g., with a collectMutable that can create missing source elements
		//	<R> IBox<E>.collectMutable((E)=>IBox<R> collector, (R)=>E generator)
		// and (possibly) a generated syntax that specifies the type of elements
		// like:
//		t._lowerValue.value8(UML!LiteralInteger) <=> s._lowerBound
		t._lowerValue.select(UML.LiteralInteger).value8 <=> s._lowerBound
		t._upperValue.select(UML.LiteralUnlimitedNatural).value11 <=> s._upperBound

		// DONE: fix these, which fails because isOrdered does not support being cleared (NPE)
		// why isDerived&isUnique work? but not isUnique for attributes!
		//	=> in fact, it works if the two boxes are same, because then no assignment occurs
		// asOne(false) is a workaround
		// ideally, the property accessor should set an appropriate defaultElement on ones
		t._isOrdered.asOne(false) <=> s._ordered

		t._isUnique.asOne(false) <=> s._unique
	}

	val eDataType2PrimitiveType =
		new Rule(Ecore.EDataType, UML.PrimitiveType)[extension b, s, t |
			t._name <=> s._name
		]

	def <E> IBox<Boolean> !=(IBox<E> left, E right) {
		left.collect[it != right]
	}

	def <E> IBox<Boolean> ==(IBox<E> left, E right) {
		left.collect[it == right]
	}

	def <E> selectM(IBox<E> box, (E)=>IBox<Boolean> selector) {
		box.selectMutable[selector.apply(it).asOne(false)]
	}

	val eOperation2Operation =
		new Rule(Ecore.EOperation, UML.Operation)[extension b, s, t |
			t._name <=> s._name

			t._ownedParameter.selectM[
				it._direction != ParameterDirectionKind.RETURN_LITERAL
			] <=> s._eParameters.apply(eParameter2Parameter)

			// TODO: how to force the UML parameter to be created?
			t._ownedParameter.selectM[
				it._direction == ParameterDirectionKind.RETURN_LITERAL
			].type <=> s._eType.apply(eClass2Class, eDataType2PrimitiveType, eEnum2Enumeration)

			// exceptions, generics?
		]

	val eParameter2Parameter =
		new Rule(Ecore.EParameter, UML.Parameter)[extension b, s, t |
			t._name <=> s._name
			// generics?

			eTypedElement2MultiplicityElement(b, s, t)

			t._type <=> s._eType.apply(
							eClass2Class,
							eDataType2PrimitiveType
						)
		]

	val eEnum2Enumeration =
		new Rule(Ecore.EEnum, UML.Enumeration)[extension b, s, t |
			t._name <=> s._name

			t._ownedLiteral <=> s._eLiterals.apply(eEnumLiteral2EnumerationLiteral)
		]

	val eEnumLiteral2EnumerationLiteral =
		new Rule(Ecore.EEnumLiteral, UML.EnumerationLiteral)[extension b, s, t |
			t._name <=> s._name
		]
}