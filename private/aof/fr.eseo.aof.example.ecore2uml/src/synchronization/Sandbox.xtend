package synchronization

import org.eclipse.papyrus.aof.core.AOFFactory

class Sandbox {
	def static void main(String[] args) {
		val factory = AOFFactory.INSTANCE
		val a = factory.createSequence(1, 2, 3)
		a.inspect("a=")
		val b = a.collect([e | e + 1], [e | e - 1])
		b.inspect("b=")
		val c = b.select[e | e > 2].inspect("c=")

//		a.add(4)
//
//		a.add(0, 5)
//
//		a.removeAt(1)
//		b.add(5)
		c.add(10)
	}
}