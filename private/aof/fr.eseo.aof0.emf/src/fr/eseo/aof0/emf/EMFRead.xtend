package fr.eseo.aof0.emf

import fr.eseo.dis.aof0.core.Addition
import fr.eseo.dis.aof0.core.Constraints
import fr.eseo.dis.aof0.core.Graph
import fr.eseo.dis.aof0.core.Mutation
import fr.eseo.dis.aof0.core.Removal
import fr.eseo.dis.aof0.core.operations.Read
import fr.eseo.dis.aof0.core.operations.Write
import java.util.Collection
import org.eclipse.emf.common.notify.Notification
import org.eclipse.emf.common.notify.impl.AdapterImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

class EMFRead extends Read {
    
    new(Graph graph, Constraints constraints, Object property, Write reverse) {
        super(graph, constraints, property, reverse)

        val property_ = property as Pair<EObject, EStructuralFeature>
//        val boolean[] initStarted = #[false]
        property_.key.eAdapters.add(new AdapterImpl {
            override notifyChanged(Notification msg) {
                if(msg.feature === property_.value) {
                    switch msg.eventType {
                        case Notification.ADD: {
                            send(new Addition(msg.newValue))
                        }
                        case Notification.SET: {
//                            if(initStarted.get(0)) {   // not removing if init (and therefore no add before)
//                            if(msg.oldValue !== msg.newValue) {
                                if(msg.oldValue !== null) send(new Removal(msg.oldValue))
                                if(msg.newValue !== null) send(new Addition(msg.newValue))
//                            }
                        }
                        default: {
                            throw new RuntimeException('''Unsupported EMF change event «msg»''')
                        }
                    }
//                    initStarted.set(0, true)
                }
            }
        })
    }

    private def send(Mutation mutation) {
        getInput().getMutationStream().push(mutation);
        getController().propagate(this);
    }

    override initialize() {
        val property_ = property as Pair<EObject, EStructuralFeature>
        val v = property_.key.eGet(property_.value)
        switch v {
            Collection<?>: {
                for (Object e : v) {
                    val addition = new Addition(e)
                    getInput().getMutationStream().push(addition)
                }
            }
            default: {
                val addition = new Addition(v)
                getInput().getMutationStream().push(addition)
            }
        }
    }
}