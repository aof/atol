package fr.eseo.aof0.emf

import fr.eseo.dis.aof0.core.Addition
import fr.eseo.dis.aof0.core.Constraints
import fr.eseo.dis.aof0.core.Graph
import fr.eseo.dis.aof0.core.Platform
import fr.eseo.dis.aof0.core.Removal
import fr.eseo.dis.aof0.core.operations.Read
import fr.eseo.dis.aof0.core.operations.Write
import java.util.Collection
import java.util.HashMap
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

class EMFPlatform implements Platform {
    override constraintsMatch(Constraints constraints, Object property_) {
        val property = property_ as Pair<EObject, EStructuralFeature>
        switch constraints {
            case SET: {
                property.value.many && property.value.unique && !property.value.ordered
            }
            case SEQ: {
                property.value.many && !property.value.unique && property.value.ordered
            }
            case OSET: {
                property.value.many && property.value.unique && property.value.ordered
            }
            case BAG: {
                property.value.many && !property.value.unique && !property.value.ordered
            }
            case OPT: {
                !property.value.many && property.value.lowerBound == 0
            }
            case ONE: {
                !property.value.many && property.value.lowerBound == 1
            }
            default: {
//                    assert false : "Invalid constraints " + constraints;
                false
            }
        }
    }

    // This should be in the super class/interface
    override read(Graph graph, Object property) {
        if (constraintsMatch(Constraints.SET, property)) {
            return read(graph, Constraints.SET, property)
        }
        else if (constraintsMatch(Constraints.SEQ, property)) {
            return read(graph, Constraints.SEQ, property)
        }
        else if (constraintsMatch(Constraints.BAG, property)) {
            return read(graph, Constraints.BAG, property)
        }
        else if (constraintsMatch(Constraints.OSET, property)) {
            return read(graph, Constraints.OSET, property)
        }
        else if (constraintsMatch(Constraints.OPT, property)) {
            return read(graph, Constraints.OPT, property)
        }
        else if (constraintsMatch(Constraints.ONE, property)) {
            return read(graph, Constraints.ONE, property)
        }
        else {
//                assert false : "Property " + property + " is not a valid type."
            return null
        }
    }

	val Map<Object,EMFRead> readByProperty = new HashMap
    override read(Graph graph, Constraints constraints, Object property) {
    	var ret = readByProperty.get(property)
    	if(ret === null) {
        	ret = new EMFRead(graph, constraints, property, null)
//        	readByProperty.put(property, ret)
        }
        ret
    }

    override supports(Object property) {
        if(property instanceof Pair<?, ?>) {
             property.key instanceof EObject && property.value instanceof EStructuralFeature
        } else {
            false
        }
    }

    override write(Graph graph, Object property) {
        new EMFWrite(graph, property, null)
    }

	static class EMFWrite extends Write {
		new(Graph graph, Object property, Read reverse) {
			super(graph, property, reverse)
		}
		private def property_() {
			this.property as Pair<EObject, EStructuralFeature>
		}
        override apply() {
            // apply the mutations to the property *without* emptying the stream
            // (this will be done when calling the super implementation)
            val stream = getInput.getMutationStream
            if(property_.value.many) {
                val v = property_.key.eGet(property_.value) as Collection<Object>
                for (mutation : stream) {
//                                assert mutation != null : "A stream cannot contain null mutation.";
                    if (mutation instanceof Addition) {
                        v.add(mutation.element)
                    }
                    else if (mutation instanceof Removal) {
                        v.remove(mutation.element)
                    }
                    else {
//                                    assert false : "Note handled mutation type:" + mutation.getClass();
                    }
                }
            } else {
                for (mutation : stream) {
//                                assert mutation != null : "A stream cannot contain null mutation.";
                    if (mutation instanceof Addition) {
                        property_.key.eSet(property_.value, mutation.element)
                    }
                    else if (mutation instanceof Removal) {
                        property_.key.eUnset(property_.value)
                    }
                    else {
//                                    assert false : "Note handled mutation type:" + mutation.getClass();
                    }
                }
            }
            // moves the mutations to the output
            super.apply();
        }
	}
}