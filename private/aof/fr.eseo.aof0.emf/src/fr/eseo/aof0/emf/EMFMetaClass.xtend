package fr.eseo.aof0.emf

import org.eclipse.emf.ecore.EClass
import org.eclipse.xtend.lib.annotations.Data

@Data
class EMFMetaClass<T> implements IMetaClass<T> {
    val EClass ec
    override newInstance() {
        ec.EPackage.EFactoryInstance.create(ec) as T
    }
}
