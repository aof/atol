package fr.eseo.aof0.xtend.utils

import java.util.HashMap
import java.util.Map
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EPackage
import org.eclipse.xtend.lib.macro.AbstractClassProcessor
import org.eclipse.xtend.lib.macro.AbstractFieldProcessor
import org.eclipse.xtend.lib.macro.Active
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

@Active(AOF0AccessorsProcessor)
annotation AOF0Accessors {
	String[] ePackage

	@Active(FieldAOF0AccessorsProcessor)
	annotation Field {}

	interface Utils {
		val factory = "org.eclipse.papyrus.aof.emf.EMFFactory.INSTANCE"

		def extend(MutableClassDeclaration cls, String ePackageName, extension TransformationContext context, Map<String, Integer> alreadyProcessed) {
			val iBoxType = "fr.eseo.dis.aof0.dsl.Box".findTypeGlobally
			val ePackage = Class.forName(ePackageName).getField("eINSTANCE").get(null) as EPackage

            val pairType = Pair.findTypeGlobally.newTypeReference(
                EObject.findTypeGlobally.newTypeReference,
                EStructuralFeature.findTypeGlobally.newTypeReference
            )

			ePackage.EClassifiers.filter(EClass)
				.forEach[c |
					val cType = c.instanceClass.findTypeGlobally?.newTypeReference
					cls.addField('''«c.name»''')[
						primarySourceElement = cls.primarySourceElement
						static = true
						visibility = Visibility.PUBLIC
						type = "fr.eseo.aof0.emf.IMetaClass".findTypeGlobally?.newTypeReference(cType)
						initializer = '''
							new fr.eseo.aof0.emf.EMFMetaClass<«cType»>(«ePackageName».eINSTANCE.get«c.name.escape»())
						'''
					]
					c.EStructuralFeatures
						.filter[!derived]
						.forEach[f |
							val n = alreadyProcessed.get(f.name) ?: 0
							alreadyProcessed.put(f.name, n + 1)
							val suffix = if(n == 0) {""} else {n}
	
							val retType = iBoxType?.newTypeReference(f.EType.instanceClass.findTypeGlobally?.newTypeReference)
                            cls.addMethod('''_«f.name»''')[
                                primarySourceElement = cls.primarySourceElement
                                addParameter("o", cType)
                                returnType = retType
                                body = '''
                                    return tr.read(new «pairType»(o, «ePackageName».eINSTANCE.get«c.name»_«f.name.toFirstUpper»()));
                                '''
                            ]
                            cls.addMethod('''__«f.name»''')[
                                primarySourceElement = cls.primarySourceElement
                                addParameter("o", cType)
                                returnType = pairType
                                body = '''
                                    return new «pairType»(o, «ePackageName».eINSTANCE.get«c.name»_«f.name.toFirstUpper»());
                                '''
                            ]
							cls.addMethod('''«f.name»«suffix»''')[
								primarySourceElement = cls.primarySourceElement
								addParameter("b", iBoxType?.newTypeReference(cType.newWildcardTypeReference))
								returnType = retType
								body = '''
									return b.collectBy(it -> tr.read(new «pairType»(it, «ePackageName».eINSTANCE.get«c.name»_«f.name.toFirstUpper»())));
								'''
							]
						]
				]
		}

		static val toEscape = #{"Class"}
		def static escape(String s) {
			if(toEscape.contains(s)) {
				'''«s»_'''
			} else {
				s
			}
		}
	}

	static class AOF0AccessorsProcessor extends AbstractClassProcessor implements Utils {
		override doTransform(MutableClassDeclaration it, extension TransformationContext context) {
			println("Generating accessors")
			val cls = it

			findAnnotation(AOF0Accessors.findTypeGlobally)
				.getStringArrayValue("ePackage")
				.forEach[ePackageName |
					cls.extend(
						ePackageName,
						context,
						new HashMap
					)
				]
		}
	}

	static class FieldAOF0AccessorsProcessor extends AbstractFieldProcessor implements Utils {
		override doTransform(MutableFieldDeclaration it, extension TransformationContext context) {
			val cls = it.declaringType as MutableClassDeclaration
			cls.extend(
				type.name,
				context,
				new HashMap
			)
		}
	}
}