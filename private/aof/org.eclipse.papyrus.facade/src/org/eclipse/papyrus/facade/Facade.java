/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IFactory;
import org.eclipse.papyrus.aof.core.IMetaClass;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.impl.operation.Inspect;
import org.eclipse.papyrus.aof.core.impl.utils.cache.StrongKeysStrongValuesUnaryCache;
import org.eclipse.papyrus.aof.emf.EMFFactory;
import org.eclipse.papyrus.aof.lang.MutablePredicate;
import org.eclipse.papyrus.aof.lang.Predicate;
import org.eclipse.papyrus.infra.core.listenerservice.IPapyrusListener;
import org.eclipse.papyrus.uml.tools.listeners.PapyrusStereotypeListener;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;

// source is profiled UML, target is a DSL metamodel
/**
 * A {@link Facade} is a transformation from a profiled UML model into a domain-specific modeling
 * language (DSML).
 * <p>
 * Concrete {@link Facade}s should subclass the present class and register {@link Rule}s before
 * calling {@link #transformRootModel()}.
 * 
 * @author Fr�d�ric Jouault
 *
 */
public abstract class Facade {

	private final boolean enableInspect = false;

	protected final UMLPackage UML = UMLPackage.eINSTANCE;
	protected final IFactory factory = EMFFactory.INSTANCE;

	protected Resource source;
	protected Resource target;

	private List<Rule<EObject, EObject>> rules = new ArrayList<Rule<EObject, EObject>>();

	private static class PapyrusListenerAdapter extends EContentAdapter {
		private IPapyrusListener papyrusListener;

		public PapyrusListenerAdapter(IPapyrusListener papyrusStereotypeListener) {
			this.papyrusListener = papyrusStereotypeListener;
		}

		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			papyrusListener.notifyChanged(notification);
		}
	}

	/**
	 * Creates a new {@link Facade}.
	 * <p>
	 * 
	 * @param source the {@link Resource} corresponding to the source profile UML model.
	 * @param target the {@link Resource} corresponding to the target DSML metamodel.
	 */
	public Facade(Resource source, Resource target) {
		this.source = source;
		this.target = target;

		boolean hasPapyrusListenerAdapter = false;
		for(Adapter adapter : this.source.eAdapters()) {
			if(adapter instanceof PapyrusListenerAdapter) {
				hasPapyrusListenerAdapter = true;
			}
		}
		if(!hasPapyrusListenerAdapter) {
			this.source.eAdapters().add(new PapyrusListenerAdapter(new PapyrusStereotypeListener()));
		}

		Model sourceModel = (Model) source.getContents().get(0);
		tempModel = UML_Model.newInstance();
		for(Profile profile : sourceModel.getAllAppliedProfiles()) {
			tempModel.applyProfile(profile);
		}
		tempClass = UML_Class.newInstance();
		tempModel.getPackagedElements().add(tempClass);
		tempStateMachine = UML_StateMachine.newInstance();
		tempClass.getOwnedBehaviors().add(tempStateMachine);
		tempRegion = UML_Region.newInstance();
		tempStateMachine.getRegions().add(tempRegion);
		tempTransition = UML_Transition.newInstance();
		tempRegion.getTransitions().add(tempTransition);
	}

	/**
	 * Transforms the root {@link Model}.
	 * <p>
	 * This method must be called by every concrete Facade after registering its rules.
	 * 
	 */
	protected void transformRootModel() {
		Model sourceModel = (Model) source.getContents().get(0);
		EObject targetModel = transform(AOFFactory.INSTANCE.createOne(sourceModel), UML_Model).get(0);
		target.getContents().add(targetModel);
		targetModel.eAdapters().add(new BindingDisabler());
	}

	private class BindingDisabler extends EContentAdapter {
		@Override
		protected void addAdapter(Notifier notifier) {
			super.addAdapter(notifier);
			if(notifier instanceof EObject) {
				Bindings<?, ?> bindings = Facade.this.bindingsByTransformedElement.get(notifier);
				if(bindings != null) {
					List<?> siblings = siblingsByTargetElement.get(notifier);	// null for elements initially inserted in the target
					if(siblings != null && siblings.size() > 1) {
						for(EObject sibling : siblingsByTargetElement.get(notifier)) {
							if(sibling != notifier) {
//								System.out.println("Disabling bindings for " + Inspect.toString(sibling, null));
								Bindings<?, ?> siblingBindings = Facade.this.bindingsByTransformedElement.get(sibling);
								siblingBindings.disable();
							}
						}
//						System.out.println("Enabling bindings for " + Inspect.toString(notifier, null));
						bindings.enable();
					}
				}
			}
		}
	}

	// This method takes care of applying stereotypes.
	// The difficulty is that stereotypes can only be applied to an element after it has been attached to a Package
	// with the corresponding profile applications.
	// Only some types are supported so far.
	protected void applyStereotype(Element e, String stereotypeName) {
		if(e.eContainer() == null) {
			if(e instanceof PackageableElement) {
				tempModel.getPackagedElements().add((PackageableElement)e);
			} else if(e instanceof Property){
				tempClass.getOwnedAttributes().add((Property)e);
			} else if(e instanceof Connector){
				tempClass.getOwnedConnectors().add((Connector)e);
			} else if(e instanceof Vertex){
				tempRegion.getSubvertices().add((Vertex)e);
			} else if(e instanceof Trigger){
				tempTransition.getTriggers().add((Trigger)e);
			} else {
				throw new UnsupportedOperationException("cannot apply stereotype on detached element " + e);
			}
		}
		Stereotype s = e.getApplicableStereotype(stereotypeName);
		e.applyStereotype(s);
	}

	private Model tempModel;
	private Class tempClass;
	private StateMachine tempStateMachine;
	private Region tempRegion;
	private Transition tempTransition;

	protected <S extends EObject, T extends EObject> IBox<T> transform(IBox<S> source, IMetaClass<S> sourceType) {
		final List<Rule<S, T>> applicableRules = new ArrayList<Rule<S, T>>();
		for(Rule<?, ?> rule : rules) {
			if(rule.getSourceType().isSubTypeOf(sourceType)) {
				applicableRules.add((Rule<S, T>)rule);
			}
		}

		IUnaryFunction<? super S, IOne<Boolean>> conditions[] = new IUnaryFunction[applicableRules.size()];
		IUnaryFunction<? super S, T> collectors[] = new IUnaryFunction[applicableRules.size()];

		int i = 0;
		for(final Rule<S, T> rule : applicableRules) {
			conditions[i] = new MutablePredicate<S>() {
				@Override
				public IOne<Boolean> apply(S parameter) {
					if(rule.getSourceType().isInstance(parameter)) {
						if(rule.getGuard() == null) {
							return TRUE;
						} else {
							return rule.getGuard().apply(parameter);
						}
					} else {
						return FALSE;
					}
				}
			};
			collectors[i] = new IUnaryFunction<S, T>() {
				@Override
				public T apply(S sourceElement) {
					List<EObject> targetElements = targetElementsPerSourceElement.get(sourceElement);
					if(targetElements == null) {
						targetElements = new ArrayList<EObject>();
						targetElementsPerSourceElement.put(sourceElement, targetElements);
					}
					for(EObject sibling : targetElements) {
						if(ruleByTransformedElement.get(sibling) != rule) {
							// we do not disable an element if it is the same that we are going to get by calling rule.forwardApply below
							bindingsByTransformedElement.get(sibling).disable();
						}
					}

					T targetElement = rule.forwardApply(sourceElement);
					ruleByTransformedElement.put(targetElement, rule);
					Bindings<S, T> bindings = (Bindings<S, T>)bindingsByTransformedElement.get(targetElement);
					if(bindings == null) {
						bindings = new Bindings<S, T>(Facade.this, true);
						bindingsByTransformedElement.put(targetElement, bindings);
						rule.bindContents(bindings, sourceElement, targetElement);
			
						targetElements.add(targetElement);
						siblingsByTargetElement.put(targetElement, targetElements);
					} else {
						bindings.enable();
					}

					return targetElement;
				}
			};
			i++;
		}

		IBox<T> ret = source.switchCollect(conditions, collectors, new IUnaryFunction<S, T>() {
			@Override
			public T apply(S parameter) {
				return null;
			}
		}, new IUnaryFunction<T, S>() {
			@Override
			public S apply(EObject targetElement) {
				if(targetElement == null) {
					return null;
				}
				Rule<EObject, EObject> rule = (Rule<EObject, EObject>)ruleByTargetType.get(EMFFactory.INSTANCE.getMetaClass(targetElement.eClass()));
				if(rule != null) {
					boolean isNew;
					if(!ruleByTransformedElement.containsKey(targetElement)) {
						ruleByTransformedElement.put(targetElement, rule);
						isNew = true;
					} else {
						isNew = false;
					}
					S sourceElement = (S)rule.reverseApply(targetElement);

					if(isNew) {
						Bindings<EObject, EObject> bindings = new Bindings<EObject, EObject>(Facade.this, false);
						bindingsByTransformedElement.put(targetElement, bindings);
						rule.bindContents(bindings, sourceElement, targetElement);
					}
					return sourceElement;
				} else {
					throw new UnsupportedOperationException("No rule to transform target element " + targetElement);
				}
			}
		}
		);
		
		if(!ret.isSingleton()) {
			ret = ret.select(Predicate.valueEquals(null).not());
		}

		return ret;
	}

	private Map<EObject, Bindings<?, ?>> bindingsByTransformedElement = new HashMap<EObject, Bindings<?, ?>>();
	private Map<EObject, List<EObject>> targetElementsPerSourceElement = new HashMap<EObject, List<EObject>>();
	// targets with same source
	private Map<EObject, List<EObject>> siblingsByTargetElement = new HashMap<EObject, List<EObject>>();
	private Map<EObject, Rule<?, ?>> ruleByTransformedElement = new HashMap<EObject, Rule<?,?>>();


	private final Map<String, Rule<?, ?>> ruleByName = new HashMap<String, Rule<?, ?>>();
	private final Map<IMetaClass<?>, Rule<?, ?>> ruleByTargetType = new HashMap<IMetaClass<?>, Rule<?, ?>>();

	/**
	 * Registers a rule.
	 * <p>
	 * Concrete {@link Facade}s should call this method to register their rules.
	 * 
	 * @param rule the rule to register.
	 */
	protected <C extends EObject> void addRule(Rule<C, EObject> rule) {
		rules.add((Rule<EObject, EObject>)rule);
		ruleByName.put(rule.toString(), rule);
		if(ruleByTargetType.containsKey(rule.getTargetType())) {
			throw new IllegalStateException("Rule " + rule + " has the same return type as already registered rule " + ruleByTargetType.get(rule.getTargetType()));
		} else {
			ruleByTargetType.put(rule.getTargetType(), rule);
		}
	}

	private PropagationListener propagationListener;

	/**
	 * Registers the {@link PropagationListener}.
	 * <p>
	 * Clients should call this method to register a {@link PropagationListener} in order to be notified
	 * of failed propagations. They may handle such notifications by displaying appropriate advice to the
	 * user.
	 * 
	 * @param propagationListener the {@link PropagationListener} to register with this {@link Facade}.
	 */
	public void setPropagationListener(PropagationListener propagationListener) {
		this.propagationListener = propagationListener;
	}

	void reversePropagationBlocked(EObject targetElement, String targetPropertyName, Object blockedElement) {
		if(propagationListener != null) {
			propagationListener.reversePropagationBlocked(targetElement, targetPropertyName, blockedElement);
		} else {
			System.out.println("Warning: could not propagate erroneous target model change: insertion of " + Inspect.toString(blockedElement) + " in " + Inspect.toString(targetElement) + "." + Inspect.toString(targetPropertyName));
		}
	}

	/**
	 * A {@link PropagationListener} is registered with a {@link Facade} to get notifications of failed
	 * change propagations.
	 * 
	 * @author Fr�d�ric Jouault
	 *
	 */
	public static interface PropagationListener {
		/**
		 * 
		 * @param targetElement the target element to which the addition of the <code>blockedElement</code>
		 * could not be reverse propagated.
		 * @param targetPropertyName the property of the target element in which the <code>blockedElement</code>
		 * was inserted.
		 * @param blockedElement the element that caused the problem.
		 */
		void reversePropagationBlocked(EObject targetElement, String targetPropertyName, Object blockedElement);
	}

	private Map<String, MutablePredicate<? extends Element>> isStereotypeAppliedAccessorCache = new HashMap<String, MutablePredicate<? extends Element>>();

	/**
	 * Creates a {@link MutablePredicate} that checks whether a given {@link Stereotype} is applied to its argument.
	 * <p>
	 * 
	 * @param stereotypeName the qualified name of the {@link Stereotype}.
	 * @return the new {@link MutablePredicate}
	 */
	protected <E extends Element> MutablePredicate<E> isStereotypeApplied(final String stereotypeName) {
		MutablePredicate<E> ret = (MutablePredicate<E>)isStereotypeAppliedAccessorCache.get(stereotypeName);
		if(ret == null) {
			ret = new MutablePredicate<E>() {
				@Override
				public IOne<Boolean> apply(final E element) {
					IOne<Boolean> ret = AOFFactory.INSTANCE.createOne(
						(element == null) ?
								false	// because of null default value
							:
								element.getAppliedStereotype(stereotypeName) != null
					);
					// this inspect is very useful for debugging because this box is a root box
					if(enableInspect) {
						ret.inspect(Inspect.toString(element, null) + ".isStereotypeApplied(" + stereotypeName + ")");
					}
					if(element != null) {
						element.eAdapters().add(new IsStereotypeAppliedAdapter(element, stereotypeName, ret));
					}
					return ret;
				}

				@Override
				public String toString() {
					return "isStereotypeApplied(" + stereotypeName + ")";
				}
			}.cache(new StrongKeysStrongValuesUnaryCache<E, IOne<Boolean>>());
			isStereotypeAppliedAccessorCache.put(stereotypeName, ret);
		}

		return ret;
	}

	private class IsStereotypeAppliedAdapter extends AdapterImpl {
		private Element element;
		private String stereotypeName;
		private IOne<Boolean> isStereotypeApplied;

		public IsStereotypeAppliedAdapter(Element element, String stereotypeName, IOne<Boolean> isStereotypeApplied) {
			this.element = element;
			this.stereotypeName = stereotypeName;
			this.isStereotypeApplied = isStereotypeApplied;
		}

		public void notifyChanged(Notification notification) {
			switch(notification.getEventType()) {
			case PapyrusStereotypeListener.APPLIED_STEREOTYPE:
			case PapyrusStereotypeListener.MODIFIED_STEREOTYPE:
			case PapyrusStereotypeListener.UNAPPLIED_STEREOTYPE:
				isStereotypeApplied.add(0, element.getAppliedStereotype(stereotypeName) != null);
				break;
			default:
				// ignore
				break;
			}
		}
	}

	// Warning: generics of following IMetaClasses are not statically checked. Be careful when adding some.
	protected final IMetaClass<Model> UML_Model = factory.getMetaClass(UML.getModel());
	protected final IMetaClass<Class> UML_Class = factory.getMetaClass(UML.getClass_());
	protected final IMetaClass<Classifier> UML_Classifier = factory.getMetaClass(UML.getClassifier());
	protected final IMetaClass<Package> UML_Package = factory.getMetaClass(UML.getPackage());
	protected final IMetaClass<Property> UML_Property = factory.getMetaClass(UML.getProperty());
	protected final IMetaClass<Port> UML_Port = factory.getMetaClass(UML.getPort());
	protected final IMetaClass<Connector> UML_Connector = factory.getMetaClass(UML.getConnector());
	protected final IMetaClass<ConnectorEnd> UML_ConnectorEnd = factory.getMetaClass(UML.getConnectorEnd());
	protected final IMetaClass<Collaboration> UML_Collaboration = factory.getMetaClass(UML.getCollaboration());
	protected final IMetaClass<StateMachine> UML_StateMachine = factory.getMetaClass(UML.getStateMachine());
	protected final IMetaClass<Region> UML_Region = factory.getMetaClass(UML.getRegion());
	protected final IMetaClass<State> UML_State = factory.getMetaClass(UML.getState());
	protected final IMetaClass<Pseudostate> UML_Pseudostate = factory.getMetaClass(UML.getPseudostate());
	protected final IMetaClass<Transition> UML_Transition = factory.getMetaClass(UML.getTransition());
	protected final IMetaClass<Operation> UML_Operation = factory.getMetaClass(UML.getOperation());
	protected final IMetaClass<Parameter> UML_Parameter = factory.getMetaClass(UML.getParameter());
	protected final IMetaClass<LiteralInteger> UML_LiteralInteger = factory.getMetaClass(UML.getLiteralInteger());
	protected final IMetaClass<OpaqueBehavior> UML_OpaqueBehavior = factory.getMetaClass(UML.getOpaqueBehavior());
	protected final IMetaClass<Trigger> UML_Trigger = factory.getMetaClass(UML.getTrigger());
}
