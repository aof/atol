/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.aof.core.IBinding;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IMetaClass;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.papyrus.aof.core.utils.Functions;
import org.eclipse.papyrus.aof.lang.MutablePredicate;
import org.eclipse.papyrus.aof.lang.Predicate;

/**
 * Each instance of class {@link Bindings} manages all the bindings corresponding to a single application of a given
 * {@link Rule}.
 * <p>
 * It offers <code>add</code> methods for the establishment of bindings.
 * It then handles {@link Rule} application direction, and makes it possible for the {@link Facade} to
 * enable or disable all bindings of a given {@link Rule} application at once.
 * <p>
 * There are two general forms of binding.
 * In the first form, the <code>add</code> method handles all navigation:
 * <pre>
 * targetElement.targetProperty.select(targetPredicate) :=: sourceElement.sourceProperty.select(sourcePredicate)
 * </pre>
 * <p>
 * In the second form, source navigation is performed by the caller, and only an opaque source {@link IBox} is
 * passed to the <code>add</code> method:
 * <pre>
 * targetElement.targetProperty.select(targetPredicate) :=: opaqueSourceExpression
 * </pre>
 * <p>
 * This second form does not need an explicit <code>sourcePredicate</code> because is can be embedded in
 * the opaque source {@link IBox} if necessary.
 * <p>
 * The <code>:=:</code> operator is basically an equality operator with the additional capability of propagating
 * a change from left to right, or from right to left.
 * <p>
 * The first form should be preferred whenever possible, and the second form exists to handle more complex cases.
 * <p>
 * In both forms <code>targetPredicate</code> is optional, and <code>sourcePredicate</code> is optional in the first form.
 * <ul>
 * 	 <li>
 * 		The <code>targetPredicate</code> is used to filter out elements that must not be reverse propagated.
 * 		This is especially useful when the target metamodel is more permissive than the source one.
 * 		Because it is used to filter elements based on their type, it is a non-mutable predicate.
 * 	 <li>
 * 		The <code>sourcePredicate</code> is used to filter the elements to bind to bind to the target property.
 *		Because it may be used to filter elements on any property, it is a mutable predicate.
 * </ul>
 * <p>
 * The various <code>add</code> methods of this class correspond to convenience variations around these two forms.
 * In these methods, target always comes before source, and this should be respected by the caller.
 * Otherwise, automatic handling of {@link Rule} application direction will fail.
 * Generic typing is used to enable static checking.
 * 
 * @author Fr�d�ric Jouault
 * @param <S> type of source elements.
 * @param <T> type of target elements.
 */
public class Bindings<S extends EObject, T extends EObject> {

	private Facade facade;
	private boolean sourceToTarget;

	private Collection<IBinding<?>> bindings = new ArrayList<IBinding<?>>();
	private boolean disabled = false;

	protected Bindings(Facade facade, boolean sourceToTarget) {
		this.facade = facade;
		this.sourceToTarget = sourceToTarget;
	}

	/**
	 * Establishes a binding between the properties named <code>propertyName</code> of
	 * <code>sourceElement</code> and <code>targetElement</code>.
	 * <p>
	 * The established binding is a simplified variant of the first form:
	 * <pre>
	 * targetElement.property :=: sourceElement.property
	 * </pre>
	 * 
	 * @param propertyName name of the source and target properties between which to establish the binding.
	 * @param targetElement target element.
	 * @param sourceElement source element.
	 */
	public void add(String propertyName, T targetElement, S sourceElement) {
		this.add(targetElement, propertyName, sourceElement, propertyName, null);
	}

	/**
	 * Establishes a binding between the properties named <code>propertyName</code> of
	 * <code>sourceElement</code> and <code>targetElement</code>, with a predicate on the source.
	 * <p>
	 * The established binding is a simplified variant of the first form:
	 * <pre>
	 * targetElement.property :=: sourceElement.property.select(sourcePredicate)
	 * </pre>
	 * @param propertyName name of the source and target properties between which to establish the binding.
	 * @param targetElement target element.
	 * @param sourceElement source element.
	 * @param sourcePredicate mutable predicate used to filter elements of the source property.
	 * @param <SE> type of elements in source property.
	 */
	public <SE> void add(String propertyName, T targetElement, S sourceElement, IUnaryFunction<? super SE, IOne<Boolean>> sourcePredicate) {
		this.add(targetElement, propertyName, sourceElement, propertyName, sourcePredicate);
	}

	/**
	 * Establishes a binding between property named <code>sourcePropertyName</code> of
	 * <code>sourceElement</code> and property named <code>targetPropertyName</code> of
	 * <code>targetElement</code>.
	 * <p>
	 * The established binding is a simplified variant of the first form:
	 * <pre>
	 * targetElement.targetProperty :=: sourceElement.sourceProperty
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param sourceElement source element.
	 * @param sourcePropertyName name of the source property
	 */
	public void add(T targetElement, String targetPropertyName, S sourceElement, String sourcePropertyName) {
		this.add(targetElement, targetPropertyName, sourceElement, sourcePropertyName, null);
	}

	/**
	 * Establishes a binding between property named <code>sourcePropertyName</code> of
	 * <code>sourceElement</code> and property named <code>targetPropertyName</code> of
	 * <code>targetElement</code>, with a predicate on the target.
	 * <p>
	 * The established binding is a simplified variant of the first form:
	 * <pre>
	 * targetElement.targetProperty.select(targetPredicate) :=: sourceElement.sourceProperty
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param targetPredicate non-mutable predicate on the target property.
	 * @param sourceElement source element.
	 * @param sourcePropertyName name of the source property
	 * @param <TE> type of elements in target property.
	 */
	public <TE> void add(T targetElement, String targetPropertyName, IUnaryFunction<TE, Boolean> targetPredicate, S sourceElement, String sourcePropertyName) {
		this.add(targetElement, targetPropertyName, targetPredicate, sourceElement, sourcePropertyName, null);
	}

	/**
	 * Establishes a binding between property named <code>sourcePropertyName</code> of
	 * <code>sourceElement</code> and property named <code>targetPropertyName</code> of
	 * <code>targetElement</code>, with a predicate on the source.
	 * <p>
	 * The established binding is a simplified variant of the first form:
	 * <pre>
	 * targetElement.targetProperty :=: sourceElement.sourceProperty.select(sourcePredicate)
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param sourceElement source element.
	 * @param sourcePropertyName name of the source property
	 * @param sourcePredicate mutable predicate used to filter elements of the source property.
	 * @param <SE> type of elements in source property.
	 */
	public <SE> void add(T targetElement, String targetPropertyName, S sourceElement, String sourcePropertyName, IUnaryFunction<? super SE, IOne<Boolean>> sourcePredicate) {
		this.add(targetElement, targetPropertyName, null, sourceElement, sourcePropertyName, sourcePredicate);
	}

	/**
	 * Establishes a binding between property named <code>sourcePropertyName</code> of
	 * <code>sourceElement</code> and property named <code>targetPropertyName</code> of
	 * <code>targetElement</code>, with predicates on both source and target.
	 * <p>
	 * The established corresponds to the full first form:
	 * <pre>
	 * targetElement.targetProperty.select(targetPredicate) :=: sourceElement.sourceProperty.select(sourcePredicate)
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param targetPredicate non-mutable predicate on the target property.
	 * @param sourceElement source element.
	 * @param sourcePropertyName name of the source property
	 * @param sourcePredicate mutable predicate used to filter elements of the source property.
	 * @param <TE> type of elements in target property.
	 * @param <SE> type of elements in source property.
	 */
	public <SE, TE> void add(T targetElement, String targetPropertyName, IUnaryFunction<TE, Boolean> targetPredicate, S sourceElement, String sourcePropertyName, IUnaryFunction<? super SE, IOne<Boolean>> sourcePredicate) {
		IBox<SE> source = facade.factory.createPropertyBox(sourceElement, sourcePropertyName);

		if(sourcePredicate != null) {
			MutablePredicate<? super SE> actualSourcePredicate = MutablePredicate.asMutablePredicate(sourcePredicate);
			if(source instanceof IOne) {
				actualSourcePredicate = actualSourcePredicate.or(Predicate.valueEquals(null).boxOutput());
			}
			source = source.selectMutable(actualSourcePredicate);
		}

		IMetaClass<SE> sourceType = null;
		EStructuralFeature sourceFeature = sourceElement.eClass().getEStructuralFeature(sourcePropertyName);
		if(sourceFeature instanceof EReference) {
			sourceType = facade.factory.getMetaClass(sourceFeature.getEType());
		}
		this.add(targetElement, targetPropertyName, targetPredicate, source, sourceType);
	}

	/**
	 * Establishes a binding between the given source {@link IBox} and property named
	 * <code>targetPropertyName</code> of <code>targetElement</code>.
	 * <p>
	 * The established binding is a simplified variant of the second form:
	 * <pre>
	 * targetElement.targetProperty :=: opaqueSourceExpression
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param source {@link IBox} of source elements computed by caller using arbitrary navigation.
	 * @param sourceType type of source elements, which must be specified by the caller because it cannot
	 * be inferred from the source {@link IBox}.
	 * @param <SE> type of elements in source property.
	 */
	public <SE> void add(T targetElement, String targetPropertyName, IBox<SE> source, IMetaClass<SE> sourceType) {
		this.add(targetElement, targetPropertyName, null, source, sourceType);
	}

	/**
	 * Establishes a binding between the given source {@link IBox} and property named
	 * <code>targetPropertyName</code> of <code>targetElement</code>.
	 * <p>
	 * The established binding is a simplified variant of the second form:
	 * <pre>
	 * targetElement.targetProperty.select(targetPredicate) :=: opaqueSourceExpression
	 * </pre>
	 * @param targetElement target element.
	 * @param targetPropertyName name of the target property.
	 * @param targetPredicate non-mutable predicate on the target property.
	 * @param source {@link IBox} of source elements computed by caller using arbitrary navigation.
	 * @param sourceType type of source elements, which must be specified by the caller because it cannot
	 * be inferred from the source {@link IBox}.
	 * @param <SE> type of elements in source property.
	 * @param <TE> type of elements in target property.
	 */
	public <TE, SE> void add(final T targetElement, final String targetPropertyName, IUnaryFunction<TE, Boolean> targetPredicate, IBox<SE> source, IMetaClass<SE> sourceType) {
		IBox<TE> target = facade.factory.createPropertyBox(targetElement, targetPropertyName);
		EStructuralFeature targetFeature = targetElement.eClass().getEStructuralFeature(targetPropertyName);
		if(!(targetFeature instanceof EReference)) {
			// primitive types do not need transformation
			this.add(target, source);
			return;
		}
		final IMetaClass<TE> targetType = facade.factory.getMetaClass(targetFeature.getEType());
		IBox<TE> transformedSource = facade.transform((IBox)source, (IMetaClass)sourceType);
		if(transformedSource.isSingleton()) {
			// null value means empty (as per EMF semantics)
			transformedSource = transformedSource.collect(new IUnaryFunction<TE, TE>() {
				@Override
				public TE apply(TE a) {
					if(targetType.isInstance(a)) {
						return a;
					} else {
						return null;
					}
				}
			}, Functions.<TE>identity());
		} else {
			transformedSource = transformedSource.select(Predicate.isInstanceOf(targetType));	// does not work with null values coming in the reverse direction
		}

		// wrap target predicate with a predicate notifying that an element could not be reverse propagated
		if(targetPredicate != null) {
			final IUnaryFunction<TE, Boolean> fTargetPredicate = targetPredicate;
			targetPredicate = new Predicate<TE>() {
				@Override
				public Boolean apply(TE parameter) {
					Boolean ret = fTargetPredicate.apply(parameter);
					if(parameter != null && !ret) {
						facade.reversePropagationBlocked(targetElement, targetPropertyName, parameter);
					}
					return ret;
				}
			};
		}

		if(target instanceof IOne && !transformedSource.isSingleton()) {
			// we must not propagate null backwards
			if(targetPredicate == null) {
				target = target.select(Predicate.valueEquals(null).not());
			} else {
				target = target.select(Predicate.<TE>valueEquals(null).not().and(targetPredicate));
			}
		} else if(targetPredicate != null) {
			final IUnaryFunction<TE, Boolean> fTargetPredicate = targetPredicate;
			if(target.isSingleton()) {
				target = target.collect(new IUnaryFunction<TE, TE>() {
					@Override
					public TE apply(TE parameter) {
						if(fTargetPredicate.apply(parameter)) {
							return parameter;
						} else {
							return null;
						}
					}
				}, Functions.<TE>identity());
			} else {
				target = target.select(Predicate.<TE>valueEquals(null).or(targetPredicate));
			}
		}
		this.add(target, transformedSource);
	}

	private <SE, TE> void add(IBox<TE> target, IBox<SE> source) {
		source = source.asBox(target);
		if(this.sourceToTarget) {
			this.bindings.add(target.bind((IBox)source));
		} else {
			this.bindings.add(source.bind((IBox)target));
		}
	}

	void enable() {
		if(disabled) {
			disabled = false;
			for(IBinding<?> b : this.bindings) {
				b.enable();
			}
		}
	}

	void disable() {
		if(!disabled) {
			disabled = true;
			for(IBinding<?> b : this.bindings) {
				b.disable();
			}
		}
	}

	// debug
	public String toString() {
		StringBuffer ret = new StringBuffer();

		for(IBinding<?> binding : bindings) {
			ret.append(binding.getLeft() + " :=: " + binding.getRight());
			ret.append('\n');
		}

		return ret.toString();
	}
}