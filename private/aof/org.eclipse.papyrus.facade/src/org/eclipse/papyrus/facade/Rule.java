/*******************************************************************************
 *  Copyright (c) 2015 CEA.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *     Fr�d�ric Jouault - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.facade;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aof.core.IMetaClass;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IUnaryFunction;
import org.eclipse.uml2.uml.Element;

/**
 * A {@link Rule} is a {@link Facade} element that transforms elements of a given UML source type into elements
 * of a given DSML target type, and <em>vice versa</em>. Concrete rules are defined by extending this class.
 * <p>
 * The general form of a {@link Rule} (given in <a href="https://www.eclipse.org/atl/">ATL</a>-like syntax) is as follows:
 *	<pre>
 *	rule &lt;name> {
 *		from
 *			source : &lt;sourceType> (
 *				source.isStereotypeApplied(&lt;stereotypeName>) and
 *				&lt;guard>(source)
 *			)
 *		to
 *			target : &lt;targetType> (
 *				&lt;bindContents>
 *			)
 *	}
 *	</pre>
 * <p>
 * A rule has a name. It matches source elements of a given source type by checking if a given stereotype is
 * applied to them, and if they satisfy a guard. Both stereotype and guard are optional.
 * Source elements matching the {@link Rule} can be transformed into target elements of a given target type.
 * Properties of source and target elements are then bound together.
 * <p>
 * One of the specificity of Facade is that a {@link Rule} may also match a target element, create
 * a corresponding source element, optionally apply the given stereotype, and then bind their properties.
 * Only the guard is not automatically reversed. In most cases this is not necessary. When it is necessary,
 * the rule must specify how to do it in its {@link Rule#reverseInit(EObject)} method.
 * <p>
 * Creating a target element from a source element corresponds to an application of the {@link Rule} in the
 * <em>forward</em> direction. Conversely, creating a source element from a target element corresponds to
 * an application of the {@link Rule} in the <em>reverse</em> direction.
 * <p>
 * Because stereotypes are commonly used in guard of Facade rules, they are handled separately from the guard.
 * so that this aspect can be automatically reversed.
 * However, it is the responsibility of the user to make sure the guard does not need further reversal,
 * or to override method {@link Rule#reverseInit(EObject)} appropriately.
 * <p>
 * Several convenience constructors exist that correspond to variants of the general form of a rule given above.
 * <p> 
 * 
 * @author Fr�d�ric Jouault
 *
 * @param <S> type of source elements.
 * @param <T> type of target elements.
 */
public abstract class Rule<S extends EObject, T extends EObject> {

	private String name;
	private IMetaClass<S> sourceType;
	private IMetaClass<T> targetType;
	private String stereotypeName;
	private IUnaryFunction<? super S, IOne<Boolean>> guard;
	private Facade facade;

	/**
	 * Creates a {@link Rule} without stereotype or guard.
	 * <p>
	 * The new {@link Rule} follows a simplification of the general form:
	 *	<pre>
	 *	rule &lt;name> {
	 *		from
	 *			source : &lt;sourceType>
	 *		to
	 *			target : &lt;targetType> (
	 *				&lt;bindContents>
	 *			)
	 *	}
	 *	</pre>
	 *
	 * @param facade
	 * @param name
	 * @param sourceType
	 * @param targetType
	 */
	public Rule(Facade facade, String name, IMetaClass<S> sourceType, IMetaClass<T> targetType) {
		this(facade, name, sourceType, targetType, null, null);
	}

	/**
	 * Creates a {@link Rule} with stereotype but no guard.
	 * <p>
	 * The new {@link Rule} follows a simplification of the general form:
	 *	<pre>
	 *	rule &lt;name> {
	 *		from
	 *			source : &lt;sourceType> (
	 *				source.isStereotypeApplied(&lt;stereotypeName>) 
	 *			)
	 *		to
	 *			target : &lt;targetType> (
	 *				&lt;bindContents>
	 *			)
	 *	}
	 *	</pre>
	 * @param facade
	 * @param name
	 * @param sourceType
	 * @param targetType
	 * @param stereotypeName
	 */
	public Rule(Facade facade, String name, IMetaClass<S> sourceType, IMetaClass<T> targetType, String stereotypeName) {
		this(facade, name, sourceType, targetType, stereotypeName, null);
	}

	/**
	 * Creates a {@link Rule} with a guard but no stereotype.
	 * <p>
	 * The new {@link Rule} follows a simplification of the general form:
	 *	<pre>
	 *	rule &lt;name> {
	 *		from
	 *			source : &lt;sourceType> (
	 *				&lt;guard>(source)
	 *			)
	 *		to
	 *			target : &lt;targetType> (
	 *				&lt;bindContents>
	 *			)
	 *	}
	 *	</pre>
	 * @param facade
	 * @param name
	 * @param sourceType
	 * @param targetType
	 * @param guard
	 */
	public Rule(Facade facade, String name, final IMetaClass<S> sourceType, final IMetaClass<T> targetType, IUnaryFunction<? super S, IOne<Boolean>> guard) {
		this(facade, name, sourceType, targetType, null, guard);
	}

	/**
	 * Creates a {@link Rule} with stereotype and guard.
	 * <p>
	 * The new {@link Rule} follows the general form:
	 *	<pre>
	 *	rule &lt;name> {
	 *		from
	 *			source : &lt;sourceType> (
	 *				source.isStereotypeApplied(&lt;stereotypeName>) and
	 *				&lt;guard>(source)
	 *			)
	 *		to
	 *			target : &lt;targetType> (
	 *				&lt;bindContents>
	 *			)
	 *	}
	 *	</pre>
	 * @param facade
	 * @param name
	 * @param sourceType
	 * @param targetType
	 * @param stereotypeName
	 * @param guard
	 */
	public Rule(Facade facade, String name, final IMetaClass<S> sourceType, final IMetaClass<T> targetType, String stereotypeName, IUnaryFunction<? super S, IOne<Boolean>> guard) {
		this.name = name;
		this.sourceType = sourceType;
		this.targetType = targetType;
		this.stereotypeName = stereotypeName;
		this.facade = facade;
		if(stereotypeName != null) {
			if(guard == null) {
				this.guard = (IUnaryFunction<? super S, IOne<Boolean>>)facade.isStereotypeApplied(stereotypeName);
			} else {
				this.guard = (IUnaryFunction<? super S, IOne<Boolean>>)facade.isStereotypeApplied(stereotypeName).and((IUnaryFunction<Element, IOne<Boolean>>)guard);
			}
		} else {
			this.guard = guard;
		}
	}

	/**
	 * Binds the properties of the given source and target elements.
	 * <p>
	 * Because of how AOF works, this single property bindings method work both in forward, and in reverse
	 * direction.
	 * 
	 * @param bindings a {@link Bindings} instance used to manage the bindings of the specific rule activation
	 * for these specific source and target elements.
	 * @param source the source element, which properties must be bound to those of the target element.
	 * @param target the target element, which properties must be bound to those of the source element.
	 */
	protected abstract void bindContents(Bindings<S, T> bindings, S source, T target);

	/**
	 * Initializes the given source element so that it satisfies the guard.
	 * <p>
	 * This method should be overridden by concrete rules that require guard reversal.
	 * <p>
	 * This is rarely necessary in general.
	 * 
	 * @param source the source element to initialize.
	 */
	protected void reverseInit(S source) {
		// nothing to do by default
	}

	protected IMetaClass<S> getSourceType() {
		return this.sourceType;
	}

	protected IMetaClass<T> getTargetType() {
		return this.targetType;
	}

	protected IUnaryFunction<? super S, IOne<Boolean>> getGuard() {
		return this.guard;
	}

	public String toString() {
		return this.name;
	}

	private Map<S, T> forwardCache = new HashMap<S, T>();
	private Map<T, S> reverseCache = new HashMap<T, S>();

	protected T forwardApply(S source) {
		T target = forwardCache.get(source);
		if(target == null) {
			target = targetType.newInstance();
			forwardCache.put(source, target);
			reverseCache.put(target, source);
		}
		return target;
	}

	// we need a reverse cache to avoid infinite loop in some special cases like when there is no
	// opposite in target metamodel for a corresponding source opposite
	protected S reverseApply(T target) {
		S source = reverseCache.get(target);
		if(source == null) {
			source = sourceType.newInstance();
			if(this.stereotypeName != null) {
				facade.applyStereotype((Element)source, stereotypeName);
			}
			reverseInit(source);
			forwardCache.put(source, target);
			reverseCache.put(target, source);
		}
		return source;
	}
}