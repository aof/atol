package fr.eseo.aof.debug.utils

interface BoxesAnalyzer {
	def void analyze()
	def void dispose()
	def int nbBoxes()
}