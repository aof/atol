package fr.eseo.aof.debug.utils

import fr.eseo.aof.debug.utils.PipesWalker.PipesVisitorAdapter
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Stack
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IUnaryFunction
import org.eclipse.papyrus.aof.core.impl.Box
import org.eclipse.papyrus.aof.core.impl.operation.Operation
import org.eclipse.papyrus.aof.core.impl.utils.FactoryObserver

import static org.eclipse.papyrus.aof.core.impl.Box.*

class DuplicateBoxesAnalyzer implements BoxesAnalyzer {
	val boxes = new ArrayList<IBox<Object>>
	val stackTracePerBox = new HashMap<IBox<?>, Throwable>

	val FactoryObserver<IBox<?>> factoryObserver = [index, it |
		boxes.add(it as IBox<Object>)
		stackTracePerBox.put(it, new Exception)
	]

	new() {
		// DONE: check if there is already a factoryObserver
		// because there can only be one
		if(Box.factoryObserver != null) {
			println('''warning: overriding already registered Box factory observer «Box.factoryObserver»''')
		}
		Box.factoryObserver = factoryObserver
	}

	override analyze() {
		findDuplicateBoxes
	}

	def findDuplicateBoxes() {
		val boxesPerPath = new HashMap<Object, List<IBox<?>>>
		var duplicateBoxes = 0
		for(box : boxes) {
			duplicateBoxes = processBox(box, boxesPerPath, duplicateBoxes, stackTracePerBox)
		}
		if(duplicateBoxes > 0) {
			val pathsPerBoxes = new HashMap<List<IBox<?>>, Object>
			boxesPerPath.forEach[k, v|
				pathsPerBoxes.put(v, k)
			]
			val sortedDuplicateLists = boxesPerPath.values.sortBy[it.size]
			for(dupList : sortedDuplicateLists) {
				println(dupList.size)
			}
			println(pathsPerBoxes.get(sortedDuplicateLists.last))
			stackTracePerBox.get(sortedDuplicateLists.last.get(0)).printStackTrace(System.out)
			stackTracePerBox.get(sortedDuplicateLists.last.last).printStackTrace(System.out)
		}
		println('''Found «duplicateBoxes» duplicate boxes out of «boxes.size» total boxes''')
	}

	// TODO: perform a first pass to be able to reverse navigate the pipes
	// even without ResultObservers
	private def processBox(IBox<Object> box, Map<Object, List<IBox<?>>> boxesPerPath, int duplicateBoxes, Map<IBox<?>, Throwable> stackTracePerBox) {
		var ret = duplicateBoxes
//		println(box)
		val path = new Stack<List<Object>>
		val pipesWalker = new PipesWalker
		val pipesVisitor = new PipesVisitorAdapter {
			override enterOperation(Operation<?> operation, String operationName, Object...arguments) {
//							println('''	«operationName»«arguments.toList»''')
				path.push(#[operationName, arguments.map[
					switch it {
						IUnaryFunction<?, ?>: {
							// TODO: check superclasses?
							if(it.class.superclass != Object) {
								println("warning: specific superclass")
							}
							val vals = it.class.declaredFields.map[f |
								f.accessible = true
								f.get(it)
							].toArray.toList
							#[it.class, vals]
//							val cn = it.class.name.replaceFirst("^.*\\.", "")
//							switch cn {
//								case "EMFMetaClass$PropertyAccessor": it
//								case cn.startsWith("CPS2DeploymentAOFTransformation$"): {
//									// TODO: not actually true if the function depends on external state (e.g., captured variable)
//									it.class
//								}
//								default: {
//									it	//throw new RuntimeException
//								}
//							}
						}
						default: it
					}
				].toArray.toList])
			}
			override leaveOperation(Operation<?> operation, String operationName, Object...arguments) {
//							val popped = path.pop
//							if(popped != #[operationName, arguments]) {
//								throw new RuntimeException
//							}
			}
			override propertyBox(IBox<?> box, EObject object, EStructuralFeature feature) {
//							println('''	prop: «object.eClass.name»@«Integer.toHexString(object.hashCode)».«feature.name»''')
				path.push(#[object, feature])
			}
			override rootBox(IBox<?> box) {
				// should only be proxyBoxes of property boxes & constant boxes from Box
//							println('''	root''')
//							stackTracePerBox.get(box).printStackTrace(System.out)
			}
		}
		pipesWalker.accept(box, pipesVisitor, false)
		if(!path.isEmpty) {
			if(!(path.last.get(0) instanceof EObject)) {
				stackTracePerBox.get(box).printStackTrace
				println("root-less box!")
			}
			var l = boxesPerPath.get(path)
			if(l == null) {
				l = new ArrayList
				boxesPerPath.put(path, l)
			} else {
				ret++
			}
			l.add(box)
		}
		return ret
	}

	override dispose() {
		if(Box.factoryObserver == factoryObserver) {
			Box.factoryObserver = null
		}
	}
	
	override nbBoxes() {
		boxes.size
	}
	
}