package fr.eseo.atol.examples.cps

import fr.eseo.aof.xtend.utils.AOFAccessors
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.CyberPhysicalSystemPackage

@AOFAccessors(CyberPhysicalSystemPackage)
class CPS {
}
