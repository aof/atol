Pure AOF version is similar to ATL with explicit (unique lazy) rule calls.
We can use explicit rule invocation because there is always only one callable rule
(except for traces, but this is handled separately). But we needed implicit rule calls
for Façade because there were sometimes multiple callable rules.
In AOF, traceability is handled by the collectTo (i.e., on the caller side), whereas it is
handled by ATL by the "unique lazy" rules (i.e., on the callee side).

We would not be able to implement a non-incremental version in AOF:
- if we wanted to be non-incremental, we would need to do the following (from BatchSimple) in execute method:
		mapping.traces.clear
		mapping.deployment.hosts.clear
	=> possible
- PLUS forget collectTo trace
	- because if we are rerun with same source model we would be supposed to create a new target model
	=> not possible

Reflections on this case study:
- THINGS THAT MAKE IT SIMPLE for us:
	- one root element by model
	- xtend (we can use a very simple syntax)
- THINGS THAT MAKE IT COMPLEX for us:
	- having to launch an eclipse each time we want to run the tests
	- useless trace
		- notably 1-n traces should be pair-to-one traces
		=> we have to compute it in addition to the transformation, not as a by-product
  	- paradigm: we ignore re-execute... fortunately tests do not check that nothing has changed before re-calling execute
		// Apparently re-execute means forward changes incrementally
		// but we forward as soon as possible...
		// different semantics, but the tests may not notice
		// (at least they should not for a benchmark)
- OPEN QUESTIONS:
  	- could this transformation be bidir?

=> ICMT Paper on this?

Remarks
- we use xtend Pair instead of AOF IPair when we can leverage its (K -> V) concrete syntax
	- both are immutable data types
	- example from rule state2BehaviorState:
				source.key -> e
			instead of:
				new Pair(source.key, e)
			or
				AOFFactory.INSTANCE.createPair(source.key, e)

Notes
- How to create new M2M implementation for benchmark:
	- copy BatchSimple project
	- change plugin id in pom.xml and MANIFEST.MF
	- add new wrapper in org.eclipse.incquery.examples.cps.xform.m2m.tests.wrappers
	- register wrapper in org.eclipse.incquery.examples.cps.xform.m2m.tests.CPS2DepTest

TODO:
- retrieve fix for property<-null => removed instead of replaced!
- simplify support for communicateWith using aof.lang?
- why did we need modification to EMFMetaClass.PropertyAccessor?

DONE:
- clone behavior for each instance
- add support for triggers
- fix trace semantics
- ATL with explicit rule calls
	- using unique lazy rules for traceability (called rules would not have traceability)
- explain why we do not use the hostInstances derived feature
- refactor ret into target (in all rules)
- use collectTo instead of collect for traces
- extension method for navigation (if xtend uses generics for dispatch)
- add support for communicateWith
	=> apparently requires navigating containers => can be worked around

Traces:
- creating them from target makes sure we do not keep links for non transformed elements
	=> this what we do
- creating them from source keeps links that should be removed => it does not work
	- Here is obsolete code to do this:
	def bindTraceFromSource() {
		val allStateMachines =
			CPS.CyberPhysicalSystem.appTypes.apply(mapping.cps).collectMutable(
				CPS.ApplicationType.behavior
			)
		val allStates =
			allStateMachines.collectMutable(
				CPS.StateMachine.states
			)
		val allTransitions =
			allStates.collectMutable(
				CPS.State.outgoingTransitions
			)
		val allHostInstances =
			CPS.CyberPhysicalSystem.hostTypes.apply(mapping.cps).collectMutable(
				CPS.HostType.instances
			)
		val allApplicationInstances =
			allHostInstances.collectMutable(
				CPS.HostInstance.applications
			)

		Trace.CPS2Deployment.traces.apply(mapping).bind(
			(
				(allHostInstances.zip(allHostInstances.collectTo(
					hostInstance2DeploymentHost
				), true) as IBox) as IBox<IPair<Identifiable, DeploymentElement>>
			).union(
				(allApplicationInstances.zip(allApplicationInstances.collectTo(
					applicationInstance2DeploymentApplication
				), true) as IBox) as IBox<IPair<Identifiable, DeploymentElement>>
			).union(
				(allStateMachines.zip(allStateMachines.collectTo(
					stateMachine2DeploymentBehavior
				), true) as IBox) as IBox<IPair<Identifiable, DeploymentElement>>
			).union(
				(allStates.zip(allStates.collectTo(
					state2BehaviorState
				), true) as IBox) as IBox<IPair<Identifiable, DeploymentElement>>
			).union(
				(allTransitions.zip(allTransitions.collectTo(
					transition2BehaviorTransition
				), true) as IBox) as IBox<IPair<Identifiable, DeploymentElement>>
			).select[e |
				e.left != null && e.right != null
			].collect[e |
				val ret = Trace.fact.createCPS2DeplyomentTrace

//					Trace.CPS2DeplyomentTrace.cpsElements.bind(
//						e.
//					)
				ret.cpsElements.add(e.left)
				ret.deploymentElements.add(e.right)

				ret
			].asOrderedSet
		)
	}
