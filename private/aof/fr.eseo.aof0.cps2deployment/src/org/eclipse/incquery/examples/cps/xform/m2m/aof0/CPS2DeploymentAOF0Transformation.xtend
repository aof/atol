package org.eclipse.incquery.examples.cps.xform.m2m.aof0

import com.google.common.collect.HashBasedTable
import fr.eseo.aof0.emf.EMFPlatform
import fr.eseo.aof0.emf.IMetaClass
import fr.eseo.dis.aof0.dsl.Box
import fr.eseo.dis.aof0.dsl.Transformation
import java.io.FileOutputStream
import java.io.PrintStream
import java.util.HashMap
import java.util.Set
import org.apache.log4j.Logger
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.ApplicationInstance
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.HostInstance
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.Identifiable
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.State
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.StateMachine
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.Transition
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.impl.StateImpl
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.impl.StateMachineImpl
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.impl.TransitionImpl
import org.eclipse.viatra.examples.cps.deployment.BehaviorState
import org.eclipse.viatra.examples.cps.deployment.BehaviorTransition
import org.eclipse.viatra.examples.cps.deployment.DeploymentApplication
import org.eclipse.viatra.examples.cps.deployment.DeploymentBehavior
import org.eclipse.viatra.examples.cps.deployment.DeploymentElement
import org.eclipse.viatra.examples.cps.deployment.DeploymentHost
import org.eclipse.viatra.examples.cps.deployment.impl.BehaviorStateImpl
import org.eclipse.viatra.examples.cps.deployment.impl.BehaviorTransitionImpl
import org.eclipse.viatra.examples.cps.deployment.impl.DeploymentApplicationImpl
import org.eclipse.viatra.examples.cps.deployment.impl.DeploymentBehaviorImpl
import org.eclipse.viatra.examples.cps.deployment.impl.DeploymentHostImpl
import org.eclipse.viatra.examples.cps.traceability.CPS2DeploymentTrace
import org.eclipse.viatra.examples.cps.traceability.CPSToDeployment
import org.eclipse.viatra.examples.cps.traceability.impl.CPS2DeploymentTraceImpl
import org.eclipse.xtend.lib.annotations.Data

import static com.google.common.base.Preconditions.*

import static extension org.eclipse.incquery.examples.cps.xform.m2m.aof0.Graph2PlantUML.toPlantUML

class CPS2DeploymentAOF0Transformation {
    val tr = new Transformation(new EMFPlatform)

    def static <=>(Pair<? extends EObject, ? extends EStructuralFeature> t, Box<?> s) {
        s.write(t)
    }

// TARGET
//    val DEP_DeploymentHost = new EMFMetaClass<DeploymentHost>(DeploymentPackage.eINSTANCE.deploymentHost)
//
//    val DEP_DeploymentApplication = new EMFMetaClass<DeploymentApplication>(DeploymentPackage.eINSTANCE.deploymentApplication)
//
//    val DEP_DeploymentBehavior = new EMFMetaClass<DeploymentBehavior>(DeploymentPackage.eINSTANCE.deploymentBehavior)
//
//    val DEP_BehaviorState = new EMFMetaClass<BehaviorState>(DeploymentPackage.eINSTANCE.behaviorState)
//
//    val DEP_BehaviorTransition = new EMFMetaClass<BehaviorTransition>(DeploymentPackage.eINSTANCE.behaviorTransition)
//
//    def __hosts(Deployment o) {
//        o -> DeploymentPackage.eINSTANCE.deployment_Hosts
//    }
//
//    def __ip(DeploymentHost o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentHost_Ip
//    }
//
//    def __applications(DeploymentHost o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentHost_Applications
//    }
//
//    def __id(DeploymentApplication o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentApplication_Id
//    }
//
//    def __behavior(DeploymentApplication o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentApplication_Behavior
//    }
//
//    def __description(DeploymentElement o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentElement_Description
//    }
//
//    def __states(DeploymentBehavior o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentBehavior_States
//    }
//
//    def __transitions(DeploymentBehavior o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentBehavior_Transitions
//    }
//
//    def __current(DeploymentBehavior o) {
//        o -> DeploymentPackage.eINSTANCE.deploymentBehavior_Current
//    }
//
//    def __outgoing(BehaviorState o) {
//        o -> DeploymentPackage.eINSTANCE.behaviorState_Outgoing
//    }

// SOURCE
//    def Box<HostType> _hostTypes(CyberPhysicalSystem o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.cyberPhysicalSystem_HostTypes)
//    }
//
//    def Box<HostInstance> instances(Box<HostType> o) {
//        o.collectBy[
//            tr.read(it -> CyberPhysicalSystemPackage.eINSTANCE.hostType_Instances)
//        ]
//    }
//
//    def Box<HostType> _nodeIp(HostInstance o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.hostInstance_NodeIp)
//    }
//
//    def Box<ApplicationInstance> _applications(HostInstance o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.hostInstance_Applications)
//    }
//
//    def Box<String> _identifier(Identifiable o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.identifiable_Identifier)
//    }
//
//    def Box<ApplicationType> _type(ApplicationInstance o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.applicationInstance_Type)
//    }
//    
//    def Box<StateMachine> behavior(Box<ApplicationType> o) {
//        o.collectBy[
//            tr.read(it -> CyberPhysicalSystemPackage.eINSTANCE.applicationType_Behavior)
//        ]
//    }
//
//    def Box<State> _states(StateMachine o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.stateMachine_States)
//    }
//
//    def Box<State> _initial(StateMachine o) {
//        tr.read(o -> CyberPhysicalSystemPackage.eINSTANCE.stateMachine_Initial)
//    }


    extension val CPS = new CPS(tr)
    extension val DEP = new DEP(tr)



	extension Logger logger = Logger.getLogger("cps.xform.m2m.aof")

	private def traceBegin(String method) {
		trace('''Executing «method» BEGIN''')
	}

	private def traceEnd(String method) {
		trace('''Executing «method» END''')
	}

// @begin "lang"
//    def <E> Box<E> union(Box<E>...boxes) {
    def static <E> Box<E> concat(Box<E>...boxes) {
		boxes.reduce[a, b | a.union(b)]   // cheating with ordering
	}

//	def <E> IBox<E> concat(IBox<E>...boxes) {
////*
//		boxes.reduce[a, b | a.concat(b)]
///*/
//		// this is not much slower (but maybe a bit)...
//		AOFFactory.INSTANCE.<IBox<E>>createSequence(boxes).collectMutable[
//			if(it === null) {
//				AOFFactory.INSTANCE.createBox(boxes.get(0))
//			} else {
//				it
//			}
//		]
///**/
//	}

//	def <A> <=>(IBox<A> a, IBox<A> b) {
//		a.bind(b)
//	}

//	val eqOne = [Pair<IBox<Object>, IBox<Object>> p |
//		p.key.zipWith(p.value)[a, b |
//			a == b
//		].asOne(false)
//	]
//	var eqOneCache = new StrongKeysStrongValuesBinaryCache2<IBox<?>, IBox<?>, IOne<Boolean>>
//	def <E> eqOne(IBox<E> l, IBox<E> r) {
////		cache(l as IBox<Object> -> r as IBox<Object>, eqOne)
//		var ret = eqOneCache.get(l, r)
//		if(ret == null) {
//			ret = l.zipWith(r)[a, b |
//					a == b
//				].asOne(false)
//			eqOneCache.put(l, r, ret)
//		}
//		ret
//	}

//	def <A> eq(IBox<A> l, IBox<A> r) {
//		l.zipWith(r)[a, b |
//			a == b
//		]
//	}

//	def ||(IBox<Boolean> l, IBox<Boolean> r) {
//		l.zipWith(r)[a, b |
//			a || b
//		]
//	}

//	val key = [IBox<Pair<Object, Object>> s |
//		s.collect[it.key]
//	]
	def static <A, B> Box<A> key(Box<Pair<A, B>> s) {
//		cache(s as IBox<?> as IBox<Pair<Object, Object>>, key) as IBox<A>
        // TODO: cache
        s.collectWith[it.key]
	}

//	val value = [IBox<Pair<Object, Object>> s |
//		s.collect[it.value]
//	]
	def static <A, B> Box<B> value(Box<Pair<A, B>> s) {
//		cache(s as IBox<?> as IBox<Pair<Object, Object>>, value) as IBox<B>
        // TODO: cache
        s.collectWith[it.value]
	}

	// DONE: implement a specific operation
//	def <A> includes(IBox<A> set, IOne<A> elem) {
///*
//		includesEquiv(set, elem)
///*/
//		new Includes(set, elem).result.asOne(false)
////		val e = #[elem.get] as Object[]
////		val count = newIntArrayOfSize(1)
////		count.set(0,
////			set.filter[
////				it == e.get(0)
////			].size
////		)
////		val ret = AOFFactory.INSTANCE.createOne(
////			count.get(0) > 0
////		)
////		elem.addObserver(new DefaultObserver<A> {
////			override added(int index, Object element) {
////				e.set(0, element)
////				count.set(0,
////					set.filter[
////						it == e.get(0)
////					].size
////				)
////				ret.set(count.get(0) > 0)
////			}
////			override moved(int newIndex, int oldIndex, Object element) {
////				throw new UnsupportedOperationException("TODO: auto-generated method stub")
////			}
////			override removed(int index, Object element) {
////				throw new UnsupportedOperationException("TODO: auto-generated method stub")
////			}
////			override replaced(int index, Object newElement, Object oldElement) {
////				added(0, newElement)
////			}
////		})
////		set.addObserver(new DefaultObserver<A> {
////			override added(int index, Object element) {
////				if(e.get(0) == element) {
////					val prev = count.get(0)
////					count.set(0, prev + 1)
////					if(prev == 0) {
////						ret.set(true)
////					}
////				}
////			}
////			override moved(int newIndex, int oldIndex, Object element) {
////			}
////			override removed(int index, Object element) {
////				if(e.get(0) == element) {
////					val prev = count.get(0)
////					count.set(0, prev - 1)
////					if(prev == 1) {
////						ret.set(false)
////					}
////				}
////			}
////			override replaced(int index, Object newElement, Object oldElement) {
////				if(e.get(0) == oldElement) {
////					if(e.get(0) == newElement) {
////						// nothing to do
////					} else {
////						// removed
////						val prev = count.get(0)
////						count.set(0, prev - 1)
////						if(prev == 1) {
////							ret.set(false)
////						}
////					}
////				} else if(e.get(0) == newElement) {
////					// added
////					val prev = count.get(0)
////					count.set(0, prev + 1)
////					if(prev == 0) {
////						ret.set(true)
////					}
////				} else {
////					// nothing to do
////				}
////			}
////		})
////		return ret;
///**/
//	}

	// also works as includesAll
//	def <A> includesEquiv(IBox<A> set, IBox<A> elem) {
//		val set_ = set.distinct	// make sure we have a Set or OrderedSet
//		set_.union(elem).size.eq(set_.size)
//	}

	@Data
	static class Rule<S, T> {
		val IMetaClass<T> targetType
		val (S, T)=> Object init
	}

//	var weakCache = !true
//	// TODO: use trace model?
//	var forwardCache =
//		if(weakCache)
//			new WeakKeysWeakValuesBinaryCache2
//		else
//			new StrongKeysStrongValuesBinaryCache2
//
//	var reverseCache =
//		if(weakCache)
//			new WeakKeysWeakValuesUnaryCache
//		else
//			new StrongKeysStrongValuesUnaryCache

	// for trace model creation
//	var flattenedReverseCache =
//		if(weakCache)
//			new WeakKeysWeakValuesUnaryCache<DeploymentElement, Identifiable>
//		else
//			new StrongKeysStrongValuesUnaryCache<DeploymentElement, Identifiable>

    // TODO: weak caches?
    var forwardCache = HashBasedTable.create
    var reverseCache = new HashMap
	var nbTargetElements = 0

	def <S, T> Box<T> collectTo(Box<S> box, Rule<S, T> rule) {
		return box.selectWith[source |
		    source !== null && (!Pair.isInstance(source) || (source as Pair<?, ?>).value !== null)
		].collectWith[source |
//			if(source === null || (Pair.isInstance(source) && (source as Pair<?, ?>).value === null)) {
//				return null
//			}
			var target = forwardCache.get(source, rule) as T
			if(target === null) {
				target = rule.targetType.newInstance
				nbTargetElements++
				forwardCache.put(source, rule, target)
				reverseCache.put(target, source)

////				if(Pair.isInstance(source)) {
////					flattenedReverseCache.put(target as DeploymentElement, (source as Pair<Identifiable, Identifiable>).value)
////				} else {
////					flattenedReverseCache.put(target as DeploymentElement, source as Identifiable)
////				}

				rule.init.apply(source, target)
			}
			return target
		]
	}

//	val nullPair = null -> null
//	def <S, T> IBox<S> collectedFrom(IBox<T> box, Rule<S, T> _) {
//		return box.collect[target |
//			if(target === null) {return nullPair as Object as S}
//			val ret = reverseCache.get(target) as S
//			if(ret === null) {
//				println("HERE")
//			}
//			return ret
//		]
//	}
// @end "lang"

	CPSToDeployment mapping;

	/**
	 * Creates a new transformation instance. The input cyber physical system model is given in the mapping
	 * @param mapping the traceability model root
	 */
	new(CPSToDeployment mapping) {
		traceBegin("constructor")

		checkNotNull(mapping, "Mapping cannot be null!")
		checkArgument(mapping.cps != null, "CPS not defined in mapping!")
		checkArgument(mapping.deployment != null, "Deployment not defined in mapping!")

		this.mapping = mapping;

		traceEnd("constructor")
	}

//	val IUnaryCache<ApplicationInstance, IBox<Pair<ApplicationInstance, Transition>>> cache = new StrongKeysStrongValuesUnaryCache
//	val IUnaryFunction<ApplicationInstance, IBox<Pair<ApplicationInstance, Transition>>> transitionsFromApplicationInstances =
//		[ai |
//			if(ai === null) {
//				// default element for CollectBox
//				// TODO: change element type of IBox.SEQUENCE (and other constant boxes) to <?>
//				return IBox.SEQUENCE as IBox<?> as IBox<Pair<ApplicationInstance, Transition>>
//			}
//			var ret = cache.get(ai)
//			if(ret === null) {
//				ret = ai._type.behavior.states
//						.validOutgoingTransitions
//						.collect[ai -> it]
//				cache.put(ai, ret)
//			}
//			return ret
//		]

// @begin Helpers
//	val isNull = [IBox<State> it |
//		it.collect[
//			it != null
//		].asOne(false)
//	]
//	def isNull(IBox<State> s) {
//		cache(s, isNull)
//	}

//	def filterValidTransitions(IBox<Transition> ts) {
//		ts.selectMutable[
//			it._targetState.isNull
////			it._targetState.collect[
////				it != null
////			].asOne(false)
//		]
//	}

//	var fcache =
//		if(weakCache)
//			new WeakKeysWeakValuesBinaryCache2
//		else
//			new StrongKeysStrongValuesBinaryCache2
//	def <T, R> R cache(T t, (T)=>R f) {
//		var ret = fcache.get(f, t) as R
//		if(ret === null) {
//			ret = f.apply(t)
//			// comment following line to disable cache (for testing purposes)
//			fcache.put(f, t, ret)
//		}
//		return ret
//	}

//	val _validOutgoingTransitions = [State it |
//		it._outgoingTransitions.filterValidTransitions
//	]
	def Box<Transition> _validOutgoingTransitions(State s) {
////		filterValidTransitions(s._outgoingTransitions)
//		cache(s, _validOutgoingTransitions)
        // TODO
        s._outgoingTransitions
	}

//	val validOutgoingTransitions = [IBox<State> it |
//		it.outgoingTransitions.filterValidTransitions
//	]
	def Box<Transition> validOutgoingTransitions(Box<State> s) {
////		filterValidTransitions(s.outgoingTransitions)
//		cache(s, validOutgoingTransitions)
        //TODO
        s.outgoingTransitions
	}

//	val _sendParts = [Transition t |
///*
//		t._action.select[	// TODO: test merging the two selects
//			it != null
////		].select[
//			&&
//			SignalUtil.isSend(it)
//		].collect[
//			SignalUtil.getAppId(it) -> SignalUtil.getSignalId(it)
//		]
///*/
//		t._action.collect[
//			if(it != null && SignalUtil.isSend(it)) {
//				SignalUtil.getAppId(it) -> SignalUtil.getSignalId(it)
//			} else {
//				nullPair as Pair<?, ?> as Pair<String, String>
//			}
//		]
///**/
//	]
//	def _sendParts(Transition t) {
//		cache(t, _sendParts)
//	}

//	val _candidateAppTypes = [Transition t |
//		val sendParts = t._sendParts
//		mapping.cps._appTypes
//		.selectMutable[
//			it._identifier.eqOne(sendParts.key)
//		]
//	]
//	def _candidateAppTypes(Transition t) {
//		cache(t, _candidateAppTypes)
//	}

//	val _candidateHostInstances = [ApplicationInstance t |
//		val hi = t._allocatedTo
//		val cw = hi.communicateWith
//		cw.concat(hi)
//	]
//	def _candidateHostInstances(ApplicationInstance t) {
//		cache(t, _candidateHostInstances)
//	}

//	val _receiverAppInstances = [Pair<ApplicationInstance, Transition> source |
///*
//		val hi = source.key._allocatedTo
//		val cw = hi.communicateWith
//		source.value._candidateAppTypes
//		.instances
//		.selectMutable[
//			val at = it._allocatedTo.asOne(null);
//			return (
//				at.eq(hi) || cw.includes(at)
//			).asOne(false)
//		]
///*/
//		val sendParts = source.value._sendParts
//		source.key._candidateHostInstances.applications.selectMutable[
//			it._type.identifier.eqOne(sendParts.key)
//		]
///**/
//	]
//	def _receiverAppInstances(Pair<ApplicationInstance, Transition> source) {
//		cache(source, _receiverAppInstances)
//	}
// @end Helpers

	val Rule<Pair<ApplicationInstance, Transition>, BehaviorTransition> transition2BehaviorTransition =
		new Rule(DEP.BehaviorTransition)[Pair<ApplicationInstance, Transition> source, target |
			identifiable2DeploymentElement(source.value, target)

			target.__to <=>	source.value._targetState.collectWith[
								source.key -> it
							].collectTo(
								state2BehaviorState
							)

////			val isSend = source.value._isSend
////				source.value._action.select[	// TODO: test merging the two selects
////					it != null
////				].select[
////					SignalUtil.isSend(it)
////				].collect[
////					SignalUtil.getAppId(it) -> SignalUtil.getSignalId(it)
////				]
///*
//			val receiverAppInstances =
//				source._receiverAppInstances
///*/
//			val sendParts = source.value._sendParts
//			val receiverAppInstances =
//				source.key._candidateHostInstances.applications
/////*
////				.selectMutable[
////					it._type.identifier.eqOne(sendParts.key)
////				]
//				// the two calls to asOne in the following expression should return their source argument, which is already a One(default=null)
//				.selectBy(sendParts.key.asOne(null))[
//					it._type.identifier.asOne(null)
//				]
///*/
//				// to (incorrectly) avoid the cartesian product that creates many IOnes (to check if the explosion of number of boxes is really due to this)
//				.select[false]
///**/
///**/
////				mapping.cps._appTypes.selectMutable[
////					it._identifier.eq(isSend.key).asOne(false)
////				].instances.selectMutable[
////					val at = it._allocatedTo;
////					return (
////						at.eq(hi) || cw.includes(at)
////					).asOne(false)
////				]
//
////*
//			target._trigger <=>	receiverAppInstances.collectMutable(
//									transitionsFromApplicationInstances
//								)
///*
//								.selectMutable[t |
//									// TODO: create a rule for this to avoid recomputing for every sender
//									t.value._action.collect[e |
//										if(e != null && SignalUtil.isWait(e)) {
//											SignalUtil.getSignalId(e)
//										} else {
//											null
//										}
//									].eqOne(sendParts.value)
//								]
///*/
//								.selectBy(sendParts.value.asOne(null))[t |
//									t.value.waitParts
//								]
///**/
//								.collectTo(
//									transition2BehaviorTransition
//								).asOrderedSet
///*/
//			null
///**/
		]

//	val _waitParts = [Transition t |
//		t._action.collect[e |
//			if(e != null && SignalUtil.isWait(e)) {
//				SignalUtil.getSignalId(e)
//			} else {
//				null
//			}
//		].asOne(null)
//	]
//	def waitParts(Transition t) {
//		cache(t, _waitParts)
//	}

	val Rule<Pair<ApplicationInstance, State>, BehaviorState> state2BehaviorState =
		new Rule(DEP.BehaviorState)[Pair<ApplicationInstance, State> source, target |
			identifiable2DeploymentElement(source.value, target)

			target.__outgoing <=>	source.value._validOutgoingTransitions.collectWith[
										source.key -> it
									].collectTo(
										transition2BehaviorTransition
									)
		]

	def identifiable2DeploymentElement(Identifiable source, DeploymentElement target) {
			target.__description <=> source._identifier
	}

	val Rule<Pair<ApplicationInstance, StateMachine>, DeploymentBehavior> stateMachine2DeploymentBehavior =
		new Rule(DEP.DeploymentBehavior)[Pair<ApplicationInstance, StateMachine> source, target |
			identifiable2DeploymentElement(source.value, target)

//*
			target.__states <=>	source.value._states.collectWith[
									source.key -> it
								].collectTo(
									state2BehaviorState
								)

			target.__current <=>	source.value._initial.collectWith[
    									source.key -> it
    								].collectTo(
    									state2BehaviorState
    								)
/**/
//*
			target.__transitions <=>	source.value._states.validOutgoingTransitions.collectWith[
    										source.key -> it
    									].collectTo(
    										transition2BehaviorTransition
    									)
/**/
		]

	val applicationInstance2DeploymentApplication =
		new Rule<ApplicationInstance, DeploymentApplication>(DEP.DeploymentApplication)[source, target |
			target.__id <=> source._identifier
//*
			target.__behavior <=>	source._type.behavior
//									.select[
//										it != null	// added 20160512
//									]
									.collectWith[
										source -> it	// new Pair(source, it)
									].collectTo(
										stateMachine2DeploymentBehavior
									)//.asOne(null)
/**/
		]

	val Rule<HostInstance, DeploymentHost> hostInstance2DeploymentHost =
		new Rule(DEP.DeploymentHost)[HostInstance source, target |
//			identifiable2DeploymentElement(source, target)	// commented out thanks to Comparator
	
			target.__ip <=> source._nodeIp
	
			target.__applications <=>	source._applications.collectTo(
											applicationInstance2DeploymentApplication
										)
		]

	def removeNavigationHelperContentAdapter(EObject eo) {
		val adapters = eo.eAdapters
		switch adapters.size {
			case 1: {
				// removing Viatra's NavigationHelperContentAdapter 
				adapters.remove(adapters.get(0))
			}
			case 0: {
				// nothing to do
			}
			default: {
				throw new IllegalStateException
			}
		}
	}

    static val immutableSourceTypes = #{DeploymentHostImpl, DeploymentApplicationImpl, DeploymentBehaviorImpl, BehaviorStateImpl, BehaviorTransitionImpl, Pair}

    private def static registerImmutableTypes(Transformation t, Set<? extends Class<?>> types) {
        types.forEach[
            t.graph.registerImmutableType(it)
        ]
    }

	/**
	 * Executes the simple batch transformation. The transformed model is placed in the traceability model set in the constructor 
	 */
	def void execute() {
		nbExecutes++

		// avoid rebinding on re-executions... should this be detected by AOF? 
		if(mapping.traces.empty) {
//			mapping.cps.eResource.save(new FileOutputStream("test.xmi"), Collections.emptyMap)

			removeNavigationHelperContentAdapter(mapping)
			removeNavigationHelperContentAdapter(mapping.deployment)

			val usedMemoryBefore = usedMemory
			val startTime = System.currentTimeMillis

											// We do not use the hostInstances derived feature
											// to make sure we are notified of changes.
//			mapping.deployment._hosts <=>	mapping.cps._hostTypes.instances.collectTo(
//												hostInstance2DeploymentHost
//											).asOrderedSet	// only for typing since we know that we have no duplicates
											// because hostTypes yields an OrderedSet and instances is a composition
			mapping.deployment.__hosts <=>    mapping.cps._hostTypes.instances1.collectTo(
                                                hostInstance2DeploymentHost
                                			 )
            tr.registerImmutableTypes(immutableSourceTypes)
            // DONE register in bindTraceFromTarget2 for when we use a different Transformation instance there


			val phase1 = System.currentTimeMillis - startTime
//			println("computing trace")

			// compute traces artificially (could be done before the main binding above)
			// doing it after is probably better for performance
			// because then the trace boxes do not need propagation on first run
			// DONE: checked that no (or only few) propagations occur on first run
//*
//			val t = new Thread[

            val traceTr =
                tr
//                new Transformation(new EMFPlatform)
            new TraceCreator(mapping, reverseCache,  traceTr).bindTraceFromTarget2
//            traceTr.initialize
            tr.initialize
//              bindTraceFromTarget
//			]
//			t.start
//			t.join

			val out = new PrintStream(new FileOutputStream("graph.plantuml"))
			out.println(tr.graph.toPlantUML)
			out.close

/*/
			mapping.traces.add(Trace.CPS2DeploymentTrace.clazz.newInstance)
/**/

			val phase2 = System.currentTimeMillis - startTime - phase1

			println('''Created «nbTargetElements» target elements & «mapping.traces.size» trace elements.''')
//			if(boxesAnalyzer != null) {
//				boxesAnalyzer.analyze
//
//				val out = new PrintStream(new FileOutputStream("measures.txt", true))
//
////				boxesAnalyzer.clearDelegates
//
//				out.println('''«nbTargetElements»	«boxesAnalyzer.nbBoxes»	«usedMemoryBefore»	«usedMemory»	«phase1»	«phase2»''')
//				out.close
//
//				boxesAnalyzer.dispose
//			}
		}
	}

	def getUsedMemory() {
/*
		System.gc
		System.gc
		System.gc
		System.gc
		System.gc
		Thread.sleep(1000)
		System.gc
/**/
		Runtime.runtime.totalMemory - Runtime.runtime.freeMemory
	}

//	def <A> A get(Object o, String fieldName) {
//		val field = o.class.getDeclaredField(fieldName)
//		field.accessible = true
//		field.get(o) as A
//	}

	// Rationale: we only keep traces for elements that are actually in the target model
//	def bindTraceFromTarget() {
//		val allDeploymentHosts =
//			mapping.deployment._hosts
//		val allDeploymentApplications =
//			allDeploymentHosts.applications
//		val allDeploymentBehaviors =
//			allDeploymentApplications.behavior
//		val allBehaviorStates =
//			allDeploymentBehaviors.states
//		val allBehaviorTransitions =
//			allDeploymentBehaviors.transitions	// TODO: filter valid transitions?
//
//		// all 1-1 links (with some having the same source)
//		val allTraceLinks =
//			// concat is equivalent to union here, but much less expensive
//			concat(
//				(allDeploymentHosts.collectedFrom(
//					hostInstance2DeploymentHost
//				).zip(allDeploymentHosts, true) as IBox<?>) as IBox<IPair<Identifiable, DeploymentElement>>
//			,
//				(allDeploymentApplications.collectedFrom(
//					applicationInstance2DeploymentApplication
//				).zip(allDeploymentApplications, true) as IBox<?>) as IBox<IPair<Identifiable, DeploymentElement>>
//			,
//				(allDeploymentBehaviors.collectedFrom(
//					stateMachine2DeploymentBehavior
//				).collect[value].zip(allDeploymentBehaviors, true) as IBox<?>) as IBox<IPair<Identifiable, DeploymentElement>>
//			,
//				(allBehaviorStates.collectedFrom(
//					state2BehaviorState
//				).collect[value].zip(allBehaviorStates, true) as IBox<?>) as IBox<IPair<Identifiable, DeploymentElement>>
//			,
//				(allBehaviorTransitions.collectedFrom(
//					transition2BehaviorTransition
//				).collect[value].zip(allBehaviorTransitions, true) as IBox<?>) as IBox<IPair<Identifiable, DeploymentElement>>
//			).select[
//				left != null && right != null	// TODO: why do we have nulls? because of the way we use zips?
//				// + do we (transiently) have non-matching pairs?
//				// that would lead to useless transient trace elements
//			]
////*
//		// TODO: try to collectTo over each of the 5 boxes individually then concat the results
//		// Remark: the first two boxes do not need the select (because there is only one target element
//		// for a given source element)
//		mapping._traces <=>	allTraceLinks.collect[
//								it.left
//							].asOrderedSet	// remove duplicates
//							.collectTo(new Rule(Trace.CPS2DeploymentTrace.clazz)[Identifiable e, ret |
//
//								ret.cpsElements.add(e)
//								ret._deploymentElements <=>	allTraceLinks.select[
//																left === e
//															].collect[right].asOrderedSet
//							])//.asOrderedSet	// why don't we need the asOrderedSet
///**/
//	}

//	def <E, I, G> groupByAs(IBox<E> source, IUnaryFunction<E, I> getGroupId, IUnaryFunction<I, G> groupCreator, IUnaryFunction<G, IBox<E>> getBox){
//		new GroupByAs(source, getGroupId, groupCreator, getBox).result
//	}
//
//	def <E, I, G> groupBy(IBox<E> source, IUnaryFunction<E, I> getGroupId){
//		new GroupByAs(source, getGroupId, [
//			it -> AOFFactory.INSTANCE.<E>createSet
//		], [
//			it.value
//		]).result
//	}

//	def <E, K> IBox<E> selectBy(IBox<E> source, IOne<K> key, IUnaryFunction<E, IOne<K>> collector) {
///*
//		// naive implementation, which makes the number of Ones explode (equivalent to the original selectMutable[...eqOne...]
//		source.selectMutable[
//			collector.apply(it).eqOne(key)
//		]
///*/
//		new SelectBy(source, key, collector).result
///**/
//	}

    @Data
    static class TraceCreator {
        val CPSToDeployment mapping
        val HashMap<Object, Object> reverseCache
        val Transformation tr
        extension val DEP dep
        extension val Trace trace

        new(CPSToDeployment mapping, HashMap<Object, Object> reverseCache, Transformation tr) {
            this.mapping = mapping
            this.reverseCache = reverseCache
            this.tr = tr
            this.dep = new DEP(tr)
            this.trace = new Trace(tr)
        }

    	def bindTraceFromTarget2() {
    
                tr.registerImmutableTypes(immutableSourceTypes)
                // for trace:
                tr.registerImmutableTypes(#{CPS2DeploymentTraceImpl, StateMachineImpl, TransitionImpl, StateImpl})
    
    
    		val allDeploymentHosts =
    			mapping.deployment._hosts
    		val allDeploymentApplications =
    			allDeploymentHosts.applications
    		val allDeploymentBehaviors =
    			allDeploymentApplications.behavior.selectWith[it !== null]
    		val allBehaviorStates =
    			allDeploymentBehaviors.states
    		val allBehaviorTransitions =
    			allDeploymentBehaviors.transitions	// TODO: filter valid transitions?
    
    		val allTraceLinks =
    //			// concat is equivalent to union here, but much less expensive
    			concat(
    				createTrace(allDeploymentHosts)
    			,
    				createTrace(allDeploymentApplications)
    			,
    				createFlatTraceWithGroupByAs(allDeploymentBehaviors)
    			,
    				createFlatTraceWithGroupByAs(allBehaviorStates)
    			,
    				createFlatTraceWithGroupByAs(allBehaviorTransitions)
    			)
    ////			.asOrderedSet		// should not be necessary
    //			.asAlreadyUniqueOrderedSet
    //
    //		// DONE: try to collectTo over each of the 5 boxes individually then concat the results
    //		// DONE: the first two boxes do not need the select (because there is only one target element
    //		// for a given source element)
    		mapping.__traces <=>	allTraceLinks

//    		tr.initialize
    //	}
    
    //	def <E> asAlreadyUniqueOrderedSet(IBox<E> source) {
    //		val result = AOFFactory.INSTANCE.createOrderedSet();
    //		new Copy(source, result)
    //		result
    	}


        // without flattening, only for flat trace links (no Y)
        // TODO: we may not need the flattenedReverseCache but could rather directly use the reverseCache
        def Box<CPS2DeploymentTrace> createTrace(Box<? extends DeploymentElement> targetElements) {
            targetElements
    //        .selectWith[it != null]
              .collectTo[target |
                val ret = Trace.CPS2DeploymentTrace.newInstance
    //          val source = flattenedReverseCache.get(target)
                val source = reverseCache.get(target) as Identifiable
                ret.cpsElements.add(source)
                ret.deploymentElements.add(target)
                ret
            ]
        }
    
        def createFlatTrace(Box<? extends DeploymentElement> targetElements) {
            // building sourceTargetPairs reduces calls to cache.get, which is more expensive
            val sourceTargetPairs = targetElements.selectWith[it !== null].collectWith[target |
    //          val source = flattenedReverseCache.get(target)
                val source = (reverseCache.get(target) as Pair<?, Identifiable>).value
                source -> target
            ]
            
            
            sourceTargetPairs.key
    //      .asOrderedSet   // necessary because there are several target elements for a given source element
            .collectTo[source |
                val ret = Trace.CPS2DeploymentTrace.newInstance
                ret.cpsElements.add(source)
    
                ret.__deploymentElements <=>    sourceTargetPairs.selectWith[
                                                    it.key === source
                                                ].value
    //                                          .asOrderedSet   // TODO: should not be mandatory
    //                                          as IBox<DeploymentElement>

                ret
            ]
        }

        def createFlatTraceWithGroupByAs(Box<? extends DeploymentElement> targetElements) {
            // TODO
            createFlatTrace(targetElements)
        }
    //  def createFlatTraceWithGroupByAs(IBox<? extends DeploymentElement> targetElements) {
    //      new GroupByAs(targetElements, [
    //          (reverseCache.get(it) as Pair<?, Identifiable>).value
    //      ],
    //      [
    //          val ret = Trace.CPS2DeploymentTrace.clazz.newInstance
    //          ret.cpsElements.add(it)
    //          ret
    //      ],
    //      [
    //          it._deploymentElements
    //      ]).result
    //  }

        val cache = new HashMap<Object, Object>
        def <E, R> Box<R> collectTo(Box<E> box, (E)=>R collector) {
            box.collectWith[e |
                var ret = cache.get(e)
                if(!cache.containsKey(e)) {
                    ret = collector.apply(e)
                    cache.put(e, ret)
                }
                ret as R
            ]
        }
    }

	var nbExecutes = 0

	/**
	 * Cleans up the transformation
	 */
	def dispose() {
		traceBegin("dispose()")
		forwardCache = null
		reverseCache = null
//		flattenedReverseCache = null
//		fcache = null
//		eqOneCache = null
		println('''Number of executions: «nbExecutes»''')
		nbExecutes = 0
		traceEnd("dispose()")
	}
}
