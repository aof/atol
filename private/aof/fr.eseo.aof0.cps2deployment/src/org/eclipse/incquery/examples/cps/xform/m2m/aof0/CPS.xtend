package org.eclipse.incquery.examples.cps.xform.m2m.aof0

import fr.eseo.aof0.xtend.utils.AOF0Accessors
import fr.eseo.dis.aof0.dsl.Transformation
import org.eclipse.viatra.examples.cps.cyberPhysicalSystem.CyberPhysicalSystemPackage

class CPS {
    val Transformation tr

    new(Transformation tr) {
        this.tr = tr
    }

    @AOF0Accessors.Field
    var CyberPhysicalSystemPackage cps
}
