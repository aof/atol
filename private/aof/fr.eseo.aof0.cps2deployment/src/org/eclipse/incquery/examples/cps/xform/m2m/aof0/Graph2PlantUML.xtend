package org.eclipse.incquery.examples.cps.xform.m2m.aof0

import fr.eseo.dis.aof0.core.Graph
import fr.eseo.dis.aof0.core.Operation
import fr.eseo.dis.aof0.core.operations.Read
import fr.eseo.dis.aof0.core.operations.Write
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

class Graph2PlantUML {

	def static toPlantUML(Graph graph) '''
		@startuml
		«FOR read : graph.operations.toSet»
			«read.toPlantUML»
		«ENDFOR»
		«FOR read : graph.operations.toSet»
			«read.toPlantUMLStreams»
		«ENDFOR»
		@enduml
	'''
//					«output.mutationStreams»

	def static toPlantUML(Operation op) '''
		state "«op.label» " as op_«op.id» {
			«FOR i : 0..<op.nbInputs»
				state "in«i»" as input_«op.getInput(i).id» <<inputPin>>
			«ENDFOR»
			«FOR i : 0..<op.nbInnerInputs»
				state "inner«i»" as input_«op.getInnerInput(i).id» <<inputPin>>
			«ENDFOR»
			«FOR i : 0..<op.nbOutputs»
				state "out«i»" as output_«op.getOutput(i).id» <<outputPin>>
			«ENDFOR»
		}
		«IF op instanceof Read»
			obj_«op.object.id» --> input_«op.input.id»
			state "«op.object.class.simpleName»@«op.object.id»" as obj_«op.object.id»
		«ENDIF»
		«IF op instanceof Write»
			output_«op.output.id» --> obj_«op.object.id»
			state "«op.object.class.simpleName»@«op.object.id»" as obj_«op.object.id»
		«ENDIF»
	'''

	// EMF-specific
	def static object(Read r) {
		(r.property as Pair<?,?>).key
	}

	// EMF-specific
	def static object(Write r) {
		(r.property as Pair<?,?>).key
	}

	def static toPlantUMLStreams(Operation op) '''
		«FOR output : op.outputs»
			«FOR stream : output.mutationStreams»
				output_«stream.output.id» --> input_«stream.input.id»
			«ENDFOR»
		«ENDFOR»
	'''

	def static id(Object o) {
		System.identityHashCode(o).toString
	}

	def static outputs(Operation o) {
		(0..<o.nbOutputs).map[
			o.getOutput(it)
		]
	} 

	def static inputs(Operation o) {
		(0..<o.nbInputs).map[
			o.getInput(it)
		]
	}

	def static innnerInputs(Operation o) {
		(0..<o.nbInnerInputs).map[
			o.getInnerInput(it)
		]
	}

	def static Iterable<Operation> operations(Graph graph) {
		graph.reads.map[read |
			read.operations
		].flatten
	}

	def static Iterable<Operation> operations(Operation op) {
		#{
			#{op},
			op.outputs.map[out |
				out.mutationStreams.map[stream |
					stream.input.operation.operations
				].flatten
			].flatten
		}.flatten
	}

	def static label(Operation op) {
		switch op {
			Read: {
				val p = op.property
				"Read " + p.propertyLabel
			}
			Write: {
				val p = op.property
				"Write " + p.propertyLabel
			}
			default: {
				var sn = op.class.simpleName
				if(sn == "") {
					sn = "noname"
				}
				sn
			}
		}
	}

	def static propertyLabel(Object p) {
		switch p {
			EStructuralFeature: p.name
			Pair<EObject, EStructuralFeature>: p.value.name
			default: p
		}
	}
}