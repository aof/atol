package org.eclipse.incquery.examples.cps.xform.m2m.aof0

import fr.eseo.aof0.xtend.utils.AOF0Accessors
import fr.eseo.dis.aof0.dsl.Transformation
import org.eclipse.viatra.examples.cps.traceability.TraceabilityPackage

class Trace {
    val Transformation tr

    new(Transformation tr) {
        this.tr = tr
    }

    @AOF0Accessors.Field
    var TraceabilityPackage cps
}