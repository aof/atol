package fr.eseo.aof2.exploration;

import java.util.List;

// TODO: this interface should not extend Box... notably to avoid method name conflicts
// or StoredBox?
public interface ModifiableBox<E> extends List<E> {
	// accessors
//	E get(int index);

	// mutators
//	void add(E element);
//	void remove(E element);
//	E removeAt(int index);

	Box<E> asBox();

	// moving methods (same as in EMF EList)

	void move(int newPosition, E element);

	E move(int newPosition, int oldPosition);

	// TODO: move to Box?
	void setObserver(Observer<E> observer);
}
