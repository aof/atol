package fr.eseo.aof2.exploration.impl.symbolic;

import fr.eseo.aof2.exploration.ModifiableBox;

public class StoredSymbolicBox<E> extends SymbolicBox<E> {

	public StoredSymbolicBox(ModifiableSymbolicBox<E> mb) {
		this.mb = mb;
	}

	public StoredSymbolicBox() {
		this(new ModifiableSymbolicBox<E>());
	}

	@Override
	public ModifiableBox<E> stored() {
		return this.mb;
	}

	private ModifiableSymbolicBox<E> mb;

	@Override
	public boolean isStored() {
		return true;
	}

	@Override
	public String toString() {
		return mb.toString();
	}
}
