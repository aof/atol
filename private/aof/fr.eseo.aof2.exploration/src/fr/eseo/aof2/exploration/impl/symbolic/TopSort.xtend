package fr.eseo.aof2.exploration.impl.symbolic

import AOFGraph.Box
import AOFGraph.ComputedBox
import AOFGraph.Operation
import AOFGraph.StoredBox
import java.util.LinkedHashSet
import java.util.Set

// https://en.wikipedia.org/wiki/Topological_sorting
class TopSort {
	// so far, only for forward propagation
	public static def Set<Operation> sort(Box box) {
		val ops = new LinkedHashSet<Operation>
		sort(box, ops)
		ops
	}

	// TODO:
	//	- test
	//	- add cycle detection?
	//		- e.g., by testing for each edge whether the obtained ordering is valid
	//			see https://en.wikipedia.org/wiki/Directed_acyclic_graph#Topological_sorting_and_recognition 
	//	- make sure graph analysis (which includes topological sorting) is only done once per (sub)graph
	//		even if it is used in multiple propagations, and/or is multiply instantiated
	private static def void sort(Box box, Set<Operation> ops) {
		for(op : box.consumers) {
			if(!ops.contains(op)) {
				if(
					op.sources.forall[b |
						switch b {
							ComputedBox: ops.contains(b.producer)
							StoredBox: true
							default: throw new IllegalStateException
						}
					]
				) {
					ops.add(op)
					op.results.forEach[
						sort(it, ops)
					]
				}
			}
		}
	}
}