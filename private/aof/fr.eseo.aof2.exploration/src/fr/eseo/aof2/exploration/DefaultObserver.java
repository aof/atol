package fr.eseo.aof2.exploration;

import java.util.Collection;

// TODO: support default translation of events?
public class DefaultObserver<E> implements Observer<E> {

	@Override
	public void add(E e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insertAt(int index, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insertAllAt(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeAt(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void replaceAt(int index, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void move(int newPosition, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void move(int newPosition, int oldPosition) {
		throw new UnsupportedOperationException();
	}
}
