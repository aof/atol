package fr.eseo.aof2.exploration.graph.interpreter

import AOFGraph.AOFGraphPackage
import AOFGraph.SubGraph
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IOne
import org.eclipse.xtend.lib.annotations.Data

import static fr.eseo.aof2.exploration.graph.interpreter.GraphInterpreterWithAOF1.*

class GraphInterpreterWithAOF1 {
	static val factory = AOFFactory.INSTANCE

	def static void main(String[] args) {
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

		val path = "../fr.eseo.aof2.exploration.graph.model/model/"

		val p = AOFGraphPackage.eINSTANCE
		rs.packageRegistry.put(p.nsURI, p)

		val m = rs.getResource(URI.createFileURI('''«path»/collectM_example.xmi'''), true)
		val sg = m.contents.get(0) as SubGraph

		sg.interpret(factory.createSequence(
			new A(new B(1))
		))
	}

	@Data
	static class A {
		val IBox<B> b = factory.createOne(null)
		new(B b) {
			this.b.assign(b)
		}
	}

	@Data
	static class B {
		val IOne<Integer> c = factory.createOne(0)
		new(Integer c) {
			this.c.assign(c)
		}
	}

	def static interpret(SubGraph sg, IBox<?>...arguments) {
		
	}
}