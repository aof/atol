package fr.eseo.aof2.exploration;

import java.util.Collection;

public interface Observer<E> {
// Unordered
	// If ordered this means "append"
	void add(E e);

	// If ordered this means "appendAll"
	void addAll(Collection<? extends E> c);

	void clear();

	void remove(Object o);

	void removeAll(Collection<?> c);

	void retainAll(Collection<?> c);

// Ordered
	void insertAt(int index, E element);

	void insertAllAt(int index, Collection<? extends E> c);

	void removeAt(int index);

	void replaceAt(int index, E element);

	void move(int newPosition, E element);

	void move(int newPosition, int oldPosition);
}
