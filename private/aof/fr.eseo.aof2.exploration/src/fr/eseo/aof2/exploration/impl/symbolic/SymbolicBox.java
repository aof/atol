package fr.eseo.aof2.exploration.impl.symbolic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.emf.ecore.EClass;

import AOFGraph.AOFGraphFactory;
import AOFGraph.AOFGraphPackage;
import AOFGraph.Collect;
import AOFGraph.CollectMutable;
import AOFGraph.ComputedBox;
import AOFGraph.Get;
import AOFGraph.ObjectParameter;
import AOFGraph.Operation;
import AOFGraph.ParameterBox;
import AOFGraph.PropertyBox;
import AOFGraph.Select;
import AOFGraph.SelectMutable;
import AOFGraph.SetProperty;
import AOFGraph.SubGraph;
import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.ModifiableBox;
import fr.eseo.aof2.exploration.SymbolicObject;

public class SymbolicBox<E> implements Box<E> {

	private AOFGraph.Box box;

	public SymbolicBox() {
		this(AOFGraphFactory.eINSTANCE.createStoredBox());
	}

	public SymbolicBox(String name) {
		this(AOFGraphFactory.eINSTANCE.createParameterBox());
		((ParameterBox)box).setName(name);
	}

	public SymbolicBox(AOFGraph.Box box) {
		this.box = box;
	}

	private static int index = 0;

	private ComputedBox apply_(String opName, AOFGraph.Box...sourceBoxes) {
		ComputedBox ret = AOFGraphFactory.eINSTANCE.createComputedBox();
		Operation op = AOFGraphFactory.eINSTANCE.createOperation();
		op.getSources().add(this.box);
		for(AOFGraph.Box sourceBox : sourceBoxes) {
			op.getSources().add(sourceBox);
		}
		op.getResults().add(ret);
		op.setOpName(index++ + ":" + opName);
		return ret;
	}

	private ComputedBox apply2(EClass operationType, AOFGraph.Box...sourceBoxes) {
		ComputedBox ret = AOFGraphFactory.eINSTANCE.createComputedBox();
		Operation op = (Operation)AOFGraphFactory.eINSTANCE.create(operationType);
		op.getSources().add(this.box);
		for(AOFGraph.Box sourceBox : sourceBoxes) {
			op.getSources().add(sourceBox);
		}
		op.getResults().add(ret);
		return ret;
	}

	@Override
	public Box<E> select(Predicate<? super E> selector) {
		// TODO: selector
//		return new SymbolicBox<>(apply("select"));
		ComputedBox ret = apply2(AOFGraphPackage.eINSTANCE.getSelect());
		((Select<E>)ret.getProducer()).setSelector(selector);
		return new SymbolicBox<>(ret);
	}

	@Override
	public Box<E> selectMutable(Function<? super E, Box<Boolean>> selector) {
		// TODO: selector
		throw new UnsupportedOperationException("only selectMutableSym is supported now");
//		return new SymbolicBox<>(apply("selectMutable"));
	}

	@Override
	public Box<E> selectMutableSym(Function<SymbolicObject<E>, Box<Boolean>> selector) {
		ComputedBox b = apply2(AOFGraphPackage.eINSTANCE.getSelectMutable());
		((SelectMutable)b.getProducer()).setSelector(reify(selector));
		return new SymbolicBox<>(b);
	}

	@Override
	public <R> Box<R> collect(Function<? super E, ? extends R> collector) {
		// TODO: collector
//		return new SymbolicBox<>(apply("collect"));
		ComputedBox ret = apply2(AOFGraphPackage.eINSTANCE.getCollect());
		((Collect<E,R>)ret.getProducer()).setCollector(collector);
		return new SymbolicBox<>(ret);
	}

	@Override
	public <R> Box<R> collectMutable(Function<? super E, ? extends Box<R>> collector) {
		// TODO: collector
		throw new UnsupportedOperationException("only collectMutableSym is supported now");
//		return new SymbolicBox<>(apply("collectMutable"));
	}

	private <P, R> SubGraph reify(Function<SymbolicObject<P>, Box<R>> f) {
		SubGraph ret = AOFGraphFactory.eINSTANCE.createSubGraph();
		ObjectParameter p = AOFGraphFactory.eINSTANCE.createObjectParameter();
		ret.getObjectParameters().add(p);
		SymbolicBox<R> output = (SymbolicBox<R>)f.apply(new SymbolicObject<P>() {
			@Override
			public <E> Box<E> get(Object property) {
				PropertyBox ret = AOFGraphFactory.eINSTANCE.createPropertyBox();
				ret.setObject(p);
				ret.setPropertyName((String)property);
				return new SymbolicBox<E>(ret);
			}
		});
		ret.getOutputs().add(output.box);
		return ret;
	}

	@Override
	public <R> Box<R> collectMutableSym(Function<SymbolicObject<E>, Box<R>> collector) {
		ComputedBox b = apply2(AOFGraphPackage.eINSTANCE.getCollectMutable());
		((CollectMutable)b.getProducer()).setCollector(reify(collector));
		return new SymbolicBox<>(b);
	}


	private static <E> AOFGraph.Box unwrap(Box<E> b) {
		return ((SymbolicBox<E>)b).box;
	}

	@Override
	public Box<E> concat(Box<E> other) {
//		return new SymbolicBox<>(apply("concat", unwrap(other)));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getConcat(), unwrap(other)));
	}

	@Override
	public Box<E> first() {
//		return new SymbolicBox<>(apply("first"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getFirst()));
	}

	@Override
	public Box<E> last() {
//		return new SymbolicBox<>(apply("last"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getLast()));
	}

//	public Box<E> get(Box<Integer> index) {

	@Override
	public Box<E> get(int index) {
		// DONE: index
//		return new SymbolicBox<>(apply("get"));
		ComputedBox b = apply2(AOFGraphPackage.eINSTANCE.getGet());
		((Get)b.getProducer()).setIndex(index);
		return new SymbolicBox<>(b);
	}

	@Override
	public Box<Integer> size() {
//		return new SymbolicBox<>(apply("size"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getSize()));
	}

	@Override
	public Box<Boolean> isEmpty() {
//		return new SymbolicBox<>(apply("isEmpty"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getIsEmpty()));
	}

	@Override
	public Box<Boolean> notEmpty() {
//		return new SymbolicBox<>(apply("notEmpty"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getNotEmpty()));
	}

	@Override
	public Box<E> asBag() {
//		return new SymbolicBox<>(apply("asBag"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getAsBag()));
	}

	@Override
	public Box<E> asOrderedSet() {
//		return new SymbolicBox<>(apply("asOrderedSet"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getAsOrderedSet()));
	}

	@Override
	public Box<E> asSequence() {
//		return new SymbolicBox<>(apply("asSequence"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getAsSequence()));
	}

	@Override
	public Box<E> asSet() {
//		return new SymbolicBox<>(apply("asSet"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getAsSet()));
	}

	@Override
	public Box<E> unique() {
//		return new SymbolicBox<>(apply("unique"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getUnique()));
	}

	@Override
	public Box<E> nonUnique() {
//		return new SymbolicBox<>(apply("nonUnique"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getNonUnique()));
	}

	@Override
	public Box<E> unordered() {
//		return new SymbolicBox<>(apply("unordered"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getUnordered()));
	}

	@Override
	public Box<E> ordered() {
//		return new SymbolicBox<>(apply("ordered"));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getOrdered()));
	}

	@Override
	public ModifiableBox<E> stored() {
		// TODO: return the same ModifiableSymbolicBox?
		// Actually, a SymbolicBox that is only used to create the underlying graph should probably not allow stored() to be called
		return new ModifiableSymbolicBox<>((List<E>)new ArrayList(Arrays.asList(1, 2, 3 ,4)));
	}

	@Override
	public boolean isStored() {
		return false;
	}

	@Override
	public Box<E> inspect(String message) {
		box.setInspectMessage(message);
		return this;
	}

	@Override
	public <F, R> Box<R> zipWith(Box<F> other, BiFunction<? super E, ? super F, ? extends R> zipper) {
//		return new SymbolicBox<>(apply("zipWith", unwrap(other)));
		return new SymbolicBox<>(apply2(AOFGraphPackage.eINSTANCE.getZipWith(), unwrap(other)));
	}

	public AOFGraph.Box getBox() {
		return box;
	}

	public void set(Object object, Object feature) {
	    SetProperty ret = AOFGraphFactory.eINSTANCE.createSetProperty();
	    ret.setObject((ObjectParameter)object);
	    ret.setPropertyName((String)feature);
	    ret.getSources().add(box);
	}
}
