package fr.eseo.aof2.exploration.impl.aof1;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IBag;
import org.eclipse.papyrus.aof.core.IBox;
import org.eclipse.papyrus.aof.core.IOne;
import org.eclipse.papyrus.aof.core.IOrderedSet;
import org.eclipse.papyrus.aof.core.ISequence;
import org.eclipse.papyrus.aof.core.ISet;
import org.eclipse.papyrus.aof.core.IUnaryFunction;

import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.ModifiableBox;
import fr.eseo.aof2.exploration.SymbolicObject;

public class AOF1Box<E> implements Box<E> {

	private IBox<E> box;

	public AOF1Box(IBox<E> box) {
		this.box = box;
	}

	@Override
	public Box<E> select(Predicate<? super E> selector) {
		return new AOF1Box<>(box.select(e -> selector.test(e)));
	}

	@Override
	public Box<E> selectMutable(Function<? super E, Box<Boolean>> selector) {
		return new AOF1Box<>(box.selectMutable(e -> (IOne<Boolean>)unwrap(selector.apply(e))));
	}

	private <P, R> IUnaryFunction<P, IBox<R>> lambdaConverter(Function<SymbolicObject<P>, Box<R>> f) {
		return e -> {
			return unwrap(
				f.apply(new SymbolicObject<P>() {
					@Override
					public <T> Box<T> get(Object property) {
						if(e == null) {
							return new AOF1Box<>((IBox<T>)IBox.SEQUENCE);// TODO: adapt type... should probably not be done here
						} else {
							// TODO: should factory choice be done at the "Context" level?
							// taking into account that multiple factories may be used in a given a transformation
							return new AOF1Box<>(AOFFactory.INSTANCE.createPropertyBox(e, property));
						}
					}
				})
			);
		};
	}

	// TODO: factorize lambda transformation with collectMutableSym
	@Override
	public Box<E> selectMutableSym(Function<SymbolicObject<E>, Box<Boolean>> selector) {
//*											// TODO: find a better way to convert the return type from IBox<Boolean> to IOne<Boolean> than yet another lambda
		return new AOF1Box<E>(box.selectMutable(e -> (IOne<Boolean>)lambdaConverter(selector).apply(e)));
/*/
		return new AOF1Box<E>(
				box.selectMutable(e -> {
					return (IOne<Boolean>)unwrap(
						selector.apply(new SymbolicObject<E>() {
							@Override
							public <T> Box<T> get(Object property) {
								if(e == null) {
									return new AOF1Box<>((IBox<T>)IBox.SEQUENCE);// TODO: adapt type... should probably not be done here
								} else {
									// TODO: should factory choice be done at the "Context" level?
									// taking into account that multiple factories may be used in a given a transformation
									return new AOF1Box<>(AOFFactory.INSTANCE.createPropertyBox(e, property));
								}
							}
						})
					);
				})
			);
/**/
	}

	@Override
	public <R> Box<R> collect(Function<? super E, ? extends R> collector) {
		return new AOF1Box<>(box.collect(collector::apply));
	}

	@Override
	public <R> Box<R> collectMutable(Function<? super E, ? extends Box<R>> collector) {
		// TODO: use unwrap...
		return new AOF1Box<>(box.collectMutable(e -> unwrap(collector.apply(e))));
	}

	@Override
	public <R> Box<R> collectMutableSym(Function<SymbolicObject<E>, Box<R>> collector) {
//*
		return new AOF1Box<>(box.collectMutable(lambdaConverter(collector)));
/*/
		return new AOF1Box<R>(
			box.collectMutable(e -> {
				return unwrap(
					collector.apply(new SymbolicObject<E>() {
						@Override
						public <T> Box<T> get(Object property) {
							if(e == null) {
								return new AOF1Box<>((IBox<T>)IBox.SEQUENCE);// TODO: adapt type... should probably not be done here
							} else {
								// TODO: should factory choice be done at the "Context" level?
								// taking into account that multiple factories may be used in a given a transformation
								return new AOF1Box<>(AOFFactory.INSTANCE.createPropertyBox(e, property));
							}
						}
					})
				);
			})
		);
/**/
	}

	@Override
	public Box<E> first() {
		return new AOF1Box<>(box.asOne(null));
	}

	@Override
	public Box<E> last() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> get(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> asBag() {
		return new AOF1Box<>(box.asBag());
	}

	@Override
	public Box<E> asOrderedSet() {
		return new AOF1Box<>(box.asOrderedSet());
	}

	@Override
	public Box<E> asSequence() {
		return new AOF1Box<>(box.asSequence());
	}

	@Override
	public Box<E> asSet() {
		return new AOF1Box<>(box.asSet());
	}

	@Override
	public Box<E> unique() {
		if(box instanceof ISequence) {
			return new AOF1Box<>(box.asOrderedSet());
		} else if(box instanceof IBag) {
			return new AOF1Box<>(box.asSet());
		} else {
			return this;
		}
	}

	@Override
	public Box<E> nonUnique() {
		if(box instanceof IOrderedSet) {
			return new AOF1Box<>(box.asSequence());
		} else if(box instanceof ISet) {
			return new AOF1Box<>(box.asBag());
		} else {
			return this;
		}
	}

	@Override
	public Box<E> unordered() {
		if(box instanceof ISequence) {
			return new AOF1Box<>(box.asBag());
		} else if(box instanceof IOrderedSet) {
			return new AOF1Box<>(box.asSet());
		} else {
			return this;
		}
	}

	@Override
	public Box<E> ordered() {
		if(box instanceof IBag) {
			return new AOF1Box<>(box.asSequence());
		} else if(box instanceof ISet) {
			return new AOF1Box<>(box.asOrderedSet());
		} else {
			return this;
		}
	}

	@Override
	public String toString() {
		return box.toString();
	}

	@Override
	public ModifiableBox<E> stored() {
		return new ModifiableAOF1Box<>(box);
	}

	@Override
	public boolean isStored() {
		return true;
	}

	@Override
	public Box<E> inspect(String message) {
		box.inspect(message);
		return this;
	}

	private static <E> IBox<E> unwrap(Box<E> box) {
		return ((AOF1Box<E>)box).box;
	}

	@Override
	public Box<E> concat(Box<E> other) {
		return new AOF1Box<>(box.concat(unwrap(other)));
	}

	@Override
	public Box<Integer> size() {
		return new AOF1Box<>(box.size());
	}

	@Override
	public Box<Boolean> isEmpty() {
		return new AOF1Box<>(box.isEmpty());
	}

	@Override
	public Box<Boolean> notEmpty() {
		return new AOF1Box<>(box.notEmpty());
	}

	@Override
	public <F, R> Box<R> zipWith(Box<F> other, BiFunction<? super E, ? super F, ? extends R> zipper) {
		return new AOF1Box<>(box.zipWith(unwrap(other), (e, f) -> zipper.apply(e, f)));
	}
}
