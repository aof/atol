package fr.eseo.aof2.exploration.impl.streams;

import java.util.List;

import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.Observer;

public class ModifiableStreamBox<E> extends AbstractListModifiableBox<E> {

	public ModifiableStreamBox(List<E> list) {
		super(list);
	}

	@Override
	public Box<E> asBox() {
		return new StreamBox<>(getList()::stream);
	}

	@Override
	public void setObserver(Observer<E> observer) {
		throw new UnsupportedOperationException();
	}
}
