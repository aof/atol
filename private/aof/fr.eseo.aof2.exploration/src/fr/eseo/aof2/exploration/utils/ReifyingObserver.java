package fr.eseo.aof2.exploration.utils;

import java.util.Collection;

import fr.eseo.aof2.exploration.Observer;

public abstract class ReifyingObserver<E> implements Observer<E> {

	public static class Event<E> {
		private EventType type;
		private Object e;
		private int index;
		private int oldPosition;

		public EventType getType() {
			return type;
		}

		public Object getE() {
			return e;
		}

		public int getIndex() {
			return index;
		}

		public int getOldPosition() {
			return oldPosition;
		}

		public Event(EventType type) {
			this.type = type;
		}

		public Event(EventType type, Object e) {
			this(type);
			this.e = e;
		}

		public Event(EventType type, int index, E e) {
			this(type, e);
			this.index = index;
		}

		public Event(EventType type, int newPosition, int oldPosition) {
			this(type);
			this.index = newPosition;
			this.oldPosition = oldPosition;
		}

		// no "extends E" because of re{move,tain}All
		// this compiles with "extends E" on Fred's machine, but not on Mickael's
		public Event(EventType type, Collection<?> c) {
			this(type, (Object)c);
		}

		public Event(EventType type, int index, Collection<? extends E> c) {
			this(type, (Object)c);
			this.index = index;
		}

		//"set(" + index + ", " + element + ")"
		public String toString() {
			StringBuilder sb = new StringBuilder(type.name().toLowerCase());
			sb.append('(');
			switch(type) {
			case REPLACE_AT:
			case INSERT_ALL_AT:
			case MOVE:
			case INSERT_AT: sb.append(index); sb.append(", ");
			case ADD:
			case ADD_ALL:
			case REMOVE:
			case REMOVE_ALL:
			case RETAIN_ALL: sb.append(e); break;
			case MOVE_AT: sb.append(index); sb.append(", ");
			case REMOVE_AT: sb.append(index); break;
			case CLEAR:
			default: break;
			}
			sb.append(')');
			return sb.toString();
		}
	}

	public static enum EventType {
		ADD, ADD_ALL, CLEAR, REMOVE, REMOVE_ALL, RETAIN_ALL,
		INSERT_AT, INSERT_ALL_AT, REMOVE_AT, REPLACE_AT, MOVE, MOVE_AT
	}

	public abstract void process(Event<E> e);

	@Override
	public void add(E e) {
		process(new Event<>(EventType.ADD, e));
	}

	@Override
	public void addAll(Collection<? extends E> c) {
		process(new Event<>(EventType.ADD_ALL, c));
	}

	@Override
	public void clear() {
		process(new Event<>(EventType.CLEAR));
	}

	@Override
	public void remove(Object o) {
		process(new Event<>(EventType.REMOVE, o));
	}

	@Override
	public void removeAll(Collection<?> c) {
		process(new Event<>(EventType.REMOVE_ALL, c));
	}

	@Override
	public void retainAll(Collection<?> c) {
		process(new Event<>(EventType.RETAIN_ALL, c));
	}

	@Override
	public void insertAt(int index, E element) {
		process(new Event<>(EventType.INSERT_AT, index));
	}

	@Override
	public void insertAllAt(int index, Collection<? extends E> c) {
		process(new Event<>(EventType.INSERT_ALL_AT, index, c));
	}

	@Override
	public void removeAt(int index) {
		process(new Event<>(EventType.REMOVE_AT, index));
	}

	@Override
	public void replaceAt(int index, E element) {
		process(new Event<>(EventType.REPLACE_AT, index, element));
	}

	@Override
	public void move(int newPosition, E element) {
		process(new Event<>(EventType.MOVE, newPosition, element));
	}

	@Override
	public void move(int newPosition, int oldPosition) {
		process(new Event<E>(EventType.MOVE_AT, newPosition, oldPosition));
	}

}
