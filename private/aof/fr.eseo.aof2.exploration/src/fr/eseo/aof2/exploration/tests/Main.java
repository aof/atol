package fr.eseo.aof2.exploration.tests;

import java.util.function.Predicate;

import org.eclipse.papyrus.aof.core.AOFFactory;
import org.eclipse.papyrus.aof.core.IFactory;

import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.ModifiableBox;
import fr.eseo.aof2.exploration.impl.aof1.AOF1Box;
import fr.eseo.aof2.exploration.impl.aof1.ModifiableAOF1Box;
import fr.eseo.aof2.exploration.impl.symbolic.ModifiableSymbolicBox;

public class Main {

	public static void main(String args[]) {
		IFactory factory = AOFFactory.INSTANCE;

		Box<Integer> a = new AOF1Box<>(factory.createSequence(1, 2, 3, 4));

//		test1(a);

//		test1(new StreamBox<>(() -> IntStream.of(1, 2, 3, 4).boxed()));

//		test2(a);

//		test1(new SymbolicBox<>());

		testForkMerge(new ModifiableAOF1Box<>(factory.createOne(false)));
		testForkMerge(new ModifiableSymbolicBox<>(true));
	}

	private static void testForkMerge(ModifiableBox<Boolean> _a) {
		Box<Boolean> a = _a.asBox();
		a.inspect("a: ");
		Box<Boolean> b =
				a
					.collect(e->e)
						.inspect("A: ")
					.zipWith(
						a
							.collect(e -> e)
								.inspect("B: ")
							.collect(e -> !e)
								.inspect("C: ")
						, (e, f) -> e && f
					)
		;
		b.inspect("b: ");
		_a.set(0, true);
	}

	private static void test1(Box<Integer> a) {
		Box<Integer> b = a.select(e -> e > 2);

		a.inspect("a: ");
		b.inspect("b: ");

		b.first().inspect("b.first: ");
		show("b.stored", b.stored());
		show("b.stored.get(0)", b.stored().get(0));

		// TODO: make sure change is shown first (by inspect) on originating box (i.e., b then a here),
		// which is not what AOF 1 does...
		b.stored().add(6);

		b.stored().add(0, 7);
	}

	public interface MutableFunctionBuilder<E, R> {
		static <E> MutableFunctionBuilder<E, E> identity() {
			return e -> e;
		}

		default MutableFunctionBuilder<E, R> inspect(String message) {
			return (e) -> this.apply(e).inspect(message);
		}

		default MutableFunctionBuilder<E, R> select(Predicate<R> predicate) {
			return (e) -> this.apply(e).select(predicate);
		}

		default MutableFunctionBuilder<E, R> first() {
			return (e) -> this.apply(e).first();
		}

		Box<R> apply(Box<E> source);
	}

	private static void test2(Box<Integer> a) {
		MutableFunctionBuilder<Integer, Integer> f = MutableFunctionBuilder.<Integer>identity()
			.inspect("a: ")
			.select(e -> e > 2)
				.inspect("b: ")
			.first()
				.inspect("b.first: ");
		f.apply(a).stored().add(6);
		f.apply(a).stored().add(7);
	}

	private static void show(String message, Object v) {
		System.out.println(message + ": " + v);
	}
}
