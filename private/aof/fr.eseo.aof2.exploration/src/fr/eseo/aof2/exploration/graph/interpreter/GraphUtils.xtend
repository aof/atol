package fr.eseo.aof2.exploration.graph.interpreter

import AOFGraph.AOFGraphFactory
import AOFGraph.ComputedBox
import AOFGraph.ParameterBox
import AOFGraph.SubGraph
import fr.eseo.aof2.exploration.Box
import fr.eseo.aof2.exploration.impl.symbolic.SymbolicBox
import java.util.Collection

class GraphUtils {
    def static <E> SubGraph wrap(Box<E> b) {
		val sb = b as SymbolicBox<E>
		val ret = AOFGraphFactory.eINSTANCE.createSubGraph
		ret.boxes.addAll(sb.box.allBoxes)
		ret.operations.addAll(sb.box.allOperations)
		ret.boxParameters.addAll(sb.box.params)
		println(ret.operations.size)
		ret
    }

    def static <E> SubGraph wrapAsOutput(Box<E> b) {
		val ret = b.wrap
		val sb = b as SymbolicBox<E>
		ret.outputs.add(sb.box)
		ret
    }

	private def static Collection<ParameterBox> params(AOFGraph.Box b) {
		switch b {
			ParameterBox: #[b]
			ComputedBox: b.producer.sources.map[it.params].flatten.toList
			default: throw new IllegalStateException
		}
	}

	private def static Collection<AOFGraph.Box> allBoxes(AOFGraph.Box b) {
		#[#[b],
		    switch b {
				ParameterBox: #[]
				ComputedBox: b.producer.sources.map[it.allBoxes].flatten.toList
				default: throw new IllegalStateException
			}
		].flatten.toList
	}

	private def static Collection<AOFGraph.Operation> allOperations(AOFGraph.Box b) {
	    switch b {
			ComputedBox: #[#[b.producer], b.producer.sources.map[it.allOperations].flatten.toList].flatten.toList
			ParameterBox: #[]
			default: throw new IllegalStateException
		}
	}
}