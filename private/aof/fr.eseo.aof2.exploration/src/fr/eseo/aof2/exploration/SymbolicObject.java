package fr.eseo.aof2.exploration;

public interface SymbolicObject<T> {
	<E> Box<E> get(Object property);
}
