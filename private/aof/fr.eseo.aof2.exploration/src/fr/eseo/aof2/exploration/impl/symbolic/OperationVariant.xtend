package fr.eseo.aof2.exploration.impl.symbolic

import AOFGraph.Operation

interface OperationVariant<S, R> {
	def Object createDataStructure()
	def Algorithm<S, R> getForwardAlgorithm()
	def Algorithm<R, S> getReverseAlgorithm()
}

interface Algorithm<S, R> {
	def void replace(int index, S newElement, R oldElement, Operation operation, Object dataStructure, Consumer<R> downstream)
}

interface Consumer<R> {
	def void set(int index, R newElement)
}

/*class CollectAlgorithm<S, R> implements Algorithm<S, R> {
	override replace(int index, S newElement, R oldElement, Operation operation, Object dataStructure, Consumer<R> downstream) {
		downstream.set(index, operation.collector.apply(newElement));
	}
}*/