package fr.eseo.aof2.exploration.impl.streams;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.ModifiableBox;

// REMARK:
//	this is only for exploration... and NOT incremental... but lazy
// DONE:
//	- reusing streams? => wrapping Supplier<Stream<E>> instead of wrapping Stream<E>
//		- but everything is always recomputed... unless the user explicitly calls stored().asBox()
public class StreamBox<E> implements Box<E> {

	private Supplier<Stream<E>> stream;
//	private Stream<E> stream;

	public StreamBox(Supplier<Stream<E>> stream) {
//	public StreamBox(Stream<E> stream) {
		this.stream = stream;
	}

	@Override
	public Box<E> select(Predicate<? super E> selector) {
//		return new StreamBox<>(stream.filter(selector));
		return new StreamBox<>(() -> stream.get().filter(selector));
	}

	@Override
	public Box<E> selectMutable(Function<? super E, Box<Boolean>> selector) {
//		return new StreamBox<>(stream.filter(e -> selector.apply(e).stored().get(0)));
		return new StreamBox<>(() -> stream.get().filter(e -> selector.apply(e).stored().get(0)));
	}

	@Override
	public <R> Box<R> collect(Function<? super E, ? extends R> collector) {
//		return new StreamBox<>(stream.map(collector));
		return new StreamBox<>(() -> stream.get().map(collector));
	}

	private static <E> Supplier<Stream<E>> unwrap(Box<E> box) {
		return ((StreamBox<E>)box).stream;
	}

	private static <E> Box<E> wrap(Supplier<Stream<E>> supplier) {
		return new StreamBox<E>(supplier);
	}

	@Override
	public <R> Box<R> collectMutable(Function<? super E, ? extends Box<R>> collector) {
//		return new StreamBox<>(stream.flatMap(e -> ((StreamBox<R>)collector.apply(e)).stream));
		return new StreamBox<>(() -> stream.get().flatMap(e -> unwrap(collector.apply(e)).get()));
	}

	// Optional::stream in Java 9
	private static <E> Stream<E> optstream(Optional<E> opt) {
		if(opt.isPresent()) {
			return Stream.of(opt.get());
		} else {
			return Stream.empty();
		}
	}

	@Override
	public Box<E> first() {
//		return new StreamBox<>(stream.limit(1));
		return new StreamBox<>(() -> stream.get().limit(1));
//		return new StreamBox<>(optstream(stream.findFirst()));
//		return new StreamBox<E>(stream.findFirst().flatMap(StreamBox::optstream));
	}

	@Override
	public Box<E> last() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> get(int index) {
//		return new StreamBox<>(stream.skip(index).limit(1));
		return new StreamBox<>(() -> stream.get().skip(index).limit(1));
	}

	@Override
	public Box<E> asBag() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> asOrderedSet() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> asSequence() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> asSet() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> unique() {
//		return new StreamBox<>(stream.distinct());
		return new StreamBox<>(() -> stream.get().distinct());
	}

	@Override
	public Box<E> nonUnique() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Box<E> unordered() {
//		return new StreamBox<>(stream.unordered());
		return new StreamBox<>(() -> stream.get().unordered());
	}

	@Override
	public Box<E> ordered() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ModifiableBox<E> stored() {
//		return new ListModifiableBox<>(stream.collect(Collectors.toList()));
		return new ModifiableStreamBox<>(stream.get().collect(Collectors.toList()));
	}

	@Override
	public boolean isStored() {
		return false;
	}

	@Override
	public Box<E> inspect(String message) {
		System.out.println(message + this.toString());
		return this;
	}

	@Override
	public String toString() {
		return stream.get().collect(Collectors.toList()).toString();
	}

	@Override
	public Box<E> concat(Box<E> other) {
		return wrap(() -> Stream.concat(stream.get(), unwrap(other).get()));
	}

	@Override
	public Box<Integer> size() {
		return wrap(() -> Stream.of(stream.get().toArray().length));
	}

	@Override
	public Box<Boolean> isEmpty() {
		return wrap(() -> Stream.of(stream.get().toArray().length == 0));
	}

	@Override
	public Box<Boolean> notEmpty() {
		return wrap(() -> Stream.of(stream.get().toArray().length != 0));
	}

	@Override
	public <F, R> Box<R> zipWith(Box<F> other, BiFunction<? super E, ? super F, ? extends R> zipper) {
		throw new UnsupportedOperationException();
	}
}
