package fr.eseo.aof2.exploration.impl.aof1;

import java.util.AbstractList;
import java.util.Iterator;

import org.eclipse.papyrus.aof.core.IBox;

import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.ModifiableBox;
import fr.eseo.aof2.exploration.Observer;

public class ModifiableAOF1Box<E> extends AbstractList<E> implements ModifiableBox<E> {

	private IBox<E> box;

	public ModifiableAOF1Box(IBox<E> box) {
		this.box = box;
	}

	@Override
	public Box<E> asBox() {
		return new AOF1Box<>(box);
	}

	@Override
	public E get(int index) {
		return box.get(index);
	}

	@Override
	public int size() {
		return box.length();
	}

	@Override
	public boolean add(E element) {
		int bl = box.length();
		box.add(element);
		return bl != box.length();
	}

	@Override
	public boolean remove(Object element) {
		int bl = box.length();
		box.remove((E)element);
		return bl != box.length();
	}

	@Override
	public E remove(int index) {
		E ret = box.get(index);
		box.removeAt(index);
		return ret;
	}

	@Override
	public String toString() {
		return box.toString();
	}

	@Override
	public void add(int index, E element) {
		box.add(index, element);
	}

//	@Override
//	public boolean addAll(Collection<? extends E> c) {
//		// remark: we rely on map being called sequentially
//		return c.stream().map(e -> add(e)).reduce(false, Boolean::logicalOr);
//	}
//
//	@Override
//	public boolean addAll(int index, Collection<? extends E> c) {
//		int bl = box.length();
//		for(E e : c) {
//			add(index++, e);
//		}
//		return bl != box.length();
//	}

	@Override
	public void clear() {
		box.clear();
	}

	@Override
	public boolean contains(Object o) {
		return box.contains((E)o);
	}

//	@Override
//	public boolean containsAll(Collection<?> c) {
//		return c.stream().map(e -> box.contains((E)e)).reduce(true, Boolean::logicalAnd);
//	}

	@Override
	public int indexOf(Object o) {
		return box.indexOf((E)o);
	}

//	@Override
//	public boolean isEmpty() {
//		return box.length() == 0;
//	}

	@Override
	public Iterator<E> iterator() {
		return box.iterator();
	}

//	@Override
//	public int lastIndexOf(Object o) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public ListIterator<E> listIterator() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public ListIterator<E> listIterator(int index) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public boolean removeAll(Collection<?> c) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean retainAll(Collection<?> c) {
//		// TODO Auto-generated method stub
//		return false;
//	}

	@Override
	public E set(int index, E element) {
		E ret = box.get(index);
		box.set(index, element);
		return ret;
	}

	@Override
	public void move(int newPosition, E element) {
		box.move(newPosition, box.indexOf(element));
	}

	@Override
	public E move(int newPosition, int oldPosition) {
		E ret = box.get(oldPosition);
		box.move(newPosition, oldPosition);
		return ret;
	}

	@Override
	public void setObserver(Observer<E> observer) {
		throw new UnsupportedOperationException();
	}

//	@Override
//	public List<E> subList(int fromIndex, int toIndex) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Object[] toArray() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public <T> T[] toArray(T[] a) {
//		// TODO Auto-generated method stub
//		return null;
//	}
}
