package fr.eseo.aof2.exploration.impl.symbolic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import AOFGraph.Operation;
import fr.eseo.aof2.exploration.Box;
import fr.eseo.aof2.exploration.Observer;
import fr.eseo.aof2.exploration.impl.streams.AbstractListModifiableBox;

public class ModifiableSymbolicBox<E> extends AbstractListModifiableBox<E> {
	public ModifiableSymbolicBox(SymbolicBox<E> sb) {
		this();
		this.sb = sb;
	}

	public ModifiableSymbolicBox(List<E> list) {
		super(list);
		this.sb = new SymbolicBox<>();
	}

	public E setOld(int index, E element) {
		E ret = super.set(index, element);
		// TODO: propagate to sb
		sendSet(sb.getBox(), index, element);
		return ret;
	}

	private void sendSet(AOFGraph.Box box, int index, E element) {
		System.out.println("Starting propagation");
//*
		System.out.println("* DF");
		sendSetDF(box, index, element);
/**/
//*
		// Ideally: a topological sort that keeps as many contiguous sequences as possible
		// so that they can actually be connected without reifying events
		System.out.println("* Topological sort");
		Set<Operation> ops = TopSort.sort(box);
		System.out.println("Top sort: " + ops.stream().map(e -> e.getOpName()).collect(Collectors.joining(", ")));
		applySet(box, index, element);
		for(Operation op : ops) {
			for(AOFGraph.Box result : op.getResults()) {
				applySet(result, index, element);
			}
		}
/**/
	}

	private void sendSetDF(AOFGraph.Box box, int index, E element) {
		// This algorithm is similar to the one in AOF1: depth first
		applySet(box, index, element);
		for(Operation o : box.getConsumers()) {
			System.out.println(o.getOpName());
			for(AOFGraph.Box result : o.getResults()) {
				// TODO: convert event
				sendSetDF(result, index, element);
			}
		}
	}

	private void applySet(AOFGraph.Box box, int index, E element) {
		if(box.getInspectMessage() != null) {
			System.out.println(box.getInspectMessage() + "set(" + index + ", " + element + ")");
		}
	}

	public ModifiableSymbolicBox(E...elements) {
		super(new ArrayList<>(Arrays.asList(elements)));
		this.sb = new StoredSymbolicBox<>(this);
	}

	private SymbolicBox<E> sb;

	@Override
	public Box<E> asBox() {
		return sb;
	}

	private Observer<E> observer;

	@Override
	public void setObserver(Observer<E> observer) {
		if(this.observer != null) {
			throw new IllegalStateException("Already observed");
		} else {
			this.observer = observer;
		}
	}

// @begin Writing
	@Override
	public boolean add(E e) {
		boolean ret = super.add(e);

		if(observer != null) {
			observer.add(e);
		}

		return ret;
	}

	@Override
	public void add(int index, E element) {
		super.add(index, element);

		if(observer != null) {
			observer.insertAt(index, element);
		}
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean ret = super.addAll(c);

		if(observer != null) {
			observer.addAll(c);
		}

		return ret;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		boolean ret = super.addAll(index, c);

		if(observer != null) {
			observer.insertAllAt(index, c);
		}

		return ret;
	}

	@Override
	public void clear() {
		super.clear();

		if(observer != null) {
			observer.clear();
		}
	}

	@Override
	public boolean remove(Object o) {
		boolean ret = super.remove(o);

		if(observer != null) {
			observer.remove(o);
		}

		return ret;
	}

	@Override
	public E remove(int index) {
		E ret = super.remove(index);

		if(observer != null) {
			observer.removeAt(index);
		}

		return ret;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean ret = super.removeAll(c);

		if(observer != null) {
			observer.removeAll(c);
		}

		return ret;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean ret = super.retainAll(c);

		if(observer != null) {
			observer.retainAll(c);
		}

		return ret;
	}

	@Override
	public E set(int index, E element) {
		E ret = super.set(index, element);

		if(observer != null) {
			observer.replaceAt(index, element);
		}

		return ret;
	}

	@Override
	public void move(int newPosition, E element) {
//		move(newPosition, super.indexOf(element));
		super.indexOf(element);
		super.add(newPosition, element);

		if(observer != null) {
			observer.move(newPosition, element);
		}
	}

	@Override
	public E move(int newPosition, int oldPosition) {
		E ret = super.remove(oldPosition);
		super.add(newPosition, ret);

		if(observer != null) {
			observer.move(newPosition, oldPosition);
		}

		return ret;
	}
// @end Writing
}
