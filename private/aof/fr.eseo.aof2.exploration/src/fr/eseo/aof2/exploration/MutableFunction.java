package fr.eseo.aof2.exploration;

/*
 * The default implementation of MutableFunction will create a new propagation graph on each call to apply
 * 	(necessary with AOF 1 anyway)
 * An optimized version could create a single reusable propagation graph (with AOF 2)
 * 
 * TODO: is this the interface we need?
 * or we may even need a special implementation of Box
 */
public interface MutableFunction<E, R> {

	default void test1(Box<Integer> a) {
		MutableFunction.<Box<Integer>>identity()
			.andThen(e -> e.select(f -> f > 2));
	}

	static <T> MutableFunction<T, T> identity() {
		return e -> e;
	}

	default <T> MutableFunction<E, T> andThen(MutableFunction<? super R, ? extends T> f) {
		return e -> f.apply(apply(e));
	}

	default <S> MutableFunction<S, R> compose(MutableFunction<? super S, ? extends E> f) {
		return e -> apply(f.apply(e));
	}

	R apply(E e);
}
