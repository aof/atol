package fr.eseo.aof2.exploration;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/*
 * TODO:
 * - implement this API using AOF 1 as back-end, for testing purposes
 * 		- does not need double-boxing of boxes in the propagation graph (only during creation)
 * 		- requires wrapping lambdas
 * - how to create a graph independently of source box?
 * - how to set parameters to operations
 * 
 * TODO longer term:
 * - primitive boxes (e.g., IntBox, BooleanBox)? (like IntStream, LongStream, DoubleStream in Java 8)
 * 		or wait until Java does this automatically for us
 */
 
 /*
  * Objective: this API should not need to be modified, should we decide to change the internal implementation
  * 
  * 
  * No mutator (modification methods, e.g., add, remove)
  * No accessor (direct access method, e.g., E get(int), E first()), for contents AND for type (e.g., isUnique, isOrdered)
  * TODO: how to prevent users from directly calling EMF accessors instead of getting property boxes
  * 	Maybe not possible (unless we do our own modeling framework)
  * 		=> something that remains in our expertise?
  */
public interface Box<E> {

	default void test1(Box<Integer> a) {
		a.inspect("a: ");
		Box<Integer> b =
//				a.select(e -> e > 5);	// select propagation may be relatively costly if a is ordered
				a.unordered().select(e -> e > 5);	// makes select propagation less costly BUT only if we
												// do not need to preserve ordering
		b.inspect("b: ");
		Box<Integer> c = b.collect(e -> e / 2);
		c.inspect("c: ");
	}

	default void test2(Box<Integer> a) {
		// TODO
	}

	Box<E> select(Predicate<? super E> selector);

	// selector return value should be a "one"
	@Deprecated
	Box<E> selectMutable(Function<? super E, Box<Boolean>> selector);

	// TODO: make non-default once all relevant implementing classes are updated
	// TODO: variance?
	default Box<E> selectMutableSym(Function<SymbolicObject<E>, Box<Boolean>> selector) {
		throw new UnsupportedOperationException();
	}

	<R> Box<R> collect(Function<? super E, ? extends R> collector);

	@Deprecated
	<R> Box<R> collectMutable(Function<? super E, ? extends Box<R>>	 collector);

	// TODO: make non-default once all relevant implementing classes are updated
	// TODO: variance?
	default <R> Box<R> collectMutableSym(Function<SymbolicObject<E>, Box<R>> collector) {
        throw new UnsupportedOperationException();
	}

	<F,R> Box<R> zipWith(Box<F> other, BiFunction<? super E, ? super F, ? extends R> zipper);

	Box<E> concat(Box<E> other);

	Box<E> first();

	Box<E> last();

	Box<E> get(int index);

	// could also work with collections of indices
//	Box<E> get(Box<Integer> index);

	Box<Integer> size();

	Box<Boolean> isEmpty();

	Box<Boolean> notEmpty();

	// OCL type conversion methods, also work from singletons
	Box<E> asBag();
	Box<E> asOrderedSet();
	Box<E> asSequence();
	Box<E> asSet();
	// TODO: asOne, asOption? but what does Set.asOne mean? first? last?

	// More generic collection type conversion methods (leave singletons untouched)
	// One			-> One
	// Option		-> Option

	// Bag			-> Set
	// OrderedSet	-> OrderedSet
	// Sequence		-> OrderedSet
	// Set			-> Set
	Box<E> unique();	// TODO: call this distinct

	// Bag			-> Bag
	// OrderedSet	-> Sequence
	// Sequence		-> Sequence
	// Set			-> Bag
	Box<E> nonUnique();

	// Bag			-> Bag
	// OrderedSet	-> Set
	// Sequence		-> Bag
	// Set			-> Set
	Box<E> unordered();

	// Is this useful?
	// Warning: non-deterministic operation (wrt. ordering)
	// Bag			-> Sequence
	// OrderedSet	-> OrderedSet
	// Sequence		-> Sequence
	// Set			-> OrderedSet
	Box<E> ordered();

	// TODO: should we really keep this in the API?
	ModifiableBox<E> stored();
	boolean isStored();

	Box<E> inspect(String message);

	default void set(Object object, Object feature) {
	    throw new UnsupportedOperationException();
	}
}
