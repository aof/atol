package fr.eseo.aof2.exploration.graph.interpreter

import AOFGraph.Box
import AOFGraph.CollectMutable
import AOFGraph.ComputedBox
import AOFGraph.ObjectParameter
import AOFGraph.Operation
import AOFGraph.ParameterBox
import AOFGraph.PropertyBox
import AOFGraph.SelectMutable
import AOFGraph.SetProperty
import AOFGraph.SubGraph
import java.io.PrintWriter
import java.util.HashSet
import java.util.Set
import org.eclipse.emf.ecore.EObject

class Graph2PlantUML {
	// TODO: improve toPlantUML
	def dispatch static String toPlantUML_(SubGraph sg, PrintWriter out, Set<EObject> serialized) {
		if(!serialized.contains(sg)) {
		    serialized.add(sg)
		    out.println('''state "Lambda" as sg_«sg.id» {''')
		    if(sg.outputs.empty) {
		        sg.boxes.forEach[it |
//		            out.println('''
//		            	state box_«it.toPlantUML_(out, serialized)»
//		            ''')
		            it.consumers.forEach[o |
						o.toPlantUML_(out, serialized)
		            ]
		        ]
		//	        sg.operations.forEach[it |
		//	            out.println('''
		//	            	state op_«it.id»
		//	            ''')
		//	        ]
		    } else {
		        sg.outputs.forEach[it, i |
		            out.println('''
		                box_«it.toPlantUML_(out, serialized)» --> out_«it.id»
		                state "<i>outBox«i»</i>" as out_«it.id» <<outputPin>>
		            ''')
		        ]
		    }
		    out.println('''}''')
		}
		sg.id
	}

    def dispatch static String toPlantUML_(Operation o, PrintWriter out, Set<EObject> serialized) {
        if(!serialized.contains(o)) {
            serialized.add(o)
	        o.sources.forEach[
	            out.println('''box_«it.toPlantUML_(out, serialized)» --> op_«o.id» : Source''')
	        ]
	        switch o {
	            CollectMutable: {
	                out.println('''sg_«o.collector.toPlantUML_(out, serialized)» --> op_«o.id» : collector''')
	            }
	            SelectMutable: {
	                out.println('''sg_«o.selector.toPlantUML_(out, serialized)» --> op_«o.id» : selector''')
	            }
	            SetProperty: {
	                out.println('''param_«o.object.toPlantUML_(out, serialized)» --> op_«o.id»''')
	                out.println('''op_«o.id»: «o.propertyName»''')
	            }
	            default: {
	                // nothing to do
	            }
	        }
	        out.println('''state "«o.eClass.name»" as op_«o.id»''')
        }
        o.id
    }

	def dispatch static String toPlantUML_(ComputedBox b, PrintWriter out, Set<EObject> serialized) {
		if(!serialized.contains(b)) {
		    serialized.add(b)
			out.println('''op_«b.producer.toPlantUML_(out, serialized)» --> box_«b.id» : Result''')
			out.println('''«b.inspect»''')
		}
		b.id
	}

	def dispatch static String toPlantUML_(ParameterBox b, PrintWriter out, Set<EObject> serialized) {
		if(!serialized.contains(b)) {
		    serialized.add(b)
			out.println('''state "«b.name?:"inBox"»" as box_«b.id» <<inputPin>>''')
			// How to show inspect?
		}
		b.id
	}

	def dispatch static String toPlantUML_(PropertyBox pb, PrintWriter out, Set<EObject> serialized) {
        if(!serialized.contains(pb)) {
            serialized.add(pb)
			out.println('''
				param_«pb.object.toPlantUML_(out, serialized)» --> box_«pb.id»
				state "get «pb.propertyName»" as box_«pb.id»
				«pb.inspect»
			''')
		}
		pb.id
	}

	def static inspect(Box pb) {
	    '''
			«IF pb.inspectMessage !== null»
				box_«pb.id»: "«pb.inspectMessage»"
			«ENDIF»
	    '''
	}

	def dispatch static String toPlantUML_(ObjectParameter op, PrintWriter out, Set<EObject> serialized) {
		if(!serialized.contains(op)) {
		    serialized.add(op)
			out.println('''state "«op.name?:"<i>inObj</i>"»" as param_«op.id» <<inputPin>>''')
		}
		op.id
	}

    def static String id(Object o) {
        '''«System.identityHashCode(o)»'''
    }

    def static toPlantUML(EObject o, String fileName) {
        val out = new PrintWriter(fileName)
        out.println("@startuml")
        o.toPlantUML_(out, new HashSet)
        out.println("@enduml")
        out.close
    }
}