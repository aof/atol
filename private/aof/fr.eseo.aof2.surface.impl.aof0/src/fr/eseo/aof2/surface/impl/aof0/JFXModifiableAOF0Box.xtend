package fr.eseo.aof2.surface.impl.aof0

import fr.eseo.aof2.exploration.ModifiableBox
import fr.eseo.aof2.exploration.Observer
import fr.eseo.dis.aof0.core.Addition
import fr.eseo.dis.aof0.core.Removal
import fr.eseo.dis.aof0.core.operations.Write
import fr.eseo.dis.aof0.dsl.Transformation
import java.util.Collection
import java.util.Set

class JFXModifiableAOF0Box<E> implements ModifiableBox<E> {
    val Transformation tr
    val Set<E> property
    val AOF0Box<E> box

    new(Transformation tr, Set<E> property) {
        this(tr, property, new AOF0Box<E>(tr, tr.read(property)))
    }

    new(Transformation tr, Set<E> property, AOF0Box<E> box) {
        this.tr = tr
        this.property = property
        this.box = box
    }
    
    override asBox() {
        this.box
    }

    // currently with the semantics of addObserver... cannot remove one
    override setObserver(Observer<E> observer) {
        this.box.box.getOutput().connect(
            new Write(tr.graph, null, null) {
                override apply() {
                    val stream = getInput().getMutationStream();
                    for (mutation : stream) {
                        switch mutation {
                            Addition: {
                                observer.add(mutation.element as E)
                            }
                            Removal: {
                                observer.remove(mutation.element as E)
                            }
                            default: {
                                throw new UnsupportedOperationException
                            }
                        }
                    }
                    // moves the mutations to the output
                    super.apply();
                }
            }
        .getInput());
    }
    
    override add(E e) {
        this.property.add(e)
    }
    
    override addAll(Collection<? extends E> c) {
        this.property.addAll(c)
    }
    
    override addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override clear() {
        this.property.clear
    }
    
    override contains(Object o) {
        this.property.contains(o)
    }
    
    override containsAll(Collection<?> c) {
        this.property.containsAll(c)
    }
    
    override isEmpty() {
        this.property.isEmpty
    }
    
    override iterator() {
        this.property.iterator
    }
    
    override remove(Object o) {
        this.property.remove(o)
    }
    
    override removeAll(Collection<?> c) {
        property.removeAll(c)
    }
    
    override retainAll(Collection<?> c) {
        property.retainAll(c)
    }
    
    override size() {
        property.size
    }
    
    override toArray() {
        this.property.toArray
    }
    
    override <T> toArray(T[] a) {
        this.property.toArray(a)
    }

    override toString() {
        property.toString
    }

    // unsupported methods for ordered boxes
    override move(int newPosition, E element) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override move(int newPosition, int oldPosition) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override add(int index, E element) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override get(int index) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override indexOf(Object o) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override lastIndexOf(Object o) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override listIterator() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override listIterator(int index) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override remove(int index) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override set(int index, E element) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
}