package fr.eseo.aof2.surface.impl.aof0

import fr.eseo.aof2.exploration.Box
import fr.eseo.aof2.exploration.SymbolicObject
import fr.eseo.dis.aof0.dsl.Transformation
import fr.eseo.dis.aof0.javafx.JavaFXFactory
import java.util.function.BiFunction
import java.util.function.Function
import java.util.function.Predicate

// TODO: directly create the AOF0 graph instead of using the AOF0 surface API (maybe even replace the AOF0 surface API by the AOF2 surface API)
class AOF0Box<E> implements Box<E> {

    val Transformation tr
    val fr.eseo.dis.aof0.dsl.Box<E> box
    var JFXModifiableAOF0Box<E> stored

    new(Transformation tr, JFXModifiableAOF0Box<E> stored, fr.eseo.dis.aof0.dsl.Box<E> box) {
        this.tr = tr
        this.stored = stored
        this.box = box
    }

    new(Transformation tr, fr.eseo.dis.aof0.dsl.Box<E> box) {
        this(tr, null, box)
    }

    package def getBox() {
        this.box
    }

    override asBag() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override asOrderedSet() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override asSequence() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override asSet() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override <R> collect(Function<? super E, ? extends R> collector) {
        new AOF0Box(tr, box.collectWith(collector))
    }
    
    override <R> collectMutable(Function<? super E, ? extends Box<R>> collector) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }

    private def <P, R> Function<P, fr.eseo.dis.aof0.dsl.Box<R>> lambdaConverter(Function<SymbolicObject<P>, Box<R>> f) {
        [e |
//            return unwrap(
//                f.apply(new SymbolicObject<P>() {
//                    @Override
//                    public <T> Box<T> get(Object property) {
//                        if(e == null) {
//                            return new AOF1Box<>((IBox<T>)IBox.SEQUENCE);// TODO: adapt type... should probably not be done here
//                        } else {
//                            // TODO: should factory choice be done at the "Context" level?
//                            // taking into account that multiple factories may be used in a given a transformation
//                            return new AOF1Box<>(AOFFactory.INSTANCE.createPropertyBox(e, property));
//                        }
//                    }
//                })
//            );
            unwrap(f.apply(new SymbolicObject<P> {
                override <E> get(Object property) {
                    // TODO
                }
            }))
        ]
    }

    private def <E> fr.eseo.dis.aof0.dsl.Box<E> unwrap(Box<E> box) {
        (box as AOF0Box<E>).box
    }

    override <R> collectMutableSym(Function<SymbolicObject<E>, Box<R>> collector) {
        new AOF0Box(tr, box.collectBy(lambdaConverter(collector)))
        
    }

    override concat(Box<E> other) {
        new AOF0Box(tr, this.box.union(unwrap(other)))
    }
    
    override first() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override get(int index) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override inspect(String message) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override isEmpty() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override isStored() {
        stored !== null
    }
    
    override last() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override nonUnique() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override notEmpty() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override ordered() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override select(Predicate<? super E> selector) {
        new AOF0Box(tr, box.selectWith[selector.test(it)])
    }
    
    override selectMutable(Function<? super E, Box<Boolean>> selector) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override selectMutableSym(Function<SymbolicObject<E>, Box<Boolean>> selector) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
//        new AOF0Box(tr, box.selectBy(lambdaConverter(selector)))
    }
    
    override size() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override stored() {
        if(stored === null) {
            val s = JavaFXFactory.createSet
            this.stored = new JFXModifiableAOF0Box<E>(tr, s, this)
            box.write(s)
        }
        this.stored
    }
    
    override unique() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override unordered() {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
    }
    
    override <F,R> zipWith(Box<F> other, BiFunction<? super E, ? super F, ? extends R> zipper) {
        val b = this.box.zip(unwrap(other))
        new AOF0Box(tr, b.collectWith[f, s | zipper.apply(f, s)])
    }
    
}