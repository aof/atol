package fr.eseo.aof2.surface.impl.aof0

import fr.eseo.aof2.exploration.ModifiableBox
import fr.eseo.aof2.exploration.utils.ReifyingObserver
import fr.eseo.dis.aof0.dsl.Transformation
import fr.eseo.dis.aof0.javafx.JavaFXFactory
import fr.eseo.dis.aof0.javafx.JavaFXPlatform
import fr.eseo.aof2.exploration.utils.ReifyingObserver.Event

class Test {
    def static void main(String[] args) {
        new Test().test
    }

    val tr = new Transformation(JavaFXPlatform.INSTANCE)

    def test() {
        val a = <Integer>createSet()
        a.add(1)
        a.add(2)
        println('''a: «a»''')

        val b = a.asBox.collect[it * 2].stored()
        a.inspect("a: ")
        b.inspect("b: ")

        tr.initialize

        println('''b: «b»''')
        a.add(3)
        println('''b: «b»''')
    }

    def <E> inspect(ModifiableBox<E> box, String msg) {
        box.observer = new ReifyingObserver<E> {
            override process(Event<E> e) {
                println('''«msg»«e»''')
            }
        }
    }

    def <E> createSet() {
        new JFXModifiableAOF0Box<E>(tr, JavaFXFactory.createSet())
    }
}